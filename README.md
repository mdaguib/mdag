The project architecture has been built on top of Docker using Docker-Compose.

Steps to get up in a dev environment:

1. Build all the containers using docker-compose -> `make build`
2. Create and apply all the django basic migrations ->  `make makemigrations && make migrate`
3. Create the first super user for the application -> `make init-project`
4. Compile the frontend code ( You'll need angular-cli for this task )

	1. Go to mdag/project/mDagFront/scripts
	2. Run ./compile.sh
	

This steps should work for any kind of linux-like environment ( due to the shell scripting ).