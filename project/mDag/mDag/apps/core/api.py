# -*- coding: utf-8 -*-
from typing import Callable

from mDag.apps.core.constants import ExperimentType
from mDag.apps.core.exceptions import CoreException, UnknownExperimentType, \
    ForbiddenAccess
from mDag.apps.core.forms import BasicExperimentForm, \
    OrganismMetabolicBuilderForm
from mDag.apps.utils.exceptions import BadRequestException
from .models import Experiment
from .serializers import ExperimentSerializer


class CoreApi:
    EXPERIMENT_VALIDATORS = {
        ExperimentType.OrganismMetabolism: OrganismMetabolicBuilderForm
    }

    @staticmethod
    def get_visible_experiments(
        user: dict,
        organism=None,
        reaction=None,
        organism_group=None,
        experiment_group=None
    ) -> list:
        """
        Obtain all the visible experiments for the given users.
        Commonly there will be the next kind of experiments:
            - Public experiments
            - Private experiments whose access has been granted for this user
            - Own experiments whichever it's visibility
        """
        filters = {}
        if organism:
            filters['organisms__in'] = [organism]

        if reaction:
            filters['reactions__in'] = [reaction]

        if experiment_group:
            filters['experiment_groups__in'] = [experiment_group]

        return ExperimentSerializer(
            Experiment.get_experiments(
                user if not user['is_anonymous'] else None,
                **filters),
            many=True
        ).data

    @staticmethod
    def get_experiment_details(experiment_id: int, user: dict) -> dict:
        """
        Obtain the experiment details.
        If the experiment requested does not matches any visible experiment
        for the user there won't be any details to share
        """
        experiment = Experiment.get_experiment_details(experiment_id, user)
        return ExperimentSerializer(experiment).data

    @classmethod
    def create_experiment(cls, user, experiment_type, **data):
        """
        Experiment creation dispatcher.
        This method is in charge of routing the experiment request to its
        correct method and validating that all the inputs are correct
        depending on the specific experiment
        """
        experiment_type = int(experiment_type)
        form = cls._get_validation_form(experiment_type, data)
        if not form.is_valid():
            print(form.errors)
            raise BadRequestException

        cleaned_data = form.cleaned_data
        try:
            cleaned_data['user'] = user
            service = cls._get_service(experiment_type)
            experiment = service(**cleaned_data)
            response = ExperimentSerializer(experiment).data
        except Exception:
            import traceback
            traceback.print_exc()
            raise CoreException

        return response

    @classmethod
    def delete_experiment(cls, experiment_id, user):
        experiment = Experiment.get_experiment_details(experiment_id, user)
        if not experiment:
            raise ForbiddenAccess

        if not experiment.deleted:
            experiment.logical_delete()
        elif user['is_superuser']:
            # Remove all the non-deletable 
            experiment.delete()

    @classmethod
    def update_experiment(cls, experiment_id, user, name=None,
                          description=None, visible_for=None):

        filters = {
            'name': name,
            'description': description,
            'visible_for': visible_for,
        }

        experiment = Experiment.get_experiment_details(experiment_id, user)
        if not experiment_id:
            raise ForbiddenAccess

        experiment.update(**filters)

        return ExperimentSerializer(experiment).data
        

    @classmethod
    def get_experiment_zipped_files(cls, user, experiment_id) -> dict:
        """
        Obtain the experiment zipped content.
        For those public
        :param user:
        :param experiment_id:
        :return:
        """
        experiment = Experiment.get_experiment_details(experiment_id, user)
        if not experiment:
            # Not allowed access
            raise ForbiddenAccess
                
        return Experiment.get_experiment_zip(experiment_id)

    @classmethod
    def _get_validation_form(cls, exp_type, data) -> BasicExperimentForm:
        """
        Obtain the validator for the requested experiment type
        :param exp_type:
        :return:
        """
        form_class = cls.EXPERIMENT_VALIDATORS.get(exp_type)
        if not form_class:
            print("Experiment type", exp_type, type(exp_type))
            raise UnknownExperimentType

        return form_class(data)

    @classmethod
    def _get_service(cls, type) -> Callable:
        services = {
            ExperimentType.OrganismMetabolism:
                Experiment.create_organism_metabolism_experiment
        }

        service = services.get(type)
        if not service:
            raise UnknownExperimentType

        return service
