from enum import IntEnum, Enum


class ExperimentType(IntEnum):
    SimpleExperiment = 1
    OrganismMetabolism = 2


class VisibilityState(Enum):
    Private = 'pr'
    Public = 'pu'

    @classmethod
    def get_choices(cls):
        return [
            (state.name, state.value)
            for state in VisibilityState
        ]

    @classmethod
    def get_default_value(cls):
        return cls.Private.value
