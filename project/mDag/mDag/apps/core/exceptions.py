
class CoreException(Exception):
    pass


class UnknownExperimentType(CoreException):
    pass


class ForbiddenAccess(CoreException):
    pass
