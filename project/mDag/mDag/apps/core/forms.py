from django import forms


class BasicExperimentForm(forms.Form):
    """
    Common basic field validation for all the experiments
    """
    name = forms.CharField()
    description = forms.CharField()


class OrganismMetabolicBuilderForm(BasicExperimentForm):
    """
    The organism is the only information needed in order to build all the
    pathway for that organism
    """
    organism = forms.CharField()
    # TODO Check if required stills means print = True
    print_experiment = forms.BooleanField(required=False)
