# -*- coding: utf-8 -*-
from __future__ import annotations

import os

import shutil
from django.conf import settings
from django.contrib.auth.models import Group
from django.db import models
from django.db.models import QuerySet, Q

from mDag.apps.core.constants import VisibilityState
from mDag.apps.metadagcommander.api import MetaDagCommanderApi
from mDag.apps.metadagcommander.constants import ResultStatus


class ElementType:
    ORTHOLOG = "ko"
    PATHWAY = "ec"

    CHOICES = (
        (ORTHOLOG, "Ortholog"),
        (PATHWAY, "Pathway"),
    )


# KEGG Mapped hierarchy
class KElement(models.Model):
    # KEGG declared max length
    # https://www.genome.jp/kegg/kegg3a.html
    kid = models.CharField(primary_key=True, max_length=9)

    class Meta:
        abstract = True


class Classification(models.Model):
    # Name is a normalized name. It's done to try to avoid classification
    # duplicities when it comes to whitespaces or capitalization.
    # This won't detect wrong spelled classes (huminid instead of hominid)
    name = models.TextField(primary_key=True)
    raw_name = models.TextField()

    @classmethod
    def add_classes(cls, classes):
        normalized_classes = set()
        normal_class_to_genuine_class = {}

        for klass in classes:
            normalized_class = klass.lower().strip()
            normalized_classes.add(normalized_class)
            normal_class_to_genuine_class[normalized_class] = klass

        # Create only the non-existent classes
        existing_classes = {
            klass
            for klass in cls.objects.filter(
                name__in=normalized_classes
            )
        }
        existing_classes_names = {
            klass.name
            for klass in existing_classes
        }
        missing_classes = normalized_classes - existing_classes_names

        new_classes = {
            Classification(
                name=klass,
                raw_name=normal_class_to_genuine_class.get(klass)
            )
            for klass in missing_classes
        }

        if new_classes:
            cls.objects.bulk_create(new_classes)

        return new_classes.union(existing_classes)


class Organism(KElement):
    classifications = models.ManyToManyField(Classification)

    @classmethod
    def add_organisms(cls, organisms, classes=None):
        existing_organisms = cls.objects.filter(kid__in=organisms)
        existing_organism_names = set(
            existing_organisms.values_list("kid", flat=True)
        )
        organisms_set = set(organisms)

        missing_organisms = organisms_set - existing_organism_names
        if not missing_organisms:
            return

        instances = {Organism(kid=kid) for kid in missing_organisms}
        instances = set(cls.objects.bulk_create(instances))
        instances = instances - set(existing_organisms)

        if classes:
            class_instances = Classification.add_classes(classes)
            for instance in instances:
                instance.classifications.add(*class_instances)


class Reaction(KElement):
    pass


class Enzyme(KElement):
    pass


class Compound(KElement):
    pass


class ExperimentStatus:
    """
    Status diagram proposed for these status:

    Pending -> Calculating -> Done -> Logical Delete
                    |                        |
                    v                        v
                 Cancelled      ->        Deleted

    The other proposal is the next one:

    Pending -> Calculating -> Done -> Logical Delete
                    |                        |
                    |                        v
                    ------------>        Deleted
    And this could be useful if the service does have a cancelling system
    """
    PENDING = "pe"
    REQUESTED = "re"
    CALCULATING = "ca"
    DONE = "do"
    CANCELLED = "cx"
    LOGICAL_DELETE = "de"
    # Deleted does not have status because it's the actual delete

    CHOICES = (
        (PENDING, "Pending"),
        (REQUESTED, "Requested"),
        (CALCULATING, "Calculating"),
        (DONE, "Done"),
        (CANCELLED, "Cancelled"),
        (LOGICAL_DELETE, "Delete"),

    )

    LABEL = {
        PENDING: "Pending",
        REQUESTED: "Requested",
        CALCULATING: "Calculating",
        DONE: "Done",
        CANCELLED: "Cancelled",
        LOGICAL_DELETE: "Deleted",
    }


class ExperimentGroup(models.Model):
    """
    Group class that allows grouping experiments that are somehow related.
    The relation between experiments depend just on the users and they're
    responsible of providing this groups
    """
    name = models.TextField()


class Experiment(models.Model):
    """
    An experiment is the result of running the mDAG services and storing
    everything returned.
    Thus, it could be seen as a wrapper or a binding between all the files
    for a concrete algorithm config.

    Notice that the primary key it's completely independent. It's due to the
    same experiment could be done with different organisms or compound measures.
    """
    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now=True)
    name = models.CharField("Experiment name", max_length=50)
    description = models.TextField("Experiment description")
    uid = models.TextField("MetaDag calculator query identifier")

    experiment_status = models.CharField(
        max_length=2,
        choices=ExperimentStatus.CHOICES,
        default=ExperimentStatus.PENDING
    )

    organisms = models.ManyToManyField(Organism)
    reactions = models.ManyToManyField(Reaction)
    enzymes = models.ManyToManyField(Enzyme)
    compounds = models.ManyToManyField(Compound)

    experiment_groups = models.ManyToManyField(ExperimentGroup)

    # Accept null values to be able to delete users without losing
    # experiments
    owner = models.ForeignKey(
        'user.User',
        models.SET_NULL,
        null=True,
        related_name='own_experiments'
    )

    # General visibility settings. This one applies in all the situations and
    # its setup is overridden by user/group special allowance
    visible_for = models.TextField(
        choices=VisibilityState.get_choices(),
        default=VisibilityState.get_default_value()
    )

    # Visibility options for individuals and groups.
    # This has been added in order to allow sharing the experiments without
    # publishing them to the world.
    allowed_users = models.ManyToManyField(
        'user.User',
        related_name="others_experiments"
    )
    allowed_groups = models.ManyToManyField('user.ResearchGroup')

    def __str__(self):
        return "{}({}) - Status: {} - Visible {}".format(
            self.name,
            self.id,
            self.experiment_status,
            self.visible_for
        )

    @classmethod
    def get_experiments(cls, user: dict = None, **kwargs) -> QuerySet:
        """
        Get the experiments related with the user.
        If no user is provided, this function simply returns all the experiments
        TODO: This function may need some kind of pagination

        If an user is provided, this function must return those experiments
        that met at least one of these conditions:
            - The user is the owner of the experiment
            - It's been granted the access ( by user or by group ) to the
              experiment
            - The experiment is public
        """
        filters = Q()

        print("User", user)
        if user:
            if not user['is_superuser']:
                filters = filters | Q(
                    Q(
                        Q(owner_id=user['id'])
                        | Q(allowed_users=user['id'])
                        | Q(allowed_groups__in=user['groups'])
                        | Q(visible_for=VisibilityState.Public.value)
                    )
                    & ~Q(experiment_status=ExperimentStatus.LOGICAL_DELETE)
                )
        else:
            filters = Q(
                Q(visible_for=VisibilityState.Public.value)
                & ~Q(experiment_status=ExperimentStatus.LOGICAL_DELETE)
            )

        # This launches a service request.
        # TODO This could be better done in the details request (not yet used)
        for experiment in cls.objects.filter(filters, **kwargs):
            experiment.refresh_experiment_status()

        # Force a double query to obtain the refreshed status
        return cls.objects.filter(filters, **kwargs)

    @classmethod
    def get_public_experiments(cls, **filters) -> QuerySet:
        """
        Obtain all the published experiments.
        """
        filters['visible_for'] = VisibilityState.Public.value
        return cls.objects.filter(**filters)

    @classmethod
    def get_experiment_details(cls, _id: int, user: dict = None) -> Experiment:
        experiments = cls.get_experiments(user)
        print("Experiments", experiments, _id)
        return experiments.filter(id=_id).first()

    @classmethod
    def create_organism_metabolism_experiment(cls, user, organism, name,
                                              description, print_experiment,
                                              organism_groups_file=None):

        result = MetaDagCommanderApi().organism_metabolic_builder(
            organism,
            class_file=organism_groups_file
        )
        if result:
            # The result dir is returned, but we'already know it by the
            # qid.
            instance = cls.objects.create(
                name=name,
                description=description,
                uid=result['qid'],
                owner_id=user['id'],
                experiment_status=cls._cast_mdagcommander_status(
                    result['status']
                )
            )

            organism, _ = Organism.objects.update_or_create(
                kid=organism,
                defaults={'kid': organism}
            )
            instance.organisms.add(organism)

            # Add class and organism
            # klass = ClassificationFile(organism_groups_file).get_class()
            return instance

    @classmethod
    def get_experiment_zip(cls, experiment_id):
        experiment = cls.objects.get(id=experiment_id)
        if experiment.uid is None:
            # TODO This shall be an error as far as no experiment can exist
            #  without an uid
            return None

        return FileManager.zipFiles(
            experiment_uid=experiment.uid
        )

    @staticmethod
    def _cast_mdagcommander_status(commander_status):
        print(commander_status)
        statuses = {
            ResultStatus.IN_PROGRESS: ExperimentStatus.CALCULATING,
            ResultStatus.SUCCESSFUL: ExperimentStatus.DONE
        }

        return statuses.get(commander_status, ExperimentStatus.PENDING)

    def refresh_experiment_status(self):
        non_requestable_statuses = [
            ExperimentStatus.DONE,
            ExperimentStatus.LOGICAL_DELETE
        ]

        if self.experiment_status in non_requestable_statuses:
            return

        status = MetaDagCommanderApi().query_status(self.uid)
        self.experiment_status = self._cast_mdagcommander_status(status)
        self.save()

    def logical_delete(self):
        self.experiment_status = ExperimentStatus.LOGICAL_DELETE
        self.save()

    def update(self, name=None, description=None, visible_for=None):
        if name is not None:
            self.name = name

        if description is not None:
            self.description = description

        if visible_for is not None:
            self.visible_for = visible_for

        self.save()

    @property
    def deleted(self):
        return self.experiment_status == ExperimentStatus.LOGICAL_DELETE

    class Meta:
        ordering = ('-id',)


class FileType:
    """
       Resource types returned by the algorithm
       This may be a model in order to allow extending it without manipulating
       the code.
       However, I'll keep it as an enum nowadays
    """
    CODE_SIZE = 3
    SVG = "svg"
    PDF = "pdf"
    CSV = "csv"  # This is a group of CSV and all its derivatives
    OTHERS = "oth"

    # NON-DB files
    ZIP = 'zip'

    CHOICES = (
        (SVG, SVG),
        (PDF, PDF),
        (CSV, CSV),
        (OTHERS, OTHERS),
    )


class Resource(models.Model):
    """
    Represents the available resources for the mDag calculations
    """
    file = models.FileField()
    file_type = models.CharField(
        max_length=FileType.CODE_SIZE,
        choices=FileType.CHOICES
    )

    # Delete all the resources bound to a Calculation when this is deleted
    calculation = models.ForeignKey(Experiment, models.CASCADE)


class FileManager:

    @staticmethod
    def zipFiles(experiment_uid: str):
        experiment_path = os.path.realpath(
            os.path.join(settings.EXPERIMENT_ROOT, experiment_uid)
        )
        zipped_file_path = os.path.realpath(
            os.path.join(
                settings.EXPERIMENT_ROOT_ZIPPED_FILES,
                experiment_uid + '.zip'
            )
        )

        if not os.path.exists(experiment_path):
            raise Exception("Experiment folder does not exist")

        if not os.path.exists(zipped_file_path):
            shutil.make_archive(
                os.path.splitext(zipped_file_path)[0],
                'zip',
                root_dir=experiment_path
            )

        try:
            file = open(zipped_file_path, 'rb')
        except IOError:
            import traceback
            traceback.print_exc()
            file = None

        return file
