# -*- coding: utf-8 -*-
from rest_framework import serializers

from mDag.apps.core.constants import VisibilityState
from mDag.apps.core.models import ExperimentStatus
from . import models


class ExperimentSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Experiment
        fields = [
            'id',
            'name',
            'description',
            'status',
            'creation_date',
            'modification_date',
            'visible_for',
            'owner'
        ]

    status = serializers.SerializerMethodField()

    @staticmethod
    def get_status(obj):
        return ExperimentStatus.LABEL[obj.experiment_status]


class ExperimentDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Experiment
        fields = [
            'id',
            'name',
            'description',
            'experiment_groups',
            'status'
        ]

    status = serializers.SerializerMethodField()

    @staticmethod
    def get_status(obj):
        return ExperimentStatus.LABEL[obj.experiment_status]
