# -*- coding: utf-8 -*-
"""
TreeBuilder interface.
This offers a pythonic way of launching commmands to the TreeBuilder Commander.
"""
from typing import Optional

import re

from itertools import chain

import subprocess
import requests
from django.conf import settings

from mDag.apps.metadagcommander.constants import ResultStatus
from mDag.apps.metadagcommander.exceptions import MissingQIDException, \
    MetaDagCommanderException, UnsuccessfulRequest


class MetaDagCommanderApi:
    # TODO This could be somewhat dependant of the experiment.
    #  Depends on how the output for the different resources handled.
    #  It could need copying from side to side to keep information together
    # Config dict holding "Command line flag" - "Value" pairs
    # This might be placed in a config file or in a DB value.
    # However, as far as it's unlikely to change, we could simply store the
    # options in here avoiding those access.
    # Based on the service catalog provided - it's been shared along the code
    # in the doc folder.
    COMMON_SERVICE_SETUP = {
        '--outDir': settings.EXPERIMENT_ROOT,
        '--rnDir': settings.EXPERIMENT_ROOT,
        '--refDir': settings.EXPERIMENT_ROOT,
        '--orgDir': settings.EXPERIMENT_ROOT,
        '--koDir': settings.EXPERIMENT_ROOT,
    }
    COMMANDER_URL = settings.COMMANDER_URL

    # Response formatter fields
    QID_REGEX = re.compile(r'<h4>([-a-z0-9]*)</h4>', re.IGNORECASE)
    FINAL_DIR_REGEX = re.compile(
        r'<b>Final directory:</b> ([-/a-z0-9]*)</p>',
        re.IGNORECASE
    )

    DATETIME_REGEX = r'\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\]'
    STATUS_REGEX = re.compile(
        r'{} :<b>{} ([\w ]+)'.format(
            "Current status for the service is",
            DATETIME_REGEX
        ),
        re.IGNORECASE
    )

    TEXT_TO_STATUS = {
        'process successfully finished': ResultStatus.SUCCESSFUL,
        'starting': ResultStatus.IN_PROGRESS,

    }

    @classmethod
    def _request_service(cls, service, data):
        """
        Requests a service run to the commander.
        Right now, I'm supposing its a local command in the same machine. Thus,
        I need to launch a shell command with all the
        :param data:
        """
        command = [
            'java',
            'org.biocom.metabolomics.metadag.metadagcommander.Commander',
            '--service',
            service,
        ]

        # Add common setups
        options = chain(cls.COMMON_SERVICE_SETUP.items(), data.command_options)
        for option_key, option_value in options:
            command.append(option_key)
            # Tokens does not hold any value
            if option_value is not None:
                command.append(option_value)

        result = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
        )

        # Decouple the callers from the subprocess implementation
        return result.returncode, result.stdout, result.stderr

    @classmethod
    def _transform_response(cls, response: requests.Response) -> dict:
        """
        Parse a successful response coming from WebCommander
        :param response:
        :return:
        """
        # Capture the Query ID
        try:
            qid = cls.QID_REGEX.search(response.text).group(1)
        except AttributeError:
            # No match. This should not happen as this is a successful response
            raise MissingQIDException()

        try:
            content_dir = cls.FINAL_DIR_REGEX.search(response.text).group(1)
        except AttributeError:
            # This could happen if metadag service add new chars (
            # underscores, dots, ..) to the result path without prior notice.

            # Do not re-raise this error! This could be a recoverable error
            # and we could notice/handle it manually without losing the
            # experiment
            print("Missing final directory for request {}".format(qid))
            print("Original response: {}".format(response.text))
            content_dir = None

        return {
            'qid': qid,
            'content_dir': content_dir
        }

    @classmethod
    def _parse_status(cls, status_text: Optional[str]):
        """
        Turn a text string into a "constant" that can be used system wide.
        :param status_text:
        :return:
        """
        _status = status_text.strip().lower()
        return cls.TEXT_TO_STATUS.get(_status, ResultStatus.ERROR)

    def query_status(self, qid):

        files = {
            '--service': (None, 'Commander'),
            '--queryStatus': (None, qid),
        }
        
        if __debug__:
            print("-" * 120)
            print("Request")
            for key, value in files.items():
                print("\t", key, "-", value)


        response = requests.post(
            self.COMMANDER_URL,
            files=files
        )

        if __debug__:
            print("Response", response.status_code)
            print("\tContent", response.text)
            print("-" * 120)

        if not response:
            # Force error
            return self._parse_status(None)

        search = self.STATUS_REGEX.search(response.text)
        return self._parse_status(search.group(1))

    def tree_builder(self, organism, pathway):
        """
        This service is supposed to hold two functions:
            - Creating data for a combination of organism and pathway
            - Creating pathway data from a given XML containing mDag data.
              todo check how's this useful
        :return:
        """
        # Dummy response
        return {
            "name": "holamundo",
            "id": 190,
        }

    def metabolic_builder(self, organism):
        """
        Builds all the mDAG for an organism
        :return:
        """
        return {}

    def metabolic_comparator(self, organisms):
        """
        Builds all the comparison data for all the pathways for the given
        organisms.

        TODO Check how to implement the genes additional data
        :return:
        """
        return {}

    def organism_metabolic_builder(self, organism, print_experiment=False,
                                   class_file=None):
        """
        Builds all the metabolic data for a given organism
        :return:
        """
        service = 'org.biocom.metabolomics.metadag.treebuilder.MetabolicBuilder'

        # This is mainly because the web commander service expects a multi-part
        # form-data
        # Note that removing the first None field states that you're actually
        # sending a file and the server won't know what to expect leading to
        # a 400 failure
        # print_experiment_flag = 'on' if print_experiment else 'off'
        files = {
            '--service': (None, service),
            '--organism': (None, organism),

            # TODO There are some missing options
            # '--print': (None, print_experiment_flag),
        }
        
        if __debug__:
            print("-" * 120)
            print("Request")
            for key, value in files.items():
                print("\t", key, "-", value)

        # TODO Write file to wherever it needs to be to be
        response = requests.post(
            self.COMMANDER_URL,
            files=files
        )

        if __debug__:
            print("Response", response.status_code)
            print("\tContent", response.text)
            print("-" * 120)

        # We're not expecting redirects right now. Thus, we only need to take
        # care of unsuccessful responses (4XX, 5XX).
        if response.status_code > 299:
            # Return the raw response
            raise UnsuccessfulRequest(response.text)

        try:
            response_dict = self._transform_response(response)
            response_dict['status'] = self.query_status(
                response_dict.get('qid')
            )
        except MissingQIDException:
            response_dict = None
        
        return response_dict

    def organism_comparator(self, pathway):
        """
        Builds all the data relative to the given pathway for all the organisms
        This combines the information of the EC and KO
        :param pathway:
        :return:
        """
        return {}

    def similar_metabolic_builder(self, organism, enzymes, compounds,
                                  reactions):
        """
        Build all the metabolic data for the given organism taking into account
        either:
            * the file of the enzymes
            * the file with pair of related compounds
            * the file of reactions

        Note that these three options are mutually exclusive
        :param organism:
        :param enzymes:
        :param compounds:
        :param reactions:
        :return:
        """
        pass

    def similar_metabolic_comparator(self, organisms, enzymes, compounds,
                                     reactions):
        """
        Builds all the comparison data for all the pathways for the given
        organisms and creates a synthetic organism
        :return:
        """
        pass
