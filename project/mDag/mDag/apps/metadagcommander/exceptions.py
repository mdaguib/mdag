
class MetaDagCommanderException(Exception):
    pass


class MissingQIDException(Exception):
    pass


class MissingFinalDirectoryException(Exception):
    pass


class UnsuccessfulRequest(Exception):
    pass
