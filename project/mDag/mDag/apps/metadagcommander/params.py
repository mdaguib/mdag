# -*- coding: utf-8 -*-
"""
TreeBuilder interface.
This offers a pythonic way of launching commmands to the TreeBuilder Commander.
"""
import abc
from typing import List, Tuple

from django import forms


class ExperimentsComparatorParams(forms.Form):

    dump_dags = forms.BooleanField(required=False)
    measure_similarities = forms.BooleanField(required=False)
    no_filter_pathways = forms.BooleanField(required=False)

    FIELD_TO_TOKEN_MAP = {
        'dump_dags': '--dumpDAGS',
        'measure_similarities': '--measureSimilarities',
        'no_filter_pathways': '--nofilterPathways',
    }

    def command_options(self) -> List[Tuple[str, object]]:
        return [
            (
                self.FIELD_TO_TOKEN_MAP[key],
                None if isinstance(value,bool) else value
            )
            for key, value in self.cleaned_data.items()
        ]
