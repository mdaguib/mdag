from itertools import chain


class UserPermission(object):
    """
    User permission constants.

    These are constants aimed to ease working with Permissions.
    Add a new entry in the Permission model whenever
    you add a new user permission
    """

    EXPERIMENTS = 'experiments'
    EXPERIMENT_CREATION = 'experiments_create'
    USER_MANAGEMENT = 'user-management'
    GROUPS = 'groups'

    NORMAL_USER_PERMISSIONS = [
        EXPERIMENTS,
        EXPERIMENT_CREATION,
        GROUPS
    ]

    ADMIN_USER_PERMISSIONS = [
        EXPERIMENTS,
        EXPERIMENT_CREATION,
        USER_MANAGEMENT,
        GROUPS
    ]

    @classmethod
    def get_all_user_permissions(cls):
        return set(
            chain(cls.NORMAL_USER_PERMISSIONS, cls.ADMIN_USER_PERMISSIONS)
        )
