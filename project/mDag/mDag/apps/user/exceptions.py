
class UserModuleException(Exception):
    pass


class ResearchGroupDoesNotExist(UserModuleException):
    pass


class UserDoesNotExist(UserModuleException):
    pass
