"""
Create the initial admin user manually.
This command is the result of a limitation in the migration system where the
content types for the models are not created yet and it won't be fixed.

Thus, after running the migrations, this command should be run.
"""
import uuid
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand

from mDag.apps.user.constants import UserPermission
from mDag.apps.user.models import User


class Command(BaseCommand):
    help = 'Creates the initial admin user for the web app'

    def add_arguments(self, parser):
        """
        Register the arguments for the django command
        """
        parser.add_argument(
            'experiments_amount',
            nargs='?',
            type=int,
            default=10
        )

    def handle(self, *args, **options):
        """
        Generate the number of experiments specified in the command line.
        These kind of test experiments will be as simple as possible.

        If you need to build a more complex at any time you should be able to
        improve this script to add flags to allow another kind of different
        experiments to be created
        """
        self._create_basic_nav_groups()
        self._create_admin_user()

    @staticmethod
    def _create_admin_user():
        admin_username = 'mdagadmin'
        admin_email = 'unique@mail.com'
        admin_password = 'HA=ocYEH5'
        admin_user, created = User.objects.get_or_create(
            email=admin_email,
            defaults={
                'username': admin_username,
                'email': admin_email,
                'password': make_password(admin_password),
                'is_superuser': True
            }
        )

        if created:
            group = Group.objects.get(name="admingroup")
            admin_user.groups.add(group)

        print("Admin groups", admin_user.groups.all())
        print("Admin permissions", admin_user.groups.first().permissions.all())

    @staticmethod
    def _create_basic_nav_groups():
        group_content_type = ContentType.objects.get(
            app_label="auth",
            model="group"
        )

        needs_to_create_permissions = not Permission.objects.filter(
            content_type=group_content_type,
            codename__in=UserPermission.get_all_user_permissions()
        ).exists()

        if needs_to_create_permissions:
            print("Creating permissions")
            existent_permissions = Permission.objects.filter(
                content_type=group_content_type
            ).count()
            for permission in UserPermission.get_all_user_permissions():
                perm = Permission.objects.create(
                    content_type=group_content_type,
                    codename=permission,
                    name=permission
                )
                print("Created", perm)

            print(
                "Created", Permission.objects.filter(
                    content_type=group_content_type
                ).count() - existent_permissions, "permissions"
            )

        admin_group, created = Group.objects.get_or_create(
            name="admingroup",
            defaults={
                "name": "admingroup",
            }
        )

        if created:
            print("Admin group created")
            perm_instances = list(Permission.objects.filter(
                content_type=group_content_type,
                codename__in=UserPermission.ADMIN_USER_PERMISSIONS
            ))
            admin_group.permissions.add(*perm_instances)

        normal_group, created = Group.objects.get_or_create(
            name="normalgroup",
            defaults={
                "name": "normalgroup",
            }
        )
        if created:
            print("Normal group created")
            perm_instances = list(Permission.objects.filter(
                content_type=group_content_type,
                codename__in=UserPermission.NORMAL_USER_PERMISSIONS
            ))
            normal_group.permissions.add(*perm_instances)
