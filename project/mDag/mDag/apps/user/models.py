from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.contrib.auth.models import Group, AbstractUser, Permission
from django.db.models import Q

from mDag.apps.user.exceptions import ResearchGroupDoesNotExist, \
    UserDoesNotExist


class User(AbstractUser):
    # Make these fields non-nullable non-blankable
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=150)
    email = models.EmailField(_('email address'))

    @classmethod
    def create_user(cls, username, email, password, name, surname):
        # The username must be unique
        if cls.objects.filter(username=username).exists():
            raise Exception
        
        # Create the user if it does not exist!
        user = cls.objects.create(
            username=username,
            email=email,
            first_name=name,
            last_name=surname
        )
        user.set_password(password)
        user.save()

        user.setup_basic_permissions()
        return user

    @classmethod
    def promote(cls, _id):
        try:
            user = cls.objects.get(id=_id)
            user.is_superuser = True
            user.save()
        except cls.DoesNotExist:
            raise UserDoesNotExist

        return user

    @classmethod
    def demote(cls, _id):
        try:
            user = cls.objects.get(id=_id)
            user.is_superuser = False
            user.save()
        except cls.DoesNotExist:
            raise UserDoesNotExist

        return user

    def setup_basic_permissions(self):
        group = Group.objects.get(name="normalgroup")
        self.groups.add(group)


class ResearchGroup(models.Model):
    """
    Each research has many members with an specific role (permissions)
    Look at UserResearchGroupRelation in order to know which user do we have in
    an specific research group.
    """
    name = models.CharField(max_length=100)
    description = models.TextField(null=True)

    @classmethod
    def create_group(cls, owner, name, description):
        group = ResearchGroup.objects.create(
            name=name,
            description=description
        )

        group.userresearchgrouprelation_set.create(
            user=owner,
            research_group=group,
            owner=True
        )

        return group

    @classmethod
    def get_group(cls, gid):
        try:
            return cls.objects.get(id=gid)
        except cls.DoesNotExist:
            raise ResearchGroupDoesNotExist

    @classmethod
    def get_groups(cls, group_name=None, owner=None):
        print("Group", group_name, "owner", owner)
        filter = Q()

        if group_name:
            filter = Q(name__icontains=group_name)

        if owner is not None:
            filter &= Q(userresearchgrouprelation__owner=bool(owner))

        return cls.objects.filter(filter)

    def update_info(self, name=None, description=None):
        if name:
            self.name = name

        self.description = description or ''
        self.save()

    def update_members(self, members):
        """

        :param members:
        :type members: set
        :return:
        """
        members_to_delete = self.userresearchgrouprelation_set.exclude(
            owner=True
        ).exclude(
            id__in=members
        )

        current_member_ids = self.userresearchgrouprelation_set.values_list(
            "user",
            flat=True
        )
        current_members = set(User.objects.filter(id__in=current_member_ids))
        members_to_add = (set(members) - current_members)

        print("Incoming members", list(members))
        print("Current members", current_members)
        print("Members to add", list(members_to_add))

        # Remove the members that are no longer part of this group.
        self.userresearchgrouprelation_set.filter(
            id__in=members_to_delete
        ).delete()

        new_user_relations = [
            UserResearchGroupRelation(
                user=member,
                research_group=self,
                owner=False
            )
            for member in members_to_add
        ]
        self.userresearchgrouprelation_set.bulk_create(new_user_relations)

    def get_owner(self):
        return self.userresearchgrouprelation_set.get(
            research_group=self,
            owner=True
        ).user

    def get_members(self):
        users = {
            user_relation.user
            for user_relation in self.userresearchgrouprelation_set.all()
        }
        return users


class UserResearchGroupRelation(models.Model):
    """
    A user can be part of multiple research groups. However, it just can hold a
    role for every specific research group.

    That role (aka group in here) hold all the abstract permissions ( user
    addition, user removal, etc., related with the research group)
    """
    owner = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    research_group = models.ForeignKey(ResearchGroup, on_delete=models.CASCADE)

    class Meta:
        unique_together = (('user', 'research_group'),)
