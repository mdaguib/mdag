from rest_framework import serializers

from mDag.apps.user.models import ResearchGroup, User


class ResearchGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResearchGroup
        fields = (
            "id",
            "name",
            "description",
            "owner",
            "members"
        )

    owner = serializers.SerializerMethodField()
    members = serializers.SerializerMethodField()

    @staticmethod
    def get_owner(obj):
        return LightUserSerializer(obj.get_owner()).data

    @staticmethod
    def get_members(obj):
        return LightUserSerializer(obj.get_members(), many=True).data


class LightUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
        )
        
        first_name = serializers.SerializerMethodField()
        last_name = serializers.SerializerMethodField()
        email = serializers.SerializerMethodField()

        @staticmethod
        def get_first_name(obj):
            return getattr(obj, 'first_name', '')

        @staticmethod
        def get_last_name(obj):
            return getattr(obj, 'last_name', '')

        @staticmethod
        def get_email(obj):
            return getattr(obj, 'email', '')
