from typing import Callable

from django.contrib.auth.models import User


def admin_restricted(func: Callable) -> Callable:
    """
    Restrict a function access to admin/superuser
    :param func:
    :return:
    """
    def wrapper(*args, **kwargs):
        print(args, kwargs)
        user: User = args[0]

        if not user or not user.is_superuser:
            raise PermissionError("Seems that you have not enough permission")
        func(*args, **kwargs)
    return wrapper
