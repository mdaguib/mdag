
class MiscExceptions(Exception):
    pass


class HttpException(MiscExceptions):
    CODE = 0


class BadRequestException(MiscExceptions):
    CODE = 400





