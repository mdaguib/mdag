"""
This modules acts as the public API for the DAG website
Every single element is implemented in its own module and it should only be
accessed through the declared API in order to avoid unwanted dependencies.
"""