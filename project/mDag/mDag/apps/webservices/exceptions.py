
class CustomException(Exception):
    code = 400  # Dummy definition
    message = ''

    def __str__(self):
        return self.message or super().__str__()


# Client defined exception
class ClientException(CustomException):
    code = 400


class BadRequest(CustomException):
    code = 400


class UserDoesNotExist(ClientException):
    code = 401
    message = "Wrong username or password"


class ForbiddenAction(ClientException):
    code = 401
    message = "The current user cannot perform the action"


class UserAlreadyExists(ClientException):
    code = 403
    message = "The user already exists"


# Server defined exceptions
class ServerException(CustomException):
    code = 500


class FileNotFound(ServerException):
    code = 512
