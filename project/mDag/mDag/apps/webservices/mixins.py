import inspect


class SignatureCheckerMixin(object):
    METHOD_PARAMS = None

    @classmethod
    def _get_method_params(cls):
        members = dict(cls.__dict__)

        # Cache methods avoiding the magic ones
        funcs = dict()
        for key, value in members.items():
            if not key.startswith("__") and callable(value):
                funcs[key] = list(inspect.signature(value).parameters.keys())

        return funcs

    @classmethod
    def needs_request(cls, func_name):
        """
        Check if th e passed function needs to receive the request.
        For now it only knows about "request" parameter name
        :param func_name:
        :type func_name: str
        :return:
        """
        if cls.METHOD_PARAMS is None:
            cls.METHOD_PARAMS = cls._get_method_params()

        return "request" in cls.METHOD_PARAMS.get(func_name, [])
