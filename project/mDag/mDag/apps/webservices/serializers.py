# -*- coding: utf-8 -*-
from itertools import chain

from rest_framework import serializers
from django.contrib.auth.models import AnonymousUser

from ..user.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "email",
            "first_name",
            "last_name",
            "permissions",
            "is_superuser",
            "is_anonymous",
            "groups"
        )

    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    
    permissions = serializers.SerializerMethodField("get_permissions_list")
    is_anonymous = serializers.SerializerMethodField('serialize_is_anonymous')

    @staticmethod
    def get_permissions_list(obj) -> list:
        """
        There are two sources of permissions:
            - The user permissions
            - The user's group permissions

        In this case the permissions list needs to contain all the permissions

        TODO In this case we're adding some logic to the user model.
          Thus, check if this should go inside the model itself or in other
          place
        :param obj:
        :return:
        """

        permissions = chain(
            obj.user_permissions.values_list("codename", flat=True),
            obj.groups.values_list("permissions__codename", flat=True)
        )

        return list(set(permissions))

    @staticmethod
    def get_groups(obj):
        return [
            obj.groups.values_list("id")
        ]

    @staticmethod
    def serialize_is_anonymous(obj):
        return isinstance(obj, AnonymousUser)

    @staticmethod
    def get_first_name(obj):
        return getattr(obj, 'first_name', '')

    @staticmethod
    def get_last_name(obj):
        return getattr(obj, 'last_name', '')

    @staticmethod
    def get_email(obj):
        return getattr(obj, 'email', '')
