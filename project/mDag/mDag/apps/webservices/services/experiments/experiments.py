# -*- coding: utf-8 -*-
from django.http import FileResponse

from mDag.apps.core.api import CoreApi
from mDag.apps.webservices.exceptions import ForbiddenAction, BadRequest, \
    FileNotFound
from mDag.apps.webservices.services.experiments.forms import \
    ExperimentFilterForm
from ..user.user import UserService
from ....webservices.mixins import SignatureCheckerMixin


class ExperimentsService(SignatureCheckerMixin):

    def get_visible_experiments(self, request) -> list:
        """
        Obtain all the visible experiments for the given users.
        Commonly there will be the next kind of experiments:
            - Public experiments
            - Private experiments whose access has been granted for this user
            - Own experiments whichever it's visibility
        """
        user = UserService().get_user_info(request)
        form = ExperimentFilterForm(request.C_POST)
        if not form.is_valid():
            raise BadRequest()
        print(user)
        return CoreApi.get_visible_experiments(user, **form.cleaned_data)

    def get_details(self, request) -> dict:
        """
        Obtain the details for the requested experiment.
        There are some hints to be aware of:
            1. The experiment needs to be visible for the user (a superuser
                views all the experiments no matter its state)
        """
        user = UserService().get_user_info(request)
        try:
            # Check if this is actually a dict
            experiment_id = int(request.POST.get('params').get('id'))
        except ValueError:
            experiment_id = None
        return CoreApi.get_experiment_details(experiment_id, user)

    def create_experiment(self, request):
        """
        Create an experiment. This acts like an entry point for all the kinds
        of experiment creation available.

        This request contains the type of the experiment. Thus, it can easily
        check if the params are valid and if the experiment kind is also
        correct.
        """
        if not request.user.is_authenticated:
            raise ForbiddenAction()

        user = UserService().get_user_info(request)
        new_experiment_data = request.C_POST.dict()

        # TODO Improve validation on expected file-like elements
        if 'groups' in request.FILES:
            new_experiment_data['groups'] = request.FILES.getlist('groups', [])
            for file in new_experiment_data['groups']:
                with file.open() as f:
                    print(f.readlines())

        if 'experiment_classification' in request.FILES:
            new_experiment_data['experiment_classification'] = \
                request.FILES.getlist('experiment_classification', [])
            
        print('Experiment data', new_experiment_data)
        return CoreApi.create_experiment(
            user=user,
            **new_experiment_data
        )

    def download_experiment_content(self, request, experiment_id):
        # Right we allow everyone to download experiments if the owner has
        # given access to them
        user = UserService().get_user_info(request)
        zipped_file = CoreApi.get_experiment_zipped_files(
            user,
            experiment_id
        )

        if not zipped_file:
            raise FileNotFound

        return FileResponse(
            zipped_file,
            as_attachment=True,
            filename='experiment.zip'
        )

    def delete(self, request, experiment_id):
        user = UserService().get_user_info(request)
        if user['is_anonymous']:
            raise ForbiddenAction

        CoreApi.delete_experiment(experiment_id, user)

    def update(self, request, experiment_id, **fields):
        user = UserService().get_user_info(request)
        if user['is_anonymous']:
            raise ForbiddenAction

        CoreApi.update_experiment(
            experiment_id,
            user,
            name=fields.get('name'),
            description=fields.get('description'),
            visible_for=fields.get('visible_for')
        )
