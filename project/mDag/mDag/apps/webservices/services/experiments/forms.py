from django import forms


class ExperimentFilterForm(forms.Form):
    organism = forms.CharField(required=False)
    reaction = forms.CharField(required=False)
    experiment_group = forms.CharField(required=False)
    organism_group = forms.CharField(required=False)
