from django.contrib.auth.decorators import login_required


class MDAGService(object):
    @login_required
    def create(self):
        pass

    @login_required
    def remove(self, ids):
        """
        This is intended to be a logical delete.
        That means it will just disable/hide all the
        mdag information.

        In order to allow the removal the user
        must be the mdag owner or it must hold a supervisor
        role
        :param ids:
        :return:
        """
        pass

    def get(self, ids):
        """
        Returns all the information about a given list
        of mdag ids.

        :param ids:
        :return:
        """
        pass

    def list(self):
        """
        This is intended to retrieve a list of mdag ids
        It might be useful returning all the information.
        :return:
        """
        pass
