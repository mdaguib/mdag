from mDag.apps.webservices.mixins import SignatureCheckerMixin


class PingService(SignatureCheckerMixin):

    def ping(self):
        return "Ok"
