from django import forms


class UserServiceParamForm(forms.Form):
    pass


class GetGroupsForm(UserServiceParamForm):
    owner = forms.NullBooleanField(required=False)
    group_name = forms.CharField(required=False, max_length=100)
