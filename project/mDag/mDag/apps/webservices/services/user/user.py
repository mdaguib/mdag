from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.http import JsonResponse

from mDag.apps.user import exceptions
from mDag.apps.user.models import ResearchGroup, User
from mDag.apps.user.serializers import ResearchGroupSerializer
from mDag.apps.webservices.exceptions import UserDoesNotExist, \
    UserAlreadyExists, ForbiddenAction, BadRequest
from mDag.apps.webservices.mixins import SignatureCheckerMixin
from mDag.apps.webservices.serializers import UserSerializer

# TODO Check if login required can be used this way. There are two kind of
#  services. The one for users and the one for non-users!!
from mDag.apps.webservices.services.user.forms import GetGroupsForm


class UserService(SignatureCheckerMixin):

    def get_user_info(self, request) -> dict:
        """
        Retrieves the info of the requested user.
        self should contain the user as far as this function
        requires it to be logged in
        """
        return UserSerializer(request.user).data

    @login_required
    def get_normal_users(self):
        """
        Return all the NORMAL users.
        This excludes super and admin users.
        :return:
        :rtype: list<dict>
        """
        users = User.objects.filter(is_superuser=False)
        return UserSerializer(users, many=True).data

    def sign_up(self, username, email, password, name, surname):
        """
        Create a new user.
        Requirements:
            - Unique username
            - Email not blank
            - Password not blank

        :param username:
        :param email:
        :param password:
        :param name:
        :param surname:
        :return:
        """
        try:
            user = User.create_user(
                username,
                email,
                password,
                name,
                surname
            )
            response = JsonResponse(
                {'code': 200, 'response': UserSerializer(user).data},
                status=200
            )
            response['_jwt_user_id'] = user.id
            return response

        except Exception:
            raise UserAlreadyExists()

    def sign_in(self, username, password):
        user = authenticate(username=username, password=password)
        if not user:
            raise UserDoesNotExist()

        response = JsonResponse(
            {'code': 200, 'response': UserSerializer(user).data},
            status=200
        )
        response['_jwt_user_id'] = user.id
        return response

    def sign_out(self):
        # logout(request)
        response = JsonResponse(
            {'code': 200, 'response': None},
            status=200
        )
        response['_jwt_user_id'] = ''
        return response

    def get_groups(self, request, **filters):
        if not request.user.is_authenticated:
            raise ForbiddenAction()

        print("Filters", filters)
        form = GetGroupsForm(data=filters)
        if not form.is_valid():
            raise BadRequest()

        print("Form", form.cleaned_data)
        return ResearchGroupSerializer(
            ResearchGroup.get_groups(**form.cleaned_data), many=True
        ).data

    def create_group(self, request, name, description=None, members=None):
        if not request.user.is_authenticated:
            raise ForbiddenAction()

        research_group = ResearchGroup.create_group(
            request.user,
            name,
            description
        )

        members = members or []
        member_users = User.objects.filter(id__in=members)
        research_group.update_members(member_users)
        return ResearchGroupSerializer(research_group).data

    def update_group(self, request, group_id, name=None, description=None,
                     members=None):
        if not request.user.is_authenticated:
            raise ForbiddenAction()

        print("Params", group_id, name, description, members)
        try:
            research_group = ResearchGroup.get_group(group_id)
        except exceptions.ResearchGroupDoesNotExist:
            raise BadRequest("The group does not exist")

        members = members or []
        member_users = User.objects.filter(id__in=members)

        research_group.update_info(name=name, description=description)
        research_group.update_members(member_users)
        return ResearchGroupSerializer(research_group).data

    # Admin functions
    def get_all_users(self, request, **filters):
        """
        Return all the users.
        It includes super and admin users.
        :return:
        :rtype: list<dict>
        """
        print(request, request.user)
        if not request.user.is_authenticated or not request.user.is_superuser:
            raise ForbiddenAction()

        cleaned_filters = {
            key: value
            for key, value in filters.items()
            if value
        }
        users = User.objects.filter(**cleaned_filters)
        return UserSerializer(users, many=True).data

    def delete_user(self, request, _id):
        """
        Delete the requested user.
        There are some pre-conditions in order to perform the actions:
            - The requesting user needs to be an admin
            - The removed user cannot be itself.
        :param request:
        :param _id:
        :return:
        """
        if not request.user.is_authenticated or not request.user.is_superuser:
            raise ForbiddenAction()

        if request.user.id == _id:
            raise BadRequest("You cannot remove your own user")

        deleted_users = User.objects.filter(id=_id).exclude(
            id=request.user.id
        ).delete()
        return deleted_users

    def promote(self, request, _id):
        """
        Promote a plain user to admin user
        :param request:
        :param _id:
        :return:
        """
        if not request.user.is_authenticated or not request.user.is_superuser:
            raise ForbiddenAction()

        if _id is None:
            raise BadRequest()

        try:
            user = User.promote(_id)
        except exceptions.UserDoesNotExist:
            raise UserDoesNotExist
        
        return UserSerializer(user).data

    def demote(self, request, _id):
        """
        Demote an admin user to plain user
        :param request:
        :param _id:
        :return:
        """
        if not request.user.is_authenticated or not request.user.is_superuser:
            raise ForbiddenAction()

        if _id is None:
            raise BadRequest()

        try:
            user = User.demote(_id)
        except exceptions.UserDoesNotExist:
            raise UserDoesNotExist
        
        return UserSerializer(user).data
