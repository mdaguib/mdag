from django.urls import re_path, path

# Web service urls. Keep it easy for now.
# Add every single route. It could be done like an auto discovery system
from mDag.apps.webservices.views import *

urlpatterns = [
    path('csrf_tokenizer', CsrfTokenizer.as_view(), name='csrf_tokenizer'),
    re_path(r'.*', DispatcherView.as_view(), name='entry')
]

