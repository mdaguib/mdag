import json
from collections import deque

from django.http import JsonResponse
from django.http.response import HttpResponseBase
from django.middleware.csrf import get_token
from django.views.generic.base import View

from .exceptions import CustomException
from .services.ping.ping import PingService
from .services.user.user import UserService
from .services.experiments.experiments import ExperimentsService


class DispatcherView(View):
    URL_SEPARATOR: str = "/"

    user: UserService = UserService()
    ping: PingService = PingService()
    experiments: ExperimentsService = ExperimentsService()

    @staticmethod
    def _get_post(request):
        """
        Gets the json encoded body and transform it
        int a dict
        :param request:
        :return:
        """
        # The form data automatically fills the POST method.
        if request.META['CONTENT_TYPE'].find('multipart/form-data') > -1:
            return request.POST

        # Otherwise, we need to fill it manually
        body = request.body
        try:
            return json.loads(body)
        except TypeError:
            return {}

    def _get_request_info(self, request) -> dict:
        """
        Recover the info from the request object
        and organize it in a dict to ease the access.
        :param request:
        :return:
        """
        # Divide the incoming url in 3 parts.
        #   - entry point: "webservice"
        #   TODO check if this should be in get or post. It will likely end
        #    up in post parameters
        #   - module name: "user" -> UserService
        #   - service name: "get_users" -> UserService.get_users
        url_components = request.path.strip(self.URL_SEPARATOR).split(
            self.URL_SEPARATOR
        )

        module_name = url_components[1]
        module = getattr(self, module_name, None)

        if not module:
            raise Exception("Module '{}' does not exist".format(module_name))

        service_name = url_components[2]
        service = getattr(module, service_name, None)

        if not service or not callable(service):
            raise Exception("Service '{}' does not exist".format(service_name))

        # This could be used in case it admits GET requests
        # positional_args = deque(url_components[3:])

        # POST is not automatically filled for angular json requests
        # so get a dict representing the data and store it in a custom
        # post object
        setattr(request, "C_POST", DispatcherView._get_post(request))
        args = request.C_POST.get("args", [])

        if not isinstance(args, list):
            raise Exception("The positional args should be a list")

        pos = deque(args)

        # In order to support positional parameters the posted data
        # needs to be a json string. So there are 2 main phases:
        # encoding and decoding
        opt = request.C_POST.get("optionals", {})

        return {
            'module': module,
            'service': service,
            'args': pos,
            'optionals': opt
        }

    def dispatch(self, request, *args, **kwargs):
        """
        Redirect the request to the correct service.
            If the service does not exist it should raise a 400 error
            If the service raises an error it should return a 5XX error

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            request_info = self._get_request_info(request)

            module = request_info.get("module")
            args = request_info.get("args")
            optionals = request_info.get("optionals")
            service = request_info.get("service")

            if __debug__:
                self._log_webservice_call(**request_info)

            if module and module.needs_request(service.__name__):
                args.appendleft(request)

            response = service(*args, **optionals)

            if not isinstance(response, HttpResponseBase):
                response = JsonResponse(
                    {'code': 200, 'response': response},
                    status=200
                )

            return response
        except CustomException as e:
            return JsonResponse({'code': e.code, 'response': str(e)},
                                status=e.code)
        except Exception as e:
            import traceback
            traceback.print_exc()
            print("Request Info")
            print("Service", service)
            print("Args", args)
            print("Optionals", optionals)
            return JsonResponse({'code': 500, 'response': str(e)}, status=500)

    @staticmethod
    def _log_webservice_call(args, optionals, service, module):
        print("-" * 120)
        print("Request Info")
        print("Service", service)
        print("Args", args)
        print("Optionals", optionals)
        print("-" * 120)


class CsrfTokenizer(View):
    def dispatch(self, request, *args, **kwargs):
        """
        Returns an empty response.
        It's useful to retrieve the CSRF token.

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        get_token(request)
        return JsonResponse({'code': 200, 'response': None})
