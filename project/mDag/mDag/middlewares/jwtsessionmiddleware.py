import json

import jwt
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from ..apps.user.models import User


class JWTAuthMiddleware:

    def __init__(self, get_response) -> None:
        self.get_response = get_response
        self.request_timestamp = None

    def __call__(self, request):
        """
        JWT Authentication middleware.
        This middleware sets up the user field for the request to the
        specific user or Anonymous depending on the auth cookie sent by the
        browser.
        """
        self.request_timestamp = datetime.now()
        token = request.COOKIES.get("Authorization")
        user = self._get_user_from_token(token)
        setattr(request, 'user', user)

        response = self.get_response(request)

        if response.status_code != 500:
            if self._is_signing_out(response):
                print("Deleting response cookie")
                response.delete_cookie('Authorization')
            elif self._is_signing_in(response):
                new_user = self._get_user_from_header(response)
                self._refresh_token(new_user, response)
            elif not isinstance(user, AnonymousUser):
                # It's already logged in
                # If we can refresh the token and the user is not logging in
                # then, it means that is an on-going session
                new_user = user
                self._refresh_token(new_user, response)
            else:
                # Delete the Authorization cookie just in case.
                response.delete_cookie('Authorization')

        self.request_timestamp = None
        return response

    def _get_user_from_token(self, jwt_token):
        """
        Obtain the user bound to the jwt_token.
        If no token is provided or it's wrong, the user will be handled as
        an anonymous user.
        """
        try:
            jwt_token_bytes = jwt_token.encode("utf-8")
            token_info = jwt.decode(
                jwt_token_bytes,
                settings.SECRET_KEY,
                algorithms=[settings.JWT_ALGORITHM]
            )
        except (jwt.InvalidSignatureError, jwt.DecodeError, AttributeError):
            return AnonymousUser()

        expiration_date = datetime.fromisoformat(
            token_info['expiration']
        )
        if expiration_date > self.request_timestamp:

            return AnonymousUser()

        user = User.objects.filter(
            id=token_info['user']
        ).first()

        return user

    def _get_user_from_header(self, response):
        user = User.objects.get(id=response['_jwt_user_id'])
        # Delete the private user header as far as it's no longer useful
        self._delete_private_user_header(response)
        return user

    @classmethod
    def _can_refresh_token(cls, user, response):
        """
        We're currently handling then next use-cases:
         1- An anonymous user logs in
         2- A registered user makes a request and its session has expired
         3- An anonymous user makes a request
         4- A registered user makes a request and its session has NOT expired

        Cases 1 and 4 need to refresh the token with a new expiration time
        Cases 2 and 3 do not send the Authorization header back
        """
        return response.status_code != 500

        return (
            isinstance(user, AnonymousUser) and cls._is_signing_in(response)
            or not isinstance(user, AnonymousUser)
        )

    @staticmethod
    def _delete_private_user_header(response):
        if '_jwt_user_id' in response:
            del response['_jwt_user_id']

    @staticmethod
    def _get_updated_token(user, timestamp, expiration):
        user_dict = {
            'user': user.id,
            'joined': user.date_joined.isoformat(),
            'expiration': timestamp.isoformat(),
            'timestamp': expiration.isoformat()
        }

        token = jwt.encode(
            user_dict,
            settings.SECRET_KEY,
            algorithm=settings.JWT_ALGORITHM
        )
        return token.decode("utf-8")

    @staticmethod
    def _is_signing_in(response):
        return '_jwt_user_id' in response and response["_jwt_user_id"] != ''

    @staticmethod
    def _is_signing_out(response):
        return "_jwt_user_id" in response and response["_jwt_user_id"] == ''

    @classmethod
    def _refresh_token(cls, user, response):
        timestamp = datetime.now()
        expiration = timestamp + timedelta(seconds=settings.SESSION_COOKIE_AGE)
        # Update user session
        new_token = cls._get_updated_token(user, timestamp, expiration)
        response.set_cookie(
            'Authorization',
            new_token,
            httponly=True,
            expires=expiration
        )
