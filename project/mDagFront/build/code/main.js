(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pagenotfound/pagenotfound.component */ "./src/app/pagenotfound/pagenotfound.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _storage_experiment_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./storage/experiment-list.component */ "./src/app/storage/experiment-list.component.ts");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var _experimentcreationwizard_experimentcreationwizard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./experimentcreationwizard/experimentcreationwizard.component */ "./src/app/experimentcreationwizard/experimentcreationwizard.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user-management/user-management.component */ "./src/app/user-management/user-management.component.ts");
/* harmony import */ var _groups_list_groups_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./groups-list/groups-list.component */ "./src/app/groups-list/groups-list.component.ts");











var routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    {
        path: "",
        children: [
            {
                path: "experiments",
                children: [
                    { path: "", component: _storage_experiment_list_component__WEBPACK_IMPORTED_MODULE_5__["ExperimentListComponent"] },
                ]
            },
            {
                canActivateChild: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
                canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
                path: "experiments",
                children: [
                    {
                        path: "create",
                        component: _experimentcreationwizard_experimentcreationwizard_component__WEBPACK_IMPORTED_MODULE_7__["ExperimentCreationWizardComponent"]
                    }
                ],
                runGuardsAndResolvers: 'always'
            },
            {
                canActivateChild: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
                canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
                runGuardsAndResolvers: 'always',
                path: "user-management",
                children: [
                    { path: "", component: _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_9__["UserManagementComponent"] }
                ],
            },
        ],
    },
    { path: "signup", component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_8__["SignupComponent"] },
    {
        path: "groups",
        canActivateChild: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        runGuardsAndResolvers: 'always',
        component: _groups_list_groups_list_component__WEBPACK_IMPORTED_MODULE_10__["GroupsListComponent"],
        children: []
    },
    // URLs That DO NOT need user permission check
    { path: "**", component: _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_3__["PagenotfoundComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
                    onSameUrlNavigation: 'reload',
                })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-modal-window></app-modal-window>\n<div class=\"view-container\">\n    <app-header class=\"header\"></app-header>\n    <div class=\"main-view\">\n        <app-menu></app-menu>\n        <div class=\"content-container\">\n            <div class=\"content\">\n                <!-- Calling this component makes the app route the request and show the needed info -->\n              <router-outlet></router-outlet>\n            </div>\n            <app-footer class=\"footer\"></app-footer>\n        </div>\n    </div>\n    <app-notifications></app-notifications>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\nThere are 4 levels of deepness right now:\n  5: Main page content\n  19: Right menu\n  20: Header\n  40: Overlays\n */\napp-root {\n  font-size: 18px; }\n.content {\n  overflow-y: hidden;\n  flex: 30 0;\n  z-index: 5; }\n.view-container {\n  display: flex;\n  flex-direction: column;\n  min-height: 100vh; }\n.header {\n  flex-shrink: 0;\n  z-index: 20; }\n.footer {\n  flex-shrink: 0;\n  margin-bottom: 7px; }\n.content-view-size {\n  flex: 1 0; }\n.main-view {\n  flex: 1 0;\n  display: flex; }\n.content-container {\n  display: flex;\n  flex-direction: column;\n  flex: 1 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCIuLi9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7O0VDTUU7QURFRjtFQUNFLGVBQWUsRUFBQTtBQUtqQjtFQUVFLGtCQUFrQjtFQUVsQixVQUFVO0VBQ1YsVUFBVSxFQUFBO0FBSVo7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGlCQUFpQixFQUFBO0FBR25CO0VBQ0UsY0FBYztFQUNkLFdBQVcsRUFBQTtBQUdiO0VBQ0UsY0FBYztFQUNkLGtCQUFrQixFQUFBO0FBR3BCO0VBQ0UsU0FBUyxFQUFBO0FBR1g7RUFDRSxTQUFTO0VBQ1QsYUFBYSxFQUFBO0FBR2Y7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLFNBQVMsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuVGhlcmUgYXJlIDQgbGV2ZWxzIG9mIGRlZXBuZXNzIHJpZ2h0IG5vdzpcbiAgNTogTWFpbiBwYWdlIGNvbnRlbnRcbiAgMTk6IFJpZ2h0IG1lbnVcbiAgMjA6IEhlYWRlclxuICA0MDogT3ZlcmxheXNcbiAqL1xuXG5hcHAtcm9vdCB7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cblxuLy8gVGhpcyBpcyB1c2VkIHRvIGtlZXAgdGhlIGZvb3RlciBhdCB0aGUgZW5kIG9mIHRoZSBjb250ZW50IG9yXG4vLyBhdCBsZWFzdCBhdCB0aGUgZW5kIG9mIHRoZSB2aWV3cG9ydFxuLmNvbnRlbnQge1xuICAvL3BhZGRpbmctdG9wOiAxdmg7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgLy8gVGhpcyBpcyB0aGUgbWFpbiBzb3VyY2UsIGl0IHNob3VsZCBob2xkIGFsbW9zdCBhbGwgdGhlIHZpZXdwb3J0XG4gIGZsZXg6IDMwIDA7XG4gIHotaW5kZXg6IDU7XG59XG5cblxuLnZpZXctY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgbWluLWhlaWdodDogMTAwdmg7XG59XG5cbi5oZWFkZXIge1xuICBmbGV4LXNocmluazogMDtcbiAgei1pbmRleDogMjA7XG59XG5cbi5mb290ZXIge1xuICBmbGV4LXNocmluazogMDtcbiAgbWFyZ2luLWJvdHRvbTogN3B4O1xufVxuXG4uY29udGVudC12aWV3LXNpemUge1xuICBmbGV4OiAxIDA7XG59XG5cbi5tYWluLXZpZXcge1xuICBmbGV4OiAxIDA7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5jb250ZW50LWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGZsZXg6IDEgMDtcbn0iLCIvKipcblRoZXJlIGFyZSA0IGxldmVscyBvZiBkZWVwbmVzcyByaWdodCBub3c6XG4gIDU6IE1haW4gcGFnZSBjb250ZW50XG4gIDE5OiBSaWdodCBtZW51XG4gIDIwOiBIZWFkZXJcbiAgNDA6IE92ZXJsYXlzXG4gKi9cbmFwcC1yb290IHtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi5jb250ZW50IHtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICBmbGV4OiAzMCAwO1xuICB6LWluZGV4OiA1OyB9XG5cbi52aWV3LWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIG1pbi1oZWlnaHQ6IDEwMHZoOyB9XG5cbi5oZWFkZXIge1xuICBmbGV4LXNocmluazogMDtcbiAgei1pbmRleDogMjA7IH1cblxuLmZvb3RlciB7XG4gIGZsZXgtc2hyaW5rOiAwO1xuICBtYXJnaW4tYm90dG9tOiA3cHg7IH1cblxuLmNvbnRlbnQtdmlldy1zaXplIHtcbiAgZmxleDogMSAwOyB9XG5cbi5tYWluLXZpZXcge1xuICBmbGV4OiAxIDA7XG4gIGRpc3BsYXk6IGZsZXg7IH1cblxuLmNvbnRlbnQtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgZmxleDogMSAwOyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Metabolic DAG app!';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule, initializeApp1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initializeApp1", function() { return initializeApp1; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pagenotfound/pagenotfound.component */ "./src/app/pagenotfound/pagenotfound.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./menu/menu.component */ "./src/app/menu/menu.component.ts");
/* harmony import */ var _menuitem_menuitem_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./menuitem/menuitem.component */ "./src/app/menuitem/menuitem.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular-mdc/web */ "./node_modules/@angular-mdc/web/esm5/web.es5.js");
/* harmony import */ var _undecoratedlist_undecoratedlist_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./undecoratedlist/undecoratedlist.component */ "./src/app/undecoratedlist/undecoratedlist.component.ts");
/* harmony import */ var _headedcontainer_headedcontainer_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./headedcontainer/headedcontainer.component */ "./src/app/headedcontainer/headedcontainer.component.ts");
/* harmony import */ var _storage_experiment_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./storage/experiment-list.component */ "./src/app/storage/experiment-list.component.ts");
/* harmony import */ var _fileitem_fileitem_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./fileitem/fileitem.component */ "./src/app/fileitem/fileitem.component.ts");
/* harmony import */ var _storageitem_storageitem_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./storageitem/storageitem.component */ "./src/app/storageitem/storageitem.component.ts");
/* harmony import */ var _collapsiblecontainer_collapsiblecontainer_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./collapsiblecontainer/collapsiblecontainer.component */ "./src/app/collapsiblecontainer/collapsiblecontainer.component.ts");
/* harmony import */ var _searchfilter_searchfilter_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./searchfilter/searchfilter.component */ "./src/app/searchfilter/searchfilter.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _session_session_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./session/session.service */ "./src/app/session/session.service.ts");
/* harmony import */ var _csrf_csrfincerceptor_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./csrf/csrfincerceptor.service */ "./src/app/csrf/csrfincerceptor.service.ts");
/* harmony import */ var _modal_window_modal_window_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./modal-window/modal-window.component */ "./src/app/modal-window/modal-window.component.ts");
/* harmony import */ var _experimentcreationwizard_experimentcreationwizard_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./experimentcreationwizard/experimentcreationwizard.component */ "./src/app/experimentcreationwizard/experimentcreationwizard.component.ts");
/* harmony import */ var _experimentcreationwizard_wholeorganimsexperimentform_wholeorganimsexperimentform_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component */ "./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.ts");
/* harmony import */ var _fileuploader_fileuploader_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./fileuploader/fileuploader.component */ "./src/app/fileuploader/fileuploader.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./user-management/user-management.component */ "./src/app/user-management/user-management.component.ts");
/* harmony import */ var _csrf_csrf_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./csrf/csrf.service */ "./src/app/csrf/csrf.service.ts");
/* harmony import */ var _experimentcreationwizard_organism_comparison_experiment_organism_comparison_experiment_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component */ "./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.ts");
/* harmony import */ var _experimentcreationwizard_experiment_comparison_experiment_comparison_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./experimentcreationwizard/experiment-comparison/experiment-comparison.component */ "./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.ts");
/* harmony import */ var _experimentcreationwizard_organism_metabolism_comparison_organism_metabolism_comparison_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component */ "./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.ts");
/* harmony import */ var _experimentcreationwizard_organism_pathway_calculation_organism_pathway_calculation_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component */ "./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.ts");
/* harmony import */ var _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./notifications/notifications.component */ "./src/app/notifications/notifications.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _groups_list_groups_list_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./groups-list/groups-list.component */ "./src/app/groups-list/groups-list.component.ts");
/* harmony import */ var _group_details_group_details_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./group-details/group-details.component */ "./src/app/group-details/group-details.component.ts");












// Modules from MD web




























// CSRF header name
var crsfTokenName = "X-CSRFToken";
// let crsfTokenName : string = "MDAG_S_HTTP_CSRFTOKEN";
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_6__["PagenotfoundComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
                _menu_menu_component__WEBPACK_IMPORTED_MODULE_9__["MenuComponent"],
                _menuitem_menuitem_component__WEBPACK_IMPORTED_MODULE_10__["MenuitemComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"],
                _undecoratedlist_undecoratedlist_component__WEBPACK_IMPORTED_MODULE_13__["UndecoratedlistComponent"],
                _headedcontainer_headedcontainer_component__WEBPACK_IMPORTED_MODULE_14__["HeadedcontainerComponent"],
                _storage_experiment_list_component__WEBPACK_IMPORTED_MODULE_15__["ExperimentListComponent"],
                _fileitem_fileitem_component__WEBPACK_IMPORTED_MODULE_16__["FileItemComponent"],
                _storageitem_storageitem_component__WEBPACK_IMPORTED_MODULE_17__["StorageItemComponent"],
                _collapsiblecontainer_collapsiblecontainer_component__WEBPACK_IMPORTED_MODULE_18__["CollapsiblecontainerComponent"],
                _searchfilter_searchfilter_component__WEBPACK_IMPORTED_MODULE_19__["SearchfilterComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_20__["UserComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_21__["LoginComponent"],
                _modal_window_modal_window_component__WEBPACK_IMPORTED_MODULE_25__["ModalWindowComponent"],
                _experimentcreationwizard_experimentcreationwizard_component__WEBPACK_IMPORTED_MODULE_26__["ExperimentCreationWizardComponent"],
                _fileuploader_fileuploader_component__WEBPACK_IMPORTED_MODULE_28__["FileUploaderComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_29__["SignupComponent"],
                _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_30__["UserManagementComponent"],
                _experimentcreationwizard_wholeorganimsexperimentform_wholeorganimsexperimentform_component__WEBPACK_IMPORTED_MODULE_27__["WholeOrganimsExperimentFormComponent"],
                _experimentcreationwizard_organism_comparison_experiment_organism_comparison_experiment_component__WEBPACK_IMPORTED_MODULE_32__["OrganismComparisonExperimentComponent"],
                _experimentcreationwizard_experiment_comparison_experiment_comparison_component__WEBPACK_IMPORTED_MODULE_33__["ExperimentComparisonComponent"],
                _experimentcreationwizard_organism_metabolism_comparison_organism_metabolism_comparison_component__WEBPACK_IMPORTED_MODULE_34__["OrganismMetabolismComparisonComponent"],
                _experimentcreationwizard_organism_pathway_calculation_organism_pathway_calculation_component__WEBPACK_IMPORTED_MODULE_35__["OrganismPathwayCalculationComponent"],
                _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_36__["NotificationsComponent"],
                _groups_list_groups_list_component__WEBPACK_IMPORTED_MODULE_38__["GroupsListComponent"],
                _group_details_group_details_component__WEBPACK_IMPORTED_MODULE_39__["GroupDetailsComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcFabModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcButtonModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcIconModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcIconButtonModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcDrawerModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcSelectModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcTextFieldModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcCheckboxModule"],
                _angular_mdc_web__WEBPACK_IMPORTED_MODULE_12__["MdcListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_22__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_22__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientXsrfModule"].withOptions({
                    cookieName: crsfTokenName,
                    headerName: crsfTokenName
                }),
            ],
            providers: [
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_37__["APP_BASE_HREF"], useFactory: function (s) { return s.getBaseHrefFromDOM(); }, deps: [_angular_common__WEBPACK_IMPORTED_MODULE_37__["PlatformLocation"]] },
                { provide: _angular_core__WEBPACK_IMPORTED_MODULE_2__["APP_INITIALIZER"], useFactory: initializeApp1, deps: [_csrf_csrf_service__WEBPACK_IMPORTED_MODULE_31__["CsrfService"]], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"], useClass: _csrf_csrfincerceptor_service__WEBPACK_IMPORTED_MODULE_24__["CsrfInterceptorService"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"], useClass: _session_session_service__WEBPACK_IMPORTED_MODULE_23__["SessionService"], multi: true },
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());

function initializeApp1(service) {
    return function () {
        return service.requestToken();
    };
}


/***/ }),

/***/ "./src/app/auth/auth.guard.ts":
/*!************************************!*\
  !*** ./src/app/auth/auth.guard.ts ***!
  \************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user/user.model */ "./src/app/user/user.model.ts");
/* harmony import */ var _url_permission_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url-permission.module */ "./src/app/url-permission.module.ts");





var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivateChild = function (childRoute, state) {
        return this.canActivate(childRoute, state);
    };
    AuthGuard.prototype.canActivate = function (next, state) {
        var userInfo = window.sessionStorage.getItem('mdag_user') || '{}';
        var user = new _user_user_model__WEBPACK_IMPORTED_MODULE_3__["User"](userInfo);
        var urlSegments = next.url.map(function (urlSegment) { return urlSegment.path; });
        if (!user.isAdmin && !_url_permission_module__WEBPACK_IMPORTED_MODULE_4__["PermissionModule"].canNavigate(user.getPermissions(), urlSegments)) {
            this.router.navigate(['/home']);
        }
        // Never stop a navigation!
        return true;
    };
    AuthGuard.navigatingToHome = function (route) {
        return route.length == 1 && route[0].path === 'home';
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/browsefeaturechecker/browser-feature-checker.service.ts":
/*!*************************************************************************!*\
  !*** ./src/app/browsefeaturechecker/browser-feature-checker.service.ts ***!
  \*************************************************************************/
/*! exports provided: BrowserFeatureCheckerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserFeatureCheckerService", function() { return BrowserFeatureCheckerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BrowserFeatureCheckerService = /** @class */ (function () {
    function BrowserFeatureCheckerService() {
        this.supportsFormData = "FormData" in window;
        this.supportsDataTransfer = "DataTransfer" in window;
        this.supportsFileReader = "FileReader" in window;
        this.supportsDragAndDrop = this.checkSupportForDragAndDrop();
    }
    BrowserFeatureCheckerService.prototype.doesSupportDragAndDropFileUpload = function () {
        return this.supportsFormData && (this.supportsDataTransfer || this.supportsFileReader);
    };
    BrowserFeatureCheckerService.prototype.checkSupportForDragAndDrop = function () {
        var div = document.createElement("div");
        return ("draggable" in div) || ("ondragstart" in div && "ondrop" in div);
    };
    BrowserFeatureCheckerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BrowserFeatureCheckerService);
    return BrowserFeatureCheckerService;
}());



/***/ }),

/***/ "./src/app/collapsiblecontainer/collapsiblecontainer.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/collapsiblecontainer/collapsiblecontainer.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".headed-container:focus {\n  outline: none; }\n\n.headed-container:focus .contributors-container {\n  outline: none; }\n\n.contributors-container {\n  overflow-y: hidden;\n  transition-property: max-height;\n  transition-duration: 0.2s; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9jb2xsYXBzaWJsZWNvbnRhaW5lci9jb2xsYXBzaWJsZWNvbnRhaW5lci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGFBQWEsRUFBQTs7QUFJZjtFQUNFLGtCQUFrQjtFQUVsQiwrQkFBK0I7RUFDL0IseUJBQXlCLEVBQUEiLCJmaWxlIjoiLi4vc3JjL2FwcC9jb2xsYXBzaWJsZWNvbnRhaW5lci9jb2xsYXBzaWJsZWNvbnRhaW5lci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZWQtY29udGFpbmVyOmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLmhlYWRlZC1jb250YWluZXI6Zm9jdXMgLmNvbnRyaWJ1dG9ycy1jb250YWluZXIge1xuICBvdXRsaW5lOiBub25lO1xufVxuXG5cbi5jb250cmlidXRvcnMtY29udGFpbmVyIHtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuXG4gIHRyYW5zaXRpb24tcHJvcGVydHk6IG1heC1oZWlnaHQ7XG4gIHRyYW5zaXRpb24tZHVyYXRpb246IDAuMnM7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/collapsiblecontainer/collapsiblecontainer.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/collapsiblecontainer/collapsiblecontainer.component.ts ***!
  \************************************************************************/
/*! exports provided: CollapsiblecontainerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollapsiblecontainerComponent", function() { return CollapsiblecontainerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _headedcontainer_headedcontainer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../headedcontainer/headedcontainer.component */ "./src/app/headedcontainer/headedcontainer.component.ts");



var CollapsiblecontainerComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CollapsiblecontainerComponent, _super);
    function CollapsiblecontainerComponent() {
        return _super.call(this) || this;
    }
    CollapsiblecontainerComponent.prototype.ngOnInit = function () {
    };
    CollapsiblecontainerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-collapsiblecontainer',
            template: __webpack_require__(/*! ../headedcontainer/headedcontainer.component.html */ "./src/app/headedcontainer/headedcontainer.component.html"),
            styles: [__webpack_require__(/*! ./collapsiblecontainer.component.scss */ "./src/app/collapsiblecontainer/collapsiblecontainer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CollapsiblecontainerComponent);
    return CollapsiblecontainerComponent;
}(_headedcontainer_headedcontainer_component__WEBPACK_IMPORTED_MODULE_2__["HeadedcontainerComponent"]));



/***/ }),

/***/ "./src/app/csrf/csrf.service.ts":
/*!**************************************!*\
  !*** ./src/app/csrf/csrf.service.ts ***!
  \**************************************/
/*! exports provided: CsrfService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CsrfService", function() { return CsrfService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var CsrfService = /** @class */ (function () {
    function CsrfService(http) {
        this.http = http;
    }
    CsrfService.prototype.requestToken = function () {
        var baseUrl = "webservice/csrf_tokenizer";
        return this.http.get(baseUrl, {
            observe: 'response'
        }).toPromise().catch(function (error) {
            console.log(error);
        });
    };
    CsrfService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CsrfService);
    return CsrfService;
}());



/***/ }),

/***/ "./src/app/csrf/csrfincerceptor.service.ts":
/*!*************************************************!*\
  !*** ./src/app/csrf/csrfincerceptor.service.ts ***!
  \*************************************************/
/*! exports provided: CsrfInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CsrfInterceptorService", function() { return CsrfInterceptorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _csrf_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./csrf.service */ "./src/app/csrf/csrf.service.ts");
/* harmony import */ var _global_services_cookie_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global-services/cookie-service.service */ "./src/app/global-services/cookie-service.service.ts");




// import {CookieService} from "ngx-cookie-service";
var CsrfInterceptorService = /** @class */ (function () {
    function CsrfInterceptorService(cookies, csrfService) {
        this.csrfService = csrfService;
        this.cookies = cookies;
    }
    CsrfInterceptorService.prototype.intercept = function (req, next) {
        var csrfToken = this.cookies.get("csrftoken");
        var _req;
        if (csrfToken) {
            _req = req.clone({
                headers: req.headers.set('X-CSRFToken', csrfToken)
            });
        }
        else {
            _req = req;
        }
        return next.handle(_req);
    };
    CsrfInterceptorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_global_services_cookie_service_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"], _csrf_service__WEBPACK_IMPORTED_MODULE_2__["CsrfService"]])
    ], CsrfInterceptorService);
    return CsrfInterceptorService;
}());



/***/ }),

/***/ "./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"experimentComparisonForm\">\n\n    <!-- TODO Adapt form to show the form fields -->\n    <form [formGroup]=\"form\" (ngSubmit)=\"formSubmit()\">\n        <div class=\"first-section\">\n            <h2 class=\"section-header\">Experiment data</h2>\n            <div class=\"fieldset-row\">\n                <!-- Maybe organisms and pathway should be selectors -->\n                <mdc-text-field class=\"row-item\" label=\"Name\"\n                                formControlName=\"experimentName\">\n                </mdc-text-field>\n            </div>\n            <div class=\"fieldset-row\">\n                <mdc-textarea class=\"row-item\" label=\"Description\"\n                              formControlName=\"experimentDescription\">\n                </mdc-textarea>\n            </div>\n\n            <div class=\"fieldset-row\">\n\n                <file-uploader\n                        formControlName=\"experimentFiles\"\n                        class=\"row-item\"\n                        name=\"Experiments list file\"\n                        helperText=\"Click in this area or drop a file to upload the expriments file\"\n                        (onFileLoaded)=\"onExperimentClassificationFilesAdded($event)\"\n                ></file-uploader>\n            </div>\n        </div>\n        <div class=\"Section\">\n            <h2 class=\"section-header\">Experiment options</h2>\n            <div class=\"fieldset-row\">\n                <div class=\"row-item\">\n                    <mdc-checkbox class=\"custom-checkbox\"\n                                  formControlName=\"measureSimilarities\">\n                    </mdc-checkbox>\n                    <label class=\"checkbox--label\">\n                        Measure similarities between the blocks of\n                        the experiments\n                    </label>\n                </div>\n                <div class=\"row-item\">\n                    <mdc-checkbox class=\"custom-checkbox\"\n                                  formControlName=\"useAllThePathways\">\n                    </mdc-checkbox>\n                    <label class=\"checkbox--label\">\n                        Use all the species pathways\n                    </label>\n                </div>\n\n                <div class=\"row-item\">\n                    <mdc-checkbox class=\"custom-checkbox\"\n                                  formControlName=\"generateMDagFiles\">\n                    </mdc-checkbox>\n                    <label class=\"checkbox--label\">\n                        Generate pathway independent mDAG file\n                    </label>\n                </div>\n            </div>\n\n\n        </div>\n        <div class=\"section\">\n            <h2 class=\"section-header\">Classification data</h2>\n            <div class=\"fieldset-column\">\n                <div class=\"fieldset-row\">\n\n                    <file-uploader\n                            class=\"row-item\"\n                            name=\"Groups\"\n                            helperText=\"Click in this area or drop a file to bind groups to this experiment\"\n                            (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                    ></file-uploader>\n                </div>\n                <div class=\"fieldset-row\">\n\n                    <file-uploader\n                            class=\"row-item\"\n                            name=\"Organism Classification\"\n                            helperText=\"Click in this area or drop a file to classify the organism\"\n                            (onFileLoaded)=\"onExperimentClassificationFilesAdded($event)\"\n                    ></file-uploader>\n                </div>\n            </div>\n        </div>\n        <button mdc-button raised\n                style=\"position: fixed; bottom: 5%; right: 2%;\"\n                aria-label=\"Submit experiment\"\n                type=\"submit\"\n                [disabled]=\"!form.valid\">\n            Submit\n        </button>\n    </form>\n</div>"

/***/ }),

/***/ "./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fieldset-row {\n  display: flex;\n  flex-direction: row;\n  margin-bottom: 15px; }\n\n.fieldset-column {\n  display: flex;\n  flex-direction: column; }\n\n.row-item {\n  flex: 1 0;\n  margin: 5px 10px; }\n\n.section, .first-section {\n  margin-top: 45px; }\n\n.first-section {\n  margin-top: 30px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\nfile-uploader {\n  margin-bottom: 15px; }\n\n.checkbox--label {\n  display: inline-block;\n  padding: 11px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9leHBlcmltZW50Y3JlYXRpb253aXphcmQvc3RlcHMtY29tbW9uLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWE7RUFDYixzQkFBc0IsRUFBQTs7QUFJeEI7RUFDRSxTQUFTO0VBQ1QsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usa0JBQWtCO0VBRWxCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFHbEM7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxxQkFBcUI7RUFDckIsZUFBZSxFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvZXhwZXJpbWVudGNyZWF0aW9ud2l6YXJkL2V4cGVyaW1lbnQtY29tcGFyaXNvbi9leHBlcmltZW50LWNvbXBhcmlzb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vbWF0ZXJpYWx2YXJzXCI7XG5cbi5maWVsZHNldC1yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uZmllbGRzZXQtY29sdW1uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuXG4ucm93LWl0ZW0ge1xuICBmbGV4OiAxIDA7XG4gIG1hcmdpbjogNXB4IDEwcHg7XG59XG5cbi5zZWN0aW9uIHtcbiAgbWFyZ2luLXRvcDogNDVweDtcbn1cblxuLmZpcnN0LXNlY3Rpb24ge1xuICBAZXh0ZW5kIC5zZWN0aW9uO1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG4uc2VjdGlvbi1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2RkZGRkZDtcbn1cblxuZmlsZS11cGxvYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5jaGVja2JveC0tbGFiZWwge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDExcHggMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ExperimentComparisonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExperimentComparisonComponent", function() { return ExperimentComparisonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../storage/experiment.service */ "./src/app/storage/experiment.service.ts");




var ExperimentComparisonComponent = /** @class */ (function () {
    function ExperimentComparisonComponent(experimentService) {
        this.onExperimentCreated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            experimentName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]),
            experimentDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            measureSimilarities: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            useAllThePathways: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            generateMDagFiles: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        });
        this.experimentService = experimentService;
    }
    ExperimentComparisonComponent.prototype.ngOnInit = function () {
    };
    ExperimentComparisonComponent.prototype.formSubmit = function () {
        // let request = {
        //     'type': this.experimentType,
        //     'name': this.experimentName,
        //     'description': this.experimentDescription,
        //     'organisms': [this.organism],
        //     'pathway': [this.pathway],
        //
        //     // File like elements
        //     'groups': this.groups,
        //     'experiment_classification': this.experimentClassification
        // };
        var _this = this;
        var formValue = this.form.value;
        // TODO Adapt to the new call
        this.experimentService.createExperimentComparison(formValue.experimentName, formValue.experimentDescription, formValue.organism).then(function (value) { _this.onExperimentCreated.emit(); });
    };
    ExperimentComparisonComponent.prototype.onGroupFilesAdded = function (files) {
        this.groups = files;
    };
    ExperimentComparisonComponent.prototype.onExperimentClassificationFilesAdded = function (files) {
        this.experimentClassification = files;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ExperimentComparisonComponent.prototype, "onExperimentCreated", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ExperimentComparisonComponent.prototype, "form", void 0);
    ExperimentComparisonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-experiment-comparison',
            template: __webpack_require__(/*! ./experiment-comparison.component.html */ "./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.html"),
            styles: [__webpack_require__(/*! ./experiment-comparison.component.scss */ "./src/app/experimentcreationwizard/experiment-comparison/experiment-comparison.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__["ExperimentService"]])
    ], ExperimentComparisonComponent);
    return ExperimentComparisonComponent;
}());



/***/ }),

/***/ "./src/app/experimentcreationwizard/experimentcreationwizard.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/experimentcreationwizard.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"experiment-selection-wizard-container\">\n    <div id=\"breadcrumbs\">\n        <span class=\"breadcrumb-level\">Experiment creation</span>\n\n        <div class=\"breadcrumb-level\"\n             *ngFor=\"let breadcrumb of getVisibleBreadCrumbs()\">\n            <span class=\"breadcrumb-separator\"> > </span>\n            <span class=\"breadcrumb-level\">{{breadcrumb}}</span>\n        </div>\n    </div>\n    <div id=\"main-area\" class=\"main-content-container scrollable-container\">\n        <!-- Step 1: Experiment type selection -->\n        <div id=\"step-1\" class=\"experiment-selection\">\n            <button mdc-button\n                    *ngFor=\"let experimentType of experimentTypes\"\n                    [attr.data-index]=\"experimentType.type\"\n                    class=\"experiment-selection-button\"\n                    (click)=\"onClick($event)\">\n                <span>\n                  {{experimentType.name}}\n                </span>\n            </button>\n\n        </div>\n        <!-- Step 2: Data fulfilling -->\n        <div id=\"step-2\" class=\"experiment-form scrollable-form\">\n            <app-organism-pathway-calculation\n                [hidden]=\"!isExperimentTypeSelected(1)\">\n            </app-organism-pathway-calculation>\n            <app-wholeorganimsexperimentform\n                    (onExperimentCreated)=\"onExperimentCreated()\"\n                    [hidden]=\"!isExperimentTypeSelected(2)\">\n            </app-wholeorganimsexperimentform>\n            <app-organism-comparison-experiment\n                    (onExperimentCreated)=\"onExperimentCreated()\"\n                    [hidden]=\"!isExperimentTypeSelected(3)\">\n            </app-organism-comparison-experiment>\n            <app-experiment-comparison\n                    (onExperimentCreated)=\"onExperimentCreated()\"\n                    [hidden]=\"!isExperimentTypeSelected(4)\">\n            </app-experiment-comparison>\n            <app-organism-metabolism-comparison\n                    (onExperimentCreated)=\"onExperimentCreated()\"\n                    [hidden]=\"!isExperimentTypeSelected(5)\">\n            </app-organism-metabolism-comparison>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/experimentcreationwizard/experimentcreationwizard.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/experimentcreationwizard.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".experiment-selection-wizard-container {\n  display: flex;\n  flex-direction: column; }\n\n#breadcrumbs {\n  flex: 1 0;\n  background-color: rgba(221, 221, 221, 0.3); }\n\n#breadcrumbs span {\n    cursor: default; }\n\n.breadcrumb-level {\n  padding: 2px 6px;\n  display: inline-block; }\n\n.main-content-container {\n  position: relative;\n  flex: 2 0;\n  display: flex;\n  flex-direction: row; }\n\n#step-1 {\n  flex: 1 0;\n  padding: 15px 2px 0 2px; }\n\n#step-2 {\n  flex: 7 0;\n  padding: 0 5px;\n  display: flex;\n  flex-direction: column;\n  overflow: auto;\n  max-height: 86.4vh; }\n\n.scrollable-container {\n  overflow-y: auto; }\n\n.experiment-selection {\n  justify-content: center;\n  margin: 0 10px; }\n\n.experiment-selection-selected {\n  background-color: rgba(0, 93, 185, 0.12);\n  opacity: 1; }\n\n.experiment-selection-button {\n  display: block;\n  height: auto;\n  width: 100%;\n  margin: 10px auto;\n  padding: 10px 0; }\n\n.experiment-selection-button span {\n    margin: 10px 0; }\n\n.fieldset-row {\n  display: flex;\n  flex-direction: row;\n  margin-bottom: 15px; }\n\n.fieldset-column {\n  display: flex;\n  flex-direction: column; }\n\n.row-item {\n  flex: 1 0;\n  margin: 5px 10px; }\n\n.section, .first-section {\n  margin-top: 45px; }\n\n.first-section {\n  margin-top: 30px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\nfile-uploader {\n  margin-bottom: 15px; }\n\n.checkbox--label {\n  display: inline-block;\n  padding: 11px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9leHBlcmltZW50Y3JlYXRpb253aXphcmQvZXhwZXJpbWVudGNyZWF0aW9ud2l6YXJkLmNvbXBvbmVudC5zY3NzIiwiL2hvbWUvamVzdXMvRG9jdW1lbnRzL3JlcG9zaXRvcmllcy90ZmcvbWRhZy9wcm9qZWN0L21EYWdGcm9udC9zcmMvX21hdGVyaWFsdmFycy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQixFQUFBOztBQUd4QjtFQUNFLFNBQVM7RUFDVCwwQ0NSMkIsRUFBQTs7QURNN0I7SUFJSSxlQUFlLEVBQUE7O0FBSW5CO0VBQ0UsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsYUFBYTtFQUNiLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLFNBQVM7RUFHVCx1QkFBdUIsRUFBQTs7QUFHekI7RUFDRSxTQUFTO0VBQ1QsY0FBYztFQUNkLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsY0FBYztFQUNkLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLHVCQUF1QjtFQUN2QixjQUFjLEVBQUE7O0FBR2hCO0VBQ0Usd0NDckR5QjtFRHNEekIsVUFBVSxFQUFBOztBQUdaO0VBQ0UsY0FBYTtFQUNiLFlBQVk7RUFDWixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFMakI7SUFRSSxjQUFjLEVBQUE7O0FBSWxCO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBSXhCO0VBQ0UsU0FBUztFQUNULGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGdCQUFnQixFQUFBOztBQUdsQjtFQUVFLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGtCQUFrQjtFQUVsQixZQUFZO0VBQ1osZ0NBQWdDLEVBQUE7O0FBR2xDO0VBQ0UsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UscUJBQXFCO0VBQ3JCLGVBQWUsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL2V4cGVyaW1lbnRjcmVhdGlvbndpemFyZC9leHBlcmltZW50Y3JlYXRpb253aXphcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vbWF0ZXJpYWx2YXJzXCI7XG5cbi5leHBlcmltZW50LXNlbGVjdGlvbi13aXphcmQtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuI2JyZWFkY3J1bWJzIHtcbiAgZmxleDogMSAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKCRtZGMtdGhlbWUtc2Vjb25kYXJ5LCAwLjMpO1xuICAmIHNwYW57XG4gICAgY3Vyc29yOiBkZWZhdWx0O1xuICB9XG59XG5cbi5icmVhZGNydW1iLWxldmVsIHtcbiAgcGFkZGluZzogMnB4IDZweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4ubWFpbi1jb250ZW50LWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZmxleDogMiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuXG4jc3RlcC0xIHtcbiAgZmxleDogMSAwO1xuICAvL2Rpc3BsYXk6IGZsZXg7XG4gIC8vZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgcGFkZGluZzogMTVweCAycHggMCAycHg7XG59XG5cbiNzdGVwLTIge1xuICBmbGV4OiA3IDA7XG4gIHBhZGRpbmc6IDAgNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBvdmVyZmxvdzogYXV0bztcbiAgbWF4LWhlaWdodDogODYuNHZoO1xufVxuXG4uc2Nyb2xsYWJsZS1jb250YWluZXIge1xuICBvdmVyZmxvdy15OiBhdXRvO1xufVxuXG4uZXhwZXJpbWVudC1zZWxlY3Rpb24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi5leHBlcmltZW50LXNlbGVjdGlvbi1zZWxlY3RlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoJG1kYy10aGVtZS1wcmltYXJ5LCAwLjEyKTtcbiAgb3BhY2l0eTogMTtcbn1cblxuLmV4cGVyaW1lbnQtc2VsZWN0aW9uLWJ1dHRvbiB7XG4gIGRpc3BsYXk6YmxvY2s7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMTBweCBhdXRvO1xuICBwYWRkaW5nOiAxMHB4IDA7XG5cbiAgJiBzcGFuIHtcbiAgICBtYXJnaW46IDEwcHggMDtcbiAgfVxufVxuXG4uZmllbGRzZXQtcm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLmZpZWxkc2V0LWNvbHVtbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cblxuLnJvdy1pdGVtIHtcbiAgZmxleDogMSAwO1xuICBtYXJnaW46IDVweCAxMHB4O1xufVxuXG4uc2VjdGlvbiB7XG4gIG1hcmdpbi10b3A6IDQ1cHg7XG59XG5cbi5maXJzdC1zZWN0aW9uIHtcbiAgQGV4dGVuZCAuc2VjdGlvbjtcbiAgbWFyZ2luLXRvcDogMzBweDtcbn1cblxuLnNlY3Rpb24taGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIGhlaWdodDogYXV0bztcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNkZGRkZGQ7XG59XG5cbmZpbGUtdXBsb2FkZXIge1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uY2hlY2tib3gtLWxhYmVsIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nOiAxMXB4IDA7XG59IiwiJG1kYy10aGVtZS1wcmltYXJ5OiAjMDA1ZGI5O1xuJG1kYy10aGVtZS1zZWNvbmRhcnk6ICNkZGRkZGQ7XG4kbWRjLXRoZW1lLXNlY29uZGFyeS1saWdodDogI2Y1ZjVmNTtcbiRtZGMtdGhlbWUtYmFja2dyb3VuZDogI2ZmZjtcbiJdfQ== */"

/***/ }),

/***/ "./src/app/experimentcreationwizard/experimentcreationwizard.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/experimentcreationwizard.component.ts ***!
  \********************************************************************************/
/*! exports provided: ExperimentCreationWizardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExperimentCreationWizardComponent", function() { return ExperimentCreationWizardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _storage_experiment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../storage/experiment.service */ "./src/app/storage/experiment.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notifications/notifications.service */ "./src/app/notifications/notifications.service.ts");





var ExperimentCreationWizardComponent = /** @class */ (function () {
    function ExperimentCreationWizardComponent(experimentService, notificationsService, router) {
        // HTML content
        this.experimentTypes = [
            {
                name: "Pathway for a concrete organism",
                type: 1
            },
            {
                name: "Whole organism experiment",
                type: 2
            },
            {
                name: "Pathway comparison for organisms",
                type: 3
            },
            {
                name: "Experiment comparison",
                type: 4
            },
            {
                name: "Compare metabolisms for organisms",
                type: 5
            },
        ];
        this.breadcrumbs = [
            "Experiment type selection",
            "Experiment data filling"
        ];
        this.experimentService = experimentService;
        this.notificationsService = notificationsService;
        this.router = router;
    }
    ExperimentCreationWizardComponent.prototype.ngOnInit = function () {
        this.button = null;
        this.experimentType = 0;
        this.lastVisibleBreadcrumbIndex = 0;
    };
    /**
     * Update the styles of the selected button
     * @param event
     */
    ExperimentCreationWizardComponent.prototype.onClick = function (event) {
        if (this.button != undefined) {
            console.log("Unsetting button");
            this.button.classList.remove('experiment-selection-selected');
        }
        // Retrieve the dom element where the event handler was registered.
        // Do not get the element which fired the event as it could be another
        // element. For more information, research about html event bubbling
        this.button = event.currentTarget;
        this.button.classList.add('experiment-selection-selected');
        var value = this.button.getAttribute('data-index');
        this.experimentType = parseInt(value) || null;
        console.log(this, this.experimentType);
        if (this.experimentType) {
            this.lastVisibleBreadcrumbIndex++;
        }
        event.stopPropagation();
    };
    ExperimentCreationWizardComponent.prototype.isExperimentTypeSelected = function (type) {
        return this.experimentType === type;
    };
    ExperimentCreationWizardComponent.prototype.getVisibleBreadCrumbs = function () {
        var _this = this;
        return this.breadcrumbs.filter(function (value, index) { return index <= _this.lastVisibleBreadcrumbIndex; });
    };
    ExperimentCreationWizardComponent.prototype.onExperimentCreated = function () {
        // Show notification and navigate
        this.notificationsService.notify("The experiment has been successfully requested");
        this.router.navigate(["/experiments"]);
    };
    ExperimentCreationWizardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-experimentcreationwizard',
            template: __webpack_require__(/*! ./experimentcreationwizard.component.html */ "./src/app/experimentcreationwizard/experimentcreationwizard.component.html"),
            styles: [__webpack_require__(/*! ./experimentcreationwizard.component.scss */ "./src/app/experimentcreationwizard/experimentcreationwizard.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_storage_experiment_service__WEBPACK_IMPORTED_MODULE_2__["ExperimentService"], _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__["NotificationsService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ExperimentCreationWizardComponent);
    return ExperimentCreationWizardComponent;
}());



/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"organismComparisonForm\">\n\n    <!-- TODO Adapt form to show the form fields -->\n    <form [formGroup]=\"form\" (ngSubmit)=\"formSubmit()\">\n        <div class=\"first-section\">\n            <h2 class=\"section-header\">Experiment data</h2>\n            <div class=\"fieldset-row\">\n                <!-- Maybe organisms and pathway should be selectors -->\n                <mdc-text-field class=\"row-item\" label=\"Name\"\n                                formControlName=\"experimentName\">\n                </mdc-text-field>\n\n                <mdc-text-field class=\"row-item\" label=\"Pathway\"\n                                formControlName=\"pathwayCode\">\n                </mdc-text-field>\n            </div>\n            <div class=\"fieldset-row\">\n                <mdc-textarea class=\"row-item\" label=\"Description\"\n                              formControlName=\"experimentDescription\">\n                </mdc-textarea>\n            </div>\n        </div>\n        <div class=\"section\">\n            <h2 class=\"section-header\">Classification data</h2>\n            <div class=\"fieldset-column\">\n                <div class=\"fieldset-row\">\n\n                    <file-uploader\n                            class=\"row-item\"\n                            name=\"Groups\"\n                            helperText=\"Click in this area or drop a file to bind groups to this experiment\"\n                            (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                    ></file-uploader>\n                </div>\n            </div>\n        </div>\n        <button mdc-button raised\n                style=\"position: fixed; bottom: 5%; right: 2%;\"\n                aria-label=\"Submit experiment\"\n                type=\"submit\"\n                [disabled]=\"!form.valid\">\n            Submit\n        </button>\n    </form>\n</div>"

/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.scss":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.scss ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fieldset-row {\n  display: flex;\n  flex-direction: row;\n  margin-bottom: 15px; }\n\n.fieldset-column {\n  display: flex;\n  flex-direction: column; }\n\n.row-item {\n  flex: 1 0;\n  margin: 5px 10px; }\n\n.section, .first-section {\n  margin-top: 45px; }\n\n.first-section {\n  margin-top: 30px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\nfile-uploader {\n  margin-bottom: 15px; }\n\n.checkbox--label {\n  display: inline-block;\n  padding: 11px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9leHBlcmltZW50Y3JlYXRpb253aXphcmQvc3RlcHMtY29tbW9uLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWE7RUFDYixzQkFBc0IsRUFBQTs7QUFJeEI7RUFDRSxTQUFTO0VBQ1QsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usa0JBQWtCO0VBRWxCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFHbEM7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxxQkFBcUI7RUFDckIsZUFBZSxFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvZXhwZXJpbWVudGNyZWF0aW9ud2l6YXJkL29yZ2FuaXNtLWNvbXBhcmlzb24tZXhwZXJpbWVudC9vcmdhbmlzbS1jb21wYXJpc29uLWV4cGVyaW1lbnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vbWF0ZXJpYWx2YXJzXCI7XG5cbi5maWVsZHNldC1yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uZmllbGRzZXQtY29sdW1uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuXG4ucm93LWl0ZW0ge1xuICBmbGV4OiAxIDA7XG4gIG1hcmdpbjogNXB4IDEwcHg7XG59XG5cbi5zZWN0aW9uIHtcbiAgbWFyZ2luLXRvcDogNDVweDtcbn1cblxuLmZpcnN0LXNlY3Rpb24ge1xuICBAZXh0ZW5kIC5zZWN0aW9uO1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG4uc2VjdGlvbi1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2RkZGRkZDtcbn1cblxuZmlsZS11cGxvYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5jaGVja2JveC0tbGFiZWwge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDExcHggMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.ts":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.ts ***!
  \*********************************************************************************************************************/
/*! exports provided: OrganismComparisonExperimentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganismComparisonExperimentComponent", function() { return OrganismComparisonExperimentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../storage/experiment.service */ "./src/app/storage/experiment.service.ts");




var OrganismComparisonExperimentComponent = /** @class */ (function () {
    function OrganismComparisonExperimentComponent(experimentService) {
        this.onExperimentCreated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            experimentName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]),
            experimentDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            pathwayCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.experimentService = experimentService;
    }
    OrganismComparisonExperimentComponent.prototype.ngOnInit = function () {
    };
    OrganismComparisonExperimentComponent.prototype.formSubmit = function () {
        // let request = {
        //     'type': this.experimentType,
        //     'name': this.experimentName,
        //     'description': this.experimentDescription,
        //     'organisms': [this.organism],
        //     'pathway': [this.pathway],
        //
        //     // File like elements
        //     'groups': this.groups,
        //     'experiment_classification': this.experimentClassification
        // };
        var _this = this;
        var formValue = this.form.value;
        // TODO Adapt to the new call
        this.experimentService.createOrganismComparisonExperiment(formValue.experimentName, formValue.experimentDescription, formValue.organism).then(function (value) { _this.onExperimentCreated.emit(); });
    };
    OrganismComparisonExperimentComponent.prototype.onGroupFilesAdded = function (files) {
        this.groups = files;
    };
    OrganismComparisonExperimentComponent.prototype.onExperimentClassificationFilesAdded = function (files) {
        this.experimentClassification = files;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], OrganismComparisonExperimentComponent.prototype, "onExperimentCreated", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OrganismComparisonExperimentComponent.prototype, "form", void 0);
    OrganismComparisonExperimentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-organism-comparison-experiment',
            template: __webpack_require__(/*! ./organism-comparison-experiment.component.html */ "./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.html"),
            styles: [__webpack_require__(/*! ./organism-comparison-experiment.component.scss */ "./src/app/experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__["ExperimentService"]])
    ], OrganismComparisonExperimentComponent);
    return OrganismComparisonExperimentComponent;
}());



/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"experimentComparisonForm\">\n\n    <!-- TODO Adapt form to show the form fields -->\n    <form [formGroup]=\"form\" (ngSubmit)=\"formSubmit()\">\n        <div class=\"first-section\">\n            <h2 class=\"section-header\">Experiment data</h2>\n            <div class=\"fieldset-row\">\n                <mdc-text-field class=\"row-item\" label=\"Name\"\n                                formControlName=\"pathwayCode\">\n                </mdc-text-field>\n                <mdc-text-field class=\"row-item\" label=\"Categorization name\"\n                                formControlName=\"pathwayCode\">\n                </mdc-text-field>\n            </div>\n            <div class=\"fieldset-row\">\n                <mdc-textarea class=\"row-item\" label=\"Description\"\n                              formControlName=\"experimentDescription\">\n                </mdc-textarea>\n            </div>\n\n            <div class=\"section\">\n                <h2 class=\"section-header\">Experiment options </h2>\n                <div class=\"fieldset-row\">\n                    <div class=\"row-item\">\n                        <div class=\"row-item\">\n                            <mdc-checkbox\n                                    class=\"custom-checkbox\"></mdc-checkbox>\n                            <label class=\"checkbox--label\">\n                                Group by kingdom\n                            </label>\n                        </div>\n\n                        <div class=\"row-item\">\n                            <mdc-checkbox\n                                    class=\"custom-checkbox\"></mdc-checkbox>\n                            <label class=\"checkbox--label\">\n                                Group by phylum\n                            </label>\n                        </div>\n\n                        <div class=\"row-item\">\n                            <mdc-checkbox\n                                    class=\"custom-checkbox\"></mdc-checkbox>\n                            <label class=\"checkbox--label\">\n                                Group by class\n                            </label>\n                        </div>\n\n                    </div>\n\n                    <div class=\"row-item\">\n                        <div class=\"row-item\">\n                            <mdc-checkbox\n                                    class=\"custom-checkbox\"></mdc-checkbox>\n                            <label class=\"checkbox--label\">\n                                Calculate block distances\n                            </label>\n                        </div>\n\n                        <div class=\"row-item\">\n                            <mdc-checkbox\n                                    class=\"custom-checkbox\"></mdc-checkbox>\n                            <label class=\"checkbox--label\">\n                                Measure block similarities\n                            </label>\n                        </div>\n\n                    </div>\n                    <div class=\"row-item\">\n                        <div class=\"row-item\">\n                            <mdc-checkbox\n                                    class=\"custom-checkbox\"></mdc-checkbox>\n                            <label class=\"checkbox--label\">\n                                Use all the species pathways\n                            </label>\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n\n            <div class=\"section\">\n                <h2 class=\"section-header\">Organisms data</h2>\n                <div class=\"fieldset-row\">\n                    <file-uploader\n                            class=\"row-item\"\n                            name=\"Organisms file\"\n                            helperText=\"Click in this area or drop files to load it\"\n                            (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                    ></file-uploader>\n                    <file-uploader\n                            class=\"row-item\"\n                            name=\"Organism grouping\"\n                            helperText=\"Click in this area or drop files to load a file\"\n                            (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                    ></file-uploader>\n                </div>\n            </div>\n\n            <div class=\"section\">\n                <h2 class=\"section-header\">Additional data</h2>\n                <div class=\"fieldset-row\">\n                    <file-uploader\n                            class=\"row-item\"\n                            name=\"Exceptions list\"\n                            helperText=\"Click in this area or drop files to load an exceptions list\"\n                            (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                    ></file-uploader>\n                    <file-uploader\n                            class=\"row-item\"\n                            name=\"Genes file\"\n                            helperText=\"Click in this area or drop a file to bind groups to this experiment\"\n                            (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                    ></file-uploader>\n                </div>\n            </div>\n        </div>\n        <!--        <div class=\"section\">-->\n        <!--            <h2 class=\"section-header\">Experiment classification data</h2>-->\n        <!--            <div class=\"fieldset-column\">-->\n        <!--                <div class=\"fieldset-row\">-->\n\n        <!--                    <file-uploader-->\n        <!--                            class=\"row-item\"-->\n        <!--                            name=\"Organism Classification\"-->\n        <!--                            helperText=\"Click in this area or drop a file to classify the organism\"-->\n        <!--                            (onFileLoaded)=\"onExperimentClassificationFilesAdded($event)\"-->\n        <!--                    ></file-uploader>-->\n        <!--                </div>-->\n        <!--            </div>-->\n        <!--        </div>-->\n        <button mdc-button raised\n                style=\"position: fixed; bottom: 5%; right: 2%;\"\n                aria-label=\"Submit experiment\"\n                type=\"submit\"\n                [disabled]=\"!form.valid\">\n            Submit\n        </button>\n    </form>\n</div>"

/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.scss":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.scss ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fieldset-row {\n  display: flex;\n  flex-direction: row;\n  margin-bottom: 15px; }\n\n.fieldset-column {\n  display: flex;\n  flex-direction: column; }\n\n.row-item {\n  flex: 1 0;\n  margin: 5px 10px; }\n\n.section, .first-section {\n  margin-top: 45px; }\n\n.first-section {\n  margin-top: 30px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\nfile-uploader {\n  margin-bottom: 15px; }\n\n.checkbox--label {\n  display: inline-block;\n  padding: 11px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9leHBlcmltZW50Y3JlYXRpb253aXphcmQvc3RlcHMtY29tbW9uLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWE7RUFDYixzQkFBc0IsRUFBQTs7QUFJeEI7RUFDRSxTQUFTO0VBQ1QsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usa0JBQWtCO0VBRWxCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFHbEM7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxxQkFBcUI7RUFDckIsZUFBZSxFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvZXhwZXJpbWVudGNyZWF0aW9ud2l6YXJkL29yZ2FuaXNtLW1ldGFib2xpc20tY29tcGFyaXNvbi9vcmdhbmlzbS1tZXRhYm9saXNtLWNvbXBhcmlzb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vbWF0ZXJpYWx2YXJzXCI7XG5cbi5maWVsZHNldC1yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uZmllbGRzZXQtY29sdW1uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuXG4ucm93LWl0ZW0ge1xuICBmbGV4OiAxIDA7XG4gIG1hcmdpbjogNXB4IDEwcHg7XG59XG5cbi5zZWN0aW9uIHtcbiAgbWFyZ2luLXRvcDogNDVweDtcbn1cblxuLmZpcnN0LXNlY3Rpb24ge1xuICBAZXh0ZW5kIC5zZWN0aW9uO1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG4uc2VjdGlvbi1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2RkZGRkZDtcbn1cblxuZmlsZS11cGxvYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5jaGVja2JveC0tbGFiZWwge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDExcHggMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.ts":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.ts ***!
  \*********************************************************************************************************************/
/*! exports provided: OrganismMetabolismComparisonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganismMetabolismComparisonComponent", function() { return OrganismMetabolismComparisonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../storage/experiment.service */ "./src/app/storage/experiment.service.ts");




// TODO Adaptar entero, seguramente no tiene los campos que necesita para
//  hacer la comparacion
var OrganismMetabolismComparisonComponent = /** @class */ (function () {
    function OrganismMetabolismComparisonComponent(experimentService) {
        this.onExperimentCreated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            experimentName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]),
            experimentDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            pathwayCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.experimentService = experimentService;
    }
    OrganismMetabolismComparisonComponent.prototype.ngOnInit = function () {
    };
    OrganismMetabolismComparisonComponent.prototype.formSubmit = function () {
        // let request = {
        //     'type': this.experimentType,
        //     'name': this.experimentName,
        //     'description': this.experimentDescription,
        //     'organisms': [this.organism],
        //     'pathway': [this.pathway],
        //
        //     // File like elements
        //     'groups': this.groups,
        //     'experiment_classification': this.experimentClassification
        // };
        var _this = this;
        var formValue = this.form.value;
        // TODO Adapt to the new call
        this.experimentService.createOrganismMetabolismComparison(formValue.experimentName, formValue.experimentDescription, formValue.organism).then(function (value) { _this.onExperimentCreated.emit(); });
    };
    OrganismMetabolismComparisonComponent.prototype.onGroupFilesAdded = function (files) {
        this.groups = files;
    };
    OrganismMetabolismComparisonComponent.prototype.onExperimentClassificationFilesAdded = function (files) {
        this.experimentClassification = files;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], OrganismMetabolismComparisonComponent.prototype, "onExperimentCreated", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OrganismMetabolismComparisonComponent.prototype, "form", void 0);
    OrganismMetabolismComparisonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-organism-metabolism-comparison',
            template: __webpack_require__(/*! ./organism-metabolism-comparison.component.html */ "./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.html"),
            styles: [__webpack_require__(/*! ./organism-metabolism-comparison.component.scss */ "./src/app/experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__["ExperimentService"]])
    ], OrganismMetabolismComparisonComponent);
    return OrganismMetabolismComparisonComponent;
}());



/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"simpleExperimentForm\">\n                <form [formGroup]=\"form\" (ngSubmit)=\"formSubmit()\">\n                    <div class=\"first-section\">\n                        <h2 class=\"section-header\">Experiment data</h2>\n                        <div class=\"fieldset-row\">\n                            <!-- Maybe organisms and pathway should be selectors -->\n                            <mdc-text-field class=\"row-item\" label=\"Name\"\n                                            formControlName=\"experimentName\">\n                            </mdc-text-field>\n                            <mdc-text-field class=\"row-item\" label=\"Organism\"\n                                            formControlName=\"organism\">\n                            </mdc-text-field>\n                            <mdc-text-field class=\"row-item\" label=\"Pathway\"\n                                            formControlName=\"pathway\">\n                            </mdc-text-field>\n                        </div>\n                        <div class=\"fieldset-row\">\n                            <!-- Maybe organisms and pathway should be selectors -->\n                            <mdc-textarea class=\"row-item\" label=\"Description\"\n                                          formControlName=\"experimentDescription\">\n                            </mdc-textarea>\n                        </div>\n                    </div>\n                    <div class=\"section\">\n                        <h2 class=\"section-header\">Classification data</h2>\n                        <div class=\"fieldset-column\">\n                            <div class=\"fieldset-row\">\n\n                                <file-uploader\n                                        class=\"row-item\"\n                                        name=\"Groups\"\n                                        helperText=\"Click in this area or drop a file to bind groups to this experiment\"\n                                        (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                                ></file-uploader>\n                            </div>\n                            <div class=\"fieldset-row\">\n\n                                <file-uploader\n                                        class=\"row-item\"\n                                        name=\"Organism Classification\"\n                                        helperText=\"Click in this area or drop a file to classify the organism\"\n                                        (onFileLoaded)=\"onExperimentClassificationFilesAdded($event)\"\n                                ></file-uploader>\n                            </div>\n                        </div>\n                    </div>\n                    <button mdc-button raised\n                            style=\"position: fixed; bottom: 5%; right: 2%;\"\n                            aria-label=\"Submit experiment\"\n                            type=\"submit\"\n                            [disabled]=\"!form.valid\">\n                        Submit\n                    </button>\n                </form>\n            </div>"

/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.scss":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.scss ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fieldset-row {\n  display: flex;\n  flex-direction: row;\n  margin-bottom: 15px; }\n\n.fieldset-column {\n  display: flex;\n  flex-direction: column; }\n\n.row-item {\n  flex: 1 0;\n  margin: 5px 10px; }\n\n.section, .first-section {\n  margin-top: 45px; }\n\n.first-section {\n  margin-top: 30px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\nfile-uploader {\n  margin-bottom: 15px; }\n\n.checkbox--label {\n  display: inline-block;\n  padding: 11px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9leHBlcmltZW50Y3JlYXRpb253aXphcmQvc3RlcHMtY29tbW9uLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWE7RUFDYixzQkFBc0IsRUFBQTs7QUFJeEI7RUFDRSxTQUFTO0VBQ1QsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usa0JBQWtCO0VBRWxCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFHbEM7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxxQkFBcUI7RUFDckIsZUFBZSxFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvZXhwZXJpbWVudGNyZWF0aW9ud2l6YXJkL29yZ2FuaXNtLXBhdGh3YXktY2FsY3VsYXRpb24vb3JnYW5pc20tcGF0aHdheS1jYWxjdWxhdGlvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi9tYXRlcmlhbHZhcnNcIjtcblxuLmZpZWxkc2V0LXJvdyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5maWVsZHNldC1jb2x1bW4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG5cbi5yb3ctaXRlbSB7XG4gIGZsZXg6IDEgMDtcbiAgbWFyZ2luOiA1cHggMTBweDtcbn1cblxuLnNlY3Rpb24ge1xuICBtYXJnaW4tdG9wOiA0NXB4O1xufVxuXG4uZmlyc3Qtc2VjdGlvbiB7XG4gIEBleHRlbmQgLnNlY3Rpb247XG4gIG1hcmdpbi10b3A6IDMwcHg7XG59XG5cbi5zZWN0aW9uLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBoZWlnaHQ6IGF1dG87XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjZGRkZGRkO1xufVxuXG5maWxlLXVwbG9hZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLmNoZWNrYm94LS1sYWJlbCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogMTFweCAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: OrganismPathwayCalculationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganismPathwayCalculationComponent", function() { return OrganismPathwayCalculationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../storage/experiment.service */ "./src/app/storage/experiment.service.ts");




var OrganismPathwayCalculationComponent = /** @class */ (function () {
    function OrganismPathwayCalculationComponent(experimentService) {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            experimentName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]),
            experimentDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            organism: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            pathway: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.experimentService = experimentService;
    }
    OrganismPathwayCalculationComponent.prototype.ngOnInit = function () {
    };
    OrganismPathwayCalculationComponent.prototype.formSubmit = function () {
        // let request = {
        //     'type': this.experimentType,
        //     'name': this.experimentName,
        //     'description': this.experimentDescription,
        //     'organisms': [this.organism],
        //     'pathway': [this.pathway],
        //
        //     // File like elements
        //     'groups': this.groups,
        //     'experiment_classification': this.experimentClassification
        // };
        var formValue = this.form.value;
        this.experimentService.createWholeOrganismExperiment(formValue.experimentName, formValue.experimentDescription, formValue.organism);
    };
    OrganismPathwayCalculationComponent.prototype.onGroupFilesAdded = function (files) {
        this.groups = files;
    };
    OrganismPathwayCalculationComponent.prototype.onExperimentClassificationFilesAdded = function (files) {
        this.experimentClassification = files;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OrganismPathwayCalculationComponent.prototype, "form", void 0);
    OrganismPathwayCalculationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-organism-pathway-calculation',
            template: __webpack_require__(/*! ./organism-pathway-calculation.component.html */ "./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.html"),
            styles: [__webpack_require__(/*! ./organism-pathway-calculation.component.scss */ "./src/app/experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__["ExperimentService"]])
    ], OrganismPathwayCalculationComponent);
    return OrganismPathwayCalculationComponent;
}());



/***/ }),

/***/ "./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wholeOrganismExperimentForm\">\n                <form [formGroup]=\"form\" (ngSubmit)=\"formSubmit()\">\n                    <div class=\"first-section\">\n                        <h2 class=\"section-header\">Experiment data</h2>\n                        <div class=\"fieldset-row\">\n                            <!-- Maybe organisms and pathway should be selectors -->\n                            <mdc-text-field class=\"row-item\" label=\"Name\"\n                                            formControlName=\"experimentName\">\n                            </mdc-text-field>\n                            <mdc-text-field class=\"row-item\" label=\"Organism\"\n                                            formControlName=\"organism\">\n                            </mdc-text-field>\n                        </div>\n                        <div>\n                            <mdc-checkbox class=\"custom-checkbox\"></mdc-checkbox>\n                            <label class=\"checkbox--label\">\n                                Create .DOT formatted graphs\n                            </label>\n                        </div>\n                        <div class=\"fieldset-row\">\n                            <mdc-textarea class=\"row-item\" label=\"Description\"\n                                          formControlName=\"experimentDescription\">\n                            </mdc-textarea>\n                        </div>\n                    </div>\n                    <div class=\"section\">\n                        <h2 class=\"section-header\">Classification data</h2>\n                        <div class=\"fieldset-column\">\n                            <div class=\"fieldset-row\">\n\n                                <file-uploader\n                                        class=\"row-item\"\n                                        name=\"Groups\"\n                                        helperText=\"Click in this area or drop a file to bind groups to this experiment\"\n                                        [multiple]=\"false\"\n                                        (onFileLoaded)=\"onGroupFilesAdded($event)\"\n                                ></file-uploader>\n                            </div>\n                            <div class=\"fieldset-row\">\n\n                                <file-uploader\n                                        class=\"row-item\"\n                                        name=\"Organism Classification\"\n                                        helperText=\"Click in this area or drop a file to classify the organism\"\n                                        (onFileLoaded)=\"onExperimentClassificationFilesAdded($event)\"\n                                ></file-uploader>\n                            </div>\n                        </div>\n                    </div>\n                    <button mdc-button raised style=\"position: fixed; bottom: 5%; right: 2%;\"\n                            aria-label=\"Submit experiment\"\n                            type=\"submit\"\n                            [disabled]=\"!form.valid\">\n                        Submit\n                    </button>\n                </form>\n            </div>\n"

/***/ }),

/***/ "./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.scss":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.scss ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fieldset-row {\n  display: flex;\n  flex-direction: row;\n  margin-bottom: 15px; }\n\n.fieldset-column {\n  display: flex;\n  flex-direction: column; }\n\n.row-item {\n  flex: 1 0;\n  margin: 5px 10px; }\n\n.section, .first-section {\n  margin-top: 45px; }\n\n.first-section {\n  margin-top: 30px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\nfile-uploader {\n  margin-bottom: 15px; }\n\n.checkbox--label {\n  display: inline-block;\n  padding: 11px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9leHBlcmltZW50Y3JlYXRpb253aXphcmQvc3RlcHMtY29tbW9uLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWE7RUFDYixzQkFBc0IsRUFBQTs7QUFJeEI7RUFDRSxTQUFTO0VBQ1QsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usa0JBQWtCO0VBRWxCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFHbEM7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxxQkFBcUI7RUFDckIsZUFBZSxFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvZXhwZXJpbWVudGNyZWF0aW9ud2l6YXJkL3dob2xlb3JnYW5pbXNleHBlcmltZW50Zm9ybS93aG9sZW9yZ2FuaW1zZXhwZXJpbWVudGZvcm0uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vbWF0ZXJpYWx2YXJzXCI7XG5cbi5maWVsZHNldC1yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uZmllbGRzZXQtY29sdW1uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuXG4ucm93LWl0ZW0ge1xuICBmbGV4OiAxIDA7XG4gIG1hcmdpbjogNXB4IDEwcHg7XG59XG5cbi5zZWN0aW9uIHtcbiAgbWFyZ2luLXRvcDogNDVweDtcbn1cblxuLmZpcnN0LXNlY3Rpb24ge1xuICBAZXh0ZW5kIC5zZWN0aW9uO1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG4uc2VjdGlvbi1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgaGVpZ2h0OiBhdXRvO1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2RkZGRkZDtcbn1cblxuZmlsZS11cGxvYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5jaGVja2JveC0tbGFiZWwge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDExcHggMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: WholeOrganimsExperimentFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WholeOrganimsExperimentFormComponent", function() { return WholeOrganimsExperimentFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../storage/experiment.service */ "./src/app/storage/experiment.service.ts");




var WholeOrganimsExperimentFormComponent = /** @class */ (function () {
    function WholeOrganimsExperimentFormComponent(experimentService) {
        this.onExperimentCreated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            experimentName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]),
            experimentDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            organism: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.experimentService = experimentService;
    }
    WholeOrganimsExperimentFormComponent.prototype.ngOnInit = function () {
    };
    WholeOrganimsExperimentFormComponent.prototype.formSubmit = function () {
        var _this = this;
        var formValue = this.form.value;
        this.experimentService.createWholeOrganismExperiment(formValue.experimentName, formValue.experimentDescription, formValue.organism, this.groups).then(function (value) { _this.onExperimentCreated.emit(); });
    };
    WholeOrganimsExperimentFormComponent.prototype.onGroupFilesAdded = function (files) {
        this.groups = files;
    };
    WholeOrganimsExperimentFormComponent.prototype.onExperimentClassificationFilesAdded = function (files) {
        this.experimentClassification = files;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], WholeOrganimsExperimentFormComponent.prototype, "onExperimentCreated", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WholeOrganimsExperimentFormComponent.prototype, "form", void 0);
    WholeOrganimsExperimentFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wholeorganimsexperimentform',
            template: __webpack_require__(/*! ./wholeorganimsexperimentform.component.html */ "./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.html"),
            styles: [__webpack_require__(/*! ./wholeorganimsexperimentform.component.scss */ "./src/app/experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_storage_experiment_service__WEBPACK_IMPORTED_MODULE_3__["ExperimentService"]])
    ], WholeOrganimsExperimentFormComponent);
    return WholeOrganimsExperimentFormComponent;
}());



/***/ }),

/***/ "./src/app/fileitem/fileitem.component.html":
/*!**************************************************!*\
  !*** ./src/app/fileitem/fileitem.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"item\">\n    <img class=\"item-icon\" [attr.src]=\"iconUrl\" [attr.alt]=\"file.name\">\n    <span class=\"item-text\">{{ file.name }}</span>\n</div>"

/***/ }),

/***/ "./src/app/fileitem/fileitem.component.scss":
/*!**************************************************!*\
  !*** ./src/app/fileitem/fileitem.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item {\n  text-align: center;\n  margin: 10px 15px;\n  align-self: center;\n  display: inline-block; }\n\n.item-text {\n  display: block;\n  max-width: 120px;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden; }\n\n.file-options {\n  list-style-type: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0;\n  border-top: solid 1px black;\n  padding-top: 5px;\n  line-height: 1.5em; }\n\n.file-options li:hover {\n    text-shadow: 0 0 1px black;\n    cursor: pointer;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9maWxlaXRlbS9maWxlaXRlbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLGNBQWM7RUFDZCxnQkFBZ0I7RUFHaEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxxQkFBcUI7RUFDckIsd0JBQXVCO1VBQXZCLHVCQUF1QjtFQUV2QiwyQkFBMkI7RUFDM0IsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQU5wQjtJQVVNLDBCQUEwQjtJQUMxQixlQUFlO0lBQ2YseUJBQWlCO09BQWpCLHNCQUFpQjtRQUFqQixxQkFBaUI7WUFBakIsaUJBQWlCLEVBQUEiLCJmaWxlIjoiLi4vc3JjL2FwcC9maWxlaXRlbS9maWxlaXRlbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pdGVtIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDEwcHggMTVweDtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5pdGVtLXRleHQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWF4LXdpZHRoOiAxMjBweDtcblxuICAvLyBLZWVwIHRoZSB0ZXh0IHVuZGVyIGNvbnRyb2wuXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uZmlsZS1vcHRpb25zIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICBwYWRkaW5nLWlubGluZS1zdGFydDogMDtcblxuICBib3JkZXItdG9wOiBzb2xpZCAxcHggYmxhY2s7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjVlbTtcblxuICAmIGxpIHtcbiAgICAmOmhvdmVyIHtcbiAgICAgIHRleHQtc2hhZG93OiAwIDAgMXB4IGJsYWNrO1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgfVxuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/fileitem/fileitem.component.ts":
/*!************************************************!*\
  !*** ./src/app/fileitem/fileitem.component.ts ***!
  \************************************************/
/*! exports provided: FileItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileItemComponent", function() { return FileItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _filetypeanalyzer_filetypeanalyzer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../filetypeanalyzer/filetypeanalyzer.service */ "./src/app/filetypeanalyzer/filetypeanalyzer.service.ts");



var FileItemComponent = /** @class */ (function () {
    function FileItemComponent(fileTypeAnalyzer) {
        this.file = null;
        this.iconUrl = null;
        this.fileTypeAnalyzer = null;
        this.fileTypeAnalyzer = fileTypeAnalyzer;
    }
    FileItemComponent.prototype.ngOnInit = function () {
        this.iconUrl = this.fileTypeAnalyzer.getIcon(this.file['name']);
        console.log(this.file);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FileItemComponent.prototype, "file", void 0);
    FileItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fileitem',
            template: __webpack_require__(/*! ./fileitem.component.html */ "./src/app/fileitem/fileitem.component.html"),
            styles: [__webpack_require__(/*! ./fileitem.component.scss */ "./src/app/fileitem/fileitem.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_filetypeanalyzer_filetypeanalyzer_service__WEBPACK_IMPORTED_MODULE_2__["FileTypeAnalyzer"]])
    ], FileItemComponent);
    return FileItemComponent;
}());



/***/ }),

/***/ "./src/app/filetypeanalyzer/filetypeanalyzer.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/filetypeanalyzer/filetypeanalyzer.service.ts ***!
  \**************************************************************/
/*! exports provided: FileTypeAnalyzer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileTypeAnalyzer", function() { return FileTypeAnalyzer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FileTypeAnalyzer = /** @class */ (function () {
    function FileTypeAnalyzer() {
        this.fileType = [
            "csv",
            "pdf",
            "unknown",
        ];
    }
    FileTypeAnalyzer.prototype.getFileType = function (fileName) {
        var splitFileName = fileName.split(".");
        var extension = splitFileName[splitFileName.length - 1].toLowerCase();
        for (var _i = 0, _a = this.fileType; _i < _a.length; _i++) {
            var fileType = _a[_i];
            if (extension == fileType) {
                return fileType;
            }
        }
        return this.fileType[this.fileType.length - 1];
    };
    FileTypeAnalyzer.prototype.getIcon = function (fileName) {
        return "static/img/" + this.getFileType(fileName) + "32.png";
    };
    FileTypeAnalyzer = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        })
    ], FileTypeAnalyzer);
    return FileTypeAnalyzer;
}());



/***/ }),

/***/ "./src/app/fileuploader/fileuploader.component.html":
/*!**********************************************************!*\
  !*** ./src/app/fileuploader/fileuploader.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"drag-and-drop-zone\" #dropzone (click)=\"onClick($event)\">\n  <!-- Drag and drop file selector -->\n  <label class=\"drag-and-drop-title\" *ngIf=\"name\"> {{ name }}</label>\n  <ul class=\"file-list\">\n    <li class=\"file-list-item\" *ngFor=\"let file of getFiles()\">\n      {{ file.name }}\n    </li>\n  </ul>\n  <div class=\"help-text-area\">\n    <div>\n      <i class=\"material-icons icon center\">add_circle_outlined</i>\n    </div>\n    <span class=\"help-text\">\n      {{ helperText }}\n    </span>\n  </div>\n\n  <!-- Traditional file selector -->\n  <input #fileButton type=\"file\" name=\"files\" class=\"fileinput\" \n    (change)=\"onFileSelected($event)\">\n</div>"

/***/ }),

/***/ "./src/app/fileuploader/fileuploader.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/fileuploader/fileuploader.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".drag-and-drop-zone {\n  margin-top: 10px;\n  border: 2px dashed #dddddd;\n  position: relative;\n  min-height: 110px;\n  min-width: 300px; }\n\n.drag-and-drop-title {\n  color: #005db9;\n  font-size: 1.2rem;\n  position: absolute;\n  margin-top: -12px;\n  left: 1%;\n  background-color: white; }\n\n.help-text-area {\n  z-index: -1;\n  color: #dddddd;\n  top: 50%;\n  transform: translateY(-50%);\n  position: absolute;\n  display: flex;\n  flex-direction: column;\n  text-align: center;\n  width: 100%; }\n\n.help-text {\n  font-weight: 700;\n  font-size: 1.5rem;\n  padding-top: 20px;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap; }\n\n.icon {\n  font-size: 48px;\n  width: 48px;\n  height: auto; }\n\n.file-list {\n  list-style: none;\n  display: flex;\n  flex-wrap: wrap;\n  padding: 0 5px; }\n\n.file-list-item {\n  margin: 0 3px;\n  padding: 2px;\n  background-color: white;\n  border: 1px solid #dddddd;\n  border-radius: 8px; }\n\n.fileinput {\n  width: 0.1px;\n  height: 0.1px;\n  opacity: 0;\n  overflow: hidden;\n  position: absolute;\n  z-index: -1; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9maWxldXBsb2FkZXIvZmlsZXVwbG9hZGVyLmNvbXBvbmVudC5zY3NzIiwiL2hvbWUvamVzdXMvRG9jdW1lbnRzL3JlcG9zaXRvcmllcy90ZmcvbWRhZy9wcm9qZWN0L21EYWdGcm9udC9zcmMvX21hdGVyaWFsdmFycy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLDBCQ0gyQjtFREkzQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGNDWHlCO0VEWXpCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFFBQVE7RUFDUix1QkFBdUIsRUFBQTs7QUFHekI7RUFDRSxXQUFXO0VBQ1gsY0NwQjJCO0VEcUIzQixRQUFRO0VBQ1IsMkJBQTJCO0VBQzNCLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQix1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQUdoQjtFQUVFLGFBQWE7RUFDYixZQUFZO0VBRVosdUJBQXVCO0VBQ3ZCLHlCQzFEMkI7RUQ0RDNCLGtCQUFrQixFQUFBOztBQUlwQjtFQUNDLFlBQVk7RUFDWixhQUFhO0VBQ2IsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsV0FBVyxFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvZmlsZXVwbG9hZGVyL2ZpbGV1cGxvYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi9tYXRlcmlhbHZhcnNcIjtcblxuLmRyYWctYW5kLWRyb3Atem9uZSB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGJvcmRlcjogMnB4IGRhc2hlZCAkbWRjLXRoZW1lLXNlY29uZGFyeTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiAxMTBweDtcbiAgbWluLXdpZHRoOiAzMDBweDtcbn1cblxuLmRyYWctYW5kLWRyb3AtdGl0bGUge1xuICBjb2xvcjogJG1kYy10aGVtZS1wcmltYXJ5O1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtMTJweDtcbiAgbGVmdDogMSU7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4uaGVscC10ZXh0LWFyZWEge1xuICB6LWluZGV4OiAtMTsgLy8gRm9yY2UgdG8ga2VlcCBpdCBvbiB0aGUgYmFja2dyb3VuZFxuICBjb2xvcjogJG1kYy10aGVtZS1zZWNvbmRhcnk7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmhlbHAtdGV4dCB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbi5pY29uIHtcbiAgZm9udC1zaXplOiA0OHB4O1xuICB3aWR0aDogNDhweDtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuXG4uZmlsZS1saXN0IHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBwYWRkaW5nOiAwIDVweDtcbn1cblxuLmZpbGUtbGlzdC1pdGVtIHtcblxuICBtYXJnaW46IDAgM3B4O1xuICBwYWRkaW5nOiAycHg7XG5cbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICRtZGMtdGhlbWUtc2Vjb25kYXJ5O1xuXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cblxuLy8gVHJhZGl0aW9uYWwgZmlsZSB1cGxvYWRlclxuLmZpbGVpbnB1dCB7XG5cdHdpZHRoOiAwLjFweDtcblx0aGVpZ2h0OiAwLjFweDtcblx0b3BhY2l0eTogMDtcblx0b3ZlcmZsb3c6IGhpZGRlbjtcblx0cG9zaXRpb246IGFic29sdXRlO1xuXHR6LWluZGV4OiAtMTtcbn0iLCIkbWRjLXRoZW1lLXByaW1hcnk6ICMwMDVkYjk7XG4kbWRjLXRoZW1lLXNlY29uZGFyeTogI2RkZGRkZDtcbiRtZGMtdGhlbWUtc2Vjb25kYXJ5LWxpZ2h0OiAjZjVmNWY1O1xuJG1kYy10aGVtZS1iYWNrZ3JvdW5kOiAjZmZmO1xuIl19 */"

/***/ }),

/***/ "./src/app/fileuploader/fileuploader.component.ts":
/*!********************************************************!*\
  !*** ./src/app/fileuploader/fileuploader.component.ts ***!
  \********************************************************/
/*! exports provided: FileUploaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploaderComponent", function() { return FileUploaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _browsefeaturechecker_browser_feature_checker_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../browsefeaturechecker/browser-feature-checker.service */ "./src/app/browsefeaturechecker/browser-feature-checker.service.ts");



var FileUploaderComponent = /** @class */ (function () {
    function FileUploaderComponent(bfcs) {
        this.onFileLoaded = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.enableDragAndDropUpload = bfcs.doesSupportDragAndDropFileUpload();
        // Defaults
        this.multiple = false;
    }
    FileUploaderComponent.prototype.ngOnInit = function () {
        this.files = [];
        // Ensure default texts
        this.helperText = (this.helperText
            || "Click in this area or drop a file to upload its content");
        if (this.enableDragAndDropUpload) {
            // Initialize listeners for all the drag & drop events
            var domElement = this.dragAndDropZone.nativeElement;
            domElement.addEventListener('drop', this.onDroppedFiles.bind(this));
            this.addEventListeners(domElement, 'drag dragstart dragend dragover dragenter dragleave drop', this.avoidEventDefaultBehavior.bind(this));
        }
        this.fileButton.nativeElement.multiple = this.multiple;
    };
    FileUploaderComponent.prototype.onClick = function () {
        this.fileButton.nativeElement.click();
    };
    FileUploaderComponent.prototype.isAdvancedFileUploadEnabled = function () {
        return this.enableDragAndDropUpload;
    };
    FileUploaderComponent.prototype.onDroppedFiles = function (event) {
        var files = event.dataTransfer.files;
        if (!this.multiple) {
            files = [files[0]];
        }
        this.addSelectedFiles(files);
    };
    FileUploaderComponent.prototype.onFileSelected = function (event) {
        this.addSelectedFiles(this.fileButton.nativeElement.files);
    };
    FileUploaderComponent.prototype.getFiles = function () {
        return this.files;
    };
    FileUploaderComponent.prototype.avoidEventDefaultBehavior = function (event) {
        event.preventDefault();
        event.stopPropagation();
    };
    // This is just a helper to avoid multiplicating the lines in the event
    // binding stage
    FileUploaderComponent.prototype.addEventListeners = function (element, eventNames, listener) {
        var events = eventNames.split(' ');
        for (var i = 0, iLen = events.length; i < iLen; i++) {
            element.addEventListener(events[i], listener, false);
        }
    };
    FileUploaderComponent.prototype.addSelectedFiles = function (files) {
        if (!this.multiple) {
            this.files = [];
        }
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            this.files.push(file);
        }
        this.onFileLoaded.emit(this.files);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], FileUploaderComponent.prototype, "name", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], FileUploaderComponent.prototype, "helperText", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], FileUploaderComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], FileUploaderComponent.prototype, "onFileLoaded", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("dropzone"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], FileUploaderComponent.prototype, "dragAndDropZone", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("fileButton"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], FileUploaderComponent.prototype, "fileButton", void 0);
    FileUploaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'file-uploader',
            template: __webpack_require__(/*! ./fileuploader.component.html */ "./src/app/fileuploader/fileuploader.component.html"),
            styles: [__webpack_require__(/*! ./fileuploader.component.scss */ "./src/app/fileuploader/fileuploader.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_browsefeaturechecker_browser_feature_checker_service__WEBPACK_IMPORTED_MODULE_2__["BrowserFeatureCheckerService"]])
    ], FileUploaderComponent);
    return FileUploaderComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"footer\">\n    <hr>\n    <span class=\"footer-text\">\n        This work has been partially supported by the Spanish government through project <b>DPI2015-67082-P</b>, and the Programa Pont La Caixa per a grups de recerca de la UIB.\n    </span>\n</div>\n"

/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#footer {\n  bottom: 0;\n  text-align: center; }\n\nhr {\n  width: 98%; }\n\n.footer-text {\n  align-self: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsU0FBUztFQUNULGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLFVBQVUsRUFBQTs7QUFHWjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuI2Zvb3RlciB7XG4gIGJvdHRvbTogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5ociB7XG4gIHdpZHRoOiA5OCU7XG59XG5cbi5mb290ZXItdGV4dCB7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/global-services/cookie-service.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/global-services/cookie-service.service.ts ***!
  \***********************************************************/
/*! exports provided: CookieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");



var CookieService = /** @class */ (function () {
    function CookieService(baseHref) {
        this.baseHref = baseHref;
    }
    CookieService_1 = CookieService;
    /**
     *
     * @param key
     * @param value
     * @param duration Time in milliseconds that the cookie should be set
     */
    CookieService.prototype.set = function (key, value, duration, path) {
        var cookieString = '' + key + '=' + value + ';samesite=lax;';
        if (duration) {
            var stringTime = CookieService_1.getExpireTime(1800000).toUTCString();
            cookieString += 'expires=' + stringTime + ';';
        }
        var _path = path ? path : this.baseHref;
        cookieString += "path=" + _path + ";";
        document.cookie = cookieString;
    };
    CookieService.prototype.remove = function (key) {
        var cookieString = key + ("=; Path=" + this.baseHref + "; Expires=Thu, 01 Jan 1970 00:00:01 GMT;");
        document.cookie = cookieString;
    };
    CookieService.prototype.get = function (key) {
        var targetCookie = CookieService_1.getCookie(key);
        var value = undefined;
        if (targetCookie) {
            value = targetCookie.split("=")[1];
        }
        return value;
    };
    CookieService.prototype.check = function (key) {
        var targetCookie = CookieService_1.getCookie(key);
        return !!targetCookie;
    };
    CookieService.getCookie = function (key) {
        var decodedCookie = decodeURIComponent(document.cookie);
        var cookies = decodedCookie.split(";");
        var cname = key + "=";
        var idx = cookies.length - 1;
        var targetCookie = null;
        while (idx >= 0 && !targetCookie) {
            var cookie = cookies[idx].trim();
            if (cookie.indexOf(cname) == 0) {
                targetCookie = cookie;
            }
            idx--;
        }
        return targetCookie;
    };
    CookieService.getExpireTime = function (milliseconds) {
        var d = new Date();
        var time = d.getTime() + milliseconds;
        d.setTime(time);
        return d;
    };
    var CookieService_1;
    CookieService = CookieService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_2__["APP_BASE_HREF"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String])
    ], CookieService);
    return CookieService;
}());



/***/ }),

/***/ "./src/app/group-details/group-details.component.html":
/*!************************************************************!*\
  !*** ./src/app/group-details/group-details.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"details-container\">\n    <form [formGroup]=\"groupForm\">\n        <div class=\"detail-header\">\n            <div class=\"text-wrapper\">\n                <h1 class=\"details-header\">\n                    {{getName()}}\n                </h1>\n            </div>\n        </div>\n\n        <div class=\"line-container\">\n            <div class=\"line\"></div>\n        </div>\n\n        <div class=\"details-body\">\n            <div class=\"detail-container vertically-spaced\">\n                <div class=\"content-body\">\n\n                    <div class=\"row\" style=\"display: flex;\n                         flex-direction: row;\">\n                        <mdc-text-field class=\"first-element\"\n                                        label=\"Name\"\n                                        formControlName=\"name\"\n                                        readonly=\"{{!canEdit()}}\"\n                                        type=\"text\">\n                        </mdc-text-field>\n                    </div>\n\n                    <!-- The current mdc-textarea version does not support the\n                    readonly mark -->\n                    <mdc-textarea class=\"element\"\n                                  label=\"Description\"\n                                  formControlName=\"description\"\n                                  disabled=\"{{!canEdit()}}\"\n                    >\n                    </mdc-textarea>\n\n                </div>\n            </div>\n\n            <div class=\"detail-container\">\n                <div>\n                    <h2 class=\"content-title\">Owner</h2>\n                    <ul class=\"undecorated-list horizontal-list\">\n                        <li>\n                            {{getOwner().fullName}} - {{getOwner().email}}\n                        </li>\n                    </ul>\n                </div>\n\n                <div class=\"detail-header\">\n                    <h2 class=\"content-title\">Members</h2>\n                    <div class=\"actions-container\">\n                        <div class=\"content-body\">\n                            <ul class=\"actions\">\n                                <li class=\"action-item\" aria-label=\"Add user\"\n                                    (click)=\"onAddUserClicked()\">\n                                    <i class=\"material-icons\">person_add</i>\n                                </li>\n                            </ul>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"content-body\">\n                    <div class=\"row vertically-spaced\" *ngIf=\"addUserClicked\">\n                        <form class=\"row\" [formGroup]=\"addUserForm\">\n                            <mdc-text-field class=\"full-width-element\"\n                                            label=\"E-mail\"\n                                            type=\"email\"\n                                            formControlName=\"email\"\n                                            (keypress)=\"addUser($event)\">\n                            </mdc-text-field>\n                        </form>\n                    </div>\n\n                    <ul class=\"undecorated-list horizontal-list\"\n                        *ngIf=\"hasMembers()\">\n                        <li *ngFor=\"let member of getMembers()\">\n                            {{member.fullName}} - {{member.email}}\n                        </li>\n                    </ul>\n                    <div class=\"no-results\" *ngIf=\"!hasMembers()\">\n                        <span> This groups does not have members</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"buttons-container huge-vertically-spaced\">\n                <button class=\"submit-button\" mdc-button primary\n                        [disabled]=\"!groupForm.valid\"\n                        (click)=\"saveGroup()\">\n                    {{creation ? \"Create\" : \"Save\" }}\n                </button>\n            </div>\n        </div>\n    </form>\n</div>\n"

/***/ }),

/***/ "./src/app/group-details/group-details.component.scss":
/*!************************************************************!*\
  !*** ./src/app/group-details/group-details.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#details-container {\n  margin: 0 10px;\n  flex: 1 0; }\n  #details-container .detail-header {\n    text-align: center; }\n  #details-container .content-title {\n    margin-bottom: 0;\n    margin-top: 35px; }\n  #details-container .content-body {\n    display: flex;\n    flex-direction: column; }\n  #details-container .content-body .element, #details-container .content-body .row .first-element, #details-container .content-body .row .last-element {\n      flex: 1;\n      margin-top: 5px;\n      margin-bottom: 5px; }\n  #details-container .content-body .row {\n      display: flex;\n      flex-direction: row; }\n  .row {\n  margin: 0 10px;\n  flex: 1 0;\n  display: flex;\n  flex-direction: row; }\n  .full-width-element {\n  flex: 1;\n  margin-top: 5px;\n  margin-bottom: 5px; }\n  .text-wrapper {\n  display: inline-flex;\n  max-width: 76%; }\n  .details-header {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n  div.line-container {\n  text-align: center; }\n  div.line-container div.line {\n    border-bottom: solid 1px black;\n    width: 90%;\n    display: inline-block; }\n  .details-body {\n  display: flex;\n  flex-direction: column;\n  overflow-y: auto;\n  max-height: 79.5vh; }\n  .undecorated-list {\n  list-style: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0; }\n  .undecorated-list.horizontal-list {\n    display: flex;\n    flex-wrap: wrap; }\n  .undecorated-list.horizontal-list li {\n      display: inline;\n      margin: 3px 5px;\n      padding: 5px;\n      background-color: #dddddd;\n      border-radius: 4px; }\n  .no-results {\n  overflow-y: auto;\n  display: flex;\n  flex-direction: row;\n  height: 64.4vh;\n  width: 99.8%;\n  text-align: center;\n  flex: auto;\n  max-height: 100px;\n  background-color: #f5f5f5;\n  align-items: center;\n  justify-content: center; }\n  .no-results span {\n    font-weight: bold;\n    font-size: 1.5em; }\n  /** Action styles */\n  .actions-container {\n  display: inline-flex;\n  flex: 1;\n  justify-content: flex-end;\n  overflow: hidden; }\n  .actions-container:after {\n    clear: both; }\n  .actions-container ul {\n    -webkit-margin-after: 0;\n            margin-block-end: 0; }\n  .actions {\n  list-style-type: none;\n  -webkit-padding-start: 10px;\n          padding-inline-start: 10px;\n  -webkit-margin-before: 35px;\n          margin-block-start: 35px;\n  display: inline-flex; }\n  .actions li:hover {\n    cursor: pointer;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n  .actions li:hover.action-item i {\n      color: #005db9; }\n  .actions li:hover.action-item.delete i {\n      color: #DC0000; }\n  .actions li.action-item {\n    margin: 0 5px; }\n  .actions li.action-item i {\n      font-size: 30px; }\n  .detail-header {\n  display: flex;\n  flex-direction: row;\n  justify-content: center; }\n  .vertically-spaced {\n  margin-top: 10px;\n  margin-bottom: 10px; }\n  .huge-vertically-spaced {\n  margin-top: 40px;\n  margin-bottom: 40px; }\n  .buttons-container {\n  display: flex;\n  justify-content: flex-end; }\n  .buttons-container .submit-button {\n    max-width: 100px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9ncm91cC1kZXRhaWxzL2dyb3VwLWRldGFpbHMuY29tcG9uZW50LnNjc3MiLCIvaG9tZS9qZXN1cy9Eb2N1bWVudHMvcmVwb3NpdG9yaWVzL3RmZy9tZGFnL3Byb2plY3QvbURhZ0Zyb250L3NyYy9fbWF0ZXJpYWx2YXJzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxjQUFjO0VBQ2QsU0FBUyxFQUFBO0VBRlg7SUFLSSxrQkFBa0IsRUFBQTtFQUx0QjtJQVNJLGdCQUFnQjtJQUNoQixnQkFBZ0IsRUFBQTtFQVZwQjtJQWNJLGFBQWE7SUFDYixzQkFBc0IsRUFBQTtFQWYxQjtNQWtCTSxPQUFPO01BQ1AsZUFBZTtNQUNmLGtCQUFrQixFQUFBO0VBcEJ4QjtNQXdCTSxhQUFhO01BQ2IsbUJBQW1CLEVBQUE7RUFhekI7RUFDRSxjQUFjO0VBQ2QsU0FBUztFQUNULGFBQWE7RUFDYixtQkFBbUIsRUFBQTtFQUdyQjtFQUNFLE9BQU87RUFDUCxlQUFlO0VBQ2Ysa0JBQWtCLEVBQUE7RUFJcEI7RUFDRSxvQkFBb0I7RUFDcEIsY0FBYyxFQUFBO0VBR2hCO0VBQ0UsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQix1QkFBdUIsRUFBQTtFQUl6QjtFQUNFLGtCQUFrQixFQUFBO0VBRHBCO0lBSUksOEJBQThCO0lBQzlCLFVBQVU7SUFDVixxQkFBcUIsRUFBQTtFQU16QjtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBO0VBR3BCO0VBQ0UsZ0JBQWdCO0VBQ2hCLHdCQUF1QjtVQUF2Qix1QkFBdUIsRUFBQTtFQUZ6QjtJQUtJLGFBQWE7SUFDYixlQUFlLEVBQUE7RUFObkI7TUFTTSxlQUFlO01BQ2YsZUFBZTtNQUNmLFlBQVk7TUFDWix5QkNoR3VCO01EaUd2QixrQkFBa0IsRUFBQTtFQU14QjtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsbUJBQW1CO0VBR25CLGNBQWM7RUFDZCxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixpQkFBaUI7RUFDakIseUJDakhpQztFRG1IakMsbUJBQW1CO0VBQ25CLHVCQUF1QixFQUFBO0VBZHpCO0lBa0JJLGlCQUFpQjtJQUNqQixnQkFBZ0IsRUFBQTtFQUlwQixtQkFBQTtFQUNBO0VBQ0Usb0JBQW9CO0VBQ3BCLE9BQU87RUFDUCx5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7RUFKbEI7SUFPSSxXQUFXLEVBQUE7RUFQZjtJQVdJLHVCQUFtQjtZQUFuQixtQkFBbUIsRUFBQTtFQUl2QjtFQUNFLHFCQUFxQjtFQUNyQiwyQkFBMEI7VUFBMUIsMEJBQTBCO0VBQzFCLDJCQUF3QjtVQUF4Qix3QkFBd0I7RUFDeEIsb0JBQW9CLEVBQUE7RUFKdEI7SUFRTSxlQUFlO0lBQ2YseUJBQWlCO09BQWpCLHNCQUFpQjtRQUFqQixxQkFBaUI7WUFBakIsaUJBQWlCLEVBQUE7RUFUdkI7TUFhVSxjQzVKaUIsRUFBQTtFRCtJM0I7TUFxQlUsY0FBYyxFQUFBO0VBckJ4QjtJQTJCTSxhQUFhLEVBQUE7RUEzQm5CO01BOEJRLGVBQWUsRUFBQTtFQU12QjtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCLEVBQUE7RUFHekI7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7RUFHckI7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7RUFHckI7RUFDRSxhQUFhO0VBQ2IseUJBQXlCLEVBQUE7RUFGM0I7SUFLSSxnQkFBZ0IsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL2dyb3VwLWRldGFpbHMvZ3JvdXAtZGV0YWlscy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi9tYXRlcmlhbHZhcnNcIjtcblxuI2RldGFpbHMtY29udGFpbmVyIHtcbiAgbWFyZ2luOiAwIDEwcHg7XG4gIGZsZXg6IDEgMDtcblxuICAmIC5kZXRhaWwtaGVhZGVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAmIC5jb250ZW50LXRpdGxlIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIG1hcmdpbi10b3A6IDM1cHg7XG4gIH1cblxuICAmIC5jb250ZW50LWJvZHkge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcblxuICAgICYgLmVsZW1lbnQge1xuICAgICAgZmxleDogMTtcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICB9XG5cbiAgICAmIC5yb3cge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbiAgICAgICYgLmZpcnN0LWVsZW1lbnQge1xuICAgICAgICBAZXh0ZW5kIC5lbGVtZW50O1xuICAgICAgfVxuXG4gICAgICAmIC5sYXN0LWVsZW1lbnQge1xuICAgICAgICBAZXh0ZW5kIC5lbGVtZW50O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4ucm93IHtcbiAgbWFyZ2luOiAwIDEwcHg7XG4gIGZsZXg6IDEgMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cblxuLmZ1bGwtd2lkdGgtZWxlbWVudCB7XG4gIGZsZXg6IDE7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuXG5cbi50ZXh0LXdyYXBwZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgbWF4LXdpZHRoOiA3NiU7XG59XG5cbi5kZXRhaWxzLWhlYWRlciB7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG5cbmRpdi5saW5lLWNvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAmIGRpdi5saW5lIHtcbiAgICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggYmxhY2s7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIH1cbn1cblxuXG4vLyBEYXRlIGxpc3Qgc3R5bGVzXG4uZGV0YWlscy1ib2R5IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgbWF4LWhlaWdodDogNzkuNXZoO1xufVxuXG4udW5kZWNvcmF0ZWQtbGlzdCB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwO1xuXG4gICYuaG9yaXpvbnRhbC1saXN0IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtd3JhcDogd3JhcDtcblxuICAgICYgbGkge1xuICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgICAgbWFyZ2luOiAzcHggNXB4O1xuICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJG1kYy10aGVtZS1zZWNvbmRhcnk7XG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgfVxuXG4gIH1cbn1cblxuLm5vLXJlc3VsdHMge1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuXG4gIC8vIEV4cGVyaW1lbnRhbCB2YWx1ZXMuIFBsYXkgd2l0aCB0aGVtIGluIG9yZGVyIHRvIHNldCB1cCBjb3JyZWN0bHlcbiAgaGVpZ2h0OiA2NC40dmg7XG4gIHdpZHRoOiA5OS44JTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmbGV4OiBhdXRvO1xuICBtYXgtaGVpZ2h0OiAxMDBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogJG1kYy10aGVtZS1zZWNvbmRhcnktbGlnaHQ7XG5cbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cblxuICAmIHNwYW4ge1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gIH1cbn1cblxuLyoqIEFjdGlvbiBzdHlsZXMgKi9cbi5hY3Rpb25zLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBmbGV4OiAxO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuXG4gICY6YWZ0ZXIge1xuICAgIGNsZWFyOiBib3RoO1xuICB9XG5cbiAgJiB1bCB7XG4gICAgbWFyZ2luLWJsb2NrLWVuZDogMDtcbiAgfVxufVxuXG4uYWN0aW9ucyB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDEwcHg7XG4gIG1hcmdpbi1ibG9jay1zdGFydDogMzVweDtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG5cbiAgJiBsaSB7XG4gICAgJjpob3ZlciB7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICB1c2VyLXNlbGVjdDogbm9uZTtcblxuICAgICAgJi5hY3Rpb24taXRlbSB7XG4gICAgICAgICYgaSB7XG4gICAgICAgICAgY29sb3I6ICRtZGMtdGhlbWUtcHJpbWFyeTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmLmFjdGlvbi1pdGVtLmRlbGV0ZSB7XG4gICAgICAgICYgaSB7XG4gICAgICAgICAgLy8gUmVkaXNoIGNvbG9yLiBQdXJlIHJlZCBjb2xvciBpcyBub3QgZW5vdWdoIGZvciBBQSB3ZWIgcGFnZXNcbiAgICAgICAgICAvLyB3aGVuIGl0cyBiYWNrZ3JvdW5kIGNvbG9yIGlzIHdoaXRlLlxuICAgICAgICAgIGNvbG9yOiAjREMwMDAwO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgJi5hY3Rpb24taXRlbSB7XG4gICAgICBtYXJnaW46IDAgNXB4O1xuXG4gICAgICAmIGkge1xuICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi5kZXRhaWwtaGVhZGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi52ZXJ0aWNhbGx5LXNwYWNlZCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5odWdlLXZlcnRpY2FsbHktc3BhY2VkIHtcbiAgbWFyZ2luLXRvcDogNDBweDtcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcbn1cblxuLmJ1dHRvbnMtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcblxuICAmIC5zdWJtaXQtYnV0dG9uIHtcbiAgICBtYXgtd2lkdGg6IDEwMHB4O1xuICB9XG59XG4iLCIkbWRjLXRoZW1lLXByaW1hcnk6ICMwMDVkYjk7XG4kbWRjLXRoZW1lLXNlY29uZGFyeTogI2RkZGRkZDtcbiRtZGMtdGhlbWUtc2Vjb25kYXJ5LWxpZ2h0OiAjZjVmNWY1O1xuJG1kYy10aGVtZS1iYWNrZ3JvdW5kOiAjZmZmO1xuIl19 */"

/***/ }),

/***/ "./src/app/group-details/group-details.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/group-details/group-details.component.ts ***!
  \**********************************************************/
/*! exports provided: GroupDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupDetailsComponent", function() { return GroupDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user/user.service */ "./src/app/user/user.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notifications/notifications.service */ "./src/app/notifications/notifications.service.ts");
/* harmony import */ var _group_group_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../group/group.model */ "./src/app/group/group.model.ts");






var GroupDetailsComponent = /** @class */ (function () {
    function GroupDetailsComponent(userService, notificationsService) {
        /* Forms */
        this.addUserForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email])
        });
        this.groupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(100)]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.userService = userService;
        this.notificationsService = notificationsService;
        this.user = this.userService.getUser();
        this.addUserClicked = false;
        this.onGroupCreated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onGroupUpdated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    GroupDetailsComponent.prototype.ngOnInit = function () {
        if (this.group) {
            this.groupInstance = this.group;
        }
        else {
            this.groupInstance = new _group_group_model__WEBPACK_IMPORTED_MODULE_5__["Group"]({
                name: "New group",
                description: "",
                owner: null,
                members: []
            });
        }
        this.groupForm.setValue({
            "name": this.groupInstance.name,
            "description": this.groupInstance.description
        });
        this.creation = !this.group;
    };
    GroupDetailsComponent.prototype.getName = function () {
        return this.groupInstance.name;
    };
    GroupDetailsComponent.prototype.canEdit = function () {
        return (this.user && this.groupInstance.owner && this.groupInstance.owner.id === this.user.id
            || this.creation);
    };
    GroupDetailsComponent.prototype.getMembers = function () {
        return this.groupInstance['members'];
    };
    GroupDetailsComponent.prototype.hasMembers = function () {
        return this.groupInstance['members'].length > 0;
    };
    GroupDetailsComponent.prototype.getOwner = function () {
        if (this.creation) {
            return this.user;
        }
        else {
            return this.groupInstance.owner;
        }
    };
    GroupDetailsComponent.prototype.onAddUserClicked = function () {
        this.addUserClicked = true;
    };
    GroupDetailsComponent.prototype.addUser = function (event) {
        var _this = this;
        if (event.code == "Enter") {
            // TODO add user to the group
            this.userService.list({ email: this.addUserForm.get("email").value }).then(function (users) {
                _this.groupInstance['members'].push(users[0]);
                _this.addUserClicked = false;
            }).catch(function (error) {
                _this.notificationsService.notify("The user has not been found", _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__["NotificationsService"].MESSAGE_TYPES.WARNING);
            });
        }
    };
    GroupDetailsComponent.prototype.saveGroup = function () {
        var _this = this;
        var promise;
        if (this.creation) {
            this.userService.createGroup(this.groupForm.get('name').value, this.groupForm.get('description').value, this.groupInstance.members).then(function () { return _this.onGroupCreated.emit(); });
        }
        else {
            this.userService.updateGroup(this.groupInstance.id, this.groupForm.get('name').value, this.groupForm.get('description').value, this.groupInstance.members).then(function () { return _this.onGroupUpdated.emit(); });
        }
    };
    GroupDetailsComponent.prototype.ngOnChanges = function (changes) {
        var group = changes['group'].currentValue;
        if (group) {
            this.group = this.groupInstance = changes['group'].currentValue;
            this.groupForm.setValue({
                "name": this.groupInstance.name,
                "description": this.groupInstance.description
            });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], GroupDetailsComponent.prototype, "onGroupCreated", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], GroupDetailsComponent.prototype, "onGroupUpdated", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _group_group_model__WEBPACK_IMPORTED_MODULE_5__["Group"])
    ], GroupDetailsComponent.prototype, "group", void 0);
    GroupDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-group-details',
            template: __webpack_require__(/*! ./group-details.component.html */ "./src/app/group-details/group-details.component.html"),
            styles: [__webpack_require__(/*! ./group-details.component.scss */ "./src/app/group-details/group-details.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__["NotificationsService"]])
    ], GroupDetailsComponent);
    return GroupDetailsComponent;
}());



/***/ }),

/***/ "./src/app/group/group.model.ts":
/*!**************************************!*\
  !*** ./src/app/group/group.model.ts ***!
  \**************************************/
/*! exports provided: Group */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Group", function() { return Group; });
/* harmony import */ var _user_user_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../user/user.model */ "./src/app/user/user.model.ts");

var Group = /** @class */ (function () {
    function Group(data) {
        var user_obj = data;
        if (typeof (data) === 'string') {
            user_obj = this.deserialize(data);
        }
        this.initialize(user_obj);
    }
    Group.prototype.deserialize = function (input) {
        return JSON.parse(input);
    };
    Group.prototype.serialize = function () {
        var obj = {
            id: this._id,
            name: this._name,
            description: this._description,
            owner: this.owner && this.owner.serialize(),
            members: this.members.map(function (member) { return member.serialize(); })
        };
        return JSON.stringify(obj);
    };
    Object.defineProperty(Group.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (description) {
            this._description = description;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "members", {
        get: function () {
            return this._members;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "owner", {
        get: function () {
            return this._owner;
        },
        enumerable: true,
        configurable: true
    });
    Group.prototype.initialize = function (groupObj) {
        this._id = groupObj['id'];
        this._name = groupObj['name'];
        this._description = groupObj['description'];
        if (groupObj['owner']) {
            this._owner = new _user_user_model__WEBPACK_IMPORTED_MODULE_0__["User"](groupObj['owner']);
        }
        if (groupObj['members']) {
            this._members = groupObj['members'].map(function (userObj) { return new _user_user_model__WEBPACK_IMPORTED_MODULE_0__["User"](userObj); });
        }
    };
    return Group;
}());



/***/ }),

/***/ "./src/app/groups-list/groups-list.component.html":
/*!********************************************************!*\
  !*** ./src/app/groups-list/groups-list.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"group-container\">\n    <div class=\"main-container\" (click)=\"clearSelection()\">\n        <form class=\"signup-form\" [formGroup]=\"filtersForm\"\n              (ngSubmit)=\"filterResults()\">\n            <div class=\"filters group-common-styles\">\n                <h2 id=\"filters-header\" class=\"section-header\">Filters</h2>\n                <div class=\"filtering-fields\">\n                    <mdc-text-field class=\"filter-field\"\n                                    placeholder=\"Group name\"\n                                    formControlName=\"groupName\">\n                    </mdc-text-field>\n                    <mdc-select class=\"filter-field\"\n                                placeholder=\"Group owner\"\n                                formControlName=\"groupOwner\">\n                        <mdc-menu class=\"filter-field\">\n                            <mdc-list class=\"filter-field\">\n                                <mdc-list-item value=\"0\">No</mdc-list-item>\n                                <mdc-list-item value=\"1\">Yes</mdc-list-item>\n                            </mdc-list>\n                        </mdc-menu>\n                    </mdc-select>\n\n                </div>\n                <div style=\"display: flex;flex-direction: row;justify-content:\n                flex-end\">\n                    <button class=\"filter-button\" mdc-button primary\n                            (click)=\"clearFilters()\">\n                        Clear\n                    </button>\n                    <button class=\"filter-button\" mdc-button primary\n                            type=\"submit\">\n                        Filter\n                    </button>\n\n                </div>\n            </div>\n        </form>\n        <div class=\"list-zone\">\n            <div class=\"result-header-container\">\n                <h2 id=\"result-header\" class=\"section-header\">Groups</h2>\n                <button mdc-fab class=\"add-button\"\n                        aria-label=\"Create group\"\n                        (click)=\"createGroup($event)\">\n                    <i class=\"material-icons icon\">add</i>\n                </button>\n            </div>\n            <div class=\"results\" *ngIf=\"hasResults()\">\n                <ul *ngFor=\"let group of getGroups(); let i = index\"\n                    class=\"details-item\">\n                    <div class=\"details-item-wrapper\"\n                         (click)=\"selectGroup($event, group, i)\">\n                        <li>{{ group.name }}</li>\n                    </div>\n                </ul>\n            </div>\n            <div class=\"no-results\" *ngIf=\"!hasResults()\">\n                <span> No results</span>\n            </div>\n        </div>\n    </div>\n\n    <div id=\"details-container\" *ngIf=\"shouldShowDetailsPane()\">\n        <app-group-details\n                (onGroupCreated)=\"filterResults()\"\n                (onGroupUpdated)=\"filterResults()\"\n                [group]=\"getSelectedGroup()\">\n        </app-group-details>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/groups-list/groups-list.component.scss":
/*!********************************************************!*\
  !*** ./src/app/groups-list/groups-list.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".group-item {\n  margin: 10px 0;\n  display: block; }\n\n.group-container {\n  display: flex;\n  flex-direction: row; }\n\n.group-container .results {\n    overflow-y: auto;\n    display: flex;\n    flex-direction: row;\n    height: 64.4vh;\n    max-height: 64.4vh;\n    width: 99.8%;\n    padding: 6px;\n    flex-wrap: wrap; }\n\n.group-container .main-container {\n    flex: 4 0; }\n\n.group-container #details-container {\n    max-width: 32%;\n    min-width: 32%;\n    margin: 0 10px;\n    flex: 1 0; }\n\n.group-container #details-container .detail-header {\n      text-align: center; }\n\n.group-container #details-container .content-title {\n      margin-bottom: 0;\n      margin-top: 35px; }\n\n.group-container #details-container .content-body {\n      display: flex;\n      flex-direction: column; }\n\n.group-container #details-container .content-body .element, .group-container #details-container .content-body .row .first-element, .group-container #details-container .content-body .row .last-element {\n        flex: 1;\n        margin-top: 5px;\n        margin-bottom: 5px; }\n\n.group-container #details-container .content-body .row {\n        display: flex;\n        flex-direction: row; }\n\n.group-container #details-container .content-body .row .first-element {\n          margin-right: 10px; }\n\n.group-container #details-container .content-body .row .last-element {\n          margin-left: 10px; }\n\n.group-container .group-common-styles {\n    margin: 1px 10px; }\n\n.group-container .group-common-styles .filters {\n      margin-bottom: 10px; }\n\n.actions {\n  list-style-type: none;\n  -webkit-padding-start: 10px;\n          padding-inline-start: 10px;\n  display: inline-flex; }\n\n.actions li:hover {\n    text-shadow: 0 0 1px black;\n    cursor: pointer;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n\n.actions li.action-item {\n    margin: 0 5px; }\n\n.actions li.action-item i {\n      font-size: 20px; }\n\n.float-right {\n  float: right; }\n\n.actions-container {\n  display: inline-flex;\n  overflow: hidden; }\n\n.actions-container:after {\n    clear: both; }\n\n.actions-container ul {\n    -webkit-margin-after: 0;\n            margin-block-end: 0; }\n\n.details-header {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n\ndiv.line-container {\n  text-align: center; }\n\ndiv.line-container div.line {\n    border-bottom: solid 1px black;\n    width: 90%;\n    display: inline-block; }\n\n.content-body-list {\n  list-style-type: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0;\n  margin-top: 0;\n  margin-bottom: 5px; }\n\n.details-body {\n  display: flex;\n  flex-direction: column;\n  overflow-y: auto;\n  max-height: 79.5vh; }\n\n.list-zone {\n  display: flex;\n  flex-direction: column;\n  margin: 10px 10px 0px 10px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\n.result-header-container {\n  position: relative; }\n\n.result-header-container .add-button {\n    position: absolute;\n    top: 5px;\n    right: 0;\n    margin-right: 35px;\n    width: 40px;\n    height: 40px;\n    background-color: #005db9; }\n\n.result-header-container .add-button .icon {\n      color: white; }\n\n.undecorated-list {\n  list-style: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0; }\n\n.undecorated-list.horizontal-list {\n    display: flex;\n    flex-wrap: wrap; }\n\n.undecorated-list.horizontal-list li {\n      display: inline;\n      margin: 3px 5px;\n      padding: 5px;\n      background-color: #dddddd;\n      border-radius: 4px; }\n\n.wrap-content {\n  display: flex;\n  flex-wrap: wrap; }\n\n.filtering-fields {\n  display: flex;\n  flex-direction: row; }\n\n.filter-field {\n  flex: 1;\n  margin: 2px 5px; }\n\n.filter-button {\n  flex: 0;\n  margin: 5px 5px; }\n\n.remove-button {\n  color: red; }\n\n.remove-button:hover {\n    color: white;\n    background-color: red;\n    box-shadow: lightgray -1px 1px 3px; }\n\n.details-item {\n  list-style-type: none;\n  -webkit-margin-before: 0;\n          margin-block-start: 0;\n  -webkit-margin-after: 0;\n          margin-block-end: 0;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0;\n  flex: 30; }\n\n.details-item .details-item-wrapper {\n    padding: 30px; }\n\n.details-item .details-item-wrapper:hover, .details-item .details-item-wrapper.active {\n      box-shadow: rgba(0, 93, 185, 0.2) 0px 0px 5px; }\n\n.no-results {\n  overflow-y: auto;\n  display: flex;\n  flex-direction: row;\n  height: 64.4vh;\n  width: 99.8%;\n  text-align: center;\n  flex: auto;\n  max-height: 100px;\n  background-color: #f5f5f5;\n  align-items: center;\n  justify-content: center; }\n\n.no-results span {\n    font-weight: bold;\n    font-size: 1.5em; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9ncm91cHMtbGlzdC9ncm91cHMtbGlzdC5jb21wb25lbnQuc2NzcyIsIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL19tYXRlcmlhbHZhcnMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLGNBQWM7RUFDZCxjQUFjLEVBQUE7O0FBSWhCO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQixFQUFBOztBQUZyQjtJQUtJLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2IsbUJBQW1CO0lBR25CLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFlBQVk7SUFDWixlQUFlLEVBQUE7O0FBZG5CO0lBa0JJLFNBQVMsRUFBQTs7QUFsQmI7SUFzQkksY0FBYztJQUNkLGNBQWM7SUFDZCxjQUFjO0lBQ2QsU0FBUyxFQUFBOztBQXpCYjtNQTRCTSxrQkFBa0IsRUFBQTs7QUE1QnhCO01BZ0NNLGdCQUFnQjtNQUNoQixnQkFBZ0IsRUFBQTs7QUFqQ3RCO01BcUNNLGFBQWE7TUFDYixzQkFBc0IsRUFBQTs7QUF0QzVCO1FBeUNRLE9BQU87UUFDUCxlQUFlO1FBQ2Ysa0JBQWtCLEVBQUE7O0FBM0MxQjtRQStDUSxhQUFhO1FBQ2IsbUJBQW1CLEVBQUE7O0FBaEQzQjtVQW9EVSxrQkFBa0IsRUFBQTs7QUFwRDVCO1VBeURVLGlCQUFpQixFQUFBOztBQXpEM0I7SUFnRUksZ0JBQWdCLEVBQUE7O0FBaEVwQjtNQW1FTSxtQkFBbUIsRUFBQTs7QUFNekI7RUFDRSxxQkFBcUI7RUFDckIsMkJBQTBCO1VBQTFCLDBCQUEwQjtFQUMxQixvQkFBb0IsRUFBQTs7QUFIdEI7SUFPTSwwQkFBMEI7SUFDMUIsZUFBZTtJQUNmLHlCQUFpQjtPQUFqQixzQkFBaUI7UUFBakIscUJBQWlCO1lBQWpCLGlCQUFpQixFQUFBOztBQVR2QjtJQWFNLGFBQWEsRUFBQTs7QUFibkI7TUFnQlEsZUFBZSxFQUFBOztBQU12QjtFQUNFLFlBQVksRUFBQTs7QUFHZDtFQUNFLG9CQUFvQjtFQUNwQixnQkFBZ0IsRUFBQTs7QUFGbEI7SUFLSSxXQUFXLEVBQUE7O0FBTGY7SUFTSSx1QkFBbUI7WUFBbkIsbUJBQW1CLEVBQUE7O0FBSXZCO0VBQ0UsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQix1QkFBdUIsRUFBQTs7QUFJekI7RUFDRSxrQkFBa0IsRUFBQTs7QUFEcEI7SUFJSSw4QkFBOEI7SUFDOUIsVUFBVTtJQUNWLHFCQUFxQixFQUFBOztBQU16QjtFQUNFLHFCQUFxQjtFQUNyQix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLDBCQUEwQixFQUFBOztBQUc1QjtFQUNFLGtCQUFrQjtFQUVsQixZQUFZO0VBQ1osZ0NBQWdDLEVBQUE7O0FBSWxDO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRHBCO0lBSUksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixRQUFRO0lBQ1Isa0JBQWtCO0lBQ2xCLFdBVGtCO0lBVWxCLFlBVmtCO0lBV2xCLHlCQ25MdUIsRUFBQTs7QUR5SzNCO01BYU0sWUFBWSxFQUFBOztBQUtsQjtFQUNFLGdCQUFnQjtFQUNoQix3QkFBdUI7VUFBdkIsdUJBQXVCLEVBQUE7O0FBRnpCO0lBS0ksYUFBYTtJQUNiLGVBQWUsRUFBQTs7QUFObkI7TUFTTSxlQUFlO01BQ2YsZUFBZTtNQUNmLFlBQVk7TUFDWix5QkN0TXVCO01EdU12QixrQkFBa0IsRUFBQTs7QUFNeEI7RUFDRSxhQUFhO0VBQ2IsZUFBZSxFQUFBOztBQU1qQjtFQUNFLGFBQWE7RUFDYixtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxPQUFPO0VBQ1AsZUFBZSxFQUFBOztBQUlqQjtFQUNFLE9BQU87RUFDUCxlQUFlLEVBQUE7O0FBS2pCO0VBQ0UsVUFBVSxFQUFBOztBQURaO0lBSUksWUFBWTtJQUNaLHFCQUFxQjtJQUNyQixrQ0FBa0MsRUFBQTs7QUFLdEM7RUFDRSxxQkFBcUI7RUFDckIsd0JBQXFCO1VBQXJCLHFCQUFxQjtFQUNyQix1QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsUUFBUSxFQUFBOztBQUxWO0lBUUksYUFBYSxFQUFBOztBQVJqQjtNQVdNLDZDQUFxRCxFQUFBOztBQU0zRDtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsbUJBQW1CO0VBR25CLGNBQWM7RUFDZCxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixpQkFBaUI7RUFDakIseUJDN1FpQztFRCtRakMsbUJBQW1CO0VBQ25CLHVCQUF1QixFQUFBOztBQWR6QjtJQWtCSSxpQkFBaUI7SUFDakIsZ0JBQWdCLEVBQUEiLCJmaWxlIjoiLi4vc3JjL2FwcC9ncm91cHMtbGlzdC9ncm91cHMtbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi9tYXRlcmlhbHZhcnNcIjtcblxuLmdyb3VwLWl0ZW0ge1xuICBtYXJnaW46IDEwcHggMDtcbiAgZGlzcGxheTogYmxvY2s7XG5cbn1cblxuLmdyb3VwLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbiAgJiAucmVzdWx0cyB7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbiAgICAvLyBFeHBlcmltZW50YWwgdmFsdWVzLiBQbGF5IHdpdGggdGhlbSBpbiBvcmRlciB0byBzZXQgdXAgY29ycmVjdGx5XG4gICAgaGVpZ2h0OiA2NC40dmg7XG4gICAgbWF4LWhlaWdodDogNjQuNHZoO1xuICAgIHdpZHRoOiA5OS44JTtcbiAgICBwYWRkaW5nOiA2cHg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG5cbiAgJiAubWFpbi1jb250YWluZXIge1xuICAgIGZsZXg6IDQgMDtcbiAgfVxuXG4gICYgI2RldGFpbHMtY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDMyJTtcbiAgICBtaW4td2lkdGg6IDMyJTtcbiAgICBtYXJnaW46IDAgMTBweDtcbiAgICBmbGV4OiAxIDA7XG5cbiAgICAmIC5kZXRhaWwtaGVhZGVyIHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAmIC5jb250ZW50LXRpdGxlIHtcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgICBtYXJnaW4tdG9wOiAzNXB4O1xuICAgIH1cblxuICAgICYgLmNvbnRlbnQtYm9keSB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcblxuICAgICAgJiAuZWxlbWVudCB7XG4gICAgICAgIGZsZXg6IDE7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgICAgfVxuXG4gICAgICAmIC5yb3cge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuXG4gICAgICAgICYgLmZpcnN0LWVsZW1lbnQge1xuICAgICAgICAgIEBleHRlbmQgLmVsZW1lbnQ7XG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgJiAubGFzdC1lbGVtZW50IHtcbiAgICAgICAgICBAZXh0ZW5kIC5lbGVtZW50O1xuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgJiAuZ3JvdXAtY29tbW9uLXN0eWxlcyB7XG4gICAgbWFyZ2luOiAxcHggMTBweDtcblxuICAgICYgLmZpbHRlcnMge1xuICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICB9XG4gIH1cbn1cblxuXG4uYWN0aW9ucyB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDEwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuXG4gICYgbGkge1xuICAgICY6aG92ZXIge1xuICAgICAgdGV4dC1zaGFkb3c6IDAgMCAxcHggYmxhY2s7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgICB9XG5cbiAgICAmLmFjdGlvbi1pdGVtIHtcbiAgICAgIG1hcmdpbjogMCA1cHg7XG5cbiAgICAgICYgaSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLmZsb2F0LXJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uYWN0aW9ucy1jb250YWluZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcblxuICAmOmFmdGVyIHtcbiAgICBjbGVhcjogYm90aDtcbiAgfVxuXG4gICYgdWwge1xuICAgIG1hcmdpbi1ibG9jay1lbmQ6IDA7XG4gIH1cbn1cblxuLmRldGFpbHMtaGVhZGVyIHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5cblxuZGl2LmxpbmUtY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICYgZGl2LmxpbmUge1xuICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCBibGFjaztcbiAgICB3aWR0aDogOTAlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxufVxuXG5cbi8vIERhdGUgbGlzdCBzdHlsZXNcbi5jb250ZW50LWJvZHktbGlzdCB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDA7XG4gIG1hcmdpbi10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cblxuXG4uZGV0YWlscy1ib2R5IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgbWF4LWhlaWdodDogNzkuNXZoO1xufVxuXG5cbi5saXN0LXpvbmUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBtYXJnaW46IDEwcHggMTBweCAwcHggMTBweDtcbn1cblxuLnNlY3Rpb24taGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIGhlaWdodDogYXV0bztcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNkZGRkZGQ7XG59XG5cbiRhZGQtYnV0dG9uLXNpemU6IDQwcHg7XG4ucmVzdWx0LWhlYWRlci1jb250YWluZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgJiAuYWRkLWJ1dHRvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNXB4O1xuICAgIHJpZ2h0OiAwO1xuICAgIG1hcmdpbi1yaWdodDogMzVweDtcbiAgICB3aWR0aDogJGFkZC1idXR0b24tc2l6ZTtcbiAgICBoZWlnaHQ6ICRhZGQtYnV0dG9uLXNpemU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJG1kYy10aGVtZS1wcmltYXJ5O1xuXG4gICAgJiAuaWNvbiB7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICB9XG59XG5cbi51bmRlY29yYXRlZC1saXN0IHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDA7XG5cbiAgJi5ob3Jpem9udGFsLWxpc3Qge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuXG4gICAgJiBsaSB7XG4gICAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgICBtYXJnaW46IDNweCA1cHg7XG4gICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkbWRjLXRoZW1lLXNlY29uZGFyeTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICB9XG5cbiAgfVxufVxuXG4ud3JhcC1jb250ZW50IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuXG59XG5cbi8vIEZpbHRlciBzdHlsZXNcblxuLmZpbHRlcmluZy1maWVsZHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuXG4uZmlsdGVyLWZpZWxkIHtcbiAgZmxleDogMTtcbiAgbWFyZ2luOiAycHggNXB4O1xufVxuXG5cbi5maWx0ZXItYnV0dG9uIHtcbiAgZmxleDogMDtcbiAgbWFyZ2luOiA1cHggNXB4O1xufVxuXG5cbi8vIEJ1dHRvbiBzdHlsZXNcbi5yZW1vdmUtYnV0dG9uIHtcbiAgY29sb3I6IHJlZDtcblxuICAmOmhvdmVyIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICAgIGJveC1zaGFkb3c6IGxpZ2h0Z3JheSAtMXB4IDFweCAzcHg7XG4gIH1cbn1cblxuXG4uZGV0YWlscy1pdGVtIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICBtYXJnaW4tYmxvY2stc3RhcnQ6IDA7XG4gIG1hcmdpbi1ibG9jay1lbmQ6IDA7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwO1xuICBmbGV4OiAzMDtcblxuICAmIC5kZXRhaWxzLWl0ZW0td3JhcHBlciB7XG4gICAgcGFkZGluZzogMzBweDtcblxuICAgICY6aG92ZXIsICYuYWN0aXZlIHtcbiAgICAgIGJveC1zaGFkb3c6IHJnYmEoJG1kYy10aGVtZS1wcmltYXJ5LCAwLjIpIDBweCAwcHggNXB4O1xuICAgIH1cbiAgfVxuXG59XG5cbi5uby1yZXN1bHRzIHtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcblxuICAvLyBFeHBlcmltZW50YWwgdmFsdWVzLiBQbGF5IHdpdGggdGhlbSBpbiBvcmRlciB0byBzZXQgdXAgY29ycmVjdGx5XG4gIGhlaWdodDogNjQuNHZoO1xuICB3aWR0aDogOTkuOCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZmxleDogYXV0bztcbiAgbWF4LWhlaWdodDogMTAwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICRtZGMtdGhlbWUtc2Vjb25kYXJ5LWxpZ2h0O1xuXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG5cbiAgJiBzcGFuIHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEuNWVtO1xuICB9XG59IiwiJG1kYy10aGVtZS1wcmltYXJ5OiAjMDA1ZGI5O1xuJG1kYy10aGVtZS1zZWNvbmRhcnk6ICNkZGRkZGQ7XG4kbWRjLXRoZW1lLXNlY29uZGFyeS1saWdodDogI2Y1ZjVmNTtcbiRtZGMtdGhlbWUtYmFja2dyb3VuZDogI2ZmZjtcbiJdfQ== */"

/***/ }),

/***/ "./src/app/groups-list/groups-list.component.ts":
/*!******************************************************!*\
  !*** ./src/app/groups-list/groups-list.component.ts ***!
  \******************************************************/
/*! exports provided: GroupsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsListComponent", function() { return GroupsListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user/user.service */ "./src/app/user/user.service.ts");
/* harmony import */ var _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notifications/notifications.service */ "./src/app/notifications/notifications.service.ts");





var GroupsListComponent = /** @class */ (function () {
    function GroupsListComponent(userService, notificationsService) {
        this.filtersForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            groupName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100)]),
            groupOwner: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null),
        });
        this.creatingGroup = false;
        this.userService = userService;
        this.notificationsService = notificationsService;
        this.selectedGroup = null;
        this.currentIndex = null;
    }
    GroupsListComponent_1 = GroupsListComponent;
    GroupsListComponent.prototype.ngOnInit = function () {
        this.filterResults();
    };
    GroupsListComponent.prototype.filterResults = function () {
        this.clearSelection();
        var filters = {};
        filters['groupName'] = this.filtersForm.get('groupName').value;
        filters['groupOwner'] = GroupsListComponent_1.castToBool(this.filtersForm.get('groupOwner').value);
        this.fetchGroups(filters);
    };
    GroupsListComponent.prototype.clearFilters = function () {
        this.filtersForm.reset();
    };
    GroupsListComponent.prototype.selectGroup = function (event, group, index) {
        event.stopPropagation();
        if (this.currentIndex != null) {
            this.clearSelection();
        }
        this.selectIndex(index);
        this.selectedGroup = group;
        this.originalGroup = this.selectedGroup.serialize();
    };
    GroupsListComponent.prototype.clearSelection = function () {
        this.creatingGroup = false;
        if (this.selectedGroup && this.selectedGroup.serialize() != this.originalGroup) {
            // this.showUserChangedWarning();
        }
        else {
            if (this.currentIndex != null) {
                this.selectedGroup = null;
                this.unselectIndex();
            }
        }
    };
    GroupsListComponent.prototype.getSelectedGroup = function () {
        return this.selectedGroup;
    };
    GroupsListComponent.prototype.createGroup = function (event) {
        event.stopPropagation();
        this.creatingGroup = true;
    };
    GroupsListComponent.prototype.getGroups = function () {
        return this.groupList;
    };
    GroupsListComponent.prototype.hasResults = function () {
        return this.groupList && this.groupList.length > 0;
    };
    GroupsListComponent.prototype.shouldShowDetailsPane = function () {
        return this.getSelectedGroup() || this.creatingGroup;
    };
    GroupsListComponent.prototype.fetchGroups = function (filters) {
        var _this = this;
        if (filters == null) {
            filters = {};
        }
        this.userService.listGroups(filters['groupName'], filters['groupOwner']).then(function (groups) { return _this.groupList = groups; }).catch(function (error) { return _this.notificationsService.notify("Failed to obtain the groups", _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__["NotificationsService"].MESSAGE_TYPES.ERROR); });
    };
    GroupsListComponent.castToBool = function (choice) {
        if (choice == undefined) {
            return null;
        }
        else {
            return Number(choice);
        }
    };
    GroupsListComponent.prototype.unselectIndex = function () {
        var elements = document.getElementsByClassName("details-item-wrapper");
        var element = elements[this.currentIndex];
        element.classList.remove('active');
        this.currentIndex = null;
    };
    GroupsListComponent.prototype.selectIndex = function (index) {
        this.currentIndex = index;
        var elements = document.getElementsByClassName("details-item-wrapper");
        var element = elements[this.currentIndex];
        element.classList.add('active');
    };
    var GroupsListComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GroupsListComponent.prototype, "filtersForm", void 0);
    GroupsListComponent = GroupsListComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-groups-list',
            template: __webpack_require__(/*! ./groups-list.component.html */ "./src/app/groups-list/groups-list.component.html"),
            styles: [__webpack_require__(/*! ./groups-list.component.scss */ "./src/app/groups-list/groups-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__["NotificationsService"]])
    ], GroupsListComponent);
    return GroupsListComponent;
}());



/***/ }),

/***/ "./src/app/headedcontainer/headedcontainer.component.html":
/*!****************************************************************!*\
  !*** ./src/app/headedcontainer/headedcontainer.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"headed-container\" tabindex=\"-1\" #headedcontainer>\n    <div class=\"head\">\n        <ng-content  select=\".container-head\"></ng-content>\n    </div>\n\n    <div class=\"contributors-container\">\n        <ng-content select=\".container-content\"></ng-content>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/headedcontainer/headedcontainer.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/headedcontainer/headedcontainer.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".headed-container:focus {\n  outline: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9oZWFkZWRjb250YWluZXIvaGVhZGVkY29udGFpbmVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBYSxFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvaGVhZGVkY29udGFpbmVyL2hlYWRlZGNvbnRhaW5lci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZWQtY29udGFpbmVyOmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/headedcontainer/headedcontainer.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/headedcontainer/headedcontainer.component.ts ***!
  \**************************************************************/
/*! exports provided: HeadedcontainerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeadedcontainerComponent", function() { return HeadedcontainerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeadedcontainerComponent = /** @class */ (function () {
    function HeadedcontainerComponent() {
    }
    HeadedcontainerComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("headedcontainer"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], HeadedcontainerComponent.prototype, "view", void 0);
    HeadedcontainerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-headedcontainer',
            template: __webpack_require__(/*! ./headedcontainer.component.html */ "./src/app/headedcontainer/headedcontainer.component.html"),
            styles: [__webpack_require__(/*! ./headedcontainer.component.scss */ "./src/app/headedcontainer/headedcontainer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeadedcontainerComponent);
    return HeadedcontainerComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"header\">\n  <button mdc-button raised class=\"mdc-fab menu-button\" aria-label=\"menu\"\n          (click)=\"menuButtonClicked()\">\n    <i class=\"mdc-fab__icon material-icons\">menu</i>\n  </button>\n  <div id=\"header-icon\">\n    <a href=\"https://www.uib.eu/\" target=\"_blank\">\n      <img src=\"static/img/logo-uib-horizontal-40anys-white.svg\" alt=\"uib logo\">\n    </a>\n  </div>\n  <div id=\"header-text\">\n    <span>Metabolomics Analysis: Finding Out Metabolic Building Blocks</span>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#header {\n  width: 100%;\n  background-color: #222;\n  display: inline-flex; }\n\n#header-text {\n  color: #9d9d9d;\n  font-size: 1.3em;\n  width: 57%;\n  align-self: center;\n  text-align: center; }\n\n#header-icon {\n  width: 14%;\n  min-width: 200px;\n  margin-left: 1%; }\n\n#header-icon img {\n    width: 100%; }\n\n.menu-button {\n  margin: auto 2%;\n  min-width: 40px;\n  width: 40px;\n  height: 40px;\n  border-radius: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixvQkFBb0IsRUFBQTs7QUFHdEI7RUFDRSxjQUFjO0VBQ2QsZ0JBQWdCO0VBR2hCLFVBQVU7RUFHVixrQkFBa0I7RUFDbEIsa0JBQWtCLEVBQUE7O0FBR3BCO0VBR0UsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBTGpCO0lBVUksV0FBVSxFQUFBOztBQU1kO0VBQ0UsZUFBZTtFQUNmLGVBQTRCO0VBQzVCLFdBQXdCO0VBQ3hCLFlBQXlCO0VBQ3pCLGtCQUFrQixFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNoZWFkZXIge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIyMjtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG59XG5cbiNoZWFkZXItdGV4dCB7XG4gIGNvbG9yOiAjOWQ5ZDlkO1xuICBmb250LXNpemU6IDEuM2VtO1xuXG4gIC8vIFRoaXMga2VlcHMgdGhlIGhlYWRlciBtb3JlIG9yIGxlc3MgY2VudGVyZWRcbiAgd2lkdGg6IDU3JTtcblxuICAvLyBDZW50ZXIgdmVydGljYWxseVxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2hlYWRlci1pY29uIHtcblxuICAvLyBKdXN0IHVzZSB0aGUgMTMgcGVyY2VudCBvZiB0aGUgaGVhZGVyIHdpZHRoXG4gIHdpZHRoOiAxNCU7XG4gIG1pbi13aWR0aDogMjAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAxJTtcblxuXG4gIGltZyB7XG4gICAgLy8gRmlsbCB0aGUgd2hvbGUgd3JhcHBlclxuICAgIHdpZHRoOjEwMCU7XG4gIH1cbn1cblxuJGJ1dHRvbi1zaXplOiA0MDtcblxuLm1lbnUtYnV0dG9uIHtcbiAgbWFyZ2luOiBhdXRvIDIlO1xuICBtaW4td2lkdGg6ICRidXR0b24tc2l6ZSArIHB4O1xuICB3aWR0aDogJGJ1dHRvbi1zaXplICsgcHg7XG4gIGhlaWdodDogJGJ1dHRvbi1zaXplICsgcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _menu_menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../menu/menu.service */ "./src/app/menu/menu.service.ts");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(menuService) {
        this.menuService = null;
        this.menuService = menuService;
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.menuButtonClicked = function () {
        this.menuService.toggle();
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_menu_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content-container\">\n\n    <app-headedcontainer>\n        <span class=\"container-head\">\n            Paper summary\n        </span>\n\n\n        <p class=\"container-content content\">\n            In this paper we propose a new methodology for the analysis of metabolic networks. We use the notion of\n            strongly connected components of a graph, called in this context metabolic building blocks (MBB). Every\n            strongly connected component is contracted to a single node in such a way that the resulting graph is a\n            directed acyclic graph, called a m-DAG, with a considerably reduced number of nodes.\n            <br>\n            The property of being a directed acyclic graph brings out a background graph topology that reveals the\n            connectivity of the metabolic network, as well as bridges, isolated nodes and cut nodes. Altogether, it\n            becomes a key information for the discovery of functional metabolic relations. Moreover, our methodology\n            is general enough to work on any database.\n            <br>\n            We can measure the similarity of the different m-DAG and then we can build a tree of life for a given\n            metabolic pathway.\n        </p>\n    </app-headedcontainer>\n\n\n    <app-headedcontainer>\n        <span class=\"container-head\">\n            Authors information\n        </span>\n\n        <div class=\"container-content\">\n            <a href=\"http://bioinfo.uib.es/\" class=\"bioinfo-logo\">\n                <img src=\"static/img/bioinfo.png\">\n            </a>\n            <app-undecoratedlist [listElements]=\"contributors\"></app-undecoratedlist>\n        </div>\n    </app-headedcontainer>\n</div>\n"

/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content-container {\n  display: flex;\n  margin-top: 10px; }\n\n.main-text-container {\n  min-width: 50%;\n  width: 87%;\n  max-width: 87%; }\n\n.content {\n  padding: 0 10px; }\n\n.bioinfo-logo {\n  padding-top: 16px;\n  display: block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2IsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsY0FBYztFQUNkLFVBQVU7RUFDVixjQUFjLEVBQUE7O0FBSWhCO0VBQ0UsZUFBZSxFQUFBOztBQUdqQjtFQUNFLGlCQUFpQjtFQUNqQixjQUFjLEVBQUEiLCJmaWxlIjoiLi4vc3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGVudC1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4ubWFpbi10ZXh0LWNvbnRhaW5lciB7XG4gIG1pbi13aWR0aDogNTAlO1xuICB3aWR0aDogODclO1xuICBtYXgtd2lkdGg6IDg3JTtcblxufVxuXG4uY29udGVudCB7XG4gIHBhZGRpbmc6IDAgMTBweDtcbn1cblxuLmJpb2luZm8tbG9nbyB7XG4gIHBhZGRpbmctdG9wOiAxNnB4O1xuICBkaXNwbGF5OiBibG9jaztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        this.contributors = [
            "Ricardo Alberich",
            "José A. Castro",
            "Mercè Llabrés",
            "Pere Palmer Rodríguez",
        ];
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!isSignedIn()\">\n    <a (click)=\"onSignInClicked()\" class=\"login remark-link\">\n        Sign In\n    </a>\n    or\n    <a id=\"signup\" (click)=\"onSignUpClicked()\" class=\"remark-link\">\n        Sign Up\n    </a>\n</div>\n\n<div *ngIf=\"isSignedIn()\">\n    <a (click)=\"onSignOutClicked()\">Sign out</a>\n</div>\n\n<div id=\"login-container\" class=\"separate hidden\" #loginContainer>\n    <label for=\"user\">Username</label>\n    <input class=\"{{getFailureClasses()}}\" type=\"text\" id=\"user\" name=\"user\" [(ngModel)]=\"user\">\n    <!-- this password should be a generic component -->\n    <label for=\"password\">Password</label>\n    <input class=\"{{getFailureClasses()}}\" type=\"password\" id=\"password\" name=\"password\" [(ngModel)]=\"password\">\n    <input type=\"submit\" (click)=\"signIn()\">\n</div>\n\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hidden {\n  display: none; }\n\n.separate {\n  padding-top: 5px; }\n\n.failed-style {\n  border-color: red; }\n\n.remark-link {\n  color: #005db9; }\n\n.remark-link:hover {\n    font-weight: 900; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL19tYXRlcmlhbHZhcnMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNDZnlCLEVBQUE7O0FEYzNCO0lBSUksZ0JBQWdCLEVBQUEiLCJmaWxlIjoiLi4vc3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi9tYXRlcmlhbHZhcnNcIjtcblxuLmhpZGRlbiB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5zZXBhcmF0ZSB7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG59XG5cbi5mYWlsZWQtc3R5bGUge1xuICBib3JkZXItY29sb3I6IHJlZDtcbn1cblxuLnJlbWFyay1saW5rIHtcbiAgY29sb3I6ICRtZGMtdGhlbWUtcHJpbWFyeTtcblxuICAmOmhvdmVyIHtcbiAgICBmb250LXdlaWdodDogOTAwO1xuICB9XG59XG5cbiIsIiRtZGMtdGhlbWUtcHJpbWFyeTogIzAwNWRiOTtcbiRtZGMtdGhlbWUtc2Vjb25kYXJ5OiAjZGRkZGRkO1xuJG1kYy10aGVtZS1zZWNvbmRhcnktbGlnaHQ6ICNmNWY1ZjU7XG4kbWRjLXRoZW1lLWJhY2tncm91bmQ6ICNmZmY7XG4iXX0= */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user/user.service */ "./src/app/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _url_permission_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url-permission.module */ "./src/app/url-permission.module.ts");
/* harmony import */ var _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../notifications/notifications.service */ "./src/app/notifications/notifications.service.ts");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(renderer, userService, router, notificationsService) {
        this.userService = userService;
        this.notificationsService = notificationsService;
        this.renderer = renderer;
        this.router = router;
        this.isLoggedIn = false;
        this.failedLastAttempt = false;
        this.onLogout = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onLogin = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.isSignedIn = function () {
        return this.userService.isSignedIn();
    };
    /**
     * Try to sign in and if it's a successful one, just clear the fields and
     * show the sign out option.
     */
    LoginComponent.prototype.signIn = function () {
        var _this = this;
        console.log(this.user, this.password);
        this.userService.signIn(this.user, this.password)
            .then(function (user) {
            _this.user = null;
            _this.password = null;
            _this.failedLastAttempt = false;
            _this.isLoggedIn = true;
            _this.renderer.addClass(_this.loginContainer.nativeElement, "hidden");
            _this.onLogin.emit(user);
        }).catch(function (error) {
            if (error.status < 500) {
                _this.notificationsService.notify("Wrong user or password", _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_5__["NotificationsService"].MESSAGE_TYPES.WARNING);
            }
            else {
                _this.notificationsService.notify("Sorry! We've failed to identify you", _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_5__["NotificationsService"].MESSAGE_TYPES.ERROR);
            }
            _this.failedLastAttempt = true;
        });
    };
    LoginComponent.prototype.onSignOutClicked = function () {
        var _this = this;
        this.userService.signOut().then(function (response) {
            _this.onLogout.emit();
            var urlSegments = _this.router.url.split("/").filter(function (e) {
                return e != "" && e != null;
            });
            if (!_url_permission_module__WEBPACK_IMPORTED_MODULE_4__["PermissionModule"].canNavigate([], urlSegments)) {
                _this.router.navigate(['/home']);
            }
        });
    };
    LoginComponent.prototype.onSignInClicked = function () {
        this.renderer.removeClass(this.loginContainer.nativeElement, "hidden");
    };
    LoginComponent.prototype.onSignUpClicked = function () {
        var extras = {
            queryParams: {
                redirect: this.router.url
            }
        };
        this.router.navigate(['/signup'], extras);
    };
    LoginComponent.prototype.getFailureClasses = function () {
        if (this.failedLastAttempt) {
            return 'failed-style';
        }
        return '';
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], LoginComponent.prototype, "onLogin", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], LoginComponent.prototype, "onLogout", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("loginContainer"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], LoginComponent.prototype, "loginContainer", void 0);
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_5__["NotificationsService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/menu/menu.component.html":
/*!******************************************!*\
  !*** ./src/app/menu/menu.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"{{ getStyles() }}\">\n    <div class=\"mdc-drawer__header user-info-container\">\n        <div class=\"user-label\">\n            Welcome, <span id=\"user-id\">{{getUserName()}}</span>\n        </div>\n        <app-login></app-login>\n    </div>\n    <hr class=\"mdc-list-divider\">\n    <div class=\"mdc-drawer__content\">\n        <div class=\"mdc-list\">\n            <app-menuitem class=\"clear-outline\"\n                          *ngFor=\"let entry of visibleEntries\"\n                          [icon]=\"entry.icon\" [label]=\"entry.label\"\n                          [routerLink]=\"entry.url\">\n            </app-menuitem>\n        </div>\n    </div>\n</aside>"

/***/ }),

/***/ "./src/app/menu/menu.component.scss":
/*!******************************************!*\
  !*** ./src/app/menu/menu.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mdc-drawer {\n  z-index: 19;\n  position: relative; }\n\n.open {\n  display: flex; }\n\n.center, .user-info-container {\n  text-align: center; }\n\n.user-info-container {\n  margin-bottom: 10px; }\n\n.user-image {\n  display: block;\n  border-radius: 100%;\n  max-width: 150px;\n  margin: 0 auto; }\n\n.user-label {\n  margin: 10px auto 0 auto; }\n\n.login {\n  margin-left: 3px; }\n\n.clear-outline:focus {\n  outline: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9tZW51L21lbnUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0UsYUFBYSxFQUFBOztBQUdmO0VBQ0Usa0JBQWtCLEVBQUE7O0FBR3BCO0VBRUUsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUdoQjtFQUNFLHdCQUF3QixFQUFBOztBQUcxQjtFQUNFLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGFBQWEsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL21lbnUvbWVudS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tZGMtZHJhd2VyIHtcbiAgei1pbmRleDogMTk7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm9wZW4ge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uY2VudGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udXNlci1pbmZvLWNvbnRhaW5lciB7XG4gIEBleHRlbmQgLmNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLnVzZXItaW1hZ2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgbWF4LXdpZHRoOiAxNTBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi51c2VyLWxhYmVsIHtcbiAgbWFyZ2luOiAxMHB4IGF1dG8gMCBhdXRvO1xufVxuXG4ubG9naW4ge1xuICBtYXJnaW4tbGVmdDogM3B4O1xufVxuXG4uY2xlYXItb3V0bGluZTpmb2N1cyB7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi8vIG9ubHkgYXBwbHkgdGhpcyBzdHlsZSBpZiBiZWxvdyB0b3AgYXBwIGJhclxuLy8ubWRjLXRvcC1hcHAtYmFyIHtcbi8vICB6LWluZGV4OiA3O1xuLy99Il19 */"

/***/ }),

/***/ "./src/app/menu/menu.component.ts":
/*!****************************************!*\
  !*** ./src/app/menu/menu.component.ts ***!
  \****************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./menu.service */ "./src/app/menu/menu.service.ts");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user/user.service */ "./src/app/user/user.service.ts");
/* harmony import */ var _url_permission_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../url-permission.module */ "./src/app/url-permission.module.ts");





var MenuComponent = /** @class */ (function () {
    function MenuComponent(menuService, userService) {
        var _this = this;
        this.menuService = null;
        this.isOpened = false;
        this.menuEntries = [
            {
                "icon": "home",
                "label": "Home",
                "url": "home",
            },
            {
                "icon": "science",
                "label": "Experiments",
                "url": "experiments",
            },
            // Admin entries
            {
                "icon": "person",
                "label": "User management",
                "url": "user-management",
            },
            // TODO Implement these entries
            {
                "icon": "groups",
                "label": "Groups",
                "url": "groups",
            },
            {
                "icon": "contact_support",
                "label": "Contact",
                "url": "contacto",
            },
        ];
        this.menuService = menuService;
        this.menuService.change.subscribe(function (newValue) { return _this.onStateChangeRequested(newValue); });
        this.userService = userService;
        this.user = this.userService.getUser();
        this.visibleEntries = this.getMenuEntries();
        this.userService.onLogin.subscribe(function (user) { return _this.onLogin(user); });
        this.userService.onLogout.subscribe(function () { return _this.onLogout(); });
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    MenuComponent.prototype.onStateChangeRequested = function (newState) {
        this.isOpened = newState;
    };
    MenuComponent.prototype.getStyles = function () {
        var classes = "mdc-drawer mdc-drawer--dismissible" +
            " mdc-top-app-bar--fixed-adjust inline-drawer";
        return classes + (this.isOpened ? " open" : "");
    };
    MenuComponent.prototype.getMenuEntries = function () {
        var visibleEntries = [];
        var isAdmin = false;
        var showEntry;
        var permissions = [];
        if (this.user) {
            permissions = this.user.getPermissions();
            isAdmin = this.user.isAdmin;
        }
        for (var _i = 0, _a = this.menuEntries; _i < _a.length; _i++) {
            var entry = _a[_i];
            showEntry = _url_permission_module__WEBPACK_IMPORTED_MODULE_4__["PermissionModule"].canNavigate(permissions, [entry["url"]]);
            if (isAdmin || showEntry) {
                visibleEntries.push(entry);
            }
        }
        return visibleEntries;
    };
    MenuComponent.prototype.getUserName = function () {
        if (this.user) {
            return this.user.username;
        }
        return 'Unknown';
    };
    MenuComponent.prototype.onLogin = function (user) {
        this.user = user;
        this.visibleEntries = this.getMenuEntries();
    };
    MenuComponent.prototype.onLogout = function () {
        this.user = null;
        this.visibleEntries = this.getMenuEntries();
    };
    MenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.scss */ "./src/app/menu/menu.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"], _user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/menu/menu.service.ts":
/*!**************************************!*\
  !*** ./src/app/menu/menu.service.ts ***!
  \**************************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MenuService = /** @class */ (function () {
    function MenuService() {
        this.isOpen = false;
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    MenuService.prototype.toggle = function () {
        if (this.isOpen) {
            this.close();
        }
        else {
            this.open();
        }
    };
    MenuService.prototype.open = function () {
        this.setOpenStatus(true);
    };
    MenuService.prototype.close = function () {
        this.setOpenStatus(false);
    };
    MenuService.prototype.setOpenStatus = function (isOpen) {
        this.isOpen = isOpen;
        this.change.emit(isOpen);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MenuService.prototype, "change", void 0);
    MenuService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        })
    ], MenuService);
    return MenuService;
}());



/***/ }),

/***/ "./src/app/menuitem/menuitem.component.html":
/*!**************************************************!*\
  !*** ./src/app/menuitem/menuitem.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<a class=\"mdc-list-item\"  [attr.href]=\"url\" >\n    <i class=\"material-icons mdc-list-item__graphic\" aria-hidden=\"true\">{{ icon }}</i>\n    <span class=\"mdc-list-item__text\"> {{ label }} </span>\n</a>"

/***/ }),

/***/ "./src/app/menuitem/menuitem.component.scss":
/*!**************************************************!*\
  !*** ./src/app/menuitem/menuitem.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIuLi9zcmMvYXBwL21lbnVpdGVtL21lbnVpdGVtLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/menuitem/menuitem.component.ts":
/*!************************************************!*\
  !*** ./src/app/menuitem/menuitem.component.ts ***!
  \************************************************/
/*! exports provided: MenuitemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuitemComponent", function() { return MenuitemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MenuitemComponent = /** @class */ (function () {
    function MenuitemComponent() {
        this.icon = null;
        this.label = null;
        this.url = null;
    }
    MenuitemComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MenuitemComponent.prototype, "icon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MenuitemComponent.prototype, "label", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MenuitemComponent.prototype, "url", void 0);
    MenuitemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menuitem',
            template: __webpack_require__(/*! ./menuitem.component.html */ "./src/app/menuitem/menuitem.component.html"),
            styles: [__webpack_require__(/*! ./menuitem.component.scss */ "./src/app/menuitem/menuitem.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MenuitemComponent);
    return MenuitemComponent;
}());



/***/ }),

/***/ "./src/app/modal-window/modal-window.component.html":
/*!**********************************************************!*\
  !*** ./src/app/modal-window/modal-window.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-wrapper\" [hidden]=\"!isOpen\">\n    <div class=\"modal-overlay\" (click)=\"close($event)\"></div>\n    <div class=\"modal-content {{getModalTypeDependenStyles()}}\">\n        {{ getContent() }}\n        <div class=\"buttons\" *ngIf=\"showButtons()\">\n            <button class=\"button confirm\" mdc-button raised>\n                {{getConfirmText()}}\n            </button>\n            <button *ngIf=\"needsRejectButton()\" class=\"button reject\" mdc-button raised>\n                {{getRejectText()}}\n            </button>\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "./src/app/modal-window/modal-window.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/modal-window/modal-window.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modal-wrapper {\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  z-index: 100; }\n\n.modal-overlay {\n  width: 100vw;\n  height: 100vh;\n  position: absolute;\n  background: black;\n  opacity: 0.5; }\n\n.modal-content {\n  position: absolute;\n  background: white; }\n\n.modal-content.viewer-modal {\n    margin: 5vh 2vw;\n    width: 96vw;\n    height: 90vh; }\n\n.modal-content.text-modal {\n    width: 50%;\n    height: 30%;\n    left: 50%;\n    top: 50%;\n    transform: translateX(-50%) translateY(-70%); }\n\n.modal-content .buttons {\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-end; }\n\n.modal-content .buttons .button {\n      margin: 10px 0;\n      margin-right: 10px; }\n\n.modal-content .buttons .button.reject {\n        background-color: red; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9tb2RhbC13aW5kb3cvbW9kYWwtd2luZG93LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFlBQVksRUFBQTs7QUFHZDtFQUNFLGtCQUFrQjtFQUNsQixpQkFBaUIsRUFBQTs7QUFGbkI7SUFLSSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVksRUFBQTs7QUFQaEI7SUFXSSxVQUFVO0lBQ1YsV0FBVztJQUNYLFNBQVM7SUFDVCxRQUFRO0lBQ1IsNENBQTRDLEVBQUE7O0FBZmhEO0lBbUJJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIseUJBQXlCLEVBQUE7O0FBckI3QjtNQXlCTSxjQUFjO01BQ2Qsa0JBQWtCLEVBQUE7O0FBMUJ4QjtRQTZCUSxxQkFBcUIsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL21vZGFsLXdpbmRvdy9tb2RhbC13aW5kb3cuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubW9kYWwtd3JhcHBlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDEwMDtcbn1cblxuLm1vZGFsLW92ZXJsYXkge1xuICB3aWR0aDogMTAwdnc7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIG9wYWNpdHk6IDAuNTtcbn1cblxuLm1vZGFsLWNvbnRlbnQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuXG4gICYudmlld2VyLW1vZGFsIHtcbiAgICBtYXJnaW46IDV2aCAydnc7XG4gICAgd2lkdGg6IDk2dnc7XG4gICAgaGVpZ2h0OiA5MHZoO1xuICB9XG5cbiAgJi50ZXh0LW1vZGFsIHtcbiAgICB3aWR0aDogNTAlO1xuICAgIGhlaWdodDogMzAlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0b3A6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgdHJhbnNsYXRlWSgtNzAlKTtcbiAgfVxuXG4gICYgLmJ1dHRvbnMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuXG4gICAgJiAuYnV0dG9uIHtcblxuICAgICAgbWFyZ2luOiAxMHB4IDA7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cbiAgICAgICYucmVqZWN0IHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICAgICAgfVxuICAgIH1cbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/modal-window/modal-window.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modal-window/modal-window.component.ts ***!
  \********************************************************/
/*! exports provided: ModalWindowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalWindowComponent", function() { return ModalWindowComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _modal_window_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-window.service */ "./src/app/modal-window/modal-window.service.ts");



var ModalWindowComponent = /** @class */ (function () {
    function ModalWindowComponent(modalWindowService) {
        var _this = this;
        this.isOpen = false;
        this.ESC_CODES = new Set(['esc', 'escape']);
        this.modalWindowService = modalWindowService;
        this.modalWindowService.change.subscribe(function (newValue) { return _this.changeViewStatus(newValue["open"], newValue["modalType"], newValue["modalSetup"]); });
    }
    ModalWindowComponent.prototype.ngOnInit = function () {
    };
    ModalWindowComponent.prototype.changeViewStatus = function (isOpen, modalType, modalSetup) {
        this.isOpen = isOpen;
        this.modalType = modalType;
        this.modalSetup = modalSetup;
    };
    ModalWindowComponent.prototype.getContent = function () {
        var content = '';
        if (this.contentInterface != null) {
            content = this.contentInterface.getContent();
        }
        return content;
    };
    ModalWindowComponent.prototype.getModalTypeDependenStyles = function () {
        if (this.modalType == _modal_window_service__WEBPACK_IMPORTED_MODULE_2__["ModalWindowService"].MODAL_TYPES.VIEWER) {
            return "viewer-modal";
        }
        else {
            return "text-modal";
        }
    };
    ModalWindowComponent.prototype.showButtons = function () {
        return this.modalType !== _modal_window_service__WEBPACK_IMPORTED_MODULE_2__["ModalWindowService"].MODAL_TYPES.VIEWER;
    };
    ModalWindowComponent.prototype.getConfirmText = function () {
        var text = this.modalSetup && this.modalSetup["confirmButtonText"];
        return text || 'Confirm';
    };
    ModalWindowComponent.prototype.getRejectText = function () {
        var text = this.modalSetup && this.modalSetup["rejectButtonText"];
        return text || 'Cancel';
    };
    /**
     * Returns true whenever the user has requested a modal that needs two
     * buttons ( This is the confirmation one )
     */
    ModalWindowComponent.prototype.needsRejectButton = function () {
        return this.modalType === _modal_window_service__WEBPACK_IMPORTED_MODULE_2__["ModalWindowService"].MODAL_TYPES.CONFIRMATION;
    };
    /**
     * Host listener is used to listen for the escape key.
     *
     * Close the modal whenever the requester is a click on the overlay or the
     * user has clicked the esc button
     */
    ModalWindowComponent.prototype.close = function ($event) {
        if (this.hasPressedCloseKeys($event)) {
            this.modalWindowService.close();
        }
    };
    ModalWindowComponent.prototype.hasPressedCloseKeys = function (event) {
        if (!event) {
            return false;
        }
        var code = event.code || '';
        return this.ESC_CODES.has(code.toLowerCase());
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ModalWindowComponent.prototype, "contentInterface", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:keyup', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], ModalWindowComponent.prototype, "close", null);
    ModalWindowComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-window',
            template: __webpack_require__(/*! ./modal-window.component.html */ "./src/app/modal-window/modal-window.component.html"),
            styles: [__webpack_require__(/*! ./modal-window.component.scss */ "./src/app/modal-window/modal-window.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_modal_window_service__WEBPACK_IMPORTED_MODULE_2__["ModalWindowService"]])
    ], ModalWindowComponent);
    return ModalWindowComponent;
}());



/***/ }),

/***/ "./src/app/modal-window/modal-window.service.ts":
/*!******************************************************!*\
  !*** ./src/app/modal-window/modal-window.service.ts ***!
  \******************************************************/
/*! exports provided: ModalWindowService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalWindowService", function() { return ModalWindowService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ModalWindowService = /** @class */ (function () {
    function ModalWindowService() {
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    /**
     * ModalContent is a setup object that holds up to 4 params:
     *  - modalType: MODAL_TYPES Enum showing the kind of modal needs to be
     *              opened
     *  - modalContent: Modal main content.
     *  - confirmButtonText <Optional>: Text that needs to be shown in confirm
     *              button for both Info and confirmation modals
     *  - rejectButtonText <Optional>: Text that needs to be shown reject
     *              button for both Info and confirmation modals
     *
     * @param modalType
     * @param modalSetup
     */
    ModalWindowService.prototype.open = function (modalType, modalSetup) {
        var value = {
            open: true,
            modalType: modalType,
            modalSetup: modalSetup
        };
        this.change.emit(value);
    };
    ModalWindowService.prototype.close = function () {
        var value = {
            open: false,
        };
        this.change.emit(value);
    };
    ModalWindowService.MODAL_TYPES = {
        INFO: "I",
        CONFIRMATION: "C",
        VIEWER: "V"
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ModalWindowService.prototype, "change", void 0);
    ModalWindowService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ModalWindowService);
    return ModalWindowService;
}());



/***/ }),

/***/ "./src/app/models/experiment.model.ts":
/*!********************************************!*\
  !*** ./src/app/models/experiment.model.ts ***!
  \********************************************/
/*! exports provided: Experiment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Experiment", function() { return Experiment; });
var Experiment = /** @class */ (function () {
    function Experiment(jsonData) {
        this._id = jsonData['id'];
        this._name = jsonData['name'];
        this._description = jsonData['description'];
        this._experimentGroups = jsonData['experiment_groups'];
        this._status = jsonData['status'];
        this._creationDate = new Date(jsonData['creation_date']);
        this._modificationDate = new Date(jsonData['modification_date']);
        this._owner = jsonData['owner'];
        this._public = jsonData['visible_for'];
    }
    Object.defineProperty(Experiment.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Experiment.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Experiment.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (description) {
            this._description = description;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Experiment.prototype, "status", {
        get: function () {
            return this._status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Experiment.prototype, "owner", {
        get: function () {
            return this._owner;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Experiment.prototype, "public", {
        get: function () {
            return this._public;
        },
        set: function (value) {
            console.log(this._id, value);
            this._public = value;
        },
        enumerable: true,
        configurable: true
    });
    Experiment.prototype.getCreationDateInMaterialFormat = function () {
        return Experiment.getMaterialDate(this._creationDate);
    };
    Experiment.prototype.getModificationDateInMaterialFormat = function () {
        return Experiment.getMaterialDate(this._modificationDate);
    };
    Experiment.getMaterialDate = function (date) {
        var year = date.getFullYear();
        var month = ("0" + (date.getMonth() + 1)).slice(-2);
        var day = ("0" + date.getDate()).slice(-2);
        return year + '-' + month + '-' + day;
    };
    return Experiment;
}());



/***/ }),

/***/ "./src/app/notifications/notifications.component.html":
/*!************************************************************!*\
  !*** ./src/app/notifications/notifications.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"notifications\" class=\"messages-container\">\n    <div class=\"message {{message['type']}}\" *ngFor=\"let message of\n        messages\" (dblclick)=\"removeMessage(message)\">\n        <span>{{ message['message'] }}</span>\n    </div>\n    <div class=\"modal--overlay\"></div>\n    <div class=\"modal--message\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/notifications/notifications.component.scss":
/*!************************************************************!*\
  !*** ./src/app/notifications/notifications.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".messages-container {\n  display: flex;\n  flex-direction: column;\n  position: fixed;\n  z-index: 999999999;\n  top: 0;\n  left: 50%;\n  transform: translateX(-50%);\n  padding: 10px;\n  max-width: 50%;\n  width: 50%; }\n\n.message {\n  padding: 8px;\n  margin: 3px 0;\n  font-weight: bold;\n  border-width: 2px;\n  border-style: solid; }\n\n.message.warning {\n    background-color: #fffeda;\n    border-color: #ecd213; }\n\n.message.error {\n    background-color: #ffe2e2;\n    border-color: red; }\n\n.message.info {\n    background-color: white;\n    border-color: #005db9; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbnMuY29tcG9uZW50LnNjc3MiLCIvaG9tZS9qZXN1cy9Eb2N1bWVudHMvcmVwb3NpdG9yaWVzL3RmZy9tZGFnL3Byb2plY3QvbURhZ0Zyb250L3NyYy9fbWF0ZXJpYWx2YXJzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFNBQVM7RUFDVCwyQkFBMkI7RUFDM0IsYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVLEVBQUE7O0FBR1o7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsbUJBQW1CLEVBQUE7O0FBTHJCO0lBU0kseUJBQXlCO0lBQ3pCLHFCQUFxQixFQUFBOztBQVZ6QjtJQWNJLHlCQUF5QjtJQUN6QixpQkFBaUIsRUFBQTs7QUFmckI7SUFtQkksdUJBQXVCO0lBQ3ZCLHFCQ25DdUIsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi9tYXRlcmlhbHZhcnNcIjtcblxuLm1lc3NhZ2VzLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogOTk5OTk5OTk5O1xuICB0b3A6IDA7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICBwYWRkaW5nOiAxMHB4O1xuICBtYXgtd2lkdGg6IDUwJTtcbiAgd2lkdGg6IDUwJTtcbn1cblxuLm1lc3NhZ2Uge1xuICBwYWRkaW5nOiA4cHg7XG4gIG1hcmdpbjogM3B4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBib3JkZXItd2lkdGg6IDJweDtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcblxuICAmLndhcm5pbmcge1xuICAgIC8vIFllbGxvd2lzaCBjb2xvclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZlZGE7XG4gICAgYm9yZGVyLWNvbG9yOiAjZWNkMjEzO1xuICB9XG5cbiAgJi5lcnJvciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTJlMjtcbiAgICBib3JkZXItY29sb3I6IHJlZDtcbiAgfVxuXG4gICYuaW5mbyB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLWNvbG9yOiAkbWRjLXRoZW1lLXByaW1hcnk7XG4gIH1cbn0iLCIkbWRjLXRoZW1lLXByaW1hcnk6ICMwMDVkYjk7XG4kbWRjLXRoZW1lLXNlY29uZGFyeTogI2RkZGRkZDtcbiRtZGMtdGhlbWUtc2Vjb25kYXJ5LWxpZ2h0OiAjZjVmNWY1O1xuJG1kYy10aGVtZS1iYWNrZ3JvdW5kOiAjZmZmO1xuIl19 */"

/***/ }),

/***/ "./src/app/notifications/notifications.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/notifications/notifications.component.ts ***!
  \**********************************************************/
/*! exports provided: NotificationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsComponent", function() { return NotificationsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _notifications_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notifications.service */ "./src/app/notifications/notifications.service.ts");



var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(notificationsService) {
        this.notificationsService = notificationsService;
    }
    NotificationsComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(NotificationsComponent.prototype, "messages", {
        get: function () {
            return this.notificationsService.getMessages();
        },
        enumerable: true,
        configurable: true
    });
    NotificationsComponent.prototype.removeMessage = function (message) {
        this.notificationsService.remove(message);
    };
    NotificationsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-notifications',
            template: __webpack_require__(/*! ./notifications.component.html */ "./src/app/notifications/notifications.component.html"),
            styles: [__webpack_require__(/*! ./notifications.component.scss */ "./src/app/notifications/notifications.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_notifications_service__WEBPACK_IMPORTED_MODULE_2__["NotificationsService"]])
    ], NotificationsComponent);
    return NotificationsComponent;
}());



/***/ }),

/***/ "./src/app/notifications/notifications.service.ts":
/*!********************************************************!*\
  !*** ./src/app/notifications/notifications.service.ts ***!
  \********************************************************/
/*! exports provided: NotificationsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsService", function() { return NotificationsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NotificationsService = /** @class */ (function () {
    function NotificationsService() {
        // TODO Filled just for test purposes
        this.messages = [
        // {type: "info", message: "hola"},
        // {type: "warning", message: "que tal estass"},
        // {
        //     type: "warning",
        //     message: "me gustaría que me hablarascon calma y sosiego"
        // },
        // {type: "warning", message: "aqui un mensaje corto"},
        //
        // {
        //     type: "error",
        //     message: "ahora un mensaje un poquito más largo" +
        //         " que todo el resto ya que" +
        //         " tenemos que ver si todo se ajusta al tamaño o incluso splitea" +
        //         " frases"
        // }
        ];
    }
    NotificationsService_1 = NotificationsService;
    NotificationsService.prototype.getMessages = function () {
        return this.messages;
    };
    NotificationsService.prototype.notify = function (message, type) {
        if (!type) {
            type = NotificationsService_1.MESSAGE_TYPES.INFO;
        }
        this.messages.push({
            type: type,
            message: message
        });
    };
    /**
     * Remove notification
     */
    NotificationsService.prototype.remove = function (message) {
        var index = this.messages.findIndex(function (value) { return value == message; });
        var messagesCount = this.messages.length;
        if (index > -1 && messagesCount > index) {
            console.log("Removing message");
            this.messages.splice(index, 1);
        }
    };
    var NotificationsService_1;
    NotificationsService.MESSAGE_TYPES = {
        INFO: 'info',
        WARNING: 'warning',
        ERROR: 'error'
    };
    NotificationsService = NotificationsService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NotificationsService);
    return NotificationsService;
}());



/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.html":
/*!**********************************************************!*\
  !*** ./src/app/pagenotfound/pagenotfound.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  pagenotfound works!\n  <button (click)=\"onClick()\">go back to home</button>\n</p>\n"

/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pagenotfound/pagenotfound.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIuLi9zcmMvYXBwL3BhZ2Vub3Rmb3VuZC9wYWdlbm90Zm91bmQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pagenotfound/pagenotfound.component.ts ***!
  \********************************************************/
/*! exports provided: PagenotfoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagenotfoundComponent", function() { return PagenotfoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var PagenotfoundComponent = /** @class */ (function () {
    function PagenotfoundComponent(router) {
        this.router = router;
    }
    PagenotfoundComponent.prototype.ngOnInit = function () {
    };
    PagenotfoundComponent.prototype.onClick = function () {
        this.router.navigate(["/home"]);
    };
    PagenotfoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pagenotfound',
            template: __webpack_require__(/*! ./pagenotfound.component.html */ "./src/app/pagenotfound/pagenotfound.component.html"),
            styles: [__webpack_require__(/*! ./pagenotfound.component.scss */ "./src/app/pagenotfound/pagenotfound.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PagenotfoundComponent);
    return PagenotfoundComponent;
}());



/***/ }),

/***/ "./src/app/searchfilter/searchfilter.component.html":
/*!**********************************************************!*\
  !*** ./src/app/searchfilter/searchfilter.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"multiselect-filter-container multiselect-filter-container-multi\">\n    <div class=\"multiselect-wrapper\">\n        <label for=\"user-input\"></label>\n        <ul class=\"chosen-choices\">\n            <li *ngFor=\"let element of selectedData\" class=\"search-choice\">\n                <span>{{ element.display }}</span>\n                <i class=\"material-icons mdc-list-item__graphic search-choice-close\" aria-hidden=\"true\"\n                   (click)=\"removeElement($event, element)\">\n                    close\n                </i>\n            </li>\n        </ul>\n\n        <label>\n            <input class=\"user-input\" type=\"text\" name=\"user-input\"\n                   (keyup)=\"filterResults()\" [(ngModel)]=\"filterValue\">\n        </label>\n    </div>\n    <div class=\"options-drop\">\n        <ul class=\"chosen-results\">\n            <!-- Dynamically filled -->\n            <li *ngFor=\"let element of filteredData\" [ngClass]=\"{\n                'active-result': !alreadySelected(element)\n            }\" (click)=\"addElement($event, element)\">\n                {{ element.display }}\n            </li>\n\n        </ul>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/searchfilter/searchfilter.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/searchfilter/searchfilter.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".user-input {\n  width: 100%;\n  border: solid 0 black;\n  line-height: 1.2rem;\n  outline: none; }\n\n.multiselect-filter-container ul {\n  margin: 0; }\n\n.multiselect-filter-container li {\n  list-style: none; }\n\n.multiselect-filter-container .multiselect-wrapper {\n  border-radius: 4px;\n  border: 1px solid black; }\n\n.multiselect-filter-container-multi {\n  position: relative; }\n\n.multiselect-filter-container-multi .chosen-choices {\n    border-radius: 4px;\n    box-shadow: none;\n    min-height: 2.5rem;\n    display: flex;\n    flex-flow: wrap row;\n    padding: 0; }\n\n.multiselect-filter-container-multi .chosen-choices li.search-field input[type=\"text\"] {\n      font-size: 1.3rem;\n      height: 2.5rem;\n      min-height: initial; }\n\n.multiselect-filter-container-multi .chosen-choices li.search-choice {\n      color: black;\n      position: relative;\n      background: #eeeeee;\n      border: solid 1px black;\n      border-radius: 3px;\n      padding: 1px 17px 1px 3px;\n      margin: 2px 2px;\n      height: 100%; }\n\n.multiselect-filter-container-multi .chosen-choices li.search-choice span {\n        padding-right: 5px; }\n\n.multiselect-filter-container-multi .chosen-choices li.search-choice .search-choice-close {\n        position: absolute;\n        margin: 0;\n        width: auto;\n        height: auto;\n        font-size: 15px;\n        color: black; }\n\n.multiselect-filter-container-multi .chosen-choices li.search-choice .search-choice-close:hover {\n          color: white;\n          background-color: #333333;\n          border-radius: 9px;\n          cursor: default; }\n\n.multiselect-filter-container .options-drop {\n  display: none;\n  position: absolute;\n  width: 100%;\n  background-color: white;\n  border: 1px solid black;\n  border-bottom-left-radius: 3px;\n  border-bottom-right-radius: 3px; }\n\n.multiselect-filter-container .options-drop:hover {\n    display: block; }\n\n.multiselect-filter-container:focus-within .options-drop {\n  display: block; }\n\n.multiselect-filter-container .chosen-results {\n  padding: 0;\n  list-style: none;\n  color: #565656;\n  cursor: default; }\n\n.multiselect-filter-container .chosen-results li.active-result {\n    color: black; }\n\n.multiselect-filter-container .chosen-results li.active-result:hover {\n      color: white;\n      background-color: #005db9;\n      cursor: pointer; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9zZWFyY2hmaWx0ZXIvc2VhcmNoZmlsdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0UsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsYUFBYSxFQUFBOztBQUdmO0VBRUksU0FBUyxFQUFBOztBQUZiO0VBTUksZ0JBQWdCLEVBQUE7O0FBTnBCO0VBeUJNLGtCQUFrQjtFQUNsQix1QkF0Q2MsRUFBQTs7QUF5Q2xCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRG5CO0lBSUcsa0JBQWtCO0lBR2xCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLG1CQUFtQjtJQUVuQixVQUFVLEVBQUE7O0FBWmI7TUFpQlMsaUJBQWlCO01BQ2pCLGNBQWM7TUFDZCxtQkFBbUIsRUFBQTs7QUFuQjVCO01Bd0JPLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsbUJBaEVxQjtNQWlFckIsdUJBcEVVO01BcUVWLGtCQUFrQjtNQUVsQix5QkFBeUI7TUFDekIsZUFBZTtNQUNmLFlBQVksRUFBQTs7QUFoQ25CO1FBbUNTLGtCQUFrQixFQUFBOztBQW5DM0I7UUF3Q1Msa0JBQWtCO1FBQ2xCLFNBQVM7UUFDVCxXQUFXO1FBQ1gsWUFBWTtRQUNaLGVBQWU7UUFDZixZQUFZLEVBQUE7O0FBN0NyQjtVQWdEVyxZQUFZO1VBQ1oseUJBQXlCO1VBQ3pCLGtCQUFrQjtVQUNsQixlQUFlLEVBQUE7O0FBaEY3QjtFQXlGSSxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx1QkFBdUI7RUFFdkIsdUJBMUdnQjtFQTJHaEIsOEJBQThCO0VBQzlCLCtCQUErQixFQUFBOztBQWhHbkM7SUFtR00sY0FBYyxFQUFBOztBQW5HcEI7RUF3R0ksY0FBYyxFQUFBOztBQXhHbEI7RUE0R0ksVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZSxFQUFBOztBQS9HbkI7SUFrSE0sWUFBWSxFQUFBOztBQWxIbEI7TUFxSFEsWUFBWTtNQUNaLHlCQWpJb0I7TUFrSXBCLGVBQWUsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL3NlYXJjaGZpbHRlci9zZWFyY2hmaWx0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkYm9yZGVyLWNvbG9yOiBibGFjaztcbiRoaWdobGlnaHQtYmctY29sb3I6ICMwMDVkYjk7XG5cbiRjaG9zZW4tdmFsdWUtYmctY29sb3I6ICNlZWVlZWU7XG5cbi51c2VyLWlucHV0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogc29saWQgMCBibGFjaztcbiAgbGluZS1oZWlnaHQ6IDEuMnJlbTtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLm11bHRpc2VsZWN0LWZpbHRlci1jb250YWluZXIge1xuICB1bCB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG5cbiAgbGkge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIH1cblxuICAvLyYubXVsdGlzZWxlY3QtZmlsdGVyLWNvbnRhaW5lci1hY3RpdmUuY2hvc2VuLXdpdGgtZHJvcCB7XG4gIC8vICAuY2hvc2VuLXNpbmdsZSB7XG4gIC8vICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgLy8gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMDtcbiAgLy8gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDA7XG4gIC8vXG4gIC8vICAgIGRpdiB7XG4gIC8vICAgICAgYiB7XG4gIC8vICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAvLyAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMHB4IGNlbnRlcjtcbiAgLy8gICAgICB9XG4gIC8vICAgIH1cbiAgLy8gIH1cbiAgLy99XG5cbiAgLm11bHRpc2VsZWN0LXdyYXBwZXIge1xuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgJGJvcmRlci1jb2xvcjtcbiAgfVxuXG4gICYtbXVsdGkge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgIC5jaG9zZW4tY2hvaWNlcyB7XG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG5cbiAgICAgIC8vYm9yZGVyOiAxcHggc29saWQgJGJvcmRlci1jb2xvcjtcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICBtaW4taGVpZ2h0OiAyLjVyZW07XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1mbG93OiB3cmFwIHJvdztcblxuICAgICAgcGFkZGluZzogMDtcblxuICAgICAgbGkge1xuICAgICAgICAmLnNlYXJjaC1maWVsZCB7XG4gICAgICAgICAgaW5wdXRbdHlwZT1cInRleHRcIl0ge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxLjNyZW07XG4gICAgICAgICAgICBoZWlnaHQ6IDIuNXJlbTtcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IGluaXRpYWw7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgJi5zZWFyY2gtY2hvaWNlIHtcbiAgICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgIGJhY2tncm91bmQ6ICRjaG9zZW4tdmFsdWUtYmctY29sb3I7XG4gICAgICAgICAgYm9yZGVyOiBzb2xpZCAxcHggJGJvcmRlci1jb2xvcjtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG5cbiAgICAgICAgICBwYWRkaW5nOiAxcHggMTdweCAxcHggM3B4O1xuICAgICAgICAgIG1hcmdpbjogMnB4IDJweDtcbiAgICAgICAgICBoZWlnaHQ6IDEwMCU7XG5cbiAgICAgICAgICBzcGFuIHtcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAuc2VhcmNoLWNob2ljZS1jbG9zZSB7XG4gICAgICAgICAgICAvLyBDdXN0b21pemUgdGhlIG1hdGVyaWFsIGRlc2lnbiBpY29uXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICB3aWR0aDogYXV0bztcbiAgICAgICAgICAgIGhlaWdodDogYXV0bztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgICAgIGNvbG9yOiBibGFjaztcblxuICAgICAgICAgICAgJjpob3ZlciB7XG4gICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzMzMzMzMztcbiAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOXB4O1xuICAgICAgICAgICAgICBjdXJzb3I6IGRlZmF1bHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLm9wdGlvbnMtZHJvcCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG5cbiAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyLWNvbG9yO1xuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDNweDtcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogM3B4O1xuXG4gICAgJjpob3ZlciB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG4gIH1cblxuICAmOmZvY3VzLXdpdGhpbiAgLm9wdGlvbnMtZHJvcCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuICAuY2hvc2VuLXJlc3VsdHMge1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBjb2xvcjogIzU2NTY1NjtcbiAgICBjdXJzb3I6IGRlZmF1bHQ7XG5cbiAgICBsaS5hY3RpdmUtcmVzdWx0IHtcbiAgICAgIGNvbG9yOiBibGFjaztcblxuICAgICAgJjpob3ZlciB7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGhpZ2hsaWdodC1iZy1jb2xvcjtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgfVxuICAgIH1cbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/searchfilter/searchfilter.component.ts":
/*!********************************************************!*\
  !*** ./src/app/searchfilter/searchfilter.component.ts ***!
  \********************************************************/
/*! exports provided: SearchfilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchfilterComponent", function() { return SearchfilterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var SearchfilterComponent = /** @class */ (function () {
    function SearchfilterComponent() {
        /**
         * The only mandatory field for the input data is to has an id.
         * This id is the unique identifier between elements.
         */
        this.data = [];
        this.selectedData = [];
        this.filteredData = [];
    }
    SearchfilterComponent.prototype.ngOnInit = function () {
        /**
         * Mock up data for now
         */
        this.data = [
            {
                "id": 0,
                "display": "Pichon"
            },
            {
                "id": 1,
                "display": "Pichon1"
            },
            {
                "id": 2,
                "display": "Ácido desoxiribonucleico"
            },
            {
                "id": 3,
                "display": "3Omega2Etilmetilroibnol"
            },
            {
                "id": 4,
                "display": "yoquese"
            },
            {
                "id": 5,
                "display": "h2o2"
            },
            {
                "id": 6,
                "display": "meloinvento"
            }
        ];
        this.filteredData = this.data;
    };
    /**
     * This function must return true or false depending on whether the
     * data element passed has been placed in the selected collection or not.
     *

     *
     * @param dataElement
     */
    SearchfilterComponent.prototype.alreadySelected = function (dataElement) {
        return this.contains(this.selectedData, dataElement);
    };
    /** Functions to add and remove elements to/from the selected collection */
    SearchfilterComponent.prototype.addElement = function (evt, dataElement) {
        console.log(evt, dataElement);
        if (!this.contains(this.selectedData, dataElement)) {
            this.selectedData.push(dataElement);
            evt.target.classList.toggle('active-result');
        }
    };
    SearchfilterComponent.prototype.removeElement = function (evt, dataElement) {
        if (this.contains(this.selectedData, dataElement)) {
            //remove
            var idx = this.selectedData.findIndex(function (elem) { return elem.id == dataElement.id; });
            this.selectedData.splice(idx, 1);
        }
    };
    SearchfilterComponent.prototype.contains = function (list, data) {
        return list.filter(function (element) {
            return element.id == data.id;
        }).length > 0;
    };
    /**
     * Get the user's input and filter the dataset to show only the matching
     * options
     */
    SearchfilterComponent.prototype.filterResults = function () {
        if (!this.filterValue) {
            this.filteredData = this.data;
        }
        else {
            // Make a simple search of word start matching.
            // If this does not seem to be very useful, I'll need to implement
            // some kind of word comparison algorithm
            // ( E.G: levenshtein distance)
            var loweredValue_1 = this.filterValue.toLowerCase();
            this.filteredData = this.data.filter(function (el) {
                return el.display.toLowerCase().startsWith(loweredValue_1);
            });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], SearchfilterComponent.prototype, "data", void 0);
    SearchfilterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-searchfilter',
            template: __webpack_require__(/*! ./searchfilter.component.html */ "./src/app/searchfilter/searchfilter.component.html"),
            styles: [__webpack_require__(/*! ./searchfilter.component.scss */ "./src/app/searchfilter/searchfilter.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SearchfilterComponent);
    return SearchfilterComponent;
}());




/***/ }),

/***/ "./src/app/session/session.service.ts":
/*!********************************************!*\
  !*** ./src/app/session/session.service.ts ***!
  \********************************************/
/*! exports provided: SessionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SessionService", function() { return SessionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _global_services_cookie_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global-services/cookie-service.service */ "./src/app/global-services/cookie-service.service.ts");





var SessionService = /** @class */ (function () {
    function SessionService(cookieService) {
        this.cookieService = cookieService;
    }
    SessionService.prototype.intercept = function (req, next) {
        var _this = this;
        /*
        * Pipe responses checking for those that have been non-server errors
        * to update the mdag session if needed.
        * This is done in order to keep synced the user session between
        *  django and angular.
         */
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (evt) {
            if (evt instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"] && evt.status != 500) {
                var sessionSet = _this.cookieService.get("mdag_session");
                // Refresh session token
                if (sessionSet) {
                    _this.cookieService.set("mdag_session", "1", 1800000);
                }
            }
        }));
    };
    SessionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_global_services_cookie_service_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
    ], SessionService);
    return SessionService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-container\">\n    <h1 class=\"signup-title\">¡Sign up to use the app!</h1>\n\n    <!-- TODO adapt style for this error message -->\n    <div id=\"error-container\">\n        <ul id=\"error-messages\"></ul>\n    </div>\n\n    <form class=\"signup-form\" [formGroup]=\"form\" (ngSubmit)=\"registerUser()\">\n        <div class=\"row vertically-spaced\">\n            <mdc-text-field class=\"form-field\" label=\"Username\"\n                            formControlName=\"username\">\n            </mdc-text-field>\n            <mdc-text-field class=\"form-field\" type=\"password\"\n                            formControlName=\"password\"\n                            label=\"Password\">\n            </mdc-text-field>\n        </div>\n\n        <!-- User info -->\n        <div class=\"vertically-spaced\">\n            <div class=\"row\">\n                <mdc-text-field class=\"form-field\" label=\"Name\"\n                                formControlName=\"name\">\n                </mdc-text-field>\n                <mdc-text-field class=\"form-field\" label=\"Surname\"\n                                formControlName=\"surname\">\n                </mdc-text-field>\n            </div>\n            <div class=\"row\">\n                <!-- This should be somehow validated. Maybe e-mail confirmation -->\n                <mdc-text-field class=\"form-field\" type=\"email\"\n                                formControlName=\"email\"\n                                label=\"E-mail\">\n                </mdc-text-field>\n            </div>\n\n        </div>\n\n        <div class=\"form-field button-row\">\n            <button mdc-button outlined class=\"reset-button\"\n                    type=\"button\"\n                    (click)=\"clearForm()\">\n                Reset\n            </button>\n            <button type=\"submit\" mdc-button raised [disabled]=\"!form.valid\">\n                Submit\n            </button>\n\n        </div>\n\n    </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/signup/signup.component.scss":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-container {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center; }\n\n.row {\n  display: flex;\n  flex-direction: row; }\n\n.signup-title {\n  flex: 1; }\n\n.signup-form {\n  display: flex;\n  flex-direction: column;\n  flex: 8; }\n\n.button-row {\n  justify-content: center;\n  display: flex;\n  flex-direction: row; }\n\n.button-row > button {\n    margin: 0 10px; }\n\n.reset-button {\n  color: red;\n  border-color: red; }\n\n.form-field {\n  flex: 1;\n  margin: 8px 5px; }\n\n.vertically-spaced {\n  margin-top: 20px;\n  margin-bottom: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9zaWdudXAvc2lnbnVwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWE7RUFDYixtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxPQUFPLEVBQUE7O0FBR1Q7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLE9BQU8sRUFBQTs7QUFHVDtFQUNFLHVCQUF1QjtFQUN2QixhQUFhO0VBQ2IsbUJBQW1CLEVBQUE7O0FBSHJCO0lBTUksY0FBYyxFQUFBOztBQUlsQjtFQUNFLFVBQVU7RUFDVixpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxPQUFPO0VBQ1AsZUFBZSxFQUFBOztBQUdqQjtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQSIsImZpbGUiOiIuLi9zcmMvYXBwL3NpZ251cC9zaWdudXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybS1jb250YWluZXIge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cblxuLnNpZ251cC10aXRsZSB7XG4gIGZsZXg6IDE7XG59XG5cbi5zaWdudXAtZm9ybSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGZsZXg6IDg7XG59XG5cbi5idXR0b24tcm93IHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbiAgJiA+IGJ1dHRvbiB7XG4gICAgbWFyZ2luOiAwIDEwcHg7XG4gIH1cbn1cblxuLnJlc2V0LWJ1dHRvbiB7XG4gIGNvbG9yOiByZWQ7XG4gIGJvcmRlci1jb2xvcjogcmVkO1xufVxuXG4uZm9ybS1maWVsZCB7XG4gIGZsZXg6IDE7XG4gIG1hcmdpbjogOHB4IDVweDtcbn1cblxuLnZlcnRpY2FsbHktc3BhY2VkIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user/user.service */ "./src/app/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var SignupComponent = /** @class */ (function () {
    function SignupComponent(userService, router, route) {
        var _this = this;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(150)]),
            surname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(150)]),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(150)]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(new RegExp('[a-zA-Z]+')),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(new RegExp('[0-9]+')),
            ]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])
        });
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.route.queryParams.subscribe(function (params) {
            _this.redirectTo = params['redirect'] || SignupComponent_1.DEFAULT_REDIRECT_TO;
        });
    }
    SignupComponent_1 = SignupComponent;
    SignupComponent.prototype.ngOnInit = function () {
    };
    SignupComponent.prototype.clearForm = function () {
        this.form.reset();
    };
    SignupComponent.prototype.registerUser = function () {
        var _this = this;
        console.log("Formulario", this.form);
        this.userService.signUp(this.form.get("username").value, this.form.get("password").value, this.form.get("email").value, this.form.get("name").value, this.form.get("surname").value).then(function (response) { _this.router.navigate([_this.redirectTo]); }).catch((function (reason) {
        }));
    };
    var SignupComponent_1;
    SignupComponent.DEFAULT_REDIRECT_TO = '/home';
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SignupComponent.prototype, "form", void 0);
    SignupComponent = SignupComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.scss */ "./src/app/signup/signup.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/storage/experiment-list.component.html":
/*!********************************************************!*\
  !*** ./src/app/storage/experiment-list.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"storage-container\">\n    <div class=\"main-container\" (click)=\"clearSelection()\">\n        <div class=\"filters storage-common-styles\">\n            <h2 id=\"filters-header\" class=\"section-header\">Filters</h2>\n            <div class=\"filtering-fields\">\n                <mdc-text-field class=\"filter-field\"\n                                placeholder=\"Organism\"\n                                [(ngModel)]=\"organismsFilter\">\n                </mdc-text-field>\n                <mdc-text-field class=\"filter-field\"\n                                placeholder=\"Organism classification\"\n                                [(ngModel)]=\"organismsClassificationFilter\">\n                </mdc-text-field>\n                <mdc-text-field class=\"filter-field\"\n                                placeholder=\"Owner\"\n                                [(ngModel)]=\"owner\">\n                </mdc-text-field>\n                <mdc-text-field class=\"filter-field\"\n                                placeholder=\"Experiment group\"\n                                [(ngModel)]=\"experimentGroupsFilter\">\n                </mdc-text-field>\n            </div>\n            <div style=\"display: flex;flex-direction: row;justify-content:\n                flex-end\">\n                <button class=\"filter-button\" mdc-button primary\n                        (click)=\"clearFilters()\">\n                    Clear\n                </button>\n                <button class=\"filter-button\" mdc-button primary\n                        (click)=\"filterResults()\">\n                    Filter\n                </button>\n\n            </div>\n        </div>\n        <div class=\"list-zone\">\n            <div class=\"result-header-container\">\n                <h2 id=\"result-header\" class=\"section-header\">Results</h2>\n                <button mdc-fab class=\"add-button\"\n                        aria-label=\"Add experiment\"\n                        *ngIf=\"canCreateExperiment()\"\n                        [routerLink]=\"['create']\">\n                    <i class=\"material-icons icon\">add</i>\n                </button>\n            </div>\n            <div class=\"results\">\n                <app-storageitem class=\"storage-item\"\n                                 *ngFor=\"let item of getItems()\"\n                                 [experiment]=\"item\"\n                                 (click)=\"openDetails($event, item)\">\n                </app-storageitem>\n            </div>\n        </div>\n    </div>\n    <div id=\"details-container\" [hidden]=\"!getSelectedExperiment()\"\n         *ngIf=\"getSelectedExperiment()\">\n        <div class=\"detail-header\">\n            <div class=\"text-wrapper\">\n                <h2 class=\"details-header\">\n                    {{getSelectedExperiment().name}}\n                </h2>\n            </div>\n            <div class=\"actions-container\">\n                <div class=\"content-body\">\n                    <ul class=\"actions\">\n                        <li class=\"action-item\">\n                            <i class=\"material-icons\"\n                               (click)=\"openExperimentGraph()\">open_in_new\n                            </i>\n                        </li>\n                        <li class=\"action-item\">\n                            <i class=\"material-icons\"\n                               (click)=\"downloadContent()\">cloud_download</i>\n                        </li>\n                        <li class=\"action-item delete\">\n                            <i class=\"material-icons\"\n                               *ngIf=\"canDeleteExperiment()\"\n                               (click)=\"deleteExperiment()\">delete</i>\n                        </li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"line-container\">\n            <div class=\"line\"></div>\n        </div>\n\n        <div class=\"details-body\">\n            <div class=\"detail-container\">\n                <h2 class=\"content-title\">Basic Information</h2>\n                <div class=\"content-body\">\n\n                    <div class=\"row\">\n                        <mdc-text-field class=\"first-element\"\n                                        label=\"Creation\" readonly=\"1\"\n                                        type=\"date\"\n                                        [ngModel]=\"getSelectedExperiment().getCreationDateInMaterialFormat()\">\n                        </mdc-text-field>\n                        <mdc-text-field class=\"element\"\n                                        label=\"Modification\" readonly=\"1\"\n                                        type=\"date\"\n                                        [ngModel]=\"getSelectedExperiment().getModificationDateInMaterialFormat()\">\n                        </mdc-text-field>\n\n                    </div>\n                    <div class=\"row\">\n                        <mdc-text-field class=\"first-element\" readonly=\"1\"\n                                        label=\"Status\"\n                                        [ngModel]=\"getSelectedExperiment().status\">\n                        </mdc-text-field>\n                        <mdc-select class=\"last-element\"\n                                    placeholder=\"Visibility\"\n                                    [(ngModel)]=\"getSelectedExperiment().public\">\n                            <mdc-menu class=\"filter-field\">\n                                <mdc-list class=\"filter-field\">\n                                    <mdc-list-item value=\"pu\">\n                                        Public\n                                    </mdc-list-item>\n                                    <mdc-list-item value=\"pr\">\n                                        Private\n                                    </mdc-list-item>\n                                </mdc-list>\n                            </mdc-menu>\n                        </mdc-select>\n                    </div>\n\n                    <mdc-text-field class=\"element\" label=\"Name\"\n                                    [(ngModel)]=\"getSelectedExperiment().name\">\n                    </mdc-text-field>\n                    <mdc-textarea class=\"element\" label=\"Description\"\n                                  [(ngModel)]=\"getSelectedExperiment().description\">\n                    </mdc-textarea>\n\n                </div>\n            </div>\n\n            <div class=\"detail-container\">\n                <h2 class=\"content-title\">Experiment groups</h2>\n                <div class=\"content-body\">\n\n                    <ul class=\"undecorated-list horizontal-list\">\n                    </ul>\n                    <file-uploader\n                            name=\"Groups\"\n                            helperText=\"Add additional groups\"\n                    ></file-uploader>\n\n                </div>\n            </div>\n\n            <div class=\"buttons-container huge-vertically-spaced\">\n                <button class=\"submit-button\" mdc-button primary\n                        (click)=\"saveExperiment()\">\n                    Save\n                </button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/storage/experiment-list.component.scss":
/*!********************************************************!*\
  !*** ./src/app/storage/experiment-list.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".storage-item {\n  margin: 10px 0;\n  display: block; }\n\n.storage-container {\n  display: flex;\n  flex-direction: row; }\n\n.storage-container .results {\n    overflow-y: auto;\n    height: 65.5vh;\n    max-height: 65.5vh;\n    width: 100%;\n    display: flex;\n    flex-direction: column; }\n\n.storage-container .main-container {\n    flex: 4 0; }\n\n.storage-container #details-container {\n    max-width: 32%;\n    min-width: 32%;\n    margin: 0 10px;\n    flex: 1 0; }\n\n.storage-container #details-container .detail-header {\n      text-align: center; }\n\n.storage-container #details-container .content-title {\n      margin-bottom: 0;\n      margin-top: 35px; }\n\n.storage-container #details-container .content-body {\n      display: flex;\n      flex-direction: column; }\n\n.storage-container #details-container .content-body .element, .storage-container #details-container .content-body .row .first-element, .storage-container #details-container .content-body .row .last-element {\n        flex: 1;\n        margin-top: 5px;\n        margin-bottom: 5px; }\n\n.storage-container #details-container .content-body .row {\n        display: flex;\n        flex-direction: row; }\n\n.storage-container #details-container .content-body .row .first-element {\n          margin-right: 10px; }\n\n.storage-container #details-container .content-body .row .last-element {\n          margin-left: 10px; }\n\n.storage-container .storage-common-styles {\n    margin: 1px 10px; }\n\n.storage-container .storage-common-styles .filters {\n      margin-bottom: 10px; }\n\n.actions {\n  list-style-type: none;\n  -webkit-padding-start: 10px;\n          padding-inline-start: 10px;\n  display: inline-flex; }\n\n.actions li:hover {\n    cursor: pointer;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n\n.actions li:hover.action-item i {\n      color: #005db9; }\n\n.actions li:hover.action-item.delete i {\n      color: #DC0000; }\n\n.actions li.action-item {\n    margin: 0 5px; }\n\n.actions li.action-item i {\n      font-size: 20px; }\n\n.float-right {\n  float: right; }\n\n.actions-container {\n  display: inline-flex;\n  overflow: hidden; }\n\n.actions-container:after {\n    clear: both; }\n\n.actions-container ul {\n    -webkit-margin-after: 0;\n            margin-block-end: 0; }\n\n.text-wrapper {\n  display: inline-flex;\n  max-width: 76%; }\n\n.details-header {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n\ndiv.line-container {\n  text-align: center; }\n\ndiv.line-container div.line {\n    border-bottom: solid 1px black;\n    width: 90%;\n    display: inline-block; }\n\n.content-body-list {\n  list-style-type: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0;\n  margin-top: 0;\n  margin-bottom: 5px; }\n\n.details-body {\n  display: flex;\n  flex-direction: column;\n  overflow-y: auto;\n  max-height: 79.5vh; }\n\n.list-zone {\n  display: flex;\n  flex-direction: column;\n  margin: 10px 10px 0px 10px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\n.result-header-container {\n  position: relative; }\n\n.result-header-container .add-button {\n    position: absolute;\n    top: 5px;\n    right: 0;\n    margin-right: 35px;\n    width: 40px;\n    height: 40px;\n    background-color: #005db9; }\n\n.result-header-container .add-button .icon {\n      color: white; }\n\n.undecorated-list {\n  list-style: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0; }\n\n.undecorated-list.horizontal-list {\n    display: flex;\n    flex-wrap: wrap; }\n\n.undecorated-list.horizontal-list li {\n      display: inline;\n      margin: 3px 5px;\n      padding: 5px;\n      background-color: #dddddd;\n      border-radius: 4px; }\n\n.wrap-content {\n  display: flex;\n  flex-wrap: wrap; }\n\n.filtering-fields {\n  display: flex;\n  flex-direction: row; }\n\n.filter-field {\n  flex: 1;\n  margin: 2px 5px; }\n\n.mdc-text-field-custom {\n  height: 40px; }\n\n.filter-button {\n  flex: 0;\n  margin: 5px 5px; }\n\n.huge-vertically-spaced {\n  margin-top: 40px;\n  margin-bottom: 40px; }\n\n.buttons-container {\n  display: flex;\n  justify-content: flex-end; }\n\n.buttons-container .submit-button {\n    max-width: 100px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9zdG9yYWdlL2V4cGVyaW1lbnQtbGlzdC5jb21wb25lbnQuc2NzcyIsIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL19tYXRlcmlhbHZhcnMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLGNBQWM7RUFDZCxjQUFjLEVBQUE7O0FBSWhCO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQixFQUFBOztBQUZyQjtJQUtJLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxhQUFhO0lBQ2Isc0JBQXNCLEVBQUE7O0FBVjFCO0lBY0ksU0FBUyxFQUFBOztBQWRiO0lBa0JJLGNBQWM7SUFDZCxjQUFjO0lBQ2QsY0FBYztJQUNkLFNBQVMsRUFBQTs7QUFyQmI7TUF3Qk0sa0JBQWtCLEVBQUE7O0FBeEJ4QjtNQTRCTSxnQkFBZ0I7TUFDaEIsZ0JBQWdCLEVBQUE7O0FBN0J0QjtNQWlDTSxhQUFhO01BQ2Isc0JBQXNCLEVBQUE7O0FBbEM1QjtRQXFDUSxPQUFPO1FBQ1AsZUFBZTtRQUNmLGtCQUFrQixFQUFBOztBQXZDMUI7UUEyQ1EsYUFBYTtRQUNiLG1CQUFtQixFQUFBOztBQTVDM0I7VUFnRFUsa0JBQWtCLEVBQUE7O0FBaEQ1QjtVQXFEVSxpQkFBaUIsRUFBQTs7QUFyRDNCO0lBNERJLGdCQUFnQixFQUFBOztBQTVEcEI7TUErRE0sbUJBQW1CLEVBQUE7O0FBTXpCO0VBQ0UscUJBQXFCO0VBQ3JCLDJCQUEwQjtVQUExQiwwQkFBMEI7RUFDMUIsb0JBQW9CLEVBQUE7O0FBSHRCO0lBT00sZUFBZTtJQUNmLHlCQUFpQjtPQUFqQixzQkFBaUI7UUFBakIscUJBQWlCO1lBQWpCLGlCQUFpQixFQUFBOztBQVJ2QjtNQVdVLGNDeEZpQixFQUFBOztBRDZFM0I7TUFtQlUsY0FBYyxFQUFBOztBQW5CeEI7SUF5Qk0sYUFBYSxFQUFBOztBQXpCbkI7TUE0QlEsZUFBZSxFQUFBOztBQU12QjtFQUNFLFlBQVksRUFBQTs7QUFHZDtFQUNFLG9CQUFvQjtFQUNwQixnQkFBZ0IsRUFBQTs7QUFGbEI7SUFLSSxXQUFXLEVBQUE7O0FBTGY7SUFTSSx1QkFBbUI7WUFBbkIsbUJBQW1CLEVBQUE7O0FBSXZCO0VBQ0Usb0JBQW9CO0VBQ3BCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLHVCQUF1QixFQUFBOztBQUl6QjtFQUNFLGtCQUFrQixFQUFBOztBQURwQjtJQUlJLDhCQUE4QjtJQUM5QixVQUFVO0lBQ1YscUJBQXFCLEVBQUE7O0FBTXpCO0VBQ0UscUJBQXFCO0VBQ3JCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsYUFBYTtFQUNiLGtCQUFrQixFQUFBOztBQUlwQjtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUlwQjtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsMEJBQTBCLEVBQUE7O0FBRzVCO0VBQ0Usa0JBQWtCO0VBRWxCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFJbEM7RUFDRSxrQkFBa0IsRUFBQTs7QUFEcEI7SUFJSSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFFBQVE7SUFDUixrQkFBa0I7SUFDbEIsV0FUa0I7SUFVbEIsWUFWa0I7SUFXbEIseUJDaE11QixFQUFBOztBRHNMM0I7TUFhTSxZQUFZLEVBQUE7O0FBS2xCO0VBQ0UsZ0JBQWdCO0VBQ2hCLHdCQUF1QjtVQUF2Qix1QkFBdUIsRUFBQTs7QUFGekI7SUFLSSxhQUFhO0lBQ2IsZUFBZSxFQUFBOztBQU5uQjtNQVNNLGVBQWU7TUFDZixlQUFlO01BQ2YsWUFBWTtNQUNaLHlCQ25OdUI7TURvTnZCLGtCQUFrQixFQUFBOztBQU14QjtFQUNFLGFBQWE7RUFDYixlQUFlLEVBQUE7O0FBTWpCO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLE9BQU87RUFDUCxlQUFlLEVBQUE7O0FBR2pCO0VBQ0UsWUFBWSxFQUFBOztBQUdkO0VBQ0UsT0FBTztFQUNQLGVBQWUsRUFBQTs7QUFHakI7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsYUFBYTtFQUNiLHlCQUF5QixFQUFBOztBQUYzQjtJQUtJLGdCQUFnQixFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvc3RvcmFnZS9leHBlcmltZW50LWxpc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vbWF0ZXJpYWx2YXJzXCI7XG5cbi5zdG9yYWdlLWl0ZW0ge1xuICBtYXJnaW46IDEwcHggMDtcbiAgZGlzcGxheTogYmxvY2s7XG5cbn1cblxuLnN0b3JhZ2UtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcblxuICAmIC5yZXN1bHRzIHtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuICAgIGhlaWdodDogNjUuNXZoO1xuICAgIG1heC1oZWlnaHQ6IDY1LjV2aDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cblxuICAmIC5tYWluLWNvbnRhaW5lciB7XG4gICAgZmxleDogNCAwO1xuICB9XG5cbiAgJiAjZGV0YWlscy1jb250YWluZXIge1xuICAgIG1heC13aWR0aDogMzIlO1xuICAgIG1pbi13aWR0aDogMzIlO1xuICAgIG1hcmdpbjogMCAxMHB4O1xuICAgIGZsZXg6IDEgMDtcblxuICAgICYgLmRldGFpbC1oZWFkZXIge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgICYgLmNvbnRlbnQtdGl0bGUge1xuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgIG1hcmdpbi10b3A6IDM1cHg7XG4gICAgfVxuXG4gICAgJiAuY29udGVudC1ib2R5IHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuXG4gICAgICAmIC5lbGVtZW50IHtcbiAgICAgICAgZmxleDogMTtcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgICB9XG5cbiAgICAgICYgLnJvdyB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbiAgICAgICAgJiAuZmlyc3QtZWxlbWVudCB7XG4gICAgICAgICAgQGV4dGVuZCAuZWxlbWVudDtcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAmIC5sYXN0LWVsZW1lbnQge1xuICAgICAgICAgIEBleHRlbmQgLmVsZW1lbnQ7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAmIC5zdG9yYWdlLWNvbW1vbi1zdHlsZXMge1xuICAgIG1hcmdpbjogMXB4IDEwcHg7XG5cbiAgICAmIC5maWx0ZXJzIHtcbiAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgfVxuICB9XG59XG5cblxuLmFjdGlvbnMge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAxMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcblxuICAmIGxpIHtcbiAgICAmOmhvdmVyIHtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgIHVzZXItc2VsZWN0OiBub25lO1xuICAgICAgJi5hY3Rpb24taXRlbSB7XG4gICAgICAgICYgaSB7XG4gICAgICAgICAgY29sb3I6ICRtZGMtdGhlbWUtcHJpbWFyeTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmLmFjdGlvbi1pdGVtLmRlbGV0ZSB7XG4gICAgICAgICAmIGkge1xuICAgICAgICAgICAvLyBSZWRpc2ggY29sb3IuIFB1cmUgcmVkIGNvbG9yIGlzIG5vdCBlbm91Z2ggZm9yIEFBIHdlYiBwYWdlc1xuICAgICAgICAgICAvLyB3aGVuIGl0cyBiYWNrZ3JvdW5kIGNvbG9yIGlzIHdoaXRlLlxuICAgICAgICAgIGNvbG9yOiAjREMwMDAwO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgJi5hY3Rpb24taXRlbSB7XG4gICAgICBtYXJnaW46IDAgNXB4O1xuXG4gICAgICAmIGkge1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi5mbG9hdC1yaWdodCB7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLmFjdGlvbnMtY29udGFpbmVyIHtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgJjphZnRlciB7XG4gICAgY2xlYXI6IGJvdGg7XG4gIH1cblxuICAmIHVsIHtcbiAgICBtYXJnaW4tYmxvY2stZW5kOiAwO1xuICB9XG59XG5cbi50ZXh0LXdyYXBwZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgbWF4LXdpZHRoOiA3NiU7XG59XG5cbi5kZXRhaWxzLWhlYWRlciB7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG5cbmRpdi5saW5lLWNvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAmIGRpdi5saW5lIHtcbiAgICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggYmxhY2s7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIH1cbn1cblxuXG4vLyBEYXRlIGxpc3Qgc3R5bGVzXG4uY29udGVudC1ib2R5LWxpc3Qge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwO1xuICBtYXJnaW4tdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cblxuLmRldGFpbHMtYm9keSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIG92ZXJmbG93LXk6IGF1dG87XG4gIG1heC1oZWlnaHQ6IDc5LjV2aDtcbn1cblxuXG4ubGlzdC16b25lIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgbWFyZ2luOiAxMHB4IDEwcHggMHB4IDEwcHg7XG59XG5cbi5zZWN0aW9uLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICBoZWlnaHQ6IGF1dG87XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjZGRkZGRkO1xufVxuXG4kYWRkLWJ1dHRvbi1zaXplOiA0MHB4O1xuLnJlc3VsdC1oZWFkZXItY29udGFpbmVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICYgLmFkZC1idXR0b24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDVweDtcbiAgICByaWdodDogMDtcbiAgICBtYXJnaW4tcmlnaHQ6IDM1cHg7XG4gICAgd2lkdGg6ICRhZGQtYnV0dG9uLXNpemU7XG4gICAgaGVpZ2h0OiAkYWRkLWJ1dHRvbi1zaXplO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRtZGMtdGhlbWUtcHJpbWFyeTtcblxuICAgICYgLmljb24ge1xuICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbiAgfVxufVxuXG4udW5kZWNvcmF0ZWQtbGlzdCB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwO1xuXG4gICYuaG9yaXpvbnRhbC1saXN0IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtd3JhcDogd3JhcDtcblxuICAgICYgbGkge1xuICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgICAgbWFyZ2luOiAzcHggNXB4O1xuICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJG1kYy10aGVtZS1zZWNvbmRhcnk7XG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgfVxuXG4gIH1cbn1cblxuLndyYXAtY29udGVudCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcblxufVxuXG4vLyBGaWx0ZXIgc3R5bGVzXG5cbi5maWx0ZXJpbmctZmllbGRzIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cblxuLmZpbHRlci1maWVsZCB7XG4gIGZsZXg6IDE7XG4gIG1hcmdpbjogMnB4IDVweDtcbn1cblxuLm1kYy10ZXh0LWZpZWxkLWN1c3RvbSB7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLmZpbHRlci1idXR0b24ge1xuICBmbGV4OiAwO1xuICBtYXJnaW46IDVweCA1cHg7XG59XG5cbi5odWdlLXZlcnRpY2FsbHktc3BhY2VkIHtcbiAgbWFyZ2luLXRvcDogNDBweDtcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcbn1cblxuLmJ1dHRvbnMtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcblxuICAmIC5zdWJtaXQtYnV0dG9uIHtcbiAgICBtYXgtd2lkdGg6IDEwMHB4O1xuICB9XG59IiwiJG1kYy10aGVtZS1wcmltYXJ5OiAjMDA1ZGI5O1xuJG1kYy10aGVtZS1zZWNvbmRhcnk6ICNkZGRkZGQ7XG4kbWRjLXRoZW1lLXNlY29uZGFyeS1saWdodDogI2Y1ZjVmNTtcbiRtZGMtdGhlbWUtYmFja2dyb3VuZDogI2ZmZjtcbiJdfQ== */"

/***/ }),

/***/ "./src/app/storage/experiment-list.component.ts":
/*!******************************************************!*\
  !*** ./src/app/storage/experiment-list.component.ts ***!
  \******************************************************/
/*! exports provided: ExperimentListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExperimentListComponent", function() { return ExperimentListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _file_item_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../file-item-handler.service */ "./src/file-item-handler.service.ts");
/* harmony import */ var _modal_window_modal_window_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../modal-window/modal-window.service */ "./src/app/modal-window/modal-window.service.ts");
/* harmony import */ var _experiment_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./experiment.service */ "./src/app/storage/experiment.service.ts");
/* harmony import */ var _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../notifications/notifications.service */ "./src/app/notifications/notifications.service.ts");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../user/user.service */ "./src/app/user/user.service.ts");







var ExperimentListComponent = /** @class */ (function () {
    function ExperimentListComponent(fileItemHandlerService, modalWindowService, experimentService, notificationService, userService) {
        var _this = this;
        this.fileItemHandlerService = fileItemHandlerService;
        this.modalWindowService = modalWindowService;
        this.experimentService = experimentService;
        this.notificationsService = notificationService;
        this.userService = userService;
        this.user = this.userService.getUser();
        this.experiments = [];
        this.selectedExperiment = null;
        // User login and logout event bindings
        this.userService.onLogout.subscribe(function (value) { return _this.loginStatusChanged(); });
        this.userService.onLogin.subscribe(function (value) { return _this.loginStatusChanged(); });
    }
    ExperimentListComponent.prototype.ngOnInit = function () {
        this.fetchExperiments({});
    };
    ExperimentListComponent.prototype.getItems = function () {
        return this.experiments;
    };
    ExperimentListComponent.prototype.clearFilters = function () {
        this.organismsFilter = null;
        this.organismsClassificationFilter = null;
        this.owner = null;
        this.experimentGroupsFilter = null;
    };
    ExperimentListComponent.prototype.filterResults = function () {
        var filters = {};
        filters['organisms'] = this.organismsFilter;
        filters['reactions'] = this.owner;
        filters['organism_groups'] = this.organismsClassificationFilter;
        filters['experiment_groups'] = this.experimentGroupsFilter;
        this.fetchExperiments(filters);
    };
    /**
     * Retrieve the experiments that include one or more of the specified
     * Organisms, reactions, organism classification or group.
     * @param filters
     */
    ExperimentListComponent.prototype.fetchExperiments = function (filters) {
        var _this = this;
        this.experimentService.getExperiments(filters).then(function (value) {
            if (value.length == 0) {
                _this.notificationsService.notify("No experiments found");
            }
            _this.experiments = value;
        });
    };
    ExperimentListComponent.prototype.openDetails = function (event, experiment) {
        this.selectedExperiment = experiment;
        event.stopPropagation();
    };
    ExperimentListComponent.prototype.getSelectedExperiment = function () {
        return this.selectedExperiment;
    };
    ExperimentListComponent.prototype.clearSelection = function () {
        this.selectedExperiment = null;
    };
    ExperimentListComponent.prototype.openExperimentGraph = function () {
        this.modalWindowService.open(_modal_window_modal_window_service__WEBPACK_IMPORTED_MODULE_3__["ModalWindowService"].MODAL_TYPES.VIEWER);
    };
    ExperimentListComponent.prototype.downloadContent = function () {
        var _this = this;
        this.experimentService.downloadContent(this.selectedExperiment).catch(function (error) {
            _this.notificationsService.notify("Failed to download the content", _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_5__["NotificationsService"].MESSAGE_TYPES.ERROR);
        });
    };
    ExperimentListComponent.prototype.deleteExperiment = function () {
        var _this = this;
        var experiment = this.getSelectedExperiment();
        this.clearSelection();
        this.experimentService.delete(experiment).then(function (value) {
            _this.notificationsService.notify("Experiment deleted");
            _this.filterResults();
        }).catch(function (reason) { return _this.notificationsService.notify("Failed to delete the experiment"); });
    };
    ExperimentListComponent.prototype.saveExperiment = function () {
        var _this = this;
        var experiment = this.getSelectedExperiment();
        this.experimentService.update(experiment).then(function (value) {
            _this.notificationsService.notify("Experiment updated");
        });
    };
    /**
     * An experiment can be deleted either by an admin user or its owner
     */
    ExperimentListComponent.prototype.canDeleteExperiment = function () {
        return this.user && (this.user.isAdmin || this.getSelectedExperiment().owner === this.user.id);
    };
    ExperimentListComponent.prototype.canCreateExperiment = function () {
        return this.user && (this.user.isAdmin || this.user.hasPermission('experiments_create'));
    };
    ExperimentListComponent.prototype.loginStatusChanged = function () {
        this.user = this.userService.getUser();
        this.filterResults();
    };
    ExperimentListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-storage',
            template: __webpack_require__(/*! ./experiment-list.component.html */ "./src/app/storage/experiment-list.component.html"),
            styles: [__webpack_require__(/*! ./experiment-list.component.scss */ "./src/app/storage/experiment-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_file_item_handler_service__WEBPACK_IMPORTED_MODULE_2__["FileItemHandlerService"],
            _modal_window_modal_window_service__WEBPACK_IMPORTED_MODULE_3__["ModalWindowService"],
            _experiment_service__WEBPACK_IMPORTED_MODULE_4__["ExperimentService"],
            _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_5__["NotificationsService"],
            _user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]])
    ], ExperimentListComponent);
    return ExperimentListComponent;
}());



/***/ }),

/***/ "./src/app/storage/experiment.service.ts":
/*!***********************************************!*\
  !*** ./src/app/storage/experiment.service.ts ***!
  \***********************************************/
/*! exports provided: ExperimentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExperimentService", function() { return ExperimentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_experiment_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/experiment.model */ "./src/app/models/experiment.model.ts");
/* harmony import */ var querystring__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! querystring */ "./node_modules/querystring/index.js");
/* harmony import */ var querystring__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(querystring__WEBPACK_IMPORTED_MODULE_4__);





var ExperimentService = /** @class */ (function () {
    function ExperimentService(httpClient) {
        this.http = httpClient;
    }
    ExperimentService_1 = ExperimentService;
    /**
     * @param filters
     */
    ExperimentService.prototype.getExperiments = function (filters) {
        var _this = this;
        var body = JSON.stringify({
            'organism': filters['organisms'],
            'reaction': filters['reactions'],
            'organism_group': filters['organism_groups'],
            'experiment_group': filters['experiment_groups'],
        });
        var baseUrl = ExperimentService_1.URL_BASE + 'get_visible_experiments';
        return new Promise(function (resolve, reject) {
            _this.http.post(baseUrl, body).toPromise().then(function (httpResponse) {
                return resolve(ExperimentService_1.transformExperimentResponse(httpResponse['response']));
            }).catch(function (reason) { return reject(reason); });
        });
    };
    ExperimentService.prototype.getExperimentDetails = function (experiment) {
        var url = ExperimentService_1.URL_BASE + 'get_details';
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        params.append('id', Object(querystring__WEBPACK_IMPORTED_MODULE_4__["stringify"])(experiment.id));
        var promise = new Promise(function (resolve, reject) {
            // this.http.get(url, {'params': params}).toPromise().then(
            //     response => resolve(ExperimentService.transformExperimentResponse(response['data'])[0])
            // ).catch(
            //     reason => reject(reason)
            // );
            // There is only one experiment in this response
            var experiments = ExperimentService_1.transformExperimentResponse([{}]);
            resolve(experiments[0]);
        });
        return promise;
    };
    ExperimentService.prototype.downloadContent = function (experiment) {
        var _this = this;
        var url = ExperimentService_1.URL_BASE + 'download_experiment_content';
        var body = {
            'args': [experiment.id]
        };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, {
                observe: 'response',
                responseType: "blob"
            }).toPromise().then(function (a) {
                ExperimentService_1.downloadFile(a);
                resolve();
            }).catch(function (reason) {
                reject(reason);
            });
        });
    };
    ExperimentService.prototype.delete = function (experiment) {
        var url = ExperimentService_1.URL_BASE + 'delete';
        var body = {
            'args': [experiment.id]
        };
        return this.http.post(url, body).toPromise();
    };
    /**
     * There you go. This function declares a huge number of params and it
     * does not check no one. thu
     * @param experimentData
     */
    ExperimentService.prototype.createExperiment = function (experimentData) {
        var _this = this;
        // Transform formdata from front end format to backend format
        var formData = new FormData();
        var elementsObj = ExperimentService_1.classifyParams(experimentData);
        var basicElements = elementsObj['basicElements'];
        for (var _i = 0, _a = Object.entries(basicElements); _i < _a.length; _i++) {
            var entry = _a[_i];
            formData.append(entry[0], entry[1]);
        }
        for (var _b = 0, _c = Object.entries(elementsObj['fileLikeElements']); _b < _c.length; _b++) {
            var entry = _c[_b];
            if (entry[1] instanceof File) {
                formData.append(entry[0], entry[1]);
            }
            else {
                for (var fileIdx in entry[1]) {
                    formData.append(entry[0], entry[1][fileIdx]);
                }
            }
        }
        console.log(formData);
        var baseUrl = ExperimentService_1.URL_BASE + 'create_experiment';
        return new Promise(function (resolve, reject) {
            _this.http.post(baseUrl, formData).toPromise().then(function (httpResponse) {
                return resolve(ExperimentService_1.transformExperimentResponse(httpResponse['response']));
            }).catch(function (reason) { return reject(reason); });
        });
    };
    ExperimentService.prototype.createWholeOrganismExperiment = function (name, description, organism, groups, organismClassification) {
        var data = {
            experiment_type: 2,
            name: name,
            description: description,
            organism: organism
        };
        if (groups) {
            data['groups'] = groups;
        }
        return this.createExperiment(data);
    };
    ExperimentService.prototype.createOrganismComparisonExperiment = function (name, description, pathway) {
        return this.createExperiment({
            experiment_type: 3,
            name: name,
            description: description,
            pathway: pathway
            // TODO Missing flags
        });
    };
    ExperimentService.prototype.createExperimentComparison = function (name, description, experiments) {
        return this.createExperiment({
            experiment_type: 4,
            name: name,
            description: description,
            // This should be a list of experiment identifiers
            experiments: experiments
            // TODO Missing flags
        });
    };
    ExperimentService.prototype.createOrganismMetabolismComparison = function (name, description, organism) {
        return new Promise(function (resolve) { return resolve(); });
    };
    ExperimentService.prototype.update = function (experiment) {
        var url = ExperimentService_1.URL_BASE + 'update';
        var body = {
            'args': [experiment.id],
            'optionals': {
                'name': experiment.name,
                'description': experiment.description,
                'visible_for': experiment.public
            }
        };
        return this.http.post(url, body).toPromise();
    };
    ExperimentService.transformExperimentResponse = function (Experiments) {
        var experimentList = [];
        for (var _i = 0, Experiments_1 = Experiments; _i < Experiments_1.length; _i++) {
            var experimentObject = Experiments_1[_i];
            experimentList.push(new _models_experiment_model__WEBPACK_IMPORTED_MODULE_3__["Experiment"](experimentObject));
        }
        return experimentList;
    };
    ExperimentService.downloadFile = function (request) {
        var blob = this.getContentBlob(request, 'application/zip');
        var url = URL.createObjectURL(blob);
        // Create a new link to simulate the user download.
        // This is done this way in order to avoid a tab spawning for a second
        var element = document.createElement('a');
        var name = this.getDownloadName(request.headers);
        if (name) {
            element.download = name;
        }
        element.href = url;
        element.click();
        // Free the created blob
        setTimeout(function () { return URL.revokeObjectURL(url); }, 200);
    };
    ExperimentService.getDownloadName = function (headers) {
        var match = this.FILENAME_REGEX.exec(headers.get('content-disposition'));
        if (match) {
            return match[1];
        }
        else {
            return null;
        }
    };
    ExperimentService.getContentBlob = function (request, type) {
        var body = request.body;
        return new Blob([body], { type: type });
    };
    /**
     * Retrieve an object holding all the file-like elements in the passed
     * object
     */
    ExperimentService.classifyParams = function (rootObject) {
        var fileLikeElements = {};
        var basicElements = {};
        var candidate;
        for (var _i = 0, _a = Object.entries(rootObject); _i < _a.length; _i++) {
            var entry = _a[_i];
            candidate = entry[1];
            if (ExperimentService_1.isFileLikeElement(candidate)) {
                fileLikeElements[entry[0]] = entry[1];
            }
            else {
                basicElements[entry[0]] = entry[1];
            }
        }
        return {
            "fileLikeElements": fileLikeElements,
            "basicElements": basicElements
        };
    };
    ExperimentService.isFileLikeElement = function (element) {
        var candidate = element;
        if (candidate instanceof Array) {
            // Get a sample to use it in the file test
            candidate = candidate[0];
        }
        return candidate instanceof File;
    };
    /**
     * Check if the passed object has all its keys whitelisted
     * @param filesContainer
     */
    ExperimentService.validateFiles = function (filesContainer) {
        return filesContainer;
    };
    var ExperimentService_1;
    ExperimentService.FILENAME_REGEX = /filename="(\w+\.\w+)"/i;
    // Constants
    ExperimentService.URL_BASE = 'webservice/experiments/';
    ExperimentService = ExperimentService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ExperimentService);
    return ExperimentService;
}());



/***/ }),

/***/ "./src/app/storageitem/storageitem.component.html":
/*!********************************************************!*\
  !*** ./src/app/storageitem/storageitem.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"head\">\n    {{ experiment.name }}\n</div>\n\n"

/***/ }),

/***/ "./src/app/storageitem/storageitem.component.scss":
/*!********************************************************!*\
  !*** ./src/app/storageitem/storageitem.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".head {\n  color: #333;\n  margin: 0 5px;\n  border-radius: 5px;\n  background-color: #f5f5f5;\n  align-self: center;\n  text-align: left;\n  border: solid 1px #ddd;\n  padding: 5px; }\n  .head:hover {\n    background-color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC9zdG9yYWdlaXRlbS9zdG9yYWdlaXRlbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0JBQWtCO0VBRWxCLHlCQUF5QjtFQUN6QixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUN0QixZQUFZLEVBQUE7RUFUZDtJQVdJLHVCQUF1QixFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvc3RvcmFnZWl0ZW0vc3RvcmFnZWl0ZW0uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZCB7XG4gIGNvbG9yOiAjMzMzO1xuICBtYXJnaW46IDAgNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG5cbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBib3JkZXI6IHNvbGlkIDFweCAjZGRkO1xuICBwYWRkaW5nOiA1cHg7XG4gICY6aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/storageitem/storageitem.component.ts":
/*!******************************************************!*\
  !*** ./src/app/storageitem/storageitem.component.ts ***!
  \******************************************************/
/*! exports provided: StorageItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageItemComponent", function() { return StorageItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _storage_experiment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../storage/experiment.service */ "./src/app/storage/experiment.service.ts");
/* harmony import */ var _file_item_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../file-item-handler.service */ "./src/file-item-handler.service.ts");
/* harmony import */ var _models_experiment_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/experiment.model */ "./src/app/models/experiment.model.ts");





var StorageItemComponent = /** @class */ (function () {
    function StorageItemComponent(experimentService, fileHandler) {
        this.experimentService = null;
    }
    StorageItemComponent.prototype.ngOnInit = function () {
    };
    StorageItemComponent.prototype.onclick = function (event) {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("headedcontainerelement"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], StorageItemComponent.prototype, "view", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_experiment_model__WEBPACK_IMPORTED_MODULE_4__["Experiment"])
    ], StorageItemComponent.prototype, "experiment", void 0);
    StorageItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-storageitem',
            template: __webpack_require__(/*! ./storageitem.component.html */ "./src/app/storageitem/storageitem.component.html"),
            styles: [__webpack_require__(/*! ./storageitem.component.scss */ "./src/app/storageitem/storageitem.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_storage_experiment_service__WEBPACK_IMPORTED_MODULE_2__["ExperimentService"], _file_item_handler_service__WEBPACK_IMPORTED_MODULE_3__["FileItemHandlerService"]])
    ], StorageItemComponent);
    return StorageItemComponent;
}());



/***/ }),

/***/ "./src/app/undecoratedlist/undecoratedlist.component.html":
/*!****************************************************************!*\
  !*** ./src/app/undecoratedlist/undecoratedlist.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"undecorated-list\">\n    <li class=\"element\" *ngFor=\"let element of listElements\"> {{ element }} </li>\n</ul>\n"

/***/ }),

/***/ "./src/app/undecoratedlist/undecoratedlist.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/undecoratedlist/undecoratedlist.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".undecorated-list {\n  padding: 0 10px;\n  text-align: left; }\n  .undecorated-list .element {\n    list-style: none;\n    margin: 0;\n    text-overflow: ellipsis;\n    white-space: nowrap;\n    overflow: hidden; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC91bmRlY29yYXRlZGxpc3QvdW5kZWNvcmF0ZWRsaXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQixFQUFBO0VBRmxCO0lBS0ksZ0JBQWdCO0lBQ2hCLFNBQVM7SUFDVCx1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGdCQUFnQixFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvdW5kZWNvcmF0ZWRsaXN0L3VuZGVjb3JhdGVkbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51bmRlY29yYXRlZC1saXN0IHtcbiAgcGFkZGluZzogMCAxMHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuXG4gICYgLmVsZW1lbnQge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgbWFyZ2luOiAwO1xuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/undecoratedlist/undecoratedlist.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/undecoratedlist/undecoratedlist.component.ts ***!
  \**************************************************************/
/*! exports provided: UndecoratedlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UndecoratedlistComponent", function() { return UndecoratedlistComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UndecoratedlistComponent = /** @class */ (function () {
    function UndecoratedlistComponent() {
        this.listElements = null;
    }
    UndecoratedlistComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], UndecoratedlistComponent.prototype, "listElements", void 0);
    UndecoratedlistComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-undecoratedlist',
            template: __webpack_require__(/*! ./undecoratedlist.component.html */ "./src/app/undecoratedlist/undecoratedlist.component.html"),
            styles: [__webpack_require__(/*! ./undecoratedlist.component.scss */ "./src/app/undecoratedlist/undecoratedlist.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UndecoratedlistComponent);
    return UndecoratedlistComponent;
}());



/***/ }),

/***/ "./src/app/url-permission.module.ts":
/*!******************************************!*\
  !*** ./src/app/url-permission.module.ts ***!
  \******************************************/
/*! exports provided: PermissionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionModule", function() { return PermissionModule; });
var PermissionModule = /** @class */ (function () {
    function PermissionModule() {
    }
    PermissionModule.canNavigate = function (userPermissions, urlSegments) {
        var isAllowed = true;
        var neededPermission;
        var permissionObject;
        var requiredCodename;
        // Check that the user is allowed to access to each segment
        for (var _i = 0, urlSegments_1 = urlSegments; _i < urlSegments_1.length; _i++) {
            var segment = urlSegments_1[_i];
            permissionObject = PermissionModule.UrlPermissionMap[segment];
            neededPermission = permissionObject && !!permissionObject.codename;
            if (neededPermission) {
                requiredCodename = permissionObject.codename;
                isAllowed = isAllowed && userPermissions.findIndex(function (codename) { return requiredCodename === codename; }) > -1;
            }
            if (permissionObject) {
                permissionObject = permissionObject.children;
            }
        }
        return isAllowed;
    };
    /***
     * These object creates a mapping between the url section and the
     * needed permissions in order to access it.
     *
     * Note that these are just access permissions. This does not take into account
     * if the user has rights to modify/delete elements.
     *
     * Not restricted urls does not need to appear in this map. They will be allowed
     * by default
     */
    PermissionModule.UrlPermissionMap = {
        'user-management': {
            codename: 'user-management',
            children: {}
        },
        'experiments': {
            codename: null,
            children: {
                'create': {
                    codename: 'experiments_create',
                    children: {}
                }
            }
        },
        'groups': {
            codename: 'groups',
            children: {}
        },
    };
    return PermissionModule;
}());



/***/ }),

/***/ "./src/app/user-management/user-management.component.html":
/*!****************************************************************!*\
  !*** ./src/app/user-management/user-management.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"storage-container\">\n    <div class=\"main-container\" (click)=\"clearSelection()\">\n        <form class=\"signup-form\" [formGroup]=\"form\"\n              (ngSubmit)=\"filterResults()\">\n            <div class=\"filters storage-common-styles\">\n                <h2 id=\"filters-header\" class=\"section-header\">Filters</h2>\n                <div class=\"filtering-fields\">\n                    <mdc-text-field class=\"filter-field mdc-text-field-custom\"\n                                    placeholder=\"Username\"\n                                    formControlName=\"username\">\n                    </mdc-text-field>\n                    <mdc-text-field class=\"filter-field mdc-text-field-custom\"\n                                    type=\"email\"\n                                    placeholder=\"E-mail\"\n                                    formControlName=\"email\">\n                    </mdc-text-field>\n                    <mdc-text-field class=\"filter-field mdc-text-field-custom\"\n                                    placeholder=\"Group\"\n                                    formControlName=\"group\">\n                    </mdc-text-field>\n                </div>\n                <div style=\"display: flex;flex-direction: row;justify-content:\n                flex-end\">\n                    <button class=\"filter-button\" mdc-button primary\n                            (click)=\"clearFilters()\">\n                        Clear\n                    </button>\n                    <button class=\"filter-button\" mdc-button primary\n                            type=\"submit\">\n                        Filter\n                    </button>\n\n                </div>\n            </div>\n        </form>\n        <div class=\"list-zone\">\n            <div class=\"result-header-container\">\n                <h2 id=\"result-header\" class=\"section-header\">Results</h2>\n            </div>\n            <div class=\"results\">\n                <ul *ngFor=\"let user of userList; let i = index\"\n                    class=\"details-item\">\n                    <div class=\"details-item-wrapper\"\n                         (click)=\"selectUser($event, user, i)\">\n                        <li>{{ user.username }}</li>\n                        <li>{{ user.email }}</li>\n                        <li>\n                            <button mdc-button class=\"remove-button\"\n                                    (click)=\"deleteUser(user)\">\n                                <mdc-icon>delete_forever</mdc-icon>\n                                Remove\n                            </button>\n                        </li>\n\n                    </div>\n                </ul>\n            </div>\n        </div>\n    </div>\n    <div id=\"details-container\" *ngIf=\"getSelectedUser()\">\n        <!--\n            This container could be a component to encapsulate\n            all the logic of compounding the detail view\n         -->\n        <div class=\"detail-header\">\n            <div class=\"text-wrapper\">\n                <h2 class=\"details-header\">\n                    {{getSelectedUser().username}}\n                </h2>\n            </div>\n            <div class=\"actions-container\">\n                <div class=\"content-body\">\n                    <ul class=\"actions\">\n                        <li class=\"action-item\" *ngIf=\"!isSameUser()\">\n                            <i class=\"material-icons\"\n                               *ngIf=\"!getSelectedUser().isAdmin\"\n                               (click)=\"promoteUser()\">upgrade\n                            </i>\n                            <i class=\"material-icons up-down\"\n                               *ngIf=\"getSelectedUser().isAdmin\"\n                               (click)=\"demoteUser()\">upgrade\n                            </i>\n                        </li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"line-container\">\n            <div class=\"line\"></div>\n        </div>\n\n        <div class=\"details-body\">\n            <div class=\"detail-container\">\n                <h2 class=\"content-title\">Basic Information</h2>\n                <div class=\"content-body\">\n                    <div class=\"row\" style=\"display: flex;\n                         flex-direction: row;\">\n                        <mdc-text-field class=\"first-element\"\n                                        label=\"Name\"\n                                        [(ngModel)]=\"selectedUser.username\">\n                        </mdc-text-field>\n                        <mdc-text-field class=\"element\"\n                                        label=\"Last name\"\n                                        [(ngModel)]=\"selectedUser.lastname\">\n                        </mdc-text-field>\n                    </div>\n\n                    <mdc-text-field class=\"element\" label=\"email\"\n                                    type=\"email\" readonly=\"1\"\n                                    [ngModel]=\"selectedUser.email\">\n                    </mdc-text-field>\n\n                </div>\n            </div>\n\n            <div class=\"detail-container\">\n                <h2 class=\"content-title\">Groups</h2>\n                <div class=\"content-body\">\n                    <ul class=\"undecorated-list horizontal-list\">\n                        <li>uno</li>\n                    </ul>\n\n                    <!-- TODO Maybe this should be a text element with\n                          suggestions\n                    -->\n                    <file-uploader\n                            name=\"Groups\"\n                            helperText=\"Add additional groups\"\n                    ></file-uploader>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/user-management/user-management.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/user-management/user-management.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".storage-item {\n  margin: 10px 0;\n  display: block; }\n\n.storage-container {\n  display: flex;\n  flex-direction: row; }\n\n.storage-container .results {\n    overflow-y: auto;\n    display: flex;\n    flex-direction: row;\n    height: 64.4vh;\n    max-height: 64.4vh;\n    width: 99.8%;\n    padding: 6px;\n    flex-wrap: wrap; }\n\n.storage-container .main-container {\n    flex: 4 0; }\n\n.storage-container #details-container {\n    max-width: 32%;\n    min-width: 32%;\n    margin: 0 10px;\n    flex: 1 0; }\n\n.storage-container #details-container .detail-header {\n      text-align: center; }\n\n.storage-container #details-container .content-title {\n      margin-bottom: 0;\n      margin-top: 35px; }\n\n.storage-container #details-container .content-body {\n      display: flex;\n      flex-direction: column; }\n\n.storage-container #details-container .content-body .element, .storage-container #details-container .content-body .row .first-element, .storage-container #details-container .content-body .row .last-element {\n        flex: 1;\n        margin-top: 5px;\n        margin-bottom: 5px; }\n\n.storage-container #details-container .content-body .row {\n        display: flex;\n        flex-direction: row; }\n\n.storage-container #details-container .content-body .row .first-element {\n          margin-right: 10px; }\n\n.storage-container #details-container .content-body .row .last-element {\n          margin-left: 10px; }\n\n.storage-container .storage-common-styles {\n    margin: 1px 10px; }\n\n.storage-container .storage-common-styles .filters {\n      margin-bottom: 10px; }\n\n.actions {\n  list-style-type: none;\n  -webkit-padding-start: 10px;\n          padding-inline-start: 10px;\n  display: inline-flex; }\n\n.actions li:hover {\n    text-shadow: 0 0 1px black;\n    cursor: pointer;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n\n.actions li.action-item {\n    margin: 0 5px; }\n\n.actions li.action-item i {\n      font-size: 25px; }\n\n.float-right {\n  float: right; }\n\n.actions-container {\n  display: inline-flex;\n  overflow: hidden; }\n\n.actions-container:after {\n    clear: both; }\n\n.actions-container ul {\n    -webkit-margin-after: 0;\n            margin-block-end: 0; }\n\n.text-wrapper {\n  display: inline-flex;\n  max-width: 76%; }\n\n.details-header {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n\ndiv.line-container {\n  text-align: center; }\n\ndiv.line-container div.line {\n    border-bottom: solid 1px black;\n    width: 90%;\n    display: inline-block; }\n\n.content-body-list {\n  list-style-type: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0;\n  margin-top: 0;\n  margin-bottom: 5px; }\n\n.details-body {\n  display: flex;\n  flex-direction: column;\n  overflow-y: auto;\n  max-height: 79.5vh; }\n\n.list-zone {\n  display: flex;\n  flex-direction: column;\n  margin: 10px 10px 0px 10px; }\n\n.section-header {\n  position: relative;\n  height: auto;\n  border-bottom: solid 1px #dddddd; }\n\n.result-header-container {\n  position: relative; }\n\n.result-header-container .add-button {\n    position: absolute;\n    top: 5px;\n    right: 0;\n    margin-right: 35px;\n    width: 40px;\n    height: 40px;\n    background-color: #005db9; }\n\n.result-header-container .add-button .icon {\n      color: white; }\n\n.undecorated-list {\n  list-style: none;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0; }\n\n.undecorated-list.horizontal-list {\n    display: flex;\n    flex-wrap: wrap; }\n\n.undecorated-list.horizontal-list li {\n      display: inline;\n      margin: 3px 5px;\n      padding: 5px;\n      background-color: #dddddd;\n      border-radius: 4px; }\n\n.wrap-content {\n  display: flex;\n  flex-wrap: wrap; }\n\n.filtering-fields {\n  display: flex;\n  flex-direction: row; }\n\n.filter-field {\n  flex: 1;\n  margin: 2px 5px; }\n\n.filter-button {\n  flex: 0;\n  margin: 5px 5px; }\n\n.remove-button {\n  color: red; }\n\n.remove-button:hover {\n    color: white;\n    background-color: red;\n    box-shadow: lightgray -1px 1px 3px; }\n\n.details-item {\n  list-style-type: none;\n  -webkit-margin-before: 0;\n          margin-block-start: 0;\n  -webkit-margin-after: 0;\n          margin-block-end: 0;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0; }\n\n.details-item .details-item-wrapper {\n    padding: 30px; }\n\n.details-item .details-item-wrapper:hover, .details-item .details-item-wrapper.active {\n      box-shadow: rgba(0, 93, 185, 0.2) 0px 0px 5px; }\n\n.up-down {\n  transform: rotateZ(180deg); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2plc3VzL0RvY3VtZW50cy9yZXBvc2l0b3JpZXMvdGZnL21kYWcvcHJvamVjdC9tRGFnRnJvbnQvc3JjL2FwcC91c2VyLW1hbmFnZW1lbnQvdXNlci1tYW5hZ2VtZW50LmNvbXBvbmVudC5zY3NzIiwiL2hvbWUvamVzdXMvRG9jdW1lbnRzL3JlcG9zaXRvcmllcy90ZmcvbWRhZy9wcm9qZWN0L21EYWdGcm9udC9zcmMvX21hdGVyaWFsdmFycy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsY0FBYztFQUNkLGNBQWMsRUFBQTs7QUFJaEI7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CLEVBQUE7O0FBRnJCO0lBS0ksZ0JBQWdCO0lBQ2hCLGFBQWE7SUFDYixtQkFBbUI7SUFHbkIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osWUFBWTtJQUNaLGVBQWUsRUFBQTs7QUFkbkI7SUFrQkksU0FBUyxFQUFBOztBQWxCYjtJQXNCSSxjQUFjO0lBQ2QsY0FBYztJQUNkLGNBQWM7SUFDZCxTQUFTLEVBQUE7O0FBekJiO01BNEJNLGtCQUFrQixFQUFBOztBQTVCeEI7TUFnQ00sZ0JBQWdCO01BQ2hCLGdCQUFnQixFQUFBOztBQWpDdEI7TUFxQ00sYUFBYTtNQUNiLHNCQUFzQixFQUFBOztBQXRDNUI7UUF5Q1EsT0FBTztRQUNQLGVBQWU7UUFDZixrQkFBa0IsRUFBQTs7QUEzQzFCO1FBK0NRLGFBQWE7UUFDYixtQkFBbUIsRUFBQTs7QUFoRDNCO1VBb0RVLGtCQUFrQixFQUFBOztBQXBENUI7VUF5RFUsaUJBQWlCLEVBQUE7O0FBekQzQjtJQWdFSSxnQkFBZ0IsRUFBQTs7QUFoRXBCO01BbUVNLG1CQUFtQixFQUFBOztBQU16QjtFQUNFLHFCQUFxQjtFQUNyQiwyQkFBMEI7VUFBMUIsMEJBQTBCO0VBQzFCLG9CQUFvQixFQUFBOztBQUh0QjtJQU9NLDBCQUEwQjtJQUMxQixlQUFlO0lBQ2YseUJBQWlCO09BQWpCLHNCQUFpQjtRQUFqQixxQkFBaUI7WUFBakIsaUJBQWlCLEVBQUE7O0FBVHZCO0lBYU0sYUFBYSxFQUFBOztBQWJuQjtNQWdCUSxlQUFlLEVBQUE7O0FBTXZCO0VBQ0UsWUFBWSxFQUFBOztBQUdkO0VBQ0Usb0JBQW9CO0VBQ3BCLGdCQUFnQixFQUFBOztBQUZsQjtJQUtJLFdBQVcsRUFBQTs7QUFMZjtJQVNJLHVCQUFtQjtZQUFuQixtQkFBbUIsRUFBQTs7QUFJdkI7RUFDRSxvQkFBb0I7RUFDcEIsY0FBYyxFQUFBOztBQUdoQjtFQUNFLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsdUJBQXVCLEVBQUE7O0FBSXpCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRHBCO0lBSUksOEJBQThCO0lBQzlCLFVBQVU7SUFDVixxQkFBcUIsRUFBQTs7QUFNekI7RUFDRSxxQkFBcUI7RUFDckIsd0JBQXVCO1VBQXZCLHVCQUF1QjtFQUN2QixhQUFhO0VBQ2Isa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QiwwQkFBMEIsRUFBQTs7QUFHNUI7RUFDRSxrQkFBa0I7RUFFbEIsWUFBWTtFQUNaLGdDQUFnQyxFQUFBOztBQUlsQztFQUNFLGtCQUFrQixFQUFBOztBQURwQjtJQUlJLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsUUFBUTtJQUNSLGtCQUFrQjtJQUNsQixXQVRrQjtJQVVsQixZQVZrQjtJQVdsQix5QkN4THVCLEVBQUE7O0FEOEszQjtNQWFNLFlBQVksRUFBQTs7QUFLbEI7RUFDRSxnQkFBZ0I7RUFDaEIsd0JBQXVCO1VBQXZCLHVCQUF1QixFQUFBOztBQUZ6QjtJQUtJLGFBQWE7SUFDYixlQUFlLEVBQUE7O0FBTm5CO01BU00sZUFBZTtNQUNmLGVBQWU7TUFDZixZQUFZO01BQ1oseUJDM011QjtNRDRNdkIsa0JBQWtCLEVBQUE7O0FBTXhCO0VBQ0UsYUFBYTtFQUNiLGVBQWUsRUFBQTs7QUFNakI7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsT0FBTztFQUNQLGVBQWUsRUFBQTs7QUFJakI7RUFDRSxPQUFPO0VBQ1AsZUFBZSxFQUFBOztBQUtqQjtFQUNFLFVBQVUsRUFBQTs7QUFEWjtJQUlJLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsa0NBQWtDLEVBQUE7O0FBS3RDO0VBQ0UscUJBQXFCO0VBQ3JCLHdCQUFxQjtVQUFyQixxQkFBcUI7RUFDckIsdUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQix3QkFBdUI7VUFBdkIsdUJBQXVCLEVBQUE7O0FBSnpCO0lBT0ksYUFBYSxFQUFBOztBQVBqQjtNQVVNLDZDQUFxRCxFQUFBOztBQU0zRDtFQUNFLDBCQUEwQixFQUFBIiwiZmlsZSI6Ii4uL3NyYy9hcHAvdXNlci1tYW5hZ2VtZW50L3VzZXItbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi9tYXRlcmlhbHZhcnNcIjtcblxuLnN0b3JhZ2UtaXRlbSB7XG4gIG1hcmdpbjogMTBweCAwO1xuICBkaXNwbGF5OiBibG9jaztcblxufVxuXG4uc3RvcmFnZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuXG4gICYgLnJlc3VsdHMge1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuXG4gICAgLy8gRXhwZXJpbWVudGFsIHZhbHVlcy4gUGxheSB3aXRoIHRoZW0gaW4gb3JkZXIgdG8gc2V0IHVwIGNvcnJlY3RseVxuICAgIGhlaWdodDogNjQuNHZoO1xuICAgIG1heC1oZWlnaHQ6IDY0LjR2aDtcbiAgICB3aWR0aDogOTkuOCU7XG4gICAgcGFkZGluZzogNnB4O1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgfVxuXG4gICYgLm1haW4tY29udGFpbmVyIHtcbiAgICBmbGV4OiA0IDA7XG4gIH1cblxuICAmICNkZXRhaWxzLWNvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiAzMiU7XG4gICAgbWluLXdpZHRoOiAzMiU7XG4gICAgbWFyZ2luOiAwIDEwcHg7XG4gICAgZmxleDogMSAwO1xuXG4gICAgJiAuZGV0YWlsLWhlYWRlciB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgJiAuY29udGVudC10aXRsZSB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgICAgbWFyZ2luLXRvcDogMzVweDtcbiAgICB9XG5cbiAgICAmIC5jb250ZW50LWJvZHkge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5cbiAgICAgICYgLmVsZW1lbnQge1xuICAgICAgICBmbGV4OiAxO1xuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgIH1cblxuICAgICAgJiAucm93IHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcblxuICAgICAgICAmIC5maXJzdC1lbGVtZW50IHtcbiAgICAgICAgICBAZXh0ZW5kIC5lbGVtZW50O1xuICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgfVxuXG4gICAgICAgICYgLmxhc3QtZWxlbWVudCB7XG4gICAgICAgICAgQGV4dGVuZCAuZWxlbWVudDtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gICYgLnN0b3JhZ2UtY29tbW9uLXN0eWxlcyB7XG4gICAgbWFyZ2luOiAxcHggMTBweDtcblxuICAgICYgLmZpbHRlcnMge1xuICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICB9XG4gIH1cbn1cblxuXG4uYWN0aW9ucyB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDEwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuXG4gICYgbGkge1xuICAgICY6aG92ZXIge1xuICAgICAgdGV4dC1zaGFkb3c6IDAgMCAxcHggYmxhY2s7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgICB9XG5cbiAgICAmLmFjdGlvbi1pdGVtIHtcbiAgICAgIG1hcmdpbjogMCA1cHg7XG5cbiAgICAgICYgaSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLmZsb2F0LXJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uYWN0aW9ucy1jb250YWluZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcblxuICAmOmFmdGVyIHtcbiAgICBjbGVhcjogYm90aDtcbiAgfVxuXG4gICYgdWwge1xuICAgIG1hcmdpbi1ibG9jay1lbmQ6IDA7XG4gIH1cbn1cblxuLnRleHQtd3JhcHBlciB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBtYXgtd2lkdGg6IDc2JTtcbn1cblxuLmRldGFpbHMtaGVhZGVyIHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5cblxuZGl2LmxpbmUtY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICYgZGl2LmxpbmUge1xuICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCBibGFjaztcbiAgICB3aWR0aDogOTAlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxufVxuXG5cbi8vIERhdGUgbGlzdCBzdHlsZXNcbi5jb250ZW50LWJvZHktbGlzdCB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDA7XG4gIG1hcmdpbi10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cblxuXG4uZGV0YWlscy1ib2R5IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgbWF4LWhlaWdodDogNzkuNXZoO1xufVxuXG5cbi5saXN0LXpvbmUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBtYXJnaW46IDEwcHggMTBweCAwcHggMTBweDtcbn1cblxuLnNlY3Rpb24taGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gIGhlaWdodDogYXV0bztcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNkZGRkZGQ7XG59XG5cbiRhZGQtYnV0dG9uLXNpemU6IDQwcHg7XG4ucmVzdWx0LWhlYWRlci1jb250YWluZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgJiAuYWRkLWJ1dHRvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNXB4O1xuICAgIHJpZ2h0OiAwO1xuICAgIG1hcmdpbi1yaWdodDogMzVweDtcbiAgICB3aWR0aDogJGFkZC1idXR0b24tc2l6ZTtcbiAgICBoZWlnaHQ6ICRhZGQtYnV0dG9uLXNpemU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJG1kYy10aGVtZS1wcmltYXJ5O1xuXG4gICAgJiAuaWNvbiB7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICB9XG59XG5cbi51bmRlY29yYXRlZC1saXN0IHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDA7XG5cbiAgJi5ob3Jpem9udGFsLWxpc3Qge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuXG4gICAgJiBsaSB7XG4gICAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgICBtYXJnaW46IDNweCA1cHg7XG4gICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkbWRjLXRoZW1lLXNlY29uZGFyeTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICB9XG5cbiAgfVxufVxuXG4ud3JhcC1jb250ZW50IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuXG59XG5cbi8vIEZpbHRlciBzdHlsZXNcblxuLmZpbHRlcmluZy1maWVsZHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuXG4uZmlsdGVyLWZpZWxkIHtcbiAgZmxleDogMTtcbiAgbWFyZ2luOiAycHggNXB4O1xufVxuXG5cbi5maWx0ZXItYnV0dG9uIHtcbiAgZmxleDogMDtcbiAgbWFyZ2luOiA1cHggNXB4O1xufVxuXG5cbi8vIEJ1dHRvbiBzdHlsZXNcbi5yZW1vdmUtYnV0dG9uIHtcbiAgY29sb3I6IHJlZDtcblxuICAmOmhvdmVyIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICAgIGJveC1zaGFkb3c6IGxpZ2h0Z3JheSAtMXB4IDFweCAzcHg7XG4gIH1cbn1cblxuXG4uZGV0YWlscy1pdGVtIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICBtYXJnaW4tYmxvY2stc3RhcnQ6IDA7XG4gIG1hcmdpbi1ibG9jay1lbmQ6IDA7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwO1xuXG4gICYgLmRldGFpbHMtaXRlbS13cmFwcGVyIHtcbiAgICBwYWRkaW5nOiAzMHB4O1xuXG4gICAgJjpob3ZlciwgJi5hY3RpdmUge1xuICAgICAgYm94LXNoYWRvdzogcmdiYSgkbWRjLXRoZW1lLXByaW1hcnksIDAuMikgMHB4IDBweCA1cHg7XG4gICAgfVxuICB9XG5cbn1cblxuLnVwLWRvd24ge1xuICB0cmFuc2Zvcm06IHJvdGF0ZVooMTgwZGVnKTtcbn0iLCIkbWRjLXRoZW1lLXByaW1hcnk6ICMwMDVkYjk7XG4kbWRjLXRoZW1lLXNlY29uZGFyeTogI2RkZGRkZDtcbiRtZGMtdGhlbWUtc2Vjb25kYXJ5LWxpZ2h0OiAjZjVmNWY1O1xuJG1kYy10aGVtZS1iYWNrZ3JvdW5kOiAjZmZmO1xuIl19 */"

/***/ }),

/***/ "./src/app/user-management/user-management.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/user-management/user-management.component.ts ***!
  \**************************************************************/
/*! exports provided: UserManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserManagementComponent", function() { return UserManagementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user/user.service */ "./src/app/user/user.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notifications/notifications.service */ "./src/app/notifications/notifications.service.ts");





var UserManagementComponent = /** @class */ (function () {
    function UserManagementComponent(userService, notificationsService) {
        var _this = this;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(150)]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]),
            group: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(150)]),
        });
        this.userList = [];
        this.userService = userService;
        this.currentIndex = null;
        this.selectedUser = null;
        this.originalUser = null;
        this.notificationsService = notificationsService;
        this.userService.onLogin.subscribe(function (user) { return _this.setManagingUser(user); });
        this.userService.onLogout.subscribe(function () { return _this.setManagingUser(null); });
    }
    UserManagementComponent.prototype.ngOnInit = function () {
        this.user = this.userService.getUser();
        this.loadUsers();
    };
    UserManagementComponent.prototype.getSelectedUser = function () {
        return this.selectedUser;
    };
    UserManagementComponent.prototype.clearSelection = function () {
        if (this.selectedUser && this.selectedUser.serialize() != this.originalUser) {
            this.showUserChangedWarning();
        }
        else {
            if (this.currentIndex != null) {
                this.selectedUser = null;
                this.unselectIndex();
            }
        }
    };
    UserManagementComponent.prototype.selectUser = function (event, user, index) {
        event.stopPropagation();
        if (this.currentIndex != null) {
            this.unselectIndex();
        }
        this.selectIndex(index);
        this.selectedUser = user;
        this.originalUser = this.selectedUser.serialize();
    };
    UserManagementComponent.prototype.deleteUser = function (user) {
        var _this = this;
        this.userService.delete(user).then(function (response) {
            var elementIdx = _this.userList.indexOf(user);
            if (elementIdx > -1) {
                _this.userList.splice(elementIdx, 1);
            }
        }).catch(function (error) { return _this.notificationsService.notify(error['error']['response'], _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__["NotificationsService"].MESSAGE_TYPES.WARNING); });
    };
    UserManagementComponent.prototype.clearFilters = function () {
        this.form.reset();
    };
    UserManagementComponent.prototype.filterResults = function () {
        this.loadUsers(this.form.value);
    };
    /**
     * Turn a plain user into an admin
     */
    UserManagementComponent.prototype.isSameUser = function () {
        var same = false;
        var selectedUser = this.getSelectedUser();
        if (this.user && selectedUser) {
            same = this.user.id === selectedUser.id;
        }
        return same;
    };
    /**
     * Turn a plain user into an admin
     */
    UserManagementComponent.prototype.promoteUser = function () {
        if (this.isSameUser()) {
            return;
        }
        this.userService.promote(this.getSelectedUser());
    };
    /**
     * Turn an admin user into a normal user
     */
    UserManagementComponent.prototype.demoteUser = function () {
        if (this.isSameUser()) {
            return;
        }
        this.userService.demote(this.getSelectedUser());
    };
    UserManagementComponent.prototype.setManagingUser = function (user) {
        this.user = user;
    };
    UserManagementComponent.prototype.loadUsers = function (filters) {
        var _this = this;
        this.userService.list(filters).then(function (users) {
            if (users.length == 0) {
                _this.notificationsService.notify("No users found");
            }
            _this.userList = users;
        });
    };
    UserManagementComponent.prototype.showUserChangedWarning = function () {
        // TODO Missing modal
    };
    UserManagementComponent.prototype.unselectIndex = function () {
        var elements = document.getElementsByClassName("details-item-wrapper");
        var element = elements[this.currentIndex];
        element.classList.remove('active');
        this.currentIndex = null;
    };
    UserManagementComponent.prototype.selectIndex = function (index) {
        this.currentIndex = index;
        var elements = document.getElementsByClassName("details-item-wrapper");
        var element = elements[this.currentIndex];
        element.classList.add('active');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserManagementComponent.prototype, "form", void 0);
    UserManagementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-management',
            template: __webpack_require__(/*! ./user-management.component.html */ "./src/app/user-management/user-management.component.html"),
            styles: [__webpack_require__(/*! ./user-management.component.scss */ "./src/app/user-management/user-management.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_4__["NotificationsService"]])
    ], UserManagementComponent);
    return UserManagementComponent;
}());



/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  user works!\n</p>\n"

/***/ }),

/***/ "./src/app/user/user.component.scss":
/*!******************************************!*\
  !*** ./src/app/user/user.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIuLi9zcmMvYXBwL3VzZXIvdXNlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserComponent = /** @class */ (function () {
    function UserComponent() {
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.scss */ "./src/app/user/user.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/app/user/user.model.ts":
/*!************************************!*\
  !*** ./src/app/user/user.model.ts ***!
  \************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(data) {
        var user_obj = data;
        if (typeof (data) === 'string') {
            user_obj = this.deserialize(data);
        }
        this.initialize(user_obj);
    }
    /**
     * For now it returns a JSON representation of the element.
     * @param input
     */
    User.prototype.deserialize = function (input) {
        return JSON.parse(input);
    };
    /**
     * Return an string representation of the JSON element
     */
    User.prototype.serialize = function () {
        return JSON.stringify({
            id: this._id,
            username: this._username,
            name: this.name,
            lastName: this._lastName,
            email: this._email,
            permissions: this.permissions,
            is_superuser: this._isAdmin
        });
    };
    User.prototype.getPermissions = function () {
        return this.permissions;
    };
    /**
     * Check if the user has the requested permission to navigate to a
     * page.
     * @param ref
     */
    User.prototype.hasPermission = function (ref) {
        return this.permissions.findIndex(function (perm) { return perm === ref; }) >= 0;
    };
    Object.defineProperty(User.prototype, "isAdmin", {
        get: function () {
            return this._isAdmin;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "username", {
        get: function () {
            return this._username;
        },
        set: function (name) {
            this._username = name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "lastname", {
        get: function () {
            return this._lastName;
        },
        set: function (lastname) {
            this._lastName = lastname;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (email) {
            this._email = email;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "fullName", {
        get: function () {
            var name = this.name ? this.name : "Anonymous";
            var _lastName = this._lastName ? this._lastName : "Anonymous";
            return name + " " + _lastName;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Fil the User instance with the configured data
     * @param user_obj
     */
    User.prototype.initialize = function (user_obj) {
        this._username = user_obj['username'];
        this.name = user_obj['first_name'];
        this._lastName = user_obj['last_name'];
        this._email = user_obj['email'];
        this.permissions = user_obj['permissions'] || [];
        this._id = user_obj['id'];
        this._isAdmin = user_obj['is_superuser'];
    };
    return User;
}());



/***/ }),

/***/ "./src/app/user/user.service.ts":
/*!**************************************!*\
  !*** ./src/app/user/user.service.ts ***!
  \**************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user.model */ "./src/app/user/user.model.ts");
/* harmony import */ var _global_services_cookie_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global-services/cookie-service.service */ "./src/app/global-services/cookie-service.service.ts");
/* harmony import */ var _group_group_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../group/group.model */ "./src/app/group/group.model.ts");






// import {CookieService} from "ngx-cookie-service";
// We suppose angular adds the necessary cookies to keep authorization on going
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
    "Content-Type": "application/json",
});
var httpOptions = {
    headers: headers,
};
var BASE_URL = "webservice/user/";
/**
 * This service is in charge of querying the DB ( check if this should query
 * a repository instead of the DB directly) in order to retrieve any info
 * about all the files stored.
 */
var UserService = /** @class */ (function () {
    function UserService(http, cookieService) {
        this.onLogout = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onLogin = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.http = http;
        this.cookieService = cookieService;
        this._isSignedIn = this.cookieService.get('mdag_session') == 1;
        if (this._isSignedIn) {
            // Update the info of the current logged user
            this.user = new _user_model__WEBPACK_IMPORTED_MODULE_3__["User"](sessionStorage.getItem("mdag_user"));
            this.getUserInfo();
        }
    }
    UserService.prototype.list = function (filters) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = BASE_URL + "get_all_users";
            var body = {
                'optionals': filters || {}
            };
            _this.http.post(url, body, httpOptions).toPromise().then(function (response) {
                var users = response['response'].map(function (userObj) { return new _user_model__WEBPACK_IMPORTED_MODULE_3__["User"](userObj); });
                resolve(users);
            }).catch(function (reason) {
                reject(null);
            });
        });
    };
    UserService.prototype.signIn = function (username, password) {
        var _this = this;
        var url = BASE_URL + "sign_in";
        var body = { "args": [username, password] };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, httpOptions).toPromise().then(function (response) {
                if (response["code"] == 200) {
                    console.log("Nuevo login", document.cookie);
                    var user = new _user_model__WEBPACK_IMPORTED_MODULE_3__["User"](response["response"]);
                    _this.saveUserInfo(user);
                    _this.onLogin.emit(user);
                    resolve(user);
                }
                else {
                    // TODO Maybe this should be a 401 error
                    console.log("Failed to login");
                    reject("Failed to login");
                }
            }).catch(function (error) {
                reject(error);
                console.log("Error capturado en servicio", error);
            });
        });
    };
    UserService.prototype.signOut = function () {
        var _this = this;
        var url = BASE_URL + "sign_out";
        var promise = this.http.post(url, httpOptions).toPromise();
        return promise.then(function (response) {
            _this.onLogout.emit();
            sessionStorage.removeItem("mdag_user");
            _this.cookieService.remove("mdag_session");
        });
    };
    /**
     * TODO This could also work for admin users if the current session is
     *  held by an admin user
     * Register common users
     * @param username
     * @param password
     * @param email
     */
    UserService.prototype.signUp = function (username, password, email, name, lastName) {
        var _this = this;
        var url = BASE_URL + "sign_up";
        var body = { "args": [username, email, password, name, lastName] };
        var promise = this.http.post(url, body, httpOptions).toPromise();
        return promise.then(function (response) {
            if (response["code"] == 200) {
                var user = new _user_model__WEBPACK_IMPORTED_MODULE_3__["User"](response["response"]);
                _this.user = user;
                _this.saveUserInfo(user);
            }
        });
    };
    UserService.prototype.delete = function (user) {
        console.assert(user != null, "Called delete without user");
        var url = BASE_URL + 'delete_user';
        var body = { "args": [user.id] };
        return this.http.post(url, body, httpOptions).toPromise();
    };
    /**
     * Returns whether the user has signed in or not.
     * If this should be persistent the page should not be
     * reloaded or the back-end should have a service to check if
     * the user is still signed in
     */
    UserService.prototype.isSignedIn = function () {
        var isSignedIn = this.cookieService.check("mdag_session");
        if (!isSignedIn) {
            this.user = null;
            sessionStorage.removeItem("mdag_user");
            return false;
        }
        return isSignedIn;
    };
    UserService.prototype.getUser = function () {
        return this.user;
    };
    /**
     * Create a new group in the system whose owner will be the current
     * logged user
     */
    UserService.prototype.createGroup = function (name, description, members) {
        var member_ids = [];
        for (var _i = 0, members_1 = members; _i < members_1.length; _i++) {
            var member = members_1[_i];
            member_ids.push(member.id);
        }
        var body = {
            "args": [name],
            "optionals": {
                "description": description,
                "members": member_ids
            }
        };
        var url = BASE_URL + "create_group";
        var promise = this.http.post(url, body, httpOptions).toPromise();
        return promise.then(function (response) {
            if (response["code"] == 200) {
                console.log(response['response']);
            }
        });
    };
    /**
     * Upadtes an existent group. Take into account that there are 2 things
     * that are not currently allowed.
     * Removing all the name, and removing the owner from the group.
     *
     * @param id
     * @param name
     * @param description
     * @param members
     */
    UserService.prototype.updateGroup = function (id, name, description, members) {
        var member_ids = [];
        for (var _i = 0, members_2 = members; _i < members_2.length; _i++) {
            var member = members_2[_i];
            member_ids.push(member.id);
        }
        var body = {
            "args": [id],
            "optionals": {
                "name": name,
                "description": description,
                "members": member_ids
            }
        };
        var url = BASE_URL + "update_group";
        var promise = this.http.post(url, body, httpOptions).toPromise();
        return promise.then(function (response) {
            if (response["code"] == 200) {
                console.log(response['response']);
            }
        });
    };
    UserService.prototype.listGroups = function (groupName, owner) {
        var _this = this;
        var filters = {};
        if (groupName != null) {
            filters['group_name'] = groupName;
        }
        if (owner != null) {
            filters['owner'] = owner;
        }
        return new Promise(function (resolve, reject) {
            var url = BASE_URL + "get_groups";
            var body = {
                'optionals': filters || {}
            };
            _this.http.post(url, body, httpOptions).toPromise().then(function (response) {
                var groups = response['response'].map(function (groupObj) { return new _group_group_model__WEBPACK_IMPORTED_MODULE_5__["Group"](groupObj); });
                resolve(groups);
            }).catch(function (reason) {
                reject(null);
            });
        });
    };
    UserService.prototype.demote = function (user) {
        var body = { "args": [user.id] };
        var url = BASE_URL + "demote";
        var promise = this.http.post(url, body, httpOptions).toPromise();
        return promise.then(function (response) {
            if (response["code"] == 200) {
                console.log(response['response']);
            }
        });
    };
    UserService.prototype.promote = function (user) {
        var body = { "args": [user.id] };
        var url = BASE_URL + "promote";
        var promise = this.http.post(url, body, httpOptions).toPromise();
        return promise.then(function (response) {
            if (response["code"] == 200) {
                console.log(response['response']);
            }
        });
    };
    UserService.prototype.getUserInfo = function () {
        var _this = this;
        var url = BASE_URL + "get_user_info";
        var promise = this.http.post(url, {}, httpOptions).toPromise();
        return promise.then(function (response) {
            if (response["code"] == 200) {
                var user = new _user_model__WEBPACK_IMPORTED_MODULE_3__["User"](response["response"]);
                _this.user = user;
                _this.saveUserInfo(user);
            }
        });
    };
    UserService.prototype.saveUserInfo = function (user) {
        this.setSessionCookie();
        this.user = user;
        sessionStorage.setItem("mdag_user", user.serialize());
    };
    UserService.prototype.setSessionCookie = function () {
        this.cookieService.set("mdag_session", "1", 1800000);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], UserService.prototype, "onLogout", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], UserService.prototype, "onLogin", void 0);
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _global_services_cookie_service_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    webServiceRootUrl: 'webservice/',
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/file-item-handler.service.ts":
/*!******************************************!*\
  !*** ./src/file-item-handler.service.ts ***!
  \******************************************/
/*! exports provided: FileItemHandlerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileItemHandlerService", function() { return FileItemHandlerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FileItemHandlerService = /** @class */ (function () {
    function FileItemHandlerService() {
        this.fileSelected = null;
    }
    FileItemHandlerService.prototype.setSelectedFile = function (file) {
        if (this.fileSelected) {
            this.fileSelected['selected'] = false;
        }
        this.fileSelected = file;
        if (this.fileSelected) {
            this.fileSelected['selected'] = true;
        }
    };
    FileItemHandlerService.prototype.getSelectedFile = function () {
        return this.fileSelected;
    };
    FileItemHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FileItemHandlerService);
    return FileItemHandlerService;
}());



/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/jesus/Documents/repositories/tfg/mdag/project/mDagFront/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map