#!/usr/bin/env bash
# This builds the angular app to the build dir. Other actions can be performed by appending
# them in this file
code_folder=$(dirname $(dirname $(realpath "$0")))/build/code
mode=$1

if [ "$mode" = "prod" ]
then
    echo "Prod mode!!!"
		# In order to modify the index script and styles src urls use --deploy-url
    command_params="$command_params --base-href /metabolomics/metaweb/"
else
    command_params="$command_params --watch"
fi
    
# Print all the relevant paths
echo "Build folder: $code_folder"
# Remove the build folder
rm -rf $code_folder

echo "Building params: $command_params"

# Take into account that the compilation deletes the build folder
# ng build --prod --output-path $code_folder --output-hashing none --watch
ng build --output-path $code_folder --output-hashing none $command_params

# Remove useless files
rm -f $code_folder/3rdpartylicenses.txt
