import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PagenotfoundComponent} from './pagenotfound/pagenotfound.component';
import {HomeComponent} from "./home/home.component";
import {ExperimentListComponent} from "./storage/experiment-list.component";
import {AuthGuard} from "./auth/auth.guard";
import {ExperimentCreationWizardComponent} from "./experimentcreationwizard/experimentcreationwizard.component";
import {SignupComponent} from "./signup/signup.component";
import {UserManagementComponent} from "./user-management/user-management.component";
import {GroupsListComponent} from "./groups-list/groups-list.component";

const routes: Routes = [

    {path: "", redirectTo: "/home", pathMatch: "full"},
    {path: "home", component: HomeComponent},


    {
        path: "",

        children: [
            {
                path: "experiments",
                children: [
                    {path: "", component: ExperimentListComponent},
                ]
            },
            {
                canActivateChild: [AuthGuard],
                canActivate: [AuthGuard],
                path: "experiments",
                children: [
                    {
                        path: "create",
                        component: ExperimentCreationWizardComponent
                    }

                ],
                runGuardsAndResolvers: 'always'
            },
            {
                canActivateChild: [AuthGuard],
                canActivate: [AuthGuard],
                runGuardsAndResolvers: 'always',
                path: "user-management",
                children: [
                    {path: "", component: UserManagementComponent}

                ],
            },
        ],
    },
    {path: "signup", component: SignupComponent},
    {
        path: "groups",
        canActivateChild: [AuthGuard],
        canActivate: [AuthGuard],
        runGuardsAndResolvers: 'always',
        component: GroupsListComponent,
        children: []
    },
    // URLs That DO NOT need user permission check
    {path: "**", component: PagenotfoundComponent}

];


@NgModule({
    imports: [RouterModule.forRoot(routes, {
        onSameUrlNavigation: 'reload',
        // enableTracing: true
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
