import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {
    HTTP_INTERCEPTORS,
    HttpClientModule,
    HttpClientXsrfModule
} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PagenotfoundComponent} from './pagenotfound/pagenotfound.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MenuComponent} from './menu/menu.component';
import {MenuitemComponent} from './menuitem/menuitem.component';
import {HomeComponent} from './home/home.component';

// Modules from MD web
import {
    MdcFabModule,
    MdcButtonModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcDrawerModule,
    MdcSelectModule,
    MdcTextFieldModule,
    MdcCheckboxModule,
    MdcListModule
} from '@angular-mdc/web';

import {UndecoratedlistComponent} from './undecoratedlist/undecoratedlist.component';
import {HeadedcontainerComponent} from './headedcontainer/headedcontainer.component';
import {ExperimentListComponent} from './storage/experiment-list.component';
import {FileItemComponent} from './fileitem/fileitem.component';
import { StorageItemComponent } from './storageitem/storageitem.component';
import { CollapsiblecontainerComponent } from './collapsiblecontainer/collapsiblecontainer.component';
import {SearchfilterComponent} from "./searchfilter/searchfilter.component";
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SessionService} from "./session/session.service";
import {CsrfInterceptorService} from "./csrf/csrfincerceptor.service";
import { ModalWindowComponent } from './modal-window/modal-window.component';
import { ExperimentCreationWizardComponent } from './experimentcreationwizard/experimentcreationwizard.component';
import { WholeOrganimsExperimentFormComponent } from './experimentcreationwizard/wholeorganimsexperimentform/wholeorganimsexperimentform.component';
import { FileUploaderComponent } from './fileuploader/fileuploader.component';
import { SignupComponent } from './signup/signup.component';
import { UserManagementComponent } from './user-management/user-management.component';
import {CsrfService} from "./csrf/csrf.service";
import { OrganismComparisonExperimentComponent } from './experimentcreationwizard/organism-comparison-experiment/organism-comparison-experiment.component';
import { ExperimentComparisonComponent } from './experimentcreationwizard/experiment-comparison/experiment-comparison.component';
import { OrganismMetabolismComparisonComponent } from './experimentcreationwizard/organism-metabolism-comparison/organism-metabolism-comparison.component';
import { OrganismPathwayCalculationComponent } from './experimentcreationwizard/organism-pathway-calculation/organism-pathway-calculation.component';
import { NotificationsComponent } from './notifications/notifications.component';
import {APP_BASE_HREF, PlatformLocation} from "@angular/common";
import { GroupsListComponent } from './groups-list/groups-list.component';
import { GroupDetailsComponent } from './group-details/group-details.component';

// CSRF header name
let crsfTokenName : string = "X-CSRFToken";
// let crsfTokenName : string = "MDAG_S_HTTP_CSRFTOKEN";

@NgModule({
    declarations: [
        AppComponent,
        PagenotfoundComponent,
        HeaderComponent,
        FooterComponent,
        MenuComponent,
        MenuitemComponent,
        HomeComponent,
        UndecoratedlistComponent,
        HeadedcontainerComponent,
        ExperimentListComponent,
        FileItemComponent,
        StorageItemComponent,
        CollapsiblecontainerComponent,
        SearchfilterComponent,
        UserComponent,
        LoginComponent,
        ModalWindowComponent,
        ExperimentCreationWizardComponent,
        FileUploaderComponent,
        SignupComponent,
        UserManagementComponent,
        WholeOrganimsExperimentFormComponent,
        OrganismComparisonExperimentComponent,
        ExperimentComparisonComponent,
        OrganismMetabolismComparisonComponent,
        OrganismPathwayCalculationComponent,
        NotificationsComponent,
        GroupsListComponent,
        GroupDetailsComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        MdcFabModule,
        MdcButtonModule,
        MdcIconModule,
        MdcIconButtonModule,
        MdcDrawerModule,
        MdcSelectModule,
        MdcTextFieldModule,
        MdcCheckboxModule,
        MdcListModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientXsrfModule.withOptions({
            cookieName: crsfTokenName,
            headerName: crsfTokenName
        }),

    ],
    providers: [
        { provide: APP_BASE_HREF, useFactory: (s: PlatformLocation) => s.getBaseHrefFromDOM() ,deps: [PlatformLocation]},
        { provide: APP_INITIALIZER,useFactory: initializeApp1, deps: [CsrfService], multi: true},
        { provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptorService, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: SessionService, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}


export function initializeApp1(service: CsrfService) {
  return (): Promise<any> => {
      return service.requestToken();
  };
}