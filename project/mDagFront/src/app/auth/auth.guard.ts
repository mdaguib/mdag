import {Injectable} from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
    CanActivateChild, Router, UrlSegment
} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from "../user/user.model";
import {PermissionModule} from "../url-permission.module";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(private router: Router) {
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.canActivate(childRoute, state);
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        let userInfo = window.sessionStorage.getItem('mdag_user') || '{}';

        let user = new User(userInfo);
        let urlSegments = next.url.map( urlSegment => urlSegment.path);


        if (!user.isAdmin && !PermissionModule.canNavigate(user.getPermissions(), urlSegments)) {
            this.router.navigate(['/home'])
        }

        // Never stop a navigation!
        return true;
    }

    private static navigatingToHome(route: Array<UrlSegment>): boolean {
        return route.length == 1 && route[0].path === 'home';
    }

}
