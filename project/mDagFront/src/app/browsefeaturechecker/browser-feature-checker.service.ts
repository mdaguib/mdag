import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BrowserFeatureCheckerService {
  private supportsFormData: boolean;
  private supportsDataTransfer: boolean;
  private supportsFileReader: boolean;
  private supportsDragAndDrop: boolean;

  constructor() {
    this.supportsFormData = "FormData" in window;
    this.supportsDataTransfer = "DataTransfer" in window;
    this.supportsFileReader = "FileReader" in window;
    this.supportsDragAndDrop = this.checkSupportForDragAndDrop();
  }

  doesSupportDragAndDropFileUpload() {
    return this.supportsFormData && (this.supportsDataTransfer || this.supportsFileReader);
  }

  private checkSupportForDragAndDrop(){
    let div = document.createElement("div");
    return ("draggable" in div) || ("ondragstart" in div && "ondrop" in div);
  }
}
