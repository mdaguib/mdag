import { TestBed } from '@angular/core/testing';

import { BrowserFeatureCheckerService } from './browser-service-checker.service';

describe('BrowserservicecheckerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrowserFeatureCheckerService = TestBed.get(BrowserFeatureCheckerService);
    expect(service).toBeTruthy();
  });
});
