import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollapsiblecontainerComponent } from './collapsiblecontainer.component';

describe('CollapsiblecontainerComponent', () => {
  let component: CollapsiblecontainerComponent;
  let fixture: ComponentFixture<CollapsiblecontainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollapsiblecontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsiblecontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
