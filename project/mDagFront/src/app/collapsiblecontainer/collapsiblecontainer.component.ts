import {Component, Input, OnInit} from '@angular/core';
import {HeadedcontainerComponent} from "../headedcontainer/headedcontainer.component";

@Component({
    selector: 'app-collapsiblecontainer',
    templateUrl: '../headedcontainer/headedcontainer.component.html',
    styleUrls: ['./collapsiblecontainer.component.scss']
})
export class CollapsiblecontainerComponent extends HeadedcontainerComponent {

    constructor() {
        super();

    }

    ngOnInit() {
    }
}
