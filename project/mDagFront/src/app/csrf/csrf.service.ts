import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class CsrfService {

    http: HttpClient;

    token: string;

    constructor(http: HttpClient) {
        this.http = http;
    }

    requestToken() {
        let baseUrl: string = "webservice/csrf_tokenizer";
        return this.http.get(
            baseUrl,
            {
                observe: 'response'
            }
        ).toPromise().catch(
            (error) => {
                console.log(error);
            }
        )
    }

}
