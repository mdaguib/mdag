import {Injectable} from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from "@angular/common/http";
import {Observable} from "rxjs";
import {CsrfService} from "./csrf.service";
import {CookieService} from "../global-services/cookie-service.service";
// import {CookieService} from "ngx-cookie-service";

@Injectable()
export class CsrfInterceptorService implements HttpInterceptor {

    csrfService : CsrfService;
    cookies : CookieService;
    constructor(cookies: CookieService, csrfService: CsrfService) {
        this.csrfService = csrfService;
        this.cookies = cookies;
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       const csrfToken = this.cookies.get("csrftoken");
       let _req: HttpRequest<any>;
       if (csrfToken) {
           _req = req.clone({
                headers: req.headers.set('X-CSRFToken', csrfToken)
            });
       } else {
           _req = req;
       }
        return next.handle(_req);
    }

}
