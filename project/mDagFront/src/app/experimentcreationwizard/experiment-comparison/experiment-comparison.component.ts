import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ExperimentService} from "../../storage/experiment.service";

@Component({
  selector: 'app-experiment-comparison',
  templateUrl: './experiment-comparison.component.html',
  styleUrls: ['./experiment-comparison.component.scss']
})
export class ExperimentComparisonComponent implements OnInit {

    @Output() onExperimentCreated: EventEmitter<any> = new EventEmitter();

    @Input() form = new FormGroup({
        experimentName: new FormControl(
            '',
            [Validators.required, Validators.maxLength(50)]
        ),
        experimentDescription: new FormControl(
            '',
            [Validators.required]
        ),
        measureSimilarities: new FormControl(''),
        useAllThePathways: new FormControl(''),
        generateMDagFiles: new FormControl(''),
    });

    private experimentService: ExperimentService;
    private groups: any;
    private experimentClassification: any;

    constructor(experimentService: ExperimentService) {
        this.experimentService = experimentService;
    }

    ngOnInit() {
    }

    formSubmit() {
        // let request = {
        //     'type': this.experimentType,
        //     'name': this.experimentName,
        //     'description': this.experimentDescription,
        //     'organisms': [this.organism],
        //     'pathway': [this.pathway],
        //
        //     // File like elements
        //     'groups': this.groups,
        //     'experiment_classification': this.experimentClassification
        // };

        let formValue = this.form.value;
        // TODO Adapt to the new call
        this.experimentService.createExperimentComparison(
            formValue.experimentName,
            formValue.experimentDescription,
            formValue.organism
        ).then(
            value => {this.onExperimentCreated.emit()}
        );
    }

    onGroupFilesAdded(files) {
        this.groups = files;
    }

    onExperimentClassificationFilesAdded(files) {
        this.experimentClassification = files;
    }

}
