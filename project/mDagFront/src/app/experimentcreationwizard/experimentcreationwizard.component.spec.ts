import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperimentcreationwizardComponent } from './experimentcreationwizard.component';

describe('ExperimentcreationwizardComponent', () => {
  let component: ExperimentcreationwizardComponent;
  let fixture: ComponentFixture<ExperimentcreationwizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperimentcreationwizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperimentcreationwizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
