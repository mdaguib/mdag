import {Component, OnInit} from '@angular/core';
import {ExperimentService} from "../storage/experiment.service";
import {NavigationExtras, Router} from "@angular/router";
import {NotificationsService} from "../notifications/notifications.service";

@Component({
    selector: 'app-experimentcreationwizard',
    templateUrl: './experimentcreationwizard.component.html',
    styleUrls: ['./experimentcreationwizard.component.scss']
})
export class ExperimentCreationWizardComponent implements OnInit {

    private experimentService: ExperimentService;
    private notificationsService: NotificationsService;
    private router: Router;

    // HTML content
    public experimentTypes: Array<object> = [
        {
            name: "Pathway for a concrete organism", // Parece el treebuilder
            type: 1
        },
        {
            name: "Whole organism experiment", // Organism Metabolic builder: ¿Que diferencia hay con el MetabolicBuilder?
            type: 2
        },
        {
            name: "Pathway comparison for organisms", // Organism comparator
            type: 3
        },
        {
            name: "Experiment comparison",  // Experiments comparator
            type: 4
        },
        {
            name: "Compare metabolisms for organisms", // Metabolic comparator
            type: 5
        },

    ];

    public breadcrumbs: Array<string> = [
        "Experiment type selection",
        "Experiment data filling"
    ];

    private lastVisibleBreadcrumbIndex: number;

    private button: HTMLElement;
    private experimentType: number;

    constructor(experimentService: ExperimentService, notificationsService: NotificationsService, router: Router) {
        this.experimentService = experimentService;
        this.notificationsService = notificationsService;
        this.router = router;
    }

    ngOnInit() {
        this.button = null;
        this.experimentType = 0;
        this.lastVisibleBreadcrumbIndex = 0;
    }

    /**
     * Update the styles of the selected button
     * @param event
     */
    onClick(event) {
        if (this.button != undefined) {
            console.log("Unsetting button");
            this.button.classList.remove('experiment-selection-selected');
        }

        // Retrieve the dom element where the event handler was registered.
        // Do not get the element which fired the event as it could be another
        // element. For more information, research about html event bubbling
        this.button = event.currentTarget;
        this.button.classList.add('experiment-selection-selected');

        let value = this.button.getAttribute('data-index');
        this.experimentType = parseInt(value) || null;
        console.log(this, this.experimentType);
        if (this.experimentType) {
            this.lastVisibleBreadcrumbIndex++;
        }
        event.stopPropagation();
    }

    isExperimentTypeSelected(type) {
        return this.experimentType === type;
    }

    getVisibleBreadCrumbs() {
        return this.breadcrumbs.filter((value, index) => index <= this.lastVisibleBreadcrumbIndex);
    }

    onExperimentCreated() {
        // Show notification and navigate
        this.notificationsService.notify(
            "The experiment has been successfully requested"
        );
        this.router.navigate(["/experiments"]);
    }

}