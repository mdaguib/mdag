import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganismComparisonExperimentComponent } from './organism-comparison-experiment.component';

describe('OrganismComparisonExperimentComponent', () => {
  let component: OrganismComparisonExperimentComponent;
  let fixture: ComponentFixture<OrganismComparisonExperimentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganismComparisonExperimentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganismComparisonExperimentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
