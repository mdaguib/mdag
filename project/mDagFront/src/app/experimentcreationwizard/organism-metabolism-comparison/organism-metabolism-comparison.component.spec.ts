import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganismMetabolismComparisonComponent } from './organism-metabolism-comparison.component';

describe('OrganismMetabolismComparisonComponent', () => {
  let component: OrganismMetabolismComparisonComponent;
  let fixture: ComponentFixture<OrganismMetabolismComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganismMetabolismComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganismMetabolismComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
