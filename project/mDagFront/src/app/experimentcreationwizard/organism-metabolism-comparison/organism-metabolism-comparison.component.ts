import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ExperimentService} from "../../storage/experiment.service";

// TODO Adaptar entero, seguramente no tiene los campos que necesita para
//  hacer la comparacion
@Component({
  selector: 'app-organism-metabolism-comparison',
  templateUrl: './organism-metabolism-comparison.component.html',
  styleUrls: ['./organism-metabolism-comparison.component.scss']
})
export class OrganismMetabolismComparisonComponent implements OnInit {

    @Output() onExperimentCreated: EventEmitter<any> = new EventEmitter();

    @Input() form = new FormGroup({
        experimentName: new FormControl(
            '',
            [Validators.required, Validators.maxLength(50)]
        ),
        experimentDescription: new FormControl(
            '',
            [Validators.required]
        ),
        pathwayCode: new FormControl(
            '',
            [Validators.required]
        ),
    });

    private experimentService: ExperimentService;
    private groups: any;
    private experimentClassification: any;

    constructor(experimentService: ExperimentService) {
        this.experimentService = experimentService;
    }

    ngOnInit() {
    }

    formSubmit() {
        // let request = {
        //     'type': this.experimentType,
        //     'name': this.experimentName,
        //     'description': this.experimentDescription,
        //     'organisms': [this.organism],
        //     'pathway': [this.pathway],
        //
        //     // File like elements
        //     'groups': this.groups,
        //     'experiment_classification': this.experimentClassification
        // };

        let formValue = this.form.value;
        // TODO Adapt to the new call
        this.experimentService.createOrganismMetabolismComparison(
            formValue.experimentName,
            formValue.experimentDescription,
            formValue.organism
        ).then(
            value => {this.onExperimentCreated.emit()}
        );
    }

    onGroupFilesAdded(files) {
        this.groups = files;
    }

    onExperimentClassificationFilesAdded(files) {
        this.experimentClassification = files;
    }

}
