import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganismPathwayCalculationComponent } from './organism-pathway-calculation.component';

describe('OrganismPathwayCalculationComponent', () => {
  let component: OrganismPathwayCalculationComponent;
  let fixture: ComponentFixture<OrganismPathwayCalculationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganismPathwayCalculationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganismPathwayCalculationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
