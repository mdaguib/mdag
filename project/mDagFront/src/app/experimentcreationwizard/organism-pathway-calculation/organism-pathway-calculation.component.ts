import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ExperimentService} from "../../storage/experiment.service";

@Component({
    selector: 'app-organism-pathway-calculation',
    templateUrl: './organism-pathway-calculation.component.html',
    styleUrls: ['./organism-pathway-calculation.component.scss']
})
export class OrganismPathwayCalculationComponent implements OnInit {

    @Input() form = new FormGroup({
        experimentName: new FormControl(
            '',
            [Validators.required, Validators.maxLength(50)]
        ),
        experimentDescription: new FormControl(
            '',
            [Validators.required]
        ),
        organism: new FormControl(
            '',
            [Validators.required]
        ),
        pathway: new FormControl(
            '',
            [Validators.required]
        ),
    });

    private experimentService: ExperimentService;
    private groups: any;
    private experimentClassification: any;

    constructor(experimentService: ExperimentService) {
        this.experimentService = experimentService;
    }

    ngOnInit() {
    }

    formSubmit() {
        // let request = {
        //     'type': this.experimentType,
        //     'name': this.experimentName,
        //     'description': this.experimentDescription,
        //     'organisms': [this.organism],
        //     'pathway': [this.pathway],
        //
        //     // File like elements
        //     'groups': this.groups,
        //     'experiment_classification': this.experimentClassification
        // };

        let formValue = this.form.value;
        this.experimentService.createWholeOrganismExperiment(
            formValue.experimentName,
            formValue.experimentDescription,
            formValue.organism
        );
    }

    onGroupFilesAdded(files) {
        this.groups = files;
    }

    onExperimentClassificationFilesAdded(files) {
        this.experimentClassification = files;
    }

}
