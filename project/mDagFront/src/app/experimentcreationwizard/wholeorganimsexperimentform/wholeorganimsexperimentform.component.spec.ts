import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WholeOrganimsExperimentFormComponent } from './wholeorganimsexperimentform.component';

describe('WholeorganimsexperimentformComponent', () => {
  let component: WholeOrganimsExperimentFormComponent;
  let fixture: ComponentFixture<WholeOrganimsExperimentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WholeOrganimsExperimentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WholeOrganimsExperimentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
