import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ExperimentService} from "../../storage/experiment.service";

@Component({
    selector: 'app-wholeorganimsexperimentform',
    templateUrl: './wholeorganimsexperimentform.component.html',
    styleUrls: ['./wholeorganimsexperimentform.component.scss']
})
export class WholeOrganimsExperimentFormComponent implements OnInit {

    @Output() onExperimentCreated: EventEmitter<any> = new EventEmitter();

    @Input() form = new FormGroup({
        experimentName: new FormControl(
            '',
            [Validators.required, Validators.maxLength(50)]
        ),
        experimentDescription: new FormControl(
            '',
            [Validators.required]
        ),
        organism: new FormControl(
            '',
            [Validators.required]
        ),
    });

    private experimentService: ExperimentService;
    private groups: any;
    private experimentClassification: any;

    constructor(experimentService: ExperimentService) {
        this.experimentService = experimentService;
    }

    ngOnInit() {
    }

    formSubmit() {

        let formValue = this.form.value;
        this.experimentService.createWholeOrganismExperiment(
            formValue.experimentName,
            formValue.experimentDescription,
            formValue.organism,
            this.groups
        ).then(
            value => {this.onExperimentCreated.emit()}
        );
    }

    onGroupFilesAdded(files) {
        this.groups = files;
    }

    onExperimentClassificationFilesAdded(files) {
        this.experimentClassification = files;
    }

}
