import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";


// We suppose angular adds the necessary cookies to keep authorization on going
const headers = new HttpHeaders({
    "Content-type": "application/json",
});

const httpOptions = {
    headers: headers,
};

@Injectable({
  providedIn: 'root'
})
export class FileService {

  /** Variables */
  private readonly http: HttpClient;

  /** Constants */
  private static readonly BASE: string = 'webservice/file/';


  constructor(http: HttpClient) {
    this.http = http;
  }

  /**
   * Retrieves all the info stored about a file
   */
  getFileInfo(id) {
    let a = FileService.BASE;
    // this.http.get();
  }
}
