import {Component, Input, OnInit} from '@angular/core';
import {FileTypeAnalyzer} from "../filetypeanalyzer/filetypeanalyzer.service";


@Component({
    selector: 'app-fileitem',
    templateUrl: './fileitem.component.html',
    styleUrls: ['./fileitem.component.scss']
})
export class FileItemComponent implements OnInit {

    @Input() file: object = null;
    iconUrl: string = null;

    fileTypeAnalyzer: FileTypeAnalyzer = null;
    constructor(fileTypeAnalyzer: FileTypeAnalyzer) {
        this.fileTypeAnalyzer = fileTypeAnalyzer;
    }

    ngOnInit() {
        this.iconUrl = this.fileTypeAnalyzer.getIcon(this.file['name']);
        console.log(this.file);
    }
}
