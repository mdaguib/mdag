import {Injectable} from "@angular/core";


@Injectable({
    providedIn: 'root',
})
export class FileTypeAnalyzer {

    fileType: string[] = [
        "csv",
        "pdf",
        "unknown",
    ];

    getFileType(fileName: string) {
        let splitFileName = fileName.split(".");
        let extension = splitFileName[splitFileName.length - 1].toLowerCase();

        for (let fileType of this.fileType) {
            if (extension == fileType) {
                return fileType;
            }
        }
        return this.fileType[this.fileType.length - 1];
    }

    getIcon(fileName: string) {
        return "static/img/" + this.getFileType(fileName) + "32.png";
    }
}