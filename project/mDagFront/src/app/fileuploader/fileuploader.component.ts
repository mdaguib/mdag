import {
    Component,
    Output,
    Input,
    OnInit,
    EventEmitter,
    ViewChild, ElementRef
} from '@angular/core';
import {BrowserFeatureCheckerService} from "../browsefeaturechecker/browser-feature-checker.service";


@Component({
    selector: 'file-uploader',
    templateUrl: './fileuploader.component.html',
    styleUrls: ['./fileuploader.component.scss']
})
export class FileUploaderComponent implements OnInit {

    @Input() name: string;
    @Input() helperText: string;
    @Input() multiple?: boolean;
    @Output() onFileLoaded: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("dropzone") dragAndDropZone: ElementRef;
    @ViewChild("fileButton") fileButton: ElementRef;

    files: Array<any>;
    private enableDragAndDropUpload: boolean;

    constructor(bfcs: BrowserFeatureCheckerService) {
        this.enableDragAndDropUpload = bfcs.doesSupportDragAndDropFileUpload();

        // Defaults
        this.multiple = false;
    }

    ngOnInit() {
        this.files = [];
        // Ensure default texts
        this.helperText = (
            this.helperText
            || "Click in this area or drop a file to upload its content"
        );
        if (this.enableDragAndDropUpload) {
            // Initialize listeners for all the drag & drop events
            let domElement = this.dragAndDropZone.nativeElement;
            domElement.addEventListener('drop', this.onDroppedFiles.bind(this));
            this.addEventListeners(
                domElement,
                'drag dragstart dragend dragover dragenter dragleave drop',
                this.avoidEventDefaultBehavior.bind(this)
            );
        }

        this.fileButton.nativeElement.multiple = this.multiple;
    }

    onClick() {
        this.fileButton.nativeElement.click();
    }

    isAdvancedFileUploadEnabled() {
        return this.enableDragAndDropUpload;
    }

    onDroppedFiles(event) {
        let files = event.dataTransfer.files;
        if (!this.multiple) {
            files = [files[0]];
        }
        this.addSelectedFiles(files);
    }

    onFileSelected(event) {
        this.addSelectedFiles(this.fileButton.nativeElement.files);
    }

    getFiles() {
        return this.files;
    }

    private avoidEventDefaultBehavior(event) {
        event.preventDefault();
        event.stopPropagation();
    }

    // This is just a helper to avoid multiplicating the lines in the event
    // binding stage
    private addEventListeners(element, eventNames, listener) {
        var events = eventNames.split(' ');
        for (var i = 0, iLen = events.length; i < iLen; i++) {
            element.addEventListener(events[i], listener, false);
        }
    }

    private addSelectedFiles(files) {
        if (!this.multiple) {
            this.files = [];
        }
        
        for(let file of files) {
            this.files.push(file);
        }
        this.onFileLoaded.emit(this.files);
    }

}
