import {Inject, Injectable} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class CookieService {

    constructor(
        @Inject(APP_BASE_HREF) private baseHref: string
    ) {
    }

    /**
     *
     * @param key
     * @param value
     * @param duration Time in milliseconds that the cookie should be set
     */
    set(key: string, value: string, duration?: number, path?: string) {
        let cookieString = '' + key + '=' + value + ';samesite=lax;';
        if (duration) {
            let stringTime = CookieService.getExpireTime(
                1800000
            ).toUTCString();
            cookieString += 'expires=' + stringTime + ';';
        }
        let _path = path ? path : this.baseHref;
        cookieString += "path=" + _path + ";";

        document.cookie = cookieString;
    }

    remove(key: string) {
        let cookieString = key + `=; Path=${this.baseHref}; Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
        document.cookie = cookieString;
    }

    get(key: string): any {
        let targetCookie = CookieService.getCookie(key);
        let value = undefined;
        if (targetCookie) {
            value = targetCookie.split("=")[1];
        }
        return value;
    }

    check(key: string) {
        let targetCookie = CookieService.getCookie(key);
        return !!targetCookie;
    }
    
    private static getCookie(key: string) {
        let decodedCookie = decodeURIComponent(document.cookie);
        let cookies = decodedCookie.split(";");
        let cname = key + "=";
        let idx = cookies.length - 1;
        let targetCookie = null;

        while (idx >= 0 && !targetCookie) {
            let cookie = cookies[idx].trim();
            if (cookie.indexOf(cname) == 0) {
                targetCookie = cookie
            }
            idx--;
        }

        return targetCookie;
    }

    private static getExpireTime(milliseconds: number): Date {
        let d = new Date();
        let time = d.getTime() + milliseconds;
        d.setTime(time);
        return d;
    }

}
