import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import {UserService} from "../user/user.service";
import {User} from "../user/user.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NotificationsService} from "../notifications/notifications.service";
import {Group} from "../group/group.model";

@Component({
    selector: 'app-group-details',
    templateUrl: './group-details.component.html',
    styleUrls: ['./group-details.component.scss']
})
export class GroupDetailsComponent implements OnInit {

    @Output() onGroupCreated: EventEmitter<any>;
    @Output() onGroupUpdated: EventEmitter<any>;

    /* Services */
    private userService: UserService;
    private notificationsService: NotificationsService;

    /* Forms */
    addUserForm = new FormGroup({
        email: new FormControl(
            '',
            [Validators.required, Validators.email]
        )
    });

    groupForm = new FormGroup({
        name: new FormControl(
            '',
            [Validators.required, Validators.maxLength(100)]
        ),
        description: new FormControl('')
    });


    @Input() group: Group;

    private creation: boolean;
    private groupInstance: Group;

    private addUserClicked: boolean;

    private readonly user: User;

    constructor(userService: UserService, notificationsService: NotificationsService) {
        this.userService = userService;
        this.notificationsService = notificationsService;
        this.user = this.userService.getUser();
        this.addUserClicked = false;

        this.onGroupCreated = new EventEmitter<any>();
        this.onGroupUpdated = new EventEmitter<any>();
    }

    ngOnInit() {
        if (this.group) {
            this.groupInstance = this.group;
        } else {
            this.groupInstance = new Group({
                name: "New group",
                description: "",
                owner: null,
                members: []
            });
        }

        this.groupForm.setValue({
            "name": this.groupInstance.name,
            "description": this.groupInstance.description
        });

        this.creation = !this.group;
    }

    getName() {
        return this.groupInstance.name;
    }

    canEdit() {

        return (
            this.user && this.groupInstance.owner && this.groupInstance.owner.id === this.user.id
            || this.creation
        );
    }

    getMembers(): Array<User> {
        return this.groupInstance['members'];
    }

    hasMembers() {
        return this.groupInstance['members'].length > 0;
    }

    getOwner(): User {
        if (this.creation) {
            return this.user;
        } else {
            return this.groupInstance.owner;
        }
    }

    onAddUserClicked() {
        this.addUserClicked = true;
    }

    addUser(event) {
        if (event.code == "Enter") {
            // TODO add user to the group
            this.userService.list(
                {email: this.addUserForm.get("email").value}
            ).then(
                users => {
                    this.groupInstance['members'].push(users[0]);
                    this.addUserClicked = false;
                }
            ).catch(
                error => {
                    this.notificationsService.notify(
                        "The user has not been found",
                        NotificationsService.MESSAGE_TYPES.WARNING
                    )
                }
            )
        }
    }

    saveGroup() {
        let promise;
        if (this.creation) {
            this.userService.createGroup(
                this.groupForm.get('name').value,
                this.groupForm.get('description').value,
                this.groupInstance.members
            ).then(
                () => this.onGroupCreated.emit()
            );
        } else {
            this.userService.updateGroup(
                this.groupInstance.id,
                this.groupForm.get('name').value,
                this.groupForm.get('description').value,
                this.groupInstance.members
            ).then(
                () => this.onGroupUpdated.emit()
            );
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        let group = changes['group'].currentValue;

        if (group) {
            this.group = this.groupInstance = changes['group'].currentValue;
            this.groupForm.setValue({
                "name": this.groupInstance.name,
                "description": this.groupInstance.description
            });
        }
    }
}
