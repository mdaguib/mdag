import {Deserializable} from "../interfaces/deserializable.model";
import {User} from "../user/user.model";

export class Group implements Deserializable {
    private _id: number;
    private _name: string;
    private _description: string;
    private _owner: User;
    private _members: Array<User>;

    constructor(data: any) {
        let user_obj = data;
        if (typeof(data) === 'string') {
            user_obj = this.deserialize(data);
        }

        this.initialize(user_obj);
    }
    
    deserialize(input: string): this {
        return JSON.parse(input);
    }

    serialize(): string {
        let obj = {
            id: this._id,
            name: this._name,
            description: this._description,
            owner: this.owner && this.owner.serialize(),
            members: this.members.map(member => member.serialize())
        };

        return JSON.stringify(obj);
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get description(): string {
        return this._description;
    }

    set description(description: string) {
        this._description = description;
    }

    get members() {
        return this._members;
    }
    
    get owner() {
        return this._owner;
    }

    private initialize(groupObj: object) {
        this._id = groupObj['id'];
        this._name = groupObj['name'];
        this._description = groupObj['description'];
        if (groupObj['owner']) {
            this._owner = new User(groupObj['owner']);
        }

        if (groupObj['members']) {
            this._members = groupObj['members'].map(
                userObj => new User(userObj)
            );
        }
    }
}
