import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../user/user.service";
import {NotificationsService} from "../notifications/notifications.service";
import {Group} from "../group/group.model";

@Component({
    selector: 'app-groups-list',
    templateUrl: './groups-list.component.html',
    styleUrls: ['./groups-list.component.scss']
})
export class GroupsListComponent implements OnInit {

    /* Services */
    private userService: UserService;
    private notificationsService: NotificationsService;

    @Input() filtersForm = new FormGroup({
        groupName: new FormControl(
            null,
            [Validators.maxLength(100)]
        ),
        groupOwner: new FormControl(null),
    });

    private groupList: Array<Group>;
    private creatingGroup: boolean;

    private selectedGroup: Group;
    private originalGroup: string;
    private currentIndex: number;

    constructor(userService: UserService, notificationsService: NotificationsService) {
        this.creatingGroup = false;
        this.userService = userService;
        this.notificationsService = notificationsService;
        this.selectedGroup = null;
        this.currentIndex = null;
    }

    ngOnInit() {
        this.filterResults();
    }

    filterResults() {
        this.clearSelection();
        let filters = {};
        filters['groupName'] = this.filtersForm.get('groupName').value;
        filters['groupOwner'] = GroupsListComponent.castToBool(
            this.filtersForm.get('groupOwner').value
        );

        this.fetchGroups(filters);
    }

    clearFilters() {
        this.filtersForm.reset();
    }

    selectGroup(event, group, index) {
        event.stopPropagation();

        if (this.currentIndex != null) {
            this.clearSelection();
        }

        this.selectIndex(index);
        this.selectedGroup = group;
        this.originalGroup = this.selectedGroup.serialize();

    }

    clearSelection() {
        this.creatingGroup = false;
        if (this.selectedGroup && this.selectedGroup.serialize() != this.originalGroup) {
            // this.showUserChangedWarning();
        } else {
            if (this.currentIndex != null) {
                this.selectedGroup = null;
                this.unselectIndex();
            }
        }
    }

    getSelectedGroup() {
        return this.selectedGroup;
    }

    createGroup(event) {
        event.stopPropagation();
        this.creatingGroup = true;
    }

    getGroups() {
        return this.groupList;
    }

    hasResults(): boolean {
        return this.groupList && this.groupList.length > 0;
    }

    private shouldShowDetailsPane() {
        return this.getSelectedGroup() || this.creatingGroup;
    }

    private fetchGroups(filters?: object) {
        if (filters == null) {
            filters = {};
        }

        this.userService.listGroups(
            filters['groupName'],
            filters['groupOwner']
        ).then(
            groups => this.groupList = groups
        ).catch(
            error => this.notificationsService.notify(
                "Failed to obtain the groups",
                NotificationsService.MESSAGE_TYPES.ERROR
            )
        );

    }

    private static castToBool(choice: string): number {
        if (choice == undefined) {
            return null;
        } else {
            return Number(choice);
        }
    }

    private unselectIndex() {
        let elements = document.getElementsByClassName("details-item-wrapper");
        let element = elements[this.currentIndex];
        element.classList.remove('active');
        this.currentIndex = null;
    }

    private selectIndex(index: number) {
        this.currentIndex = index;
        let elements = document.getElementsByClassName("details-item-wrapper");
        let element = elements[this.currentIndex];
        element.classList.add('active');
    }

}
