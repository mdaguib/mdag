import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadedcontainerComponent } from './headedcontainer.component';

describe('HeadedcontainerComponent', () => {
  let component: HeadedcontainerComponent;
  let fixture: ComponentFixture<HeadedcontainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadedcontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadedcontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
