import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';

@Component({
    selector: 'app-headedcontainer',
    templateUrl: './headedcontainer.component.html',
    styleUrls: ['./headedcontainer.component.scss']
})
export class HeadedcontainerComponent implements OnInit {

    @ViewChild("headedcontainer") view: ElementRef;

    constructor() {
    }

    ngOnInit() {
    }

}
