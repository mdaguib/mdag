import { Component, OnInit } from '@angular/core';
import {MenuService} from "../menu/menu.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuService: MenuService = null;
  constructor(menuService: MenuService) {
    this.menuService = menuService;
  }

  ngOnInit() {
  }

  menuButtonClicked() {
    this.menuService.toggle();
  }
}
