import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    contributors: string[] = [
        "Ricardo Alberich",
        "José A. Castro",
        "Mercè Llabrés",
        "Pere Palmer Rodríguez",
    ];

    constructor() {
    }

    ngOnInit() {
    }

}
