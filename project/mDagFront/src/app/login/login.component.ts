import {
    Component,
    ElementRef, EventEmitter, Input,
    OnInit, Output,
    Renderer2,
    ViewChild
} from '@angular/core';
import {UserService} from "../user/user.service";
import {NavigationExtras, Router} from "@angular/router";
import {User} from "../user/user.model";
import {PermissionModule} from "../url-permission.module";
import {NotificationsService} from "../notifications/notifications.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    @Output() onLogin: EventEmitter<any>;
    @Output() onLogout: EventEmitter<any>;

    // Services
    private userService: UserService;
    private notificationsService: NotificationsService;
    private renderer: Renderer2;
    private router: Router;
    private user: string;
    private password: string;
    private isLoggedIn: boolean;
    private failedLastAttempt: boolean;

    @ViewChild("loginContainer") loginContainer: ElementRef;

    constructor(renderer: Renderer2, userService: UserService, router: Router, notificationsService: NotificationsService) {
        this.userService = userService;
        this.notificationsService = notificationsService;
        this.renderer = renderer;
        this.router = router;
        this.isLoggedIn = false;
        this.failedLastAttempt = false;
        this.onLogout = new EventEmitter();
        this.onLogin = new EventEmitter();
    }

    ngOnInit() {
    }

    isSignedIn() {
        return this.userService.isSignedIn();
    }

    /**
     * Try to sign in and if it's a successful one, just clear the fields and
     * show the sign out option.
     */
    signIn() {
        console.log(this.user, this.password);
        this.userService.signIn(this.user, this.password)
            .then(
                (user) => {
                    this.user = null;
                    this.password = null;
                    this.failedLastAttempt = false;
                    this.isLoggedIn = true;
                    this.renderer.addClass(this.loginContainer.nativeElement, "hidden");
                    this.onLogin.emit(user);
                }).catch(
            (error) => {
                if (error.status < 500) {
                    this.notificationsService.notify(
                        "Wrong user or password",
                        NotificationsService.MESSAGE_TYPES.WARNING
                    );
                } else {
                    this.notificationsService.notify(
                        "Sorry! We've failed to identify you",
                        NotificationsService.MESSAGE_TYPES.ERROR
                    );
                }
                this.failedLastAttempt = true;
            });
    }

    onSignOutClicked() {
        this.userService.signOut().then((response) => {
            this.onLogout.emit();
            let urlSegments = this.router.url.split("/").filter(
                (e) => {
                    return e != "" && e != null
                }
            );

            if (!PermissionModule.canNavigate([], urlSegments)) {
                this.router.navigate(['/home'])
            }
        });
    }

    onSignInClicked() {
        this.renderer.removeClass(this.loginContainer.nativeElement, "hidden");
    }
    
    onSignUpClicked() {
        let extras: NavigationExtras = {
            queryParams: {
                redirect: this.router.url
            }
        };
        this.router.navigate(['/signup'], extras);
    }

    getFailureClasses() {
        if (this.failedLastAttempt) {
            return 'failed-style';
        }
        return '';
    }

}
