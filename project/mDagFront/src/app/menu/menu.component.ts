import {Component, OnInit} from '@angular/core';
import {MenuService} from "./menu.service";
import {UserService} from "../user/user.service";
import {User} from "../user/user.model";
import {PermissionModule} from "../url-permission.module";

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    menuService: MenuService = null;

    isOpened: boolean = false;
    private menuEntries = [
        {
            "icon": "home",
            "label": "Home",
            "url": "home",
        },
        {
            "icon": "science",
            "label": "Experiments",
            "url": "experiments",
        },
        // Admin entries
        {
            "icon": "person",
            "label": "User management",
            "url": "user-management",
        },
        // TODO Implement these entries
        {
            "icon": "groups",
            "label": "Groups",
            "url": "groups",
        },
        {
            "icon": "contact_support",
            "label": "Contact",
            "url": "contacto",
        },
    ];

    private userService: UserService;
    private visibleEntries: Array<any>;
    private user: User;

    constructor(menuService: MenuService, userService: UserService) {
        this.menuService = menuService;
        this.menuService.change.subscribe((newValue) => this.onStateChangeRequested(newValue));
        this.userService = userService;
        this.user = this.userService.getUser();
        this.visibleEntries = this.getMenuEntries();

        this.userService.onLogin.subscribe(
            user => this.onLogin(user)
        );

        this.userService.onLogout.subscribe(
            () => this.onLogout()
        );

    }

    ngOnInit() {
    }

    private onStateChangeRequested(newState: boolean) {
        this.isOpened = newState;
    }

    getStyles() {
        let classes: string = "mdc-drawer mdc-drawer--dismissible" +
            " mdc-top-app-bar--fixed-adjust inline-drawer";
        return classes + (this.isOpened ? " open" : "");
    }

    getMenuEntries() {
        let visibleEntries = [];
        let isAdmin = false;
        let showEntry;

        let permissions = [];
        if (this.user) {
            permissions = this.user.getPermissions();
            isAdmin = this.user.isAdmin;
        }

        for (let entry of this.menuEntries) {
            showEntry = PermissionModule.canNavigate(
                permissions,
                [entry["url"]]
            );

            if (isAdmin || showEntry) {
                visibleEntries.push(entry);
            }
        }

        return visibleEntries;
    }

    getUserName() {
        if (this.user) {
            return this.user.username;
        }
        return 'Unknown';
    }

    onLogin(user) {
        this.user = user;
        this.visibleEntries = this.getMenuEntries();
    }

    onLogout() {
        this.user = null;
        this.visibleEntries = this.getMenuEntries();
    }
}
