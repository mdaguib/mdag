import {EventEmitter, Injectable, Output} from "@angular/core";

@Injectable({
    providedIn: 'root',
})
export class MenuService {

    isOpen = false;

    @Output() change: EventEmitter<boolean> = new EventEmitter();

    toggle() {
        if (this.isOpen) {
            this.close()
        } else {
            this.open()
        }

    }

    open() {
        this.setOpenStatus(true);
    }

    close() {
        this.setOpenStatus(false);
    }

    private setOpenStatus(isOpen: boolean) {
        this.isOpen = isOpen;
        this.change.emit(isOpen);
    }
}