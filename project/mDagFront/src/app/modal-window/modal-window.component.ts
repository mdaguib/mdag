import {Component, HostListener, Input, OnInit} from '@angular/core';
import {ModalWindowService} from "./modal-window.service";

@Component({
    selector: 'app-modal-window',
    templateUrl: './modal-window.component.html',
    styleUrls: ['./modal-window.component.scss']
})
export class ModalWindowComponent implements OnInit {
    /**
     * In order to use this modal window is important to pass an element with
     * a getContent function.
     *
     * That function needs to return an html representable element that will
     * be interplated in the template.
     *
     * Use case SVG:
     * SVG schema for the image.
     *
     * TODO: Check if this can cause a security issue
     */


    /**
     * TODO Bind this shit to an interface
     * contentInterface should be anything holding a "getContent" function
     *
     */
    @Input() contentInterface;

    private modalWindowService: ModalWindowService;
    private isOpen: boolean = false;
    private ESC_CODES: Set<string> = new Set(['esc', 'escape']);
    protected modalType: string;
    protected modalSetup: object;

    constructor(modalWindowService: ModalWindowService) {
        this.modalWindowService = modalWindowService;
        this.modalWindowService.change.subscribe(
            (newValue) => this.changeViewStatus(
                newValue["open"],
                newValue["modalType"],
                newValue["modalSetup"]
            )
        );
    }

    ngOnInit() {
    }

    changeViewStatus(isOpen: boolean, modalType: string, modalSetup?: object) {
        this.isOpen = isOpen;
        this.modalType = modalType;
        this.modalSetup = modalSetup;
    }

    getContent() {
        let content = '';
        if (this.contentInterface != null) {
            content = this.contentInterface.getContent();
        }

        return content;
    }

    getModalTypeDependenStyles() {
        if (this.modalType == ModalWindowService.MODAL_TYPES.VIEWER) {
            return "viewer-modal";
        } else {
            return "text-modal"
        }
    }

    showButtons() {
        return this.modalType !== ModalWindowService.MODAL_TYPES.VIEWER;
    }

    getConfirmText() {
        let text = this.modalSetup && this.modalSetup["confirmButtonText"];
        return text || 'Confirm';
    }

    getRejectText() {
        let text = this.modalSetup && this.modalSetup["rejectButtonText"];
        return text || 'Cancel';
    }

    /**
     * Returns true whenever the user has requested a modal that needs two
     * buttons ( This is the confirmation one )
     */
    needsRejectButton() {
        return this.modalType === ModalWindowService.MODAL_TYPES.CONFIRMATION;
    }

    /**
     * Host listener is used to listen for the escape key.
     *
     * Close the modal whenever the requester is a click on the overlay or the
     * user has clicked the esc button
     */
    @HostListener('document:keyup', ['$event'])
    public close($event?) {
        if (this.hasPressedCloseKeys($event)) {
            this.modalWindowService.close();
        }
    }

    private hasPressedCloseKeys(event) {
        if (!event) {
            return false;
        }
        
        let code = event.code || '';

        return this.ESC_CODES.has(code.toLowerCase());
    }

}
