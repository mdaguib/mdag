import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ModalWindowService {
    public static readonly MODAL_TYPES = {
        INFO: "I",
        CONFIRMATION: "C",
        VIEWER: "V"
    };

    @Output() change: EventEmitter<object> = new EventEmitter();

    constructor() {
    }

    /**
     * ModalContent is a setup object that holds up to 4 params:
     *  - modalType: MODAL_TYPES Enum showing the kind of modal needs to be
     *              opened
     *  - modalContent: Modal main content.
     *  - confirmButtonText <Optional>: Text that needs to be shown in confirm
     *              button for both Info and confirmation modals
     *  - rejectButtonText <Optional>: Text that needs to be shown reject
     *              button for both Info and confirmation modals
     *              
     * @param modalType
     * @param modalSetup
     */
    public open(modalType: string, modalSetup?: object) {
        let value = {
            open: true,
            modalType: modalType,
            modalSetup: modalSetup
        };

        this.change.emit(value);
    }

    public close() {
        let value = {
            open: false,
        };
        this.change.emit(value);
    }
}
