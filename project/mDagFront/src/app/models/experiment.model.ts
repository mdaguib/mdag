
export class Experiment {

    private readonly _id: number;
    private readonly _creationDate: Date;
    private readonly _modificationDate: Date;
    // This is actually an enum sent by the server
    private readonly _status: string;


    private _name: string;
    private _description: string;
    // This is actually an enum sent by the server
    private _public: string;
    private _experimentGroups: Array<string>;

    private _owner: number;

    constructor(jsonData: object) {
        this._id = jsonData['id'];
        this._name = jsonData['name'];
        this._description = jsonData['description'];
        this._experimentGroups = jsonData['experiment_groups'];
        this._status = jsonData['status'];
        this._creationDate = new Date(jsonData['creation_date']);
        this._modificationDate = new Date(jsonData['modification_date']);
        this._owner = jsonData['owner'];
        this._public = jsonData['visible_for'];
    }


    get id(): number {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get description(): string {
        return this._description;
    }

    set description(description: string) {
        this._description = description;
    }

    get status(): string {
        return this._status;
    }

    get owner(): number{
        return this._owner;
    }

    get public(): string {
        return this._public;
    }

    set public(value: string){
        console.log(this._id, value);
        this._public = value;
    }

    getCreationDateInMaterialFormat() {
        return Experiment.getMaterialDate(this._creationDate);
    }

    getModificationDateInMaterialFormat() {
        return Experiment.getMaterialDate(this._modificationDate);
    }

    private static getMaterialDate(date: Date): string {
        let year = date.getFullYear();
        let month = ("0" + (date.getMonth() + 1)).slice(-2);
        let day = ("0" + date.getDate()).slice(-2);
        return year + '-' + month + '-' + day;
    }
}