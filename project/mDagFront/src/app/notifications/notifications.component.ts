import {Component, OnInit} from '@angular/core';
import {NotificationsService} from "./notifications.service";

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

    private notificationsService: NotificationsService;

    constructor(notificationsService: NotificationsService) {
        this.notificationsService = notificationsService;
    }

    ngOnInit() {
    }

    get messages(): Array<object> {
      return this.notificationsService.getMessages();
    }

    removeMessage(message: object) {
        this.notificationsService.remove(message);
    }

}
