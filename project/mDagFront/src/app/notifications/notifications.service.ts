import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class NotificationsService {

    public static readonly MESSAGE_TYPES = {
        INFO: 'info',
        WARNING: 'warning',
        ERROR: 'error'
    };

    private readonly messages: Array<object>;

    constructor() {
        // TODO Filled just for test purposes
        this.messages = [
            // {type: "info", message: "hola"},
            // {type: "warning", message: "que tal estass"},
            // {
            //     type: "warning",
            //     message: "me gustaría que me hablarascon calma y sosiego"
            // },
            // {type: "warning", message: "aqui un mensaje corto"},
            //
            // {
            //     type: "error",
            //     message: "ahora un mensaje un poquito más largo" +
            //         " que todo el resto ya que" +
            //         " tenemos que ver si todo se ajusta al tamaño o incluso splitea" +
            //         " frases"
            // }
        ];
    }

    getMessages(): Array<object> {
        return this.messages;
    }

    notify(message: string, type?: string) {
        if (!type) {
            type = NotificationsService.MESSAGE_TYPES.INFO;
        }

        this.messages.push(
            {
                type: type,
                message: message
            }
        );
    }

    /**
     * Remove notification
     */
    remove(message: object) {
        let index = this.messages.findIndex(value => value == message);
        let messagesCount = this.messages.length;
        if (index > -1 && messagesCount > index) {
            console.log("Removing message");
            this.messages.splice(index, 1);
        }
    }
}
