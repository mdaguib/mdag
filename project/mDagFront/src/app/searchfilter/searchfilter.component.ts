@Component({
    selector: 'app-searchfilter',
    templateUrl: './searchfilter.component.html',
    styleUrls: ['./searchfilter.component.scss']
})
export class SearchfilterComponent implements OnInit {
    /**
     * The only mandatory field for the input data is to has an id.
     * This id is the unique identifier between elements.
     */
    @Input() data: Array<any> = [];

    private selectedData: Array<any> = [];
    private filteredData : Array<any> = [];

    private filterValue: String;

    constructor() {
    }

    ngOnInit() {
        /**
         * Mock up data for now
         */
        this.data = [
            {
                "id": 0,
                "display": "Pichon"
            },
            {
                "id": 1,
                "display": "Pichon1"
            },
            {
                "id": 2,
                "display": "Ácido desoxiribonucleico"
            },
            {
                "id": 3,
                "display": "3Omega2Etilmetilroibnol"
            },
            {
                "id": 4,
                "display": "yoquese"
            },
            {
                "id": 5,
                "display": "h2o2"
            },
            {
                "id": 6,
                "display": "meloinvento"
            }
        ];
        this.filteredData = this.data;
    }

    /**
     * This function must return true or false depending on whether the
     * data element passed has been placed in the selected collection or not.
     *

     *
     * @param dataElement
     */
    alreadySelected(dataElement) {
        return this.contains(this.selectedData, dataElement);
    }


    /** Functions to add and remove elements to/from the selected collection */
    addElement(evt, dataElement) {
        console.log(evt, dataElement);
        if (!this.contains(this.selectedData, dataElement)) {
            this.selectedData.push(dataElement);
            evt.target.classList.toggle('active-result');
        }
    }

    removeElement(evt, dataElement) {
        if (this.contains(this.selectedData, dataElement)) {
            //remove
            let idx = this.selectedData.findIndex(
                (elem) => elem.id == dataElement.id
            );

            this.selectedData.splice(idx, 1);
        }
    }

    private contains(list, data) {
        return list.filter(
            (element) => {
                return element.id == data.id
            }
        ).length > 0;
    }

    /**
     * Get the user's input and filter the dataset to show only the matching
     * options
     */
    private filterResults() {
        if (!this.filterValue) {
            this.filteredData = this.data;
        } else {
            // Make a simple search of word start matching.
            // If this does not seem to be very useful, I'll need to implement
            // some kind of word comparison algorithm
            // ( E.G: levenshtein distance)
            let loweredValue = this.filterValue.toLowerCase();
            this.filteredData = this.data.filter(
                (el) => {
                    return el.display.toLowerCase().startsWith(loweredValue);
            });
        }
    }

}

import {Component, Input, OnInit} from '@angular/core';
