import {Injectable} from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest, HttpResponse
} from "@angular/common/http";
import {Observable} from "rxjs";

import {tap} from "rxjs/operators";
import {CookieService} from "../global-services/cookie-service.service";

@Injectable()
export class SessionService implements HttpInterceptor {

    cookieService : CookieService;

    constructor(cookieService: CookieService) {
        this.cookieService = cookieService;
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        /*
        * Pipe responses checking for those that have been non-server errors
        * to update the mdag session if needed.
        * This is done in order to keep synced the user session between
        *  django and angular.
         */
        return next.handle(req).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse && evt.status != 500) {
                    const sessionSet = this.cookieService.get("mdag_session");
                    // Refresh session token
                    if (sessionSet) {
                        this.cookieService.set(
                            "mdag_session",
                            "1",
                            1800000
                        );
                    }
                }
            })
        );
    }
}
