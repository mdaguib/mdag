import {Component, OnInit, Input} from '@angular/core';
import {
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import {UserService} from "../user/user.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

    @Input() form = new FormGroup({
        name: new FormControl(
            '',
            [Validators.required, Validators.maxLength(150)]
        ),

        surname: new FormControl(
            '',
            [Validators.required, Validators.maxLength(150)]
        ),
        username: new FormControl(
            '',
            [Validators.required, Validators.maxLength(150)]
        ),
        password: new FormControl(
            '',
            [
                Validators.required,
                Validators.minLength(8),
                Validators.pattern(new RegExp('[a-zA-Z]+')),
                Validators.pattern(new RegExp('[0-9]+')),
            ]
        ),
        email: new FormControl(
            '',
            [Validators.required, Validators.email]
        )
    });

    private static readonly DEFAULT_REDIRECT_TO = '/home';

    private userService: UserService;
    private router: Router;
    private route: ActivatedRoute;
    private redirectTo: string;

    constructor(userService: UserService, router: Router, route: ActivatedRoute) {
        this.userService = userService;
        this.router = router;
        this.route = route;

        this.route.queryParams.subscribe(
            params => {
              this.redirectTo = params['redirect'] || SignupComponent.DEFAULT_REDIRECT_TO;
            }
        );
    }

    ngOnInit() {
    }

    clearForm() {
        this.form.reset();
    }

    registerUser() {
        console.log("Formulario", this.form);
        this.userService.signUp(
            this.form.get("username").value,
            this.form.get("password").value,
            this.form.get("email").value,
            this.form.get("name").value,
            this.form.get("surname").value
        ).then(
            response => { this.router.navigate([this.redirectTo]); }
        ).catch((reason => {

        }));
    }
}
