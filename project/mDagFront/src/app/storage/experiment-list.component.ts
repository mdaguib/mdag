import {Component, OnInit} from '@angular/core';
import {FileItemHandlerService} from "../../file-item-handler.service";
import {ModalWindowService} from "../modal-window/modal-window.service";
import {Experiment} from "../models/experiment.model";
import {ExperimentService} from "./experiment.service";
import {NotificationsService} from "../notifications/notifications.service";
import {UserService} from "../user/user.service";
import {User} from "../user/user.model";


@Component({
    selector: 'app-storage',
    templateUrl: './experiment-list.component.html',
    styleUrls: ['./experiment-list.component.scss']
})
export class ExperimentListComponent implements OnInit {

    // Services
    private fileItemHandlerService: FileItemHandlerService;
    private modalWindowService: ModalWindowService;
    private experimentService: ExperimentService;
    private notificationsService: NotificationsService;

    private experiments: Array<Experiment>;
    private selectedExperiment: Experiment;
    private user: User;

    // Filters
    private organismsFilter: string;
    private organismsClassificationFilter: string;
    private owner: string;
    private experimentGroupsFilter: string;
    private userService: UserService;


    constructor(
        fileItemHandlerService: FileItemHandlerService,
        modalWindowService: ModalWindowService,
        experimentService: ExperimentService,
        notificationService: NotificationsService,
        userService: UserService
    ) {
        this.fileItemHandlerService = fileItemHandlerService;
        this.modalWindowService = modalWindowService;
        this.experimentService = experimentService;
        this.notificationsService = notificationService;
        this.userService = userService;
        this.user = this.userService.getUser();
        this.experiments = [];
        this.selectedExperiment = null;

        // User login and logout event bindings
        this.userService.onLogout.subscribe(
            value => this.loginStatusChanged()
        );
        this.userService.onLogin.subscribe(
            value => this.loginStatusChanged()
        );
    }

    ngOnInit() {
        this.fetchExperiments({});
    }

    getItems() {
        return this.experiments;
    }

    clearFilters() {
        this.organismsFilter = null;
        this.organismsClassificationFilter = null;
        this.owner = null;
        this.experimentGroupsFilter = null;
    }

    filterResults() {
        let filters = {};
        filters['organisms'] = this.organismsFilter;
        filters['reactions'] = this.owner;
        filters['organism_groups'] = this.organismsClassificationFilter;
        filters['experiment_groups'] = this.experimentGroupsFilter;

        this.fetchExperiments(filters);
    }

    /**
     * Retrieve the experiments that include one or more of the specified
     * Organisms, reactions, organism classification or group.
     * @param filters
     */
    private fetchExperiments(filters: object) {
        this.experimentService.getExperiments(filters).then(
            value => {
                if (value.length == 0) {
                    this.notificationsService.notify("No experiments found");
                }
                this.experiments = value;
            }
        );
    }

    openDetails(event: Event, experiment: Experiment) {
        this.selectedExperiment = experiment;
        event.stopPropagation();
    }

    getSelectedExperiment(): Experiment {
        return this.selectedExperiment;
    }

    clearSelection() {
        this.selectedExperiment = null;
    }

    openExperimentGraph() {
        this.modalWindowService.open(
            ModalWindowService.MODAL_TYPES.VIEWER
        );
    }

    downloadContent() {
        this.experimentService.downloadContent(this.selectedExperiment).catch(
            error => {
                this.notificationsService.notify(
                    "Failed to download the content",
                    NotificationsService.MESSAGE_TYPES.ERROR
                );
            }
        );
    }

    deleteExperiment() {
        let experiment = this.getSelectedExperiment();
        this.clearSelection();
        this.experimentService.delete(experiment).then(
            value => {
                this.notificationsService.notify(
                    "Experiment deleted"
                );
                this.filterResults();
            }
        ).catch(
            reason => this.notificationsService.notify(
                "Failed to delete the experiment"
            )
        );
    }
    
    saveExperiment() {
        let experiment = this.getSelectedExperiment();
        this.experimentService.update(experiment).then(
            value => {
                this.notificationsService.notify("Experiment updated");
            }
        )
    }

    /**
     * An experiment can be deleted either by an admin user or its owner
     */
    canDeleteExperiment() {
        return this.user && (
            this.user.isAdmin || this.getSelectedExperiment().owner === this.user.id
        );
    }

    canCreateExperiment() {
        return this.user && (
            this.user.isAdmin || this.user.hasPermission('experiments_create')
        );
    }

    loginStatusChanged() {
        this.user = this.userService.getUser();
        this.filterResults();
    }
}

