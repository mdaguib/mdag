import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Experiment} from "../models/experiment.model";
import {stringify} from "querystring";
import {element} from "protractor";


@Injectable({
    providedIn: 'root',
})
export class ExperimentService {

    private static readonly FILENAME_REGEX: RegExp = /filename="(\w+\.\w+)"/i;

    // Constants
    private static readonly URL_BASE: string = 'webservice/experiments/';

    // Injected services
    private http: HttpClient;

    constructor(httpClient: HttpClient) {
        this.http = httpClient;
    }


    /**
     * @param filters
     */
    getExperiments(filters: object): Promise<Array<Experiment>> {

        let body = JSON.stringify({
            'organism': filters['organisms'],
            'reaction': filters['reactions'],
            'organism_group': filters['organism_groups'],
            'experiment_group': filters['experiment_groups'],
        });

        let baseUrl = ExperimentService.URL_BASE + 'get_visible_experiments';
        return new Promise((resolve, reject) => {

            this.http.post(baseUrl, body).toPromise().then(
                httpResponse => {
                    return resolve(ExperimentService.transformExperimentResponse(httpResponse['response']))
                }
            ).catch(
                reason => reject(reason)
            )
        });
    }

    getExperimentDetails(experiment: Experiment): Promise<Experiment> {
        let url = ExperimentService.URL_BASE + 'get_details';
        let params = new HttpParams();
        params.append('id', stringify(experiment.id));

        let promise: Promise<Experiment> = new Promise((resolve, reject) => {
            // this.http.get(url, {'params': params}).toPromise().then(
            //     response => resolve(ExperimentService.transformExperimentResponse(response['data'])[0])
            // ).catch(
            //     reason => reject(reason)
            // );
            // There is only one experiment in this response
            let experiments = ExperimentService.transformExperimentResponse([{}]);
            resolve(experiments[0]);
        });
        return promise;
    }

    downloadContent(experiment: Experiment) {
        let url = ExperimentService.URL_BASE + 'download_experiment_content';
        let body = {
            'args': [experiment.id]
        };

        return new Promise((resolve, reject) => {
            this.http.post(url, body, {
                observe: 'response',
                responseType: "blob"
            }).toPromise().then(
                a => {
                    ExperimentService.downloadFile(a);
                    resolve();
                }
            ).catch(
                reason => {
                    reject(reason);
                }
            )
        });
    }

    delete(experiment: Experiment) {
        let url = ExperimentService.URL_BASE + 'delete';
        let body = {
            'args': [experiment.id]
        };

        return this.http.post(url, body).toPromise();
    }

    /**
     * There you go. This function declares a huge number of params and it
     * does not check no one. thu
     * @param experimentData
     */
    createExperiment(experimentData: object) {
        // Transform formdata from front end format to backend format
        let formData = new FormData();

        let elementsObj: object = ExperimentService.classifyParams(experimentData);

        let basicElements: object = elementsObj['basicElements'];
        for (let entry of Object.entries(basicElements)) {
            formData.append(entry[0], entry[1]);
        }

        for (let entry of Object.entries(elementsObj['fileLikeElements'])) {
            if (entry[1] instanceof File) {
                formData.append(entry[0], entry[1]);

            } else {
                for (let fileIdx in entry[1]) {
                    formData.append(entry[0], entry[1][fileIdx]);
                }
            }
        }

        console.log(formData);
        let baseUrl = ExperimentService.URL_BASE + 'create_experiment';
        return new Promise((resolve, reject) => {
            this.http.post(baseUrl, formData).toPromise().then(
                httpResponse => {
                    return resolve(ExperimentService.transformExperimentResponse(httpResponse['response']))
                }
            ).catch(
                reason => reject(reason)
            )
        });
    }

    createWholeOrganismExperiment(name: string, description: string, organism: string, groups?: File, organismClassification?: File) {
        let data = {
            experiment_type: 2,
            name: name,
            description: description,
            organism: organism
        };

        if (groups) {
            data['groups'] = groups;
        }
        return this.createExperiment(data)
    }

    createOrganismComparisonExperiment(name: string, description: string, pathway: string) {
        return this.createExperiment(
            {
                experiment_type: 3,
                name: name,
                description: description,
                pathway: pathway
                // TODO Missing flags
            }
        )
    }

    createExperimentComparison(name: string, description: string, experiments: string) {
        return this.createExperiment(
            {
                experiment_type: 4,
                name: name,
                description: description,
                // This should be a list of experiment identifiers
                experiments: experiments
                // TODO Missing flags
            }
        )
    }

    createOrganismMetabolismComparison(name: string, description: string, organism: string) {
        return new Promise(resolve => resolve());
    }

    update(experiment: Experiment) {
        let url = ExperimentService.URL_BASE + 'update';
        let body = {
            'args': [experiment.id],
            'optionals': {
                'name': experiment.name,
                'description': experiment.description,
                'visible_for': experiment.public
            }
        };

        return this.http.post(url, body).toPromise();
    }

    private static transformExperimentResponse(Experiments: Array<object>): Array<Experiment> {
        let experimentList: Array<Experiment> = [];
        for (let experimentObject of Experiments) {
            experimentList.push(new Experiment(experimentObject));
        }
        return experimentList;
    }

    private static downloadFile(request) {
        const blob = this.getContentBlob(request, 'application/zip');
        const url = URL.createObjectURL(blob);

        // Create a new link to simulate the user download.
        // This is done this way in order to avoid a tab spawning for a second
        let element = document.createElement('a');
        let name = this.getDownloadName(request.headers);
        if (name) {
            element.download = name;
        }
        element.href = url;
        element.click();

        // Free the created blob
        setTimeout(() => URL.revokeObjectURL(url), 200);
    }

    private static getDownloadName(headers) {
        let match = this.FILENAME_REGEX.exec(
            headers.get('content-disposition')
        );

        if (match) {
            return match[1];
        } else {
            return null;
        }
    }

    private static getContentBlob(request, type) {
        let body = request.body;
        return new Blob(
            [body],
            {type: type}
        );
    }

    /**
     * Retrieve an object holding all the file-like elements in the passed
     * object
     */
    private static classifyParams(rootObject) {
        let fileLikeElements = {};
        let basicElements = {};
        let candidate;
        for (let entry of Object.entries(rootObject)) {
            candidate = entry[1];
            if (ExperimentService.isFileLikeElement(candidate)) {
                fileLikeElements[entry[0]] = entry[1];
            } else {
                basicElements[entry[0]] = entry[1];
            }
        }

        return {
            "fileLikeElements": fileLikeElements,
            "basicElements": basicElements
        };
    }

    private static isFileLikeElement(element) {
        let candidate = element;
        if (candidate instanceof Array) {
            // Get a sample to use it in the file test
            candidate = candidate[0];
        }

        return candidate instanceof File;
    }


    /**
     * Check if the passed object has all its keys whitelisted
     * @param filesContainer
     */
    private static validateFiles(filesContainer) {
        return filesContainer;
    }
}