import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageitemComponent } from './storageitem.component';

describe('StorageitemComponent', () => {
  let component: StorageitemComponent;
  let fixture: ComponentFixture<StorageitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorageitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
