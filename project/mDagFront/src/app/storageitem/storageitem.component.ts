import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ExperimentService} from "../storage/experiment.service";
import {FileItemHandlerService} from "../../file-item-handler.service";
import {Experiment} from "../models/experiment.model";

@Component({
    selector: 'app-storageitem',
    templateUrl: './storageitem.component.html',
    styleUrls: ['./storageitem.component.scss']
})
export class StorageItemComponent implements OnInit {

    @ViewChild("headedcontainerelement") view: ElementRef;
    experimentService: ExperimentService = null;

    @Input() experiment: Experiment;

    constructor(experimentService: ExperimentService, fileHandler: FileItemHandlerService) {
    }

    ngOnInit() {
    }

    onclick(event: Event) {
    }
}
