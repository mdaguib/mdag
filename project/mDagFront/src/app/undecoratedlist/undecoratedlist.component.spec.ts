import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UndecoratedlistComponent } from './undecoratedlist.component';

describe('UndecoratedlistComponent', () => {
  let component: UndecoratedlistComponent;
  let fixture: ComponentFixture<UndecoratedlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UndecoratedlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UndecoratedlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
