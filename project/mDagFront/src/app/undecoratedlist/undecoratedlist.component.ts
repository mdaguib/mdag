import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-undecoratedlist',
  templateUrl: './undecoratedlist.component.html',
  styleUrls: ['./undecoratedlist.component.scss']
})
export class UndecoratedlistComponent implements OnInit {

  @Input() listElements: any[] = null;

  constructor() { }

  ngOnInit() {
  }

}
