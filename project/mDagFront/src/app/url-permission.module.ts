export class PermissionModule {

    /***
     * These object creates a mapping between the url section and the
     * needed permissions in order to access it.
     *
     * Note that these are just access permissions. This does not take into account
     * if the user has rights to modify/delete elements.
     *
     * Not restricted urls does not need to appear in this map. They will be allowed
     * by default
     */
    static readonly UrlPermissionMap: object = {
        'user-management': {
            codename: 'user-management',
            children: {}
        },
        'experiments': {
            codename: null,
            children: {
                'create': {
                    codename: 'experiments_create',
                    children: {}
                }
            }
        },
        'groups': {
            codename: 'groups',
            children: {}
        },
    };

    static canNavigate(userPermissions: Array<string>, urlSegments: Array<string>): boolean {
        let isAllowed = true;

        let neededPermission;
        let permissionObject;
        let requiredCodename;
        // Check that the user is allowed to access to each segment
        for (let segment of urlSegments) {
            permissionObject = PermissionModule.UrlPermissionMap[segment];
            neededPermission = permissionObject && !!permissionObject.codename;
            if (neededPermission) {
                requiredCodename = permissionObject.codename;
                isAllowed = isAllowed && userPermissions.findIndex(
                    codename => requiredCodename === codename
                ) > -1;
            }

            if (permissionObject) {
                permissionObject = permissionObject.children;
            }
        }

        return isAllowed;
    }

    
}
