import {Component, Input, OnInit} from '@angular/core';
import {User} from "../user/user.model";
import {UserService} from "../user/user.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NotificationsService} from "../notifications/notifications.service";

@Component({
    selector: 'app-user-management',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {

    // Services
    private userService: UserService;
    private notificationsService: NotificationsService;

    // Variables
    private userList: Array<User>;
    @Input() form = new FormGroup({
        username: new FormControl(
            '',
            [Validators.maxLength(150)]
        ),
        email: new FormControl(
            '',
            [Validators.email]
        ),
        group: new FormControl(
            '',
            [Validators.maxLength(150)]
        ),
    });

    // User that's viewing managing the user
    private user: User;

    private selectedUser: User;
    private originalUser: string;
    private currentIndex: number;
    constructor(userService: UserService, notificationsService: NotificationsService) {
        this.userList = [];
        this.userService = userService;
        this.currentIndex = null;
        this.selectedUser = null;
        this.originalUser = null;
        this.notificationsService = notificationsService;

        this.userService.onLogin.subscribe(user => this.setManagingUser(user));
        this.userService.onLogout.subscribe(() => this.setManagingUser(null))
    }

    ngOnInit() {
        this.user = this.userService.getUser();
        this.loadUsers();
    }

    getSelectedUser() {
        return this.selectedUser;
    }

    clearSelection() {
        if (this.selectedUser && this.selectedUser.serialize() != this.originalUser) {
            this.showUserChangedWarning();
        } else {
            if (this.currentIndex != null) {
                this.selectedUser = null;
                this.unselectIndex();
            }
        }
    }

    selectUser(event: Event, user: User, index: number) {
        event.stopPropagation();

        if (this.currentIndex != null) {
            this.unselectIndex();
        }

        this.selectIndex(index);
        this.selectedUser = user;
        this.originalUser = this.selectedUser.serialize();
    }

    deleteUser(user: User) {
        this.userService.delete(user).then(
            (response) => {
                let elementIdx = this.userList.indexOf(user);
                if (elementIdx > -1) {
                    this.userList.splice(elementIdx, 1);
                }
            }
        ).catch(
            (error) => this.notificationsService.notify(
                error['error']['response'],
                NotificationsService.MESSAGE_TYPES.WARNING
            )
        );
    }

    clearFilters() {
        this.form.reset();
    }

    filterResults() {
        this.loadUsers(this.form.value);
    }
    
    /**
     * Turn a plain user into an admin
     */
    isSameUser() {
        let same = false;
        let selectedUser = this.getSelectedUser();
        if (this.user && selectedUser) {
            same = this.user.id === selectedUser.id;
        }
        return same
    }

    /**
     * Turn a plain user into an admin
     */
    promoteUser() {
        if (this.isSameUser()) {
            return;
        }

        this.userService.promote(this.getSelectedUser());
    }

    /**
     * Turn an admin user into a normal user
     */
    demoteUser() {
        if (this.isSameUser()) {
            return;
        }
        this.userService.demote(this.getSelectedUser());

    }

    private setManagingUser(user) {
        this.user = user;
    }


    private loadUsers(filters?: object) {
        this.userService.list(filters).then(
            (users) => {
                if (users.length == 0) {
                    this.notificationsService.notify("No users found");
                }
                this.userList = users;
            }
        );
    }

    private showUserChangedWarning() {
        // TODO Missing modal
    }

    private unselectIndex() {
        let elements = document.getElementsByClassName("details-item-wrapper");
        let element = elements[this.currentIndex];
        element.classList.remove('active');
        this.currentIndex = null;
    }

    private selectIndex(index: number) {
        this.currentIndex = index;
        let elements = document.getElementsByClassName("details-item-wrapper");
        let element = elements[this.currentIndex];
        element.classList.add('active');
    }
}
