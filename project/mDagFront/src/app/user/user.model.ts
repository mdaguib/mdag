import {Deserializable} from "../interfaces/deserializable.model";

export class User implements Deserializable {

    private _id: number;
    private _username: string;
    private name: string;
    private _lastName: string;
    private _email: string;
    private permissions: Array<string>;
    private _isAdmin: boolean;

    constructor(data: any) {
        let user_obj = data;
        if (typeof(data) === 'string') {
            user_obj = this.deserialize(data);
        }

        this.initialize(user_obj);
    }

    /**
     * For now it returns a JSON representation of the element.
     * @param input
     */
    deserialize(input: string): this {
        return JSON.parse(input);
    }

    /**
     * Return an string representation of the JSON element
     */
    serialize(): string {
        return JSON.stringify({
            id: this._id,
            username: this._username,
            name: this.name,
            lastName: this._lastName,
            email: this._email,
            permissions: this.permissions,
            is_superuser: this._isAdmin
        });
    }

     getPermissions(): Array<string> {
        return this.permissions;
    }

    /**
     * Check if the user has the requested permission to navigate to a
     * page.
     * @param ref
     */
    hasPermission(ref: string): boolean {
        return this.permissions.findIndex(perm => perm === ref) >= 0;
    }

    get isAdmin(): boolean {
        return this._isAdmin;
    }

    get username(): string {
        return this._username;
    }

    set username(name: string){
        this._username = name;
    }

    get lastname(): string {
        return this._lastName;
    }

    set lastname(lastname: string){
        this._lastName = lastname;
    }

    get email(): string {
        return this._email;
    }

    set email(email: string) {
        this._email = email
    }

    get id(): number {
        return this._id;
    }

    get fullName(): string {
        let name = this.name ? this.name : "Anonymous";
        let _lastName = this._lastName ? this._lastName : "Anonymous";
        return name + " " + _lastName;
    }

    /**
     * Fil the User instance with the configured data
     * @param user_obj
     */
    private initialize(user_obj: object) {
        this._username = user_obj['username'];
        this.name = user_obj['first_name'];
        this._lastName = user_obj['last_name'];
        this._email = user_obj['email'];
        this.permissions = user_obj['permissions'] || [];
        this._id = user_obj['id'];
        this._isAdmin = user_obj['is_superuser'];
    }
}
