import {EventEmitter, Injectable, Output} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "./user.model";
import {Experiment} from "../models/experiment.model";
import {CookieService} from "../global-services/cookie-service.service";
import {Group} from "../group/group.model";
// import {CookieService} from "ngx-cookie-service";


// We suppose angular adds the necessary cookies to keep authorization on going
const headers = new HttpHeaders({
    "Content-Type": "application/json",
});

const httpOptions = {
    headers: headers,
};

const BASE_URL = "webservice/user/";


/**
 * This service is in charge of querying the DB ( check if this should query
 * a repository instead of the DB directly) in order to retrieve any info
 * about all the files stored.
 */
@Injectable({
    providedIn: "root",
})
export class UserService {

    @Output() onLogout: EventEmitter<any> = new EventEmitter<any>();
    @Output() onLogin: EventEmitter<any> = new EventEmitter<any>();

    private http: HttpClient;
    private cookieService: CookieService;
    private _isSignedIn: boolean;
    private user: User;

    constructor(http: HttpClient, cookieService: CookieService) {
        this.http = http;
        this.cookieService = cookieService;
        this._isSignedIn = this.cookieService.get('mdag_session') == 1;

        if (this._isSignedIn) {
            // Update the info of the current logged user
            this.user = new User(sessionStorage.getItem("mdag_user"));
            this.getUserInfo();
        }
    }

    list(filters?: object): Promise<Array<User>> {
        return new Promise((resolve, reject) => {
            let url: string = BASE_URL + "get_all_users";
            let body = {
                'optionals': filters || {}
            };

            this.http.post(url, body, httpOptions).toPromise().then(
                (response) => {
                    let users: Array<User> = response['response'].map(
                        (userObj) => new User(userObj)
                    );
                    resolve(users);
                }
            ).catch(
                (reason) => {
                    reject(null);
                }
            );
        });
    }

    signIn(username: string, password: string) {
        let url: string = BASE_URL + "sign_in";
        let body = {"args": [username, password]};

        return new Promise((resolve, reject) => {
            this.http.post(url, body, httpOptions).toPromise().then(
                (response) => {
                    if (response["code"] == 200) {
                        console.log("Nuevo login", document.cookie);
                        let user = new User(response["response"]);
                        this.saveUserInfo(user);
                        this.onLogin.emit(user);
                        resolve(user);
                    } else {
                        // TODO Maybe this should be a 401 error
                        console.log("Failed to login");
                        reject("Failed to login");
                    }
                }
            ).catch((error) => {
                reject(error);
                console.log("Error capturado en servicio", error)
            });
        });
    }

    signOut() {
        let url: string = BASE_URL + "sign_out";
        let promise = this.http.post(url, httpOptions).toPromise();
        return promise.then(
            (response) => {
                this.onLogout.emit();
                sessionStorage.removeItem("mdag_user");
                this.cookieService.remove("mdag_session");
            }
        );
    }

    /**
     * TODO This could also work for admin users if the current session is
     *  held by an admin user
     * Register common users
     * @param username
     * @param password
     * @param email
     */
    signUp(username: string, password: string, email: string, name: string, lastName: string) {
        let url: string = BASE_URL + "sign_up";
        let body = {"args": [username, email, password, name, lastName]};
        let promise = this.http.post(url, body, httpOptions).toPromise();


        return promise.then(
            (response) => {
                if (response["code"] == 200) {
                    var user = new User(response["response"]);
                    this.user = user;
                    this.saveUserInfo(user);
                }
            }
        );
    }

    delete(user: User) {
        console.assert(user != null, "Called delete without user");
        let url: string = BASE_URL + 'delete_user';
        let body = {"args": [user.id]};
        return this.http.post(url, body, httpOptions).toPromise();
    }

    /**
     * Returns whether the user has signed in or not.
     * If this should be persistent the page should not be
     * reloaded or the back-end should have a service to check if
     * the user is still signed in
     */
    isSignedIn() {
        let isSignedIn = this.cookieService.check("mdag_session");

        if (!isSignedIn) {
            this.user = null;
            sessionStorage.removeItem("mdag_user");
            return false;
        }

        return isSignedIn;
    }

    getUser(): User {
        return this.user;
    }

    /**
     * Create a new group in the system whose owner will be the current
     * logged user
     */
    createGroup(name: string, description: string, members: Array<User>) {
        let member_ids: Array<number> = [];
        for (let member of members) {
            member_ids.push(member.id)
        }

        let body = {
            "args": [name],
            "optionals": {
                "description": description,
                "members": member_ids
            }

        };
        let url: string = BASE_URL + "create_group";
        let promise = this.http.post(url, body, httpOptions).toPromise();


        return promise.then(
            (response) => {
                if (response["code"] == 200) {
                    console.log(response['response']);
                }
            }
        );
    }

    /**
     * Upadtes an existent group. Take into account that there are 2 things
     * that are not currently allowed.
     * Removing all the name, and removing the owner from the group.
     *
     * @param id
     * @param name
     * @param description
     * @param members
     */
    updateGroup(id: number, name: string, description: string, members: Array<User>) {
        let member_ids: Array<number> = [];
        for (let member of members) {
            member_ids.push(member.id)
        }

        let body = {
            "args": [id],
            "optionals": {
                "name": name,
                "description": description,
                "members": member_ids
            }

        };

        let url: string = BASE_URL + "update_group";
        let promise = this.http.post(url, body, httpOptions).toPromise();


        return promise.then(
            (response) => {
                if (response["code"] == 200) {
                    console.log(response['response']);
                }
            }
        );
    }

    listGroups(groupName?: string, owner?: boolean): Promise<Array<Group>> {
        let filters = {};
        if (groupName != null) {
            filters['group_name'] = groupName;
        }


        if (owner != null) {
            filters['owner'] = owner;
        }

        return new Promise((resolve, reject) => {
            let url: string = BASE_URL + "get_groups";
            let body = {
                'optionals': filters || {}
            };

            this.http.post(url, body, httpOptions).toPromise().then(
                (response) => {
                    let groups: Array<Group> = response['response'].map(
                        (groupObj) => new Group(groupObj)
                    );
                    resolve(groups);
                }
            ).catch(
                (reason) => {
                    reject(null);
                }
            );
        });
    }

    demote(user: User) {
        let body = {"args": [user.id]};
        let url: string = BASE_URL + "demote";
        let promise = this.http.post(url, body, httpOptions).toPromise();


        return promise.then(
            (response) => {
                if (response["code"] == 200) {
                    console.log(response['response']);
                }
            }
        );
    }

    promote(user: User) {
        let body = {"args": [user.id]};
        let url: string = BASE_URL + "promote";
        let promise = this.http.post(url, body, httpOptions).toPromise();


        return promise.then(
            (response) => {
                if (response["code"] == 200) {
                    console.log(response['response']);
                }
            }
        );
    }

    private getUserInfo() {
        let url = BASE_URL + "get_user_info";
        let promise = this.http.post(url, {}, httpOptions).toPromise();

        return promise.then(
            (response) => {
                if (response["code"] == 200) {
                    var user = new User(response["response"]);
                    this.user = user;
                    this.saveUserInfo(user);
                }
            }
        );
    }

    private saveUserInfo(user: User) {
        this.setSessionCookie();
        this.user = user;
        sessionStorage.setItem("mdag_user", user.serialize());
    }

    private setSessionCookie() {
        this.cookieService.set(
            "mdag_session",
            "1",
            1800000
        );
    }
}