import { TestBed } from '@angular/core/testing';

import { FileItemHandlerService } from './file-item-handler.service';

describe('FileItemHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileItemHandlerService = TestBed.get(FileItemHandlerService);
    expect(service).toBeTruthy();
  });
});
