import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileItemHandlerService {
  private fileSelected: object = null;

  constructor() { }

  public setSelectedFile(file) {
    if (this.fileSelected) {
      this.fileSelected['selected'] = false;
    }

    this.fileSelected = file;

    if (this.fileSelected) {
      this.fileSelected['selected'] = true;
    }
  }

  public getSelectedFile() {
    return this.fileSelected;
  }
}
